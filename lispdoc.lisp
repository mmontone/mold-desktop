(defpackage lispdoc
  (:use :cl)
  (:export
   :write-html
   :create-html))

(in-package :lispdoc)

;; A list of commands to run after the resulting stream has been rendered as a clog element.
(defvar *lispdoc-commands*)

(defgeneric process-lispdoc-command (format command-name args stream)
  (:documentation "Process lispdoc command"))

(defmethod process-lispdoc-command (format command-name args stream)
  (error "Invalid lispdoc command: ~s for format ~s" command-name format))

(defmethod process-lispdoc-command (format (command-name (eql :eval)) args stream)
  "Generic evaluation command. Evaluate input and embed the evaluation result into the document."
  (princ (eval (car args)) stream))

(defmethod process-lispdoc-command ((format (eql :html)) (command-name (eql :h1)) args stream)
  (who:with-html-output (html stream)
    (:h1 (who:str (first args)))))

(defmethod process-lispdoc-command ((format (eql :html)) (command-name (eql :button)) args stream)
  (destructuring-bind (label action &optional (button-style :link)) args
    ;; We create a fake clog link and create a callback javascript code for it,
    ;; that we then use in the link we render to the stream.
    (let* ((a (clog:create-a mold-desktop::*body* :auto-place nil))
           (callback (mold-desktop::make-clog-callback a "click"
                                                       (lambda (&rest _)
                                                         (declare (ignore _))
                                                         (eval action)))))
      (ecase button-style
        (:link
         (who:with-html-output (html stream)
           (:a :style "cursor:pointer;text-decoration:underline;"
               :onclick callback
               (who:str label))))
        (:button
         (who:with-html-output (html stream)
           (:button :class "w3-button"
                    :onclick callback
                    (who:str label))))))))

(defmethod process-lispdoc-command ((format (eql :html)) (command-name (eql :command)) args stream)
  ;; We create a fake clog link and create a callback javascript code for it,
  ;; that we then use in the link we render to the stream.
  (let* ((a (clog:create-a mold-desktop::*body*))
         (callback (mold-desktop::make-clog-callback a "click"
                                                     (lambda (&rest _)
                                                       (declare (ignore _))
                                                       (mold-desktop::run-command-interactively (first args)))))
         (command (mold-desktop::find-command (first args))))
    (who:with-html-output (html stream)
      (:a :style "cursor:pointer;text-decoration:underline;"
          :onclick callback
          (who:str (mold-desktop::command-name command)))
      (who:str " "))))

(defmethod process-lispdoc-command ((format (eql :html)) (command-name (eql :embed)) args stream)
  "Embed an object's view in html."
  (let ((html-id (princ-to-string (gensym))))
    (who:with-html-output (html stream)
      (:div :id html-id))
    (push (lambda (clog-obj)
            (let ((place-holder (clog:attach-as-child clog-obj html-id))
                  (object (eval (first args))))
              (mold-desktop::create-object-view object place-holder)))
          *lispdoc-commands*)))

(defmethod process-lispdoc-command ((format (eql :html)) (command-name (eql :show)) args stream)
  "Render a link that evaluates the argument ands shows it in the desktop."
  (destructuring-bind (expr &optional label) args
    (let* ((a (clog:create-a mold-desktop::*body*))
           (callback (mold-desktop::make-clog-callback a "click"
                                                       (lambda (&rest _)
                                                         (declare (ignore _))
                                                         (mold-desktop::show (eval expr))))))
      (who:with-html-output (html stream)
        (:a :style "cursor:pointer;text-decoration:underline;"
            :onclick callback
            (who:str (or label (princ-to-string expr))))))))

(defun peek-n-chars (number-of-chars stream)
  (loop with chars := '()
        repeat number-of-chars
        for char := (read-char stream nil nil)
        while char
        do (push char chars)
        finally (let ((read-chars (nreverse (copy-list chars))))
                  (dotimes (j (length chars))
                    (unread-char (pop chars) stream))
                  (return read-chars))))

(defun process-lispdoc-commands (format line stream &optional (command-prefix #\:))
  (let ((command-prefix `(#\( ,command-prefix)))
    (with-input-from-string (s line)
      (loop
        while (peek-char nil s nil nil)
        do
           (let ((chars (peek-n-chars 2 s)))
             (if (equalp chars command-prefix)
                 ;; a command invocation was found, use CL:READ function to read it
                 (let ((command (read s)))
                   (destructuring-bind (command-name &rest args) command
                     (process-lispdoc-command format command-name
                                              args stream)))
                 ;; else, continue
                 (write-char (read-char s) stream)))))))

(defun write-html-without-paragraphs (source destination)
  (with-input-from-string (stream source)
    (loop for line := (read-line stream nil nil)
          while line
          do
             (process-lispdoc-commands :html line destination)
             (terpri destination))))

(defun write-html-with-paragraphs (source destination)
  (with-input-from-string (stream source)
    (let ((par 0)
          (paragraph-started nil))
      (loop for line := (read-line stream nil nil)
            while line
            do
               (cond
                 ((zerop (length line))
                  (incf par))
                 ((and (not (zerop (length line)))
                       (> par 0))
                  (setf par 0)
                  (when paragraph-started
                    (write-string "</p>" destination)
                    (terpri destination))
                  (write-string "<p>" destination)
                  (terpri destination)
                  (setf paragraph-started t)
                  (process-lispdoc-commands :html line destination)
                  (terpri destination))
                 (t
                  (process-lispdoc-commands :html line destination)
                  (terpri destination))))
      (when paragraph-started
        (write-string "</p>" destination)))))

(defun write-html (source destination &key make-paragraphs)
  (if make-paragraphs
      (write-html-with-paragraphs source destination)
      (write-html-without-paragraphs source destination)))

(defun create-html (doc-node clog-obj &key make-paragraphs)
  (let ((*lispdoc-commands* nil))
    (let ((html
            (clog:create-child clog-obj
                               (with-output-to-string (s)
                                 (write-string "<div>" s)
                                 (write-html (mold-desktop::contents doc-node) s :make-paragraphs make-paragraphs)
                                 (write-string "</div>" s)))))
      (dolist (command *lispdoc-commands*)
        (funcall command html))
      html)))

#+test
(with-output-to-string (s)
  (write-html "lalal" s))

#+test
(with-output-to-string (s)
  (write-html "(:h1 \"header1\")
lalal
lolo" s))

#+test
(with-output-to-string (s)
  (write-html "(:h1 \"header1\")

lalal
sdfsf

dfdf" s))
