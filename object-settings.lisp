(in-package :mold-desktop)

(defgeneric object-aspects (object)
  (:documentation "Returns the aspects of an object.")
  (:method-combination append))

(defmethod object-aspects append ((object t))
  '(:info))

(defmethod object-aspects append ((object object))
  '(:meta :links :status :events :settings :properties))

(defmethod display-object-settings ((setting (eql :info)) tab-content object)
  (let ((info (make-collapsible tab-content "Info")))
    (render-object-info object info))
  (let* ((help (make-collapsible tab-content "Help"))
         (doc-node (object-help object)))
    (create-icon-button help "fa fa-external-link"
                        (lambda (&rest args)
                          (declare (ignore args))
                          (show doc-node))
                        :style "float:right;")
    (create-object-view doc-node help :show-title nil)))

(defmethod display-object-settings ((setting (eql :properties)) tab-content object)
  (create-object-editor-view object tab-content))

(defmethod display-object-settings ((setting (eql :settings)) tab-content object)
  )

(defmethod display-object-settings ((setting (eql :meta)) tab-content object)
  (let ((select-id (symbol-name (gensym))))
    (create-child tab-content
                  (html (:table :class "w3-table w3-small w3-striped w3-bordered"
                                (:tr
                                 (:td (:label (who:str "Tags")))
                                 (:td
                                  (:select :id select-id
                                    :class "ui search selection dropdown"
                                    :multiple t
                                    (loop for tag in (tags object)
                                          do
                                             (who:htm (:option :value tag
                                                               :selected t
                                                               (who:str tag))))))))))
    (let ((select (attach-as-child tab-content select-id)))
      (clog:jquery-execute select "dropdown({allowAdditions: true})"))))

(defmethod display-object-settings ((setting (eql :links)) tab-content object)
  (render-object-links object tab-content))

(defview create-object-settings-view (object clog-obj &optional tab)
  (:documentation "Settings view for OBJECT.")
  (:label "Settings view")
  (:icon "fa fa-cog"))

(defmethod create-object-settings-view (object clog-obj &optional (tab :info))
  (let* ((settings (create-div clog-obj :class "object-settings"))
         (tabbar (create-div settings :class "w3-bar w3-light-gray"))
         (tab-content (create-div settings :class "w3-container")))
    (dolist (setting (reverse (object-aspects object)))
      (->
          (create-button tabbar
                         :class "w3-bar-item w3-button w3-light-gray"
                         :content (string-capitalize (string-downcase (symbol-name setting))))
          (set-on-click (lambda (button)
                          (declare (ignore button))
                          (setf (text tab-content) "")
                          (display-object-settings setting tab-content object)))))
    (display-object-settings tab tab-content object)
    settings))

(defmethod display-object-settings ((setting (eql :status)) tab-content object)
  (let ((ul (create-unordered-list tab-content)))
    (dolist (status-change (status-changes object))
      (let ((li (create-list-item ul)))
        (create-status-change-view li status-change)))))

(defmethod display-object-settings ((setting (eql :events)) tab-content object)
  (let ((ul (create-unordered-list tab-content :class "w3-ul")))
    (dolist (event (object-events object))
      (let ((li (mold-desktop/clog-gui::create-halos-wrapper
                 ul event
                 :create-function #'clog:create-list-item
                 :handle-drop t)))
        (create-object-view event li)
        (setf (clog:advisory-title li)
              (event-documentation event))))))

(defmethod create-object-view ((event event-object) clog-obj &rest args)
  (declare (ignore args))
  (create-icon "fa-regular fa-bell" clog-obj)
  (create-span clog-obj
               :content (str:sentence-case (princ-to-string (event event)))
               :style "margin-left: 10px;"))

;; Object info

(defgeneric render-object-info (object clog-obj))

(defmethod render-object-info ((object t) clog-obj)
  (create-child
   clog-obj
   (html
     (:div
      (:ul :class "w3-ul no-padding-left"
           (:li (:b "Type: ")(:span (who:str (type-of object)))))))))

(defmethod render-object-info ((object object) clog-obj)
  (let ((class-docs (documentation (class-of object) t)))
    (create-child
     clog-obj
     (html
       (:div
        (:ul :class "w3-ul no-padding-left"
             (:li (:b "Type: ")(:span :title class-docs (who:str (type-of object))))
             (:li (:b "ID: ") (:span (who:str (object-id object))))
             (:li (:b "Name: ") (:span (who:str (object-name object))))
             (:li (:b "Url: ") (:a :target "_blank" :class "w3-text-blue" :href (object-url object) (who:str (object-url object))))
             (:li (:b "Created: ") (:span (who:str (format-universal-time (created-at object)))))
             (when (updated-at object)
               (who:htm
                (:li (:b "Updated: ") (:span (who:str (format-universal-time (updated-at object)))))))
             (when (accessed-at object)
               (who:htm
                (:li (:b "Accessed: ") (:span (who:str (format-universal-time (accessed-at object)))))))))))))

;; Object links

(defun render-object-links (object clog-obj)
  (let* ((div (create-div clog-obj))
         (ul (create-unordered-list div :class "w3-ul no-padding-left")))
    (dolist (triple (naive-triple-store:find-triples (object-id object) nil nil *triple-store*))
      (let ((li (create-list-item ul)))
        (create-span li :class "w3-tag w3-blue" :content (princ-to-string (second triple)))
        (create-span li :content (who:escape-string (princ-to-string (third triple)))
                        :style "margin-left: 5px;")
        (set-on-click li (lambda* (_)
                           (show (third triple))))))))

(defun open-object-settings (object clog-obj &optional (tab :info))
  (let* ((wnd (mold-desktop/clog-gui:create-gui-window clog-obj :title "Settings")))
    (create-object-settings-view object (mold-desktop/clog-gui::window-content wnd) tab)))

(mold-desktop:defcommand
    (customize-object :icon "fa fa-cog")
    ((object object))
  ;;(open-object-settings object *body*)
  (mold-desktop::side-panel-open-object-settings
   mold-desktop::*side-panel*
   object))
