;; Copyright (C) 2022 Mariano Montone - All Rights Reserved
;; You may use, distribute and modify this code under the terms of the
;; GNU Affero General Public License

(in-package :mold-desktop)

(defclass calendar-event (object)
  ((datetime :initarg :datetime
             :accessor datetime)
   (date :initarg :date
         :accessor event-date)
   (time :initarg :time
         :accessor event-time)
   (duration :initarg :duration
             :accessor event-duration)
   (people :initarg :people
           :accessor event-people))
  (:metaclass mold-object-class))

(defmethod create-object-view ((calendar-event calendar-event) clog-obj &rest args)
  (create-child clog-obj
                (html (:ul
                       (:li (:b (who:str "Title: ")) (who:str (object-name calendar-event)))
                       (:li (:b (who:str "Date: ")) (who:str (event-date calendar-event)))
                       (:li (:b (who:str "Time: ")) (who:str (event-time calendar-event)))
                       (:li (:b (who:str "Duration: ")) (who:str (event-duration calendar-event)))
                       (:li (:b (who:str "People: ")) (who:str (event-people calendar-event)))
                       (:li (:b (who:str "Url: ")) (who:str (object-url calendar-event)))))))

(defclass temporal-object (object)
  ()
  (:metaclass mold-object-class))

(defclass year (temporal-object)
  ((year :initarg :year
         :reader year
         :initform (lt:timestamp-year (lt:today))))
  (:metaclass mold-object-class))

(defmethod initialize-instance :after ((year year) &rest initargs)
  (declare (ignore initargs))
  (setf (object-name year) (format nil "Year ~a" (year year))))

(defclass month (temporal-object)
  ((month :initarg :month
          :reader month
          :initform (lt:timestamp-month (lt:today)))
   (year :initarg :year
         :reader year
         :initform (lt:timestamp-year (lt:today))))
  (:metaclass mold-object-class))

(defmethod initialize-instance :after ((month month) &rest initargs)
  (declare (ignore initargs))
  (setf (object-name month) (format nil "~a ~a" (aref lt:+month-names+ (month month)) (year month))))

(defclass date (temporal-object)
  ((day :initarg :day
        :accessor day
        :initform (lt:timestamp-day (lt:today)))
   (month :initarg :month
          :accessor month
          :initform (lt:timestamp-month (lt:today)))
   (year :initarg :year
         :accessor year
         :initform (lt:timestamp-year (lt:today))))
  (:metaclass mold-object-class))

(defmethod initialize-instance :after ((date date) &rest initargs)
  (declare (ignore initargs))
  (let ((timestamp
          (lt:encode-timestamp
           0 0 0 0
           (day date) (month date) (year date))))
    (setf (object-name date)
          (lt:format-timestring nil timestamp :format '(:long-weekday #\space :day #\space :long-month #\space :year)))))

(defmethod tree-node-children ((year year))
  (loop for month from 1 to 12
        collect
        (make-instance 'month :month month :year (year year))))

(defmethod tree-node-children ((month month))
  (loop for day from 1 to (lt:days-in-month (month month) (year month))
        collect (make-instance 'date
                               :day day
                               :month (month month)
                               :year (year month))))

(defview create-calendar-view (object clog-obj &rest args)
  (:documentation "Calendar view")
  (:label "Calendar view")
  (:icon "fa fa-calendar"))

(defun create-month-view (clog-obj month year &optional day)
  (let* ((table (create-table clog-obj :class "calendar"))
         (thead (create-table-head table))
         (thead-tr (create-table-row thead))
         (tbody (create-table-body table)))
    ;; Header with days of the week
    (loop for dayn from 0 to 6 do
      (create-table-heading thead-tr :content (aref lt:+short-day-names+ dayn)))
    ;; Body with week rows
    (let ((tr (create-table-row tbody))
          (blank-days (lt:timestamp-day-of-week
                       (lt:encode-timestamp 0 0 0 0
                                            1 month year)))
          (daynr 1)
          (days-in-month (lt:days-in-month month year)))
      ;; Blank days
      (dotimes (i blank-days)
        (create-table-column tr))
      ;; The days of the month
      (dotimes (i days-in-month)
        (setq daynr (1+ i))
        (let ((daycol (create-table-column tr :content (princ-to-string daynr)))
	      (dayobj (make-instance 'date :day daynr :month month :year year)))
	  (mold-desktop/clog-gui::attach-presentation-handlers dayobj daycol)
          (when (and day (= day daynr))
            (setf (css-class-name daycol) "selected w3-indigo")))

        (when (and (zerop (mod (+ daynr blank-days) 7))
                   (/= daynr days-in-month))
          (setf tr (create-table-row tbody))))
      ;; The rest of the empty table cells
      (dotimes (i (- (* 5 7) (+ blank-days days-in-month)))
        (create-table-column tr)))
    table))

(defmethod create-calendar-view ((month month) clog-obj &rest args)
  (declare (ignore args))
  (let ((table (create-month-view clog-obj (month month) (year month))))
    (set-styles table '(("width" "100%")("height" "100%")))
    table))

(defmethod create-calendar-view ((date date) clog-obj &rest args)
  (declare (ignore args))
  (let ((table (create-month-view clog-obj (month date) (year date)
                                  (day date))))
    (set-styles table '(("width" "100%")("height" "100%")))
    table))

(defmethod create-calendar-view ((year year) clog-obj &rest args)
  (declare (ignore args))
  (let ((year-calendar (create-div clog-obj :class "year-calendar")))
    (loop for month from 1 to 12 do
      (let ((month-calendar (create-div year-calendar))
	    (monthobj (make-instance 'month :month month :year (year year))))
	(mold-desktop/clog-gui::attach-presentation-handlers monthobj month-calendar)
	(create-section month-calendar :h4 :content (aref lt:+month-names+ month))
	(create-month-view month-calendar month (year year))))))

(defmethod object-default-view ((object temporal-object))
  'create-calendar-view)

(defmethod object-view-types append ((object temporal-object) (module (eql :calendar)))
  '(create-calendar-view))


;; add a documentation node to the manual
(defun create-calendar-doc-node ()
  (read-doc-node-from-file (asdf:system-relative-pathname :mold-desktop "docs/modules/calendar.md")
			   :name "Calendar"))

(pushnew 'create-calendar-doc-node *mold-desktop-book-module-nodes*)

(pushnew :calendar *mold-modules*)
