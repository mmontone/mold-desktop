(in-package :mold-desktop)

;; views for pathnames

(defvar *assets* (make-hash-table :test 'equalp)
  "A table from asset id to asset path")

(defvar *assets-url-path* "/assets/")

(defun asset-lack-middleware (app)
  "Handle assets."
  (lambda (env)
    (let ((path-info (getf env :path-info)))
      (if (str:starts-with-p *assets-url-path* path-info)
          (let ((asset-id (alexandria:lastcar (str:split #\/ path-info))))
            (alexandria:if-let ((asset (gethash asset-id *assets*)))
              (lack/component:call (make-instance 'lack/app/file:lack-app-file :file asset) env)
              `(404 (:content-type "text/plain") ("Asset not found"))))
          (funcall app env)))))

(pushnew 'asset-lack-middleware *lack-middlewares*)

(defun create-asset (path)
  "Create an asset for PATH. Returns the asset id.
The asset can be accessed at /assets/<asset-id> url."
  (check-type path pathname)
  (or (probe-file path) (error "File does not exist: ~s" path))
  (let ((asset-id
          (sha1:sha1-base64 (prin1-to-string path)
                            (lambda (str)
                              (cl-base64:string-to-base64-string str :uri t)))))
    (setf (gethash asset-id *assets*) path)
    asset-id))

(defun asset-url (asset-id)
  (concatenate 'string *assets-url-path* asset-id))

(mime-functions:define-mime-function create-object-from-pathname (pathname))

(mime-functions:define-mime-method create-object-from-pathname "image/*" (pathname)
  (let ((asset (create-asset pathname)))
    (make-instance 'image
                   :url (asset-url asset)
                   :name (princ-to-string pathname))))

(mime-functions:define-mime-method create-object-from-pathname "inode/directory" (pathname)
  (make-instance 'filesystem-directory
                 :directory pathname
                 :name (princ-to-string pathname)))

;; The method that matches when no other mime method matches
(mime-functions:define-mime-method create-object-from-pathname "*" (pathname)
  (declare (ignore pathname))
  nil)

;; (create-object-from-pathname "image/png" #p"~/Descargas/Captura de pantalla -2024-11-27 12-46-06.png")

;; (create-object-from-pathname "application/pdf" #p"~/Descargas/1_A_214.pdf")

(mime-functions:define-mime-function create-file-view (file-object clog-obj &rest args))

(mime-functions:define-mime-method create-file-view "image/*" (file-object clog-obj &rest args)
  (declare (ignore args))
  (let ((asset (create-asset (file-pathname file-object))))
    (create-img clog-obj :url-src (asset-url asset))))

(mime-functions:define-mime-method create-file-view "text/*" (file-object clog-obj &rest args)
  (declare (ignore args))
  (with-slots (pathname) file-object
    (create-child clog-obj
                  (html (:pre (who:str (alexandria:read-file-into-string pathname)))))))

(mime-functions:define-mime-method create-object-from-pathname "video/*" (pathname)
  (let ((asset (create-asset pathname)))
    (make-instance 'video :url (asset-url asset)
                          :name (princ-to-string pathname))))

(mime-functions:define-mime-method create-file-view "application/pdf" (file-object clog-obj &rest args)
  (declare (ignore args))
  (let ((asset (create-asset (file-pathname file-object))))
    (clog:create-element clog-obj "object" :class "pdf"
                                           :data (asset-url asset)
                                           :width "100%"
                                           :height "100%")))

(mime-functions:define-mime-method create-file-view "*" (file-object clog-obj &rest args)
  (declare (ignore args))
  (create-p clog-obj :content (princ-to-string (file-pathname file-object))))

(defmethod show-object ((pathname pathname) context view workspace)
  (let ((object (create-object-from-pathname
                 (trivial-mimes:mime pathname)
                 pathname)))
    (if object
        (show-object object context view workspace)
        (show-object (make-instance 'filesystem-file :pathname pathname
                                                     :mime-type (trivial-mimes:mime pathname))
                     context view workspace))))
