(defpackage :heroku
  (:use :cl))

(in-package :heroku)

(defun start-self-ping (url &optional (minutes 20))
  (bt:make-thread
   (lambda ()
     (loop do
       (sleep (* minutes 60))
       (drakma:http-request url)))
   :name "heroku-self-ping"))
