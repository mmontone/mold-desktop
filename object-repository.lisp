(in-package :mold-desktop)

;; TODO: make OBJECT-REPOSITORY singleton class
;; https://hyperthings.garden/posts/2021-05-19/singleton-classes-with-mop.html

(defclass object-repository ()
  ((name :initarg :name
	 :initform nil
	 :type (or string null)
	 :accessor repository-name)
   (name-accessor :initarg :name-accessor
		  :initform #'object-title
		  :type function
		  :accessor name-accessor
		  :documentation "Accessor function for accessing the names of the repository objects.")
   (object-key :initarg :object-key
	       :initform #'object-name
	       :accessor object-key)
   (object-test :initarg :object-test
		:initform #'equalp
		:accessor object-test))
  (:documentation "Repository of objects"))

(defgeneric repository-search (repository query))
(defgeneric repository-find (repository key))
(defgeneric repository-list-all (repository))

(defmethod repository-search ((repository object-repository) (query string))
  (remove-if-not (lambda (obj)
		   (str:contains? query
				  (string (funcall (name-accessor repository) obj))
				  :ignore-case t))
		 (repository-list-all repository)))

(defmethod repository-find ((repository object-repository) key)
  (find key (repository-list-all repository)
	:key (object-key repository)
	:test (object-test repository)))

(defun list-all-repositories ()
  (all-subclasses (find-class 'object-repository)))

(defun find-repository (name &key (error-p t))
  (or (alex:when-let ((repo-class (find name (list-all-repositories) :key #'class-name)))
	(make-instance repo-class))
      (and error-p (error "Repository not found: ~s" name))))

(defcommand (search-object 
	     :icon "fa-solid fa-magnifying-glass"
             :show-result t)
    ((repository-name symbol "The respository to search."
		      :prompt '(member-of-list (mapcar #'class-name (list-all-repositories))))
     (query string "The query."))
  "Search the a respository using QUERY."
  (make-instance 'collection
		 :name (format nil "Search ~a: ~a" repository-name query)
		 :members
		 (repository-search (find-repository repository-name) query)))

(defcommand (find-object
	     :icon "fa-solid fa-magnifying-glass"
             :show-result t)
    ((repository-name symbol "The respository to search."
		      :prompt '(member-of-list (mapcar #'class-name (list-all-repositories))))
     (key t "The key"))
  (repository-find (find-repository repository-name) key))

;; Some default repositories
(defclass commands-repository (object-repository)
  ()
  (:default-initargs
   :name-accessor #'command-name
   :object-key #'command-name)
  (:documentation "Repository with system commands."))

(defmethod repository-list-all ((repo commands-repository))
  *commands*)

#+test
(search-object 'commands-repository "search")
#+test
(find-object 'commands-repository 'find-object)

(defclass collections-repository (object-repository)
  ())

(defmethod repository-list-all ((repo collections-repository))
  (members *collections*))
