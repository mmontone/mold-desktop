(in-package :mold-desktop)

(defclass plot-view (clog-element)
  ((options :initform nil
	    :accessor plot-options)))

(defclass plot (object)
  ((data :accessor plot-data
	 :initarg :data
	 :initform nil)
   (options :accessor plot-options
	    :initarg :options
	    :initform nil))
  (:metaclass mold-object-class))

(defun init-plots (body)
  (add-script (html-document body) "https://cdn.plot.ly/plotly-2.12.1.min.js"))

(pushnew 'init-plots *init-functions*)

(defun write-plot-data (plot-data stream)
  (json:with-array (stream)
    (dolist (trace plot-data)
      (json:as-array-member (stream)
	(json:encode-json-alist trace stream)))))

(defun write-plot-options (plot stream)
  (json:encode-json-alist (plot-options plot) stream))

(defmethod create-object-view ((plot plot) clog-obj &rest args)
  (declare (ignore args))
  ;; TODO: fetch plot options from ARGS?
  (let ((plot-area (create-div clog-obj)))
    (js-execute plot-area
		(with-output-to-string (s)
		  (format s "Plotly.newPlot(document.getElementById('~a')," (html-id plot-area))
		  (write-plot-data (plot-data plot) s)
		  (write-string "," s)
		  (write-plot-options plot s)
		  (write-string ");" s)))))

#+example
(show (make-instance 'plot
		     :data '(
			     ((x . #(0 1 2 3 4))
			      (y . #(1 5 3 7 5))
			      (mode . "lines+markers")
			      (type . "scatter")))))
