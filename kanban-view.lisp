;; kanban view based on the status of objects

;; TODO:
;; - Drag and drop between columns to change status

(in-package :mold-desktop)

(defgeneric object-status (object)
  (:documentation "Return status of OBJECT.")
  (:method (object)
    nil)
  (:method ((object object))
    (status object)))

(defview create-kanban-view (object clog-obj &rest args)
  (:documentation "Kanban view.")
  (:icon "fa-table")
  (:label "Kanban view"))

(defgeneric create-kanban-item-view (object clog-obj &rest args)
  (:documentation "Create view for OBJECT for a kanban view."))

(defmethod create-kanban-item-view (object clog-obj &rest args)
  (apply #'create-thumbnail-view object clog-obj args))

(defun statuses-in-collection (coll)
  (remove-duplicates
   (loop for member in (members coll)
         collect (object-status member))))

(defmethod create-kanban-view ((coll collection) clog-obj &rest args &key statuses exclude)
  "Create a kanban view for COLLection, using the status of its members.

Arguments:.
STATUSES: The list of statuses to use. If NIL, all of the statuses found in the collection members are used.
EXCLUDE: list of statues to exclude."
  (let ((statuses (statuses-in-collection coll)))
    (let ((row (create-div clog-obj :class "w3-row"))
	  (col-class (format nil "w3-col m~a" (truncate (/ 12 (length statuses))))))
      (dolist (status statuses)
        (let ((col (create-div row :class col-class)))
          (clog:create-section col :h3 :content (princ-to-string status))
	  (dolist (member (members coll))
            (when (equalp (object-status member) status)
              (create-kanban-item-view member col))))))))

(defmethod object-view-types append
    ((coll collection) (module (eql :desktop)))
  (when (statuses-in-collection coll)
    '(create-kanban-view)))

(defun test-kanban-view ()
  "Create a collection of tasks and show them in a kanban view."
  (flet ((create-task (task-spec)
	   (apply #'make-instance 'task task-spec)))
    (let* ((tasks-spec '((:name "Do something" :status :todo)
			 (:name "Something done" :status :done)
			 (:name "Do that" :status :canceled)))
	   (tasks (make-instance 'collection
				 :members (mapcar #'create-task tasks-spec))))
      (show tasks :view 'create-kanban-view))))
