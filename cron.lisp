;; CL-CRON integration

;; TODO:
;; Implement proper CL-CRON integration
;; This is a first sketch.

(in-package :mold-desktop)

;; Adapt cl-cron model

(defclass cron-job (cl-cron::cron-job)
  ((name :initarg :name
	 :accessor cron-job-name
	 :type symbol)
   (description :initarg :description
		:accessor cron-job-description
		:type string
		:initform "")
   (status :initarg :status
	   :accessor cron-job-status
	   :type keyword
	   :initform :enabled)
   (last-run-time :accessor last-run-time
		  :initform nil)))

(defun make-cron-job (name func
		      &key (minute :every) (step-min 1) (hour :every)
			(step-hour 1) (day-of-month :every) 
			(step-dom 1) (month :every) (step-month 1)
			(day-of-week :every) (step-dow 1) (boot-only nil)
			(description ""))
  "Creates a new instance of a cron-job object and appends it to the cron-jobs-list after processing its time. Note that if you wish to use multiple values for each parameter you need to provide a list of numbers or use the gen-list function. You can not have a list of symbols when it comes to month or day-of-week. Please note that as by ANSI Common Lisp for the month variable the possible values are between 1 and 12 inclusive with January=1 and for day of week the possible values are between 0 and 6 with Monday=0. Returns the hash-key"
  (setf (gethash name cl-cron::*cron-jobs-hash*)
	(make-instance 'cron-job
		       :name name
		       :description description
		       :job-minute (cl-cron::get-minutes minute step-min)
		       :job-hour (cl-cron::get-hours hour step-hour)
		       :job-dom (cl-cron::get-days-of-month day-of-month step-dom)
		       :job-month (cl-cron::get-months month step-month)
		       :job-dow (cl-cron::get-days-of-week day-of-week step-dow)
		       :job-@boot boot-only
		       :job-func (lambda ()
				   (let ((cron-job (gethash name cl-cron::*cron-jobs-hash*)))
				     (setf (last-run-time cron-job) (get-universal-time))
				     (funcall func))))))

(defun cron-job-schedule-string (cron-job)
  (with-output-to-string (s)
    (format s "Month: ~a. " (cl-cron::job-month cron-job))
    (format s "Day of month: ~a. " (cl-cron::job-dom cron-job))
    (format s "Day of week: ~a. " (cl-cron::job-dow cron-job))
    (format s "Hour: ~a. " (cl-cron::job-hour cron-job))
    (format s "Minute: ~a. " (cl-cron::job-minute cron-job))))

(defmethod create-item-view ((job cron-job) li &rest args)
  (create-icon (or (object-icon job)
                   "fa-solid fa-cog")
               li)
  ;; Title
  (arrows:->
   (create-a li :content (cron-job-name job)
                      :style "margin-left: 10px;")
   (set-on-click (ignoring-args (show job))
                 :cancel-event t)))

(defmethod create-object-view ((job cron-job) clog-obj &rest args)
  ;; Icon
  (create-icon (or (object-icon job)
                   "fa-solid fa-cog")
               clog-obj)
  ;; Title
  (arrows:->
   (create-a clog-obj :content (cron-job-name job)
                      :style "margin-left: 10px;")
   (set-on-click (ignoring-args (show job))
                 :cancel-event t))
  ;; Schedule
  (create-p clog-obj :content (cron-job-schedule-string job))
  ;; Status
  (create-child clog-obj (html (:p (:b (who:str "Status: "))
				   (who:str (cron-job-status job)))))
  ;; Description
  (create-p clog-obj :content (cron-job-description job)))  

(defun cron-jobs-collection ()
  (make-instance 'collection
		 :name "CRON jobs"
		 :members (alex:hash-table-values cl-cron::*cron-jobs-hash*)))

(defcommand (cron :icon "fa fa-cog") ()
  "Open the collection of cron jobs"
  (cron-jobs-collection))

(defcommand (cron-start :icon "fa fa-play"
			:show-result nil)
    ()
  "Start CRON jobs."
  (cl-cron:start-cron))

(defcommand (cron-stop :icon "fa fa-stop"
		       :show-result nil)
    ()
  "Stop CRON jobs."
  (cl-cron:stop-cron))

;; example:

#+example
(make-cron-job :progress-details
 (lambda ()
   (create-job 'pluggable-job
               :name "Progress details"
               :description "A cool job"
               :progress 0
               :progress-max 30
               :display-progress-details t
               :function (lambda (job)
                           (print "Sleeping 30 seconds")
                           (loop for i from 0 to 29 do
                             (print "Working ...")
                             (incf (job-progress job))
                             (sleep 1))
                           (print "Done"))))
	       :description "Progress details job"
	       :step-min 2)

;;(cl-cron:delete-cron-job :cron-job-example)

;; (cl-cron:start-cron)
