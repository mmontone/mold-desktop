;; A Mold component is a model + views + commands deployed at an url

(in-package :mold-desktop)

(defvar *remote-objects* (make-hash-table :test 'equalp))

(defun register-remote-object (remote-object)
  (setf (gethash (object-id remote-object) *remote-objects*) remote-object))

(defun find-remote-object (id)
  (gethash id *remote-objects*))

(defclass remote-object-command (command)
  ((url :initarg :url
        :accessor command-url)
   (http-method :initarg :http-method
                :accessor http-method
                :initform :post))
  (:metaclass closer-mop:funcallable-standard-class)
  (:documentation "Command that executes for a remote-object."))

(defclass remote-object (object)
  ((type :initarg :type
         :accessor remote-object-type))
  (:metaclass mold-object-class))

(defclass remote-object-service (object)
  ((views :initarg :views
          :accessor remote-object-views)
   (commands :initarg :commands
             :accessor remote-object-commands)
   (endpoints :initarg :endpoints
              :accessor remote-object-endpoints))
  (:metaclass mold-object-class)
  (:documentation "Remote objects are objects fetched and manipulated using an HTTP api.
An object manifest exposes the remote object name, attributes, views and commands that can be applied to it."))

(defmethod object-view-types append ((object remote-object) mold-module)
  (mapcar (lambda (view-spec) (access:access view-spec :name)) (remote-object-views object)))

(defmethod generic-typep ((comp remote-object) type)
  (eql (remote-object-type comp) type))

;; (generic-typep (make-instance 'remote-object :type 'my-remote-object-type) 'my-remote-object-type)

(defun call-remote-object-command (command args)
  (let ((result
          (json:decode-json-from-source
           (drakma:http-request (command-url command) :method (http-method command)
                                                      :want-stream t))))
    ;; the result should contain the list of objects that were updated
    ;; we trigger a change event on the updated objects so that their view updates
    (dolist (updated (access:access result :updated))
      (let ((remote-object (find-remote-object updated)))
        (event-emitter:emit :changed remote-object)))))

(defun register-remote-object-command (command-spec)
  (let* ((command
           (make-instance 'remote-object-command
                          :name (access:access command-spec :name)
                          :lambda-list (generalize-lambda-list (access:access command-spec :arglist))
                          :documentation (access:access command-spec :documentation)))
         (lambda-expression (lambda (&rest args)
                              (call-remote-object-command command args))))
    (closer-mop:ensure-method command lambda-expression)
    (register-command command)
    command))

(defun register-remote-object-commands (remote-object commands)
  (dolist (command-spec commands)
    (let* ((command
             (make-instance 'remote-object-command
                            :name (access:access command-spec :name)
                            :lambda-list (generalize-lambda-list (access:access command-spec :arglist))
                            :documentation (access:access command-spec :documentation)))
           (lambda-expression (lambda (&rest args)
                                (call-remote-object-command command args))))
      (closer-mop:ensure-method command lambda-expression)
      (register-command command)
      command)))

(defgeneric remote-object-commands-url (remote-object command-name))

(defmethod remote-object-commands-url ((remote-object remote-object) command-name)
  (access:access (remote-object-endpoints remote-object) :commands))

(defun call-remote-object-command (command args)
  (drakma:http-request (remote-object-commands-url (command-name command))
                       :method :post))

(defvar *ros-services* nil)
(defvar *remote-object-types* nil)

(mold-desktop:defcommand install-remote-object-service ((url string))
  "Install ROS served from URL"
  (let ((manifest (drakma:http-request url)))
    (push (make-instance 'remote-object-service
                         :name (access:access manifest :name)
                         :url url
                         :description (access:access manifest :documentation))
          *ros-services*)
    ;; Create object types

    ;; Register commands
    (dolist (command (access:access manifest :commands))
      (let ((remote-command (make-instance '
                             )
                            nil)

            (mold-desktop:defcommand create-remote-object
                ((type (member-of *remote-object-types*)))
              )
