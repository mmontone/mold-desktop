;; Copyright (C) 2022 Mariano Montone - All Rights Reserved
;; You may use, distribute and modify this code under the terms of the
;; GNU Affero General Public License

(defpackage :mold-desktop/commands
  (:use :cl :clog :mold-desktop :mold-desktop/clog-gui))

(in-package :mold-desktop/commands)

(mold-desktop:defcommand help
    ((object mold-desktop::object "The object for which to obtain help."))
  "Help about selected object."
  (mold-desktop::with-open-window wnd (mold-desktop::*app-body* :title "Help")
    (create-child wnd (mold-desktop::html
                        (:div (:h3 (who:str "Help"))
                              (:p (who:str "Some help")))))))

(mold-desktop:defcommand (load-system :icon "fa fa-upload")
    ()
  "Load objects from the store."
  (mold-desktop/store:load-database mold-desktop::*store*)
  (alert-toast mold-desktop::*body* "Objects loaded" "Objects loaded"
               :color-class "w3-green" :time-out 2))

(mold-desktop:defcommand (save-system :icon "fa fa-floppy-disk")
    ()
  "Save objects to the store."
  (mold-desktop/store:save-database mold-desktop::*store*)
  (alert-toast mold-desktop::*body* "System saved" "System objects saved"
               :color-class "w3-green" :time-out 2))

(mold-desktop:defcommand (evaluate
                          :icon "fa fa-cog"
                          :show-result t)
    ((expression string "The Lisp expression to evaluate."))
  "Evaluate a lisp expression."
  (let ((*package* (find-package 'mold-desktop))
        (* mold-desktop::*object*))
    (let ((exp (read-from-string expression)))
      (let ((evaluated (eval exp)))
        (when (listp evaluated)
          (setf evaluated (make-instance 'mold-desktop::collection :members evaluated)))
        evaluated))))

(mold-desktop:defcommand (evaluate-with-object
                          :icon "fa fa-cog"
                          :show-result t)
    ((object mold-desktop::object "The object to evaluate with.")
     (expression string "The lisp expression to evaluate."))
  "Evaluate Lisp EXPRESSION with OBJECT bound to *."
  (let ((*package* (find-package 'mold-desktop))
        (* object))
    (let ((exp (read-from-string expression)))
      (let ((evaluated (eval exp)))
        (when (listp evaluated)
          (setf evaluated (make-instance 'mold-desktop::collection :members evaluated)))
        evaluated))))

(defun prompt-collection-element-type-instance (args)
  (let ((coll (cdr (assoc 'collection args))))
    ;; If collection available ..
    (if coll
        ;; use the element type for the prompt
        (mold-desktop::elements-type coll)
        ;; otherwise, use any object
        t)))

(mold-desktop:defcommand add-to-collection
    ((collection mold-desktop::collection "The collection to add an object to.")
     (object t "The object to add to the collection."
             :prompt #'prompt-collection-element-type-instance))
  "Add OBJECT to COLLECTION."
  (mold-desktop::collection-add collection object))

(mold-desktop:defcommand add-members-to-collection
    ((collection mold-desktop::collection "The collection to add members to.")
     (from-collection mold-desktop::collection "The collection to add members from."))
  "Add members FROM-COLLECTION to COLLECTION."
  (dolist (item (mold-desktop::members from-collection))
    (pushnew item (mold-desktop::members collection)))
  (event-emitter:emit :changed collection collection))

(defun prompt-collection-element (args)
  (let ((coll (cdr (assoc 'collection args))))
    ;; If collection argument is already available, choose from one of the collection elements
    (if coll
        ;; use the element type for the prompt
        `(member ,@(mold-desktop::members coll))
        ;; otherwise, use any object
        t)))

(mold-desktop:defcommand remove-from-collection
    ((collection mold-desktop::collection "The collection to remove the object from.")
     (object t "The object to remove from the collection."
             :prompt #'prompt-collection-element))
  "Remove OBJECT from COLLECTION."
  (mold-desktop::collection-remove collection object)
  ;; If OBJECT is a COLLECTION, then we should remove its items from the COLLECTION.
  (when (typep object 'mold-desktop::collection)
    (dolist (item (mold-desktop::members object))
      (setf (mold-desktop::members collection)
            (delete item (mold-desktop::members collection))))
    (event-emitter:emit :changed collection collection)))

(mold-desktop:defcommand remove-members-from-collection
    ((collection mold-desktop::collection "The collection to remove from.")
     (to-remove mold-desktop::collection "The collection of members to remove."))
  "Remove members in TO-REMOVE from COLLECTION."
  (dolist (item (mold-desktop::members to-remove))
    (setf (mold-desktop::members collection)
          (delete item (mold-desktop::members collection))))
  (event-emitter:emit :changed collection collection))

(mold-desktop:defcommand remove-all-members
    ((collection mold-desktop::collection) &optional (confirm boolean t))
  "Remove all members of COLLECTION."
  (flet ((proceed ()
           (setf (mold-desktop::members collection) nil)
           (event-emitter:emit :changed collection collection)))
    (if (not confirm) (proceed)
        (mold-desktop::command-bar-confirm-and-do
         (format nil "Remove all members from ~a?" (mold-desktop::object-title collection))
         #'proceed mold-desktop::*command-bar*))))

(defun prompt-collection-element-type (args)
  (let ((coll (cdr (assoc 'collection args))))
    ;; If collection available ..
    (if coll
        ;; use the element type for the prompt
        `(mold-desktop::member-of-list ',(cons (mold-desktop::elements-type coll)
                                               (mapcar #'class-name (mold-desktop::all-subclasses (find-class (mold-desktop::elements-type coll))))))
        ;; otherwise, use any type
        `(mold-desktop::member-of-list ',(mapcar #'class-name (mold-desktop::all-subclasses (find-class t)))))))

(mold-desktop:defcommand add-new-to-collection
    ((collection mold-desktop::collection "The collection to add an object to.")
     (object-type symbol "The type of object to create and add to the collection."
                  :prompt #'prompt-collection-element-type))
  "Create an object of type OBJECT-TYPE and add it to the COLLECTION."
  (let ((object (make-instance object-type)))
    (mold-desktop::collection-add collection object)))

(mold-desktop:defcommand filter-collection
    ((collection mold-desktop::collection "The collection to filter.")
     (filter string "A lambda expression for filtering the collection."))
  "Filter a collection"
  (let ((filter-func (let ((*package* (find-package 'mold-desktop)))
                       (let ((exp (read-from-string filter)))
                         (eval exp)))))
    (mold-desktop::filter-collection collection filter-func)))

(mold-desktop:defcommand map-collection
    ((collection mold-desktop::collection "The collection to map")
     (exp string "A expression for mapping the collection."))
  "Map a collection"
  (let ((*package* (find-package 'mold-desktop)))
    (let ((func (eval (read-from-string exp))))
      (mold-desktop::map-collection collection func))))

;; I would like to say (lambda (object-type)
;;                           `(and (satisfies symbolp)
;;                                 (satisfies (subtypep current-type object-type)))))

(deftype mold-desktop::subclass-of (object-type)
  'symbol)

(mold-desktop:defcommand (create-new-object :icon "fa fa-file-circle-plus"
                                            :show-result t)
    ((object-type (mold-desktop::subclass-of mold-desktop::object) "The type of object"))
  "Create a new object. Prompting for its type first."
  (make-instance object-type))

(mold-desktop:defcommand (add-object-from-url :show-result t)
    ((url string "The URL of the resource"))
  "Parse an object from an URL and create and add an object from it."
  (let ((object (mold-desktop::create-object-from-url url)))
    (push object mold-desktop::*objects*)
    object))

(mold-desktop:defcommand (add-numbers :show-result t)
    ((x number "Integer 1")
     (y number "Integer 2"))
  "Add two numbers"
  (+ x y))

(mold-desktop:defcommand (apply-function :show-result t)
    ((f function "The function to apply")
     (x t "The function argument"))
  (funcall f x))

(mold-desktop:defcommand (filter-by-type :show-result t)
    ((collection mold-desktop::collection "The collection to filter")
     (type (mold-desktop::subclass-of mold-desktop::object)))
  "Filter collection by type"
  (make-instance 'mold-desktop::collection
                 :members (lambda () (remove-if-not (lambda (obj)
                                                 (typep obj type))
                                               mold-desktop::*objects*))))

(mold-desktop:defcommand
    (handle-event :icon "fa fa-cog")
    ((event mold-desktop::event-object "The event to handle.")
     (handler function "The event handler."))
  "Install event HANDLER for EVENT."
  (mold-desktop::install-event-handler handler event))

(mold-desktop:defcommand (run-program :icon "fa fa-cog")
    ((program string "The command line program"))
  "Run a program from command line"
  (uiop/run-program:run-program program))

(mold-desktop:defcommand (open-in-desktop
                          :icon "fa-solid fa-arrow-up-right-from-square"
                          :show-result t)
    ((object object "The object to open in desktop."))
  "Open OBJECT in desktop."
  object)

(mold-desktop:defcommand (notify-info :icon "fa-regular fa-bell")
    ((message string "Message to notify.")
     &optional
     (timeout integer nil "Timeout"))
  "Notify information."
  (alert-toast mold-desktop::*body* "" message
               :color-class "w3-blue"
               :time-out timeout))

(mold-desktop:defcommand (notify-success :icon "fa-regular fa-bell")
    ((message string "Message to notify.")
     &optional
     (timeout integer nil "Timeout"))
  "Notify information."
  (alert-toast mold-desktop::*body* "" message
               :color-class "w3-green"
               :time-out timeout))

(mold-desktop:defcommand (notify-warn :icon "fa-regular fa-bell")
    ((message string "Message to notify.")
     &optional
     (timeout integer "Timeout"))
  "Notify information."
  (alert-toast mold-desktop::*body* "" message
               :color-class "w3-orange"
               :time-out timeout))

(mold-desktop:defcommand (notify-error :icon "fa-regular fa-bell")
    ((message string "Message to notify.")
     &optional
     (timeout integer "Timeout"))
  "Notify information."
  (alert-toast mold-desktop::*body* "" message
               :color-class "w3-red"
               :time-out timeout))

(mold-desktop:defcommand (create-applied-command
                          :icon "fa fa-sparkles")
    ((command mold-desktop::command "A command"))
  (mold-desktop::prompt-command-arguments
   command
   mold-desktop::*command-bar*
   (lambda (arg-values)
     (mold-desktop::show
      (make-instance 'mold-desktop::applied-command
                     :command command
                     :arg-values arg-values)))))

(mold-desktop:defcommand (find-command
                          :icon "fa-solid fa-magnifying-glass")
    ((command mold-desktop::command "The command"))
  "Find a command."
  command)

(defun prompt-object-attribute (args)
  (let ((obj (cdr (assoc 'object args))))
    (if obj
        `(member ,@(mapcar #'c2mop:slot-definition-name
                           (mold-desktop::object-attributes obj)))
        'symbol)))

(defun prompt-attribute-value (args)
  (let ((obj (cdr (assoc 'object args)))
        (attr-name (cdr (assoc 'attribute-name args))))
    ;; If attribute argument is already available, extract its type
    ;; and use it for the prompt.
    (if (and obj attr-name)
        (let ((attr (mold-desktop::find-object-attribute obj attr-name)))
          ;; use the type of the attribute for the prompt
          (mold-desktop::attribute-type attr))
        ;; otherwise, use any object
        t)))

(mold-desktop:defcommand set-attribute-value
    ((object mold-desktop::object "The object")
     (attribute-name symbol "Attribute name"
                     :prompt #'prompt-object-attribute)
     (value t "Attribute value"
            :prompt #'prompt-attribute-value))
  (setf (slot-value object attribute-name) value)
  nil)


(mold-desktop:defcommand set-object-name
    ((object mold-desktop::object)
     (name string))
  (setf (mold-desktop::object-name object) name))

(defun list-all-tags* ()
  (let ((tags (loop for object in mold-desktop::*objects*
                    appending (mold-desktop::tags object))))
    (setf tags (remove-duplicates tags :test #'equalp))
    tags))

(mold-desktop:defcommand list-all-tags ()
  "List all tags."
  (let ((objects-coll (make-instance 'mold-desktop::collection :members mold-desktop::*objects*)))
    (make-instance 'mold-desktop::collection
                   :name "Tags"
                   :members (mapcar (lambda (tag)
                                      (make-instance 'mold-desktop::filtered-collection
                                                     :name (format nil "Tagged: ~a" tag)
                                                     :collection objects-coll
                                                     :filter (lambda (obj)
                                                               (member tag (mold-desktop::tags obj) :test #'equalp))))
                                    (list-all-tags*)))))

(deftype tag () `string)

(defmethod mold-desktop::prompt-command-argument-type
    ((arg-type (eql 'tag)) arg command-bar handler)

  (mold-desktop::command-bar-choose "Choose: " (list-all-tags*) handler command-bar :close nil))

(mold-desktop:defcommand (search-by-tag :show-result t)
    ((tag tag "The tag."))
  "Search objects with tag."
  (let ((objects-coll (make-instance 'mold-desktop::collection :members mold-desktop::*objects*)))
    (make-instance 'mold-desktop::filtered-collection
                   :name (format nil "Tagged: ~a" tag)
                   :collection objects-coll
                   :filter (lambda (obj)
                             (member tag (mold-desktop::tags obj) :test #'equalp)))))

;; Commands for development

(mold-desktop:defcommand start-swank-server
    (&key (port integer 4005 "The port"))
  "Start SWANK server"
  (swank:create-server :port port))

(mold-desktop:defcommand inspect-in-emacs ()
  "Inspects current *OBJECT* in Emacs"
  (swank:eval-in-emacs '(slime-inspect "MOLD-DESKTOP::*OBJECT*")))

(mold-desktop:defcommand edit-definition-in-emacs ()
  "Edit definition of *OBJECT* in Emacs"
  (swank:eval-in-emacs `(slime-edit-definition ,(prin1-to-string (type-of mold-desktop::*object*)))))
