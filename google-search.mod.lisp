(mold-desktop::defmodule mold-desktop::file-mold-module
  google-search
  :description "Search webpages using Google web search api")

(in-package :mold-desktop)

(defun google-search-api (query)
  (json:decode-json-from-source
   (drakma:http-request "https://customsearch.googleapis.com/customsearch/v1"
                        :want-stream t
                        :parameters `(("key" . ,(config-ensure-get :google :api-key))
                                      ("cx" . ,(config-ensure-get :google :custom-search-engine-id))
                                      ("q" . ,query)))))

(defcommand (google-search
	     :icon "fa-solid fa-magnifying-glass")
    ((query string "A search query"))
  "Search using Google web search api."
  ;; TODO: the results in api contain metadata that can be used to create the objects
  ;; without needing to make an http request for each.
  ;; Use that data.
  (make-instance 'collection
		 :name (format nil "Google search: ~a" query)
                 :members
                 (lparallel:pmapcar
		  (lambda (result)
                    (create-object-from-url (acc:access result :link)))
                  (acc:access (google-search-api query) :items))))
