;; Widgets for Mold Desktop, that work with value-models and support drag and drop.
(in-package :mold-desktop)

(defclass widget (clog-element)
  ((model :initarg :model
          :accessor model
          :initform (error "Provide the widget model"))
   (clog-obj :accessor clog-obj)))

(defgeneric drag-over-widget (widget dragging-object)
  (:method (widget dragging-object)
    (when (drop-on-widget-allowed? widget dragging-object)
      (add-class (clog-obj widget) "drop-allowed"))))
(defgeneric drag-leave-widget (widget)
  (:method (widget)
    (remove-class (clog-obj widget) "drop-allowed")))
(defgeneric drop-on-widget (widget dragging-object)
  (:method (widget dragging-object)
    (when (drop-on-widget-allowed? widget dragging-object)
      (setf (value-models:value (model widget)) dragging-object))))
(defgeneric drop-on-widget-allowed? (widget dragging-object)
  (:method (widget dragging-object)
    "Default method"
    nil))

(defun setup-widget-drag-and-drop (widget)
  ;; To enable drop, drag-over event handler needs to be set:
  (set-on-drag-over (clog-obj widget)
                    (lambda (&rest args)
                      (declare (ignore args))
                      (drag-over-widget widget mold-desktop/clog-gui::*dragging-object*)))

  (set-on-drag-leave (clog-obj widget)
                     (lambda (&rest args)
                       (declare (ignore args))
                       (drag-leave-widget widget)))

  (set-on-drop (clog-obj widget)
               (lambda (&rest args)
                 (declare (ignore args))
                 (drop-on-widget widget mold-desktop/clog-gui::*dragging-object*)
                 (setf mold-desktop/clog-gui::*dragging-object* nil))))

(defgeneric create-widget (clog-obj widget-type &rest args &key model &allow-other-keys)
  (:method (clog-obj widget-type &rest args &key model)
    (let ((widget (apply #'make-instance widget-type args)))
      (let ((clog-elem (render-widget clog-obj widget)))
        (setf (clog-obj widget) clog-elem)
        (propagate-changes clog-elem model)
        (setup-widget-drag-and-drop widget)))))

(defgeneric render-widget (clog-obj widget))

;;---- String

(defclass string-input-widget (widget)
  ())

(defmethod render-widget (clog-obj (widget string-input-widget))
  (create-child clog-obj
                (html (:input :type "text"
                              :class "w3-input w3-border"
                              :value (value-models:value (model widget))))))

(defmethod drop-on-widget-allowed? ((widget string-input-widget) (obj string))
  t)

;;-- Object input

(defclass object-input-widget (widget)
  ((object-type :initarg :object-type
		:accessor object-type)))

(defmethod render-widget (clog-obj (widget object-input-widget))
  (let ((input (create-child clog-obj
			     (html (:input :type "text"
					   :class "w3-input w3-border"
					   :readonly t
					   :value (object-title (value-models:value (model widget))))))))
    ;; Open the current object on click
    (set-on-click
     input (lambda (&rest args)
	     (declare (ignore args))
	     (show (value-models:value (model widget)))))

    ;; Edit on double-click
    (set-on-context-menu
     input (lambda (&rest args)
	     (declare (ignore args))
	     ;; Start an object selection
	     (command-bar-choose
	      (format nil "Select a ~a: " (object-type widget))
	      (cons
	       (list :value :new
		     :label "Create new ..."
		     :description (format nil "Create a new ~a" (object-type widget)))
	       (instances-of (object-type widget)))
	      (lambda (obj)
		(if (eq obj :new)
		    (let ((new-object (make-instance (object-type widget))))
		      (show new-object)
		      (setf (value-models:value (model widget)) new-object))
		    ;; else
		    (setf (value-models:value (model widget)) obj)))
	      *command-bar*)))
    input))

(defmethod drop-on-widget-allowed? ((widget object-input-widget) obj)
  (typep obj (object-type widget)))

;; https://www.w3schools.com/howto/howto_js_collapsible.asp
(defun make-collapsible (clog-obj title &optional (opened t))
  (let* ((opened-p opened)
         (collapsible (clog:create-div clog-obj :class "collapsible"))
         (btn (clog:create-button collapsible :content title :class "toggler"))
         (content (clog:create-div collapsible :class "content")))
    (clog:set-on-click btn (lambda (&rest args)
                             (declare (ignore args))
                             (setf opened-p (not opened-p))
                             (clog:toggle-class btn "active")
                             (setf (clog:display content) (if (not opened-p)
                                                              "none"
                                                              "block"))))
    ;; Return the CLOG element to where the content should be attached
    (values content collapsible)))
