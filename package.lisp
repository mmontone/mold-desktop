;; Copyright (C) 2022 Mariano Montone - All Rights Reserved
;; You may use, distribute and modify this code under the terms of the
;; GNU Affero General Public License

(mgl-pax:define-package :mold-desktop/clog-gui
    (:nicknames :md/clog-gui)
  (:documentation "CLOG-GUI a desktop GUI abstraction for CLOG")
  (:import-from :cl-css :inline-css :px :em)
  (:use #:cl #:parse-float #:clog #:mgl-pax)
  (:export #:create-halos-wrapper))

(defpackage :mold-desktop
  (:use :cl :clog #:mold-desktop/clog-gui #:access)
  (:local-nicknames
   (:alex :alexandria)
   (:sera :serapeum)
   (:acc :access)
   (:gcl :generic-cl)
   (:lt :local-time))
  (:import-from :cl-who :with-html-output
                :with-html-output-to-string :htm :str :fmt)
  (:export
   #:start
   #:defcommand
   #:defgeneric-command
   #:defcommand-method
   #:object
   #:create-object-view
   #:create-item-view
   #:*handle-command-errors*
   #:*handle-view-errors*
   #:toggle-debugging))

(defvar mold-desktop::*mold-modules* (list :desktop)
  "List of loaded modules.")
