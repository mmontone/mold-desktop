(defpackage :web-data-extractor
  (:use :cl)
  (:export :extract-data)
  (:documentation "Extract semantic data from urls.

See: https://github.com/fatihyildizli/web-structured-data-extractor"))

(in-package :web-data-extractor)

(defun extract-data (url)
  (json:decode-json-from-source
   (drakma:http-request "https://structured-data-web.herokuapp.com/url/schemaorg/all/summary"
			:parameters (list (cons "url" url))
			:want-stream t)))
