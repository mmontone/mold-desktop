FROM clfoundation/sbcl:slim

COPY . /root
WORKDIR /root
COPY bin/mold-desktop-docker.gz /root/mold-desktop-docker.gz
RUN gunzip /root/mold-desktop-docker.gz
CMD [ "/root/mold-desktop-docker" ]
EXPOSE 8080
