(asdf:defsystem #:mold-desktop-feeds
  :description "RSS feeds  application for Mold Desktop"
  :version "0.1"
  :author "Mariano Montone <marianomontone@gmail.com>"
  :license "AGPL"
  :serial t
  :components
  ((:module "apps"
    :components ((:file "feeds"))))
  :depends-on (:mold-desktop))
