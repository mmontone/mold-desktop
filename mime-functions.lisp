(defpackage :mime-functions
  (:use #:cl)
  (:export
   #:define-mime-function
   #:define-mime-method
   #:find-mime-function))

(in-package :mime-functions)

(defvar *mime-functions* (make-hash-table))

(defclass mime-function (closer-mop:funcallable-standard-object)
  ((name :initarg :name
         :accessor mime-function-name
         :type symbol)
   (args :initarg :args
         :accessor mime-function-args
         :initform nil)
   (methods :initarg :methods
            :initform (make-hash-table :test 'equalp)
            :accessor mime-function-methods)
   (documentation :initarg :documentation
                  :initform nil
                  :accessor mime-function-documentation))
  (:metaclass closer-mop:funcallable-standard-class))

(defmethod initialize-instance :after ((gf mime-function) &rest initargs)
  (declare (ignorable initargs))
  (closer-mop:set-funcallable-instance-function
   gf
   (lambda (mime-type &rest args)
     (dispatch-mime-function gf mime-type args))))

(defmacro define-mime-function (name args &rest options)
  `(let ((gf (make-instance 'mime-function :name ',name :args ',args ,@options)))
     (setf (gethash ',name *mime-functions*) gf)
     (setf (symbol-function ',name) gf)
     gf))

(defmacro define-mime-method (name mime-type args &body body)
  ;; TODO: validate MIME-TYPE
  ;; TODO: validate args number
  `(let ((gf (find-mime-function ',name)))
     (setf (gethash ,mime-type (mime-function-methods gf))
           (lambda ,args
             ,@body))))

(defun find-mime-function (name &optional (error-p t))
  (or (gethash name *mime-functions*)
      (when error-p
        (error "Mime generic function not found: ~s" name))))

(defun mime-function-mime-types (gf)
  (alexandria:hash-table-keys (mime-function-methods gf)))

(defun dispatch-mime-function (gf mime-type args)
  (let ((method-key
          (mimeparse:best-match (mime-function-mime-types gf) mime-type)))
    (when (str:emptyp method-key)
      (error "No method for mime type: ~a in: ~a args: ~a" mime-type gf args))
    (let ((method (gethash method-key (mime-function-methods gf))))
      (apply method args))))
