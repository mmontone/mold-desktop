(defpackage :mold-desktop/store
  (:use :cl)
  (:export
   #:save-database
   #:load-database))

(in-package :mold-desktop/store)

(defclass store ()
  ())

(defgeneric load-database (store))
(defgeneric save-database (store))

(defclass fs-store (store)
  ((filepath :initarg :filepath
             :accessor filepath
             :initform (error "Provide the filepath"))))

(defmethod load-database ((store fs-store))
  (unless (probe-file (filepath store))
    (warn "Database file does not exist: ~s" (filepath store))
    (return-from load-database))
  (with-open-file (f (filepath store) :direction :input
                                      :element-type '(unsigned-byte 8))
    (with-open-stream (decompressed-stream (chipz:make-decompressing-stream  'chipz:zlib f))
      (setf mold-desktop::*objects* (cl-store:restore decompressed-stream))))
  (dolist (object mold-desktop::*objects*)
    (when (typep object 'mold-desktop::object)
      (setf (slot-value object 'event-emitter::silo)
            (make-hash-table :test 'eq)))))

(defmethod save-database ((store fs-store))
  (with-open-file (f (filepath store) :direction :output
                                      :element-type '(unsigned-byte 8)
                                      :if-exists :supersede
                                      :if-does-not-exist :create)
    (with-open-stream (compressed-stream (salza2:make-compressing-stream 'salza2:zlib-compressor f))
      (cl-store:store mold-desktop::*objects* compressed-stream)))
  t)

(defmethod cl-store:serializable-slots-using-class ((object mold-desktop::object)
                                                    (class mold-desktop::mold-object-class))
  (let ((all-slots (call-next-method)))
    (remove 'event-emitter::silo all-slots :key 'closer-mop:slot-definition-name)))
