;; Copyright (C) 2022 Mariano Montone - All Rights Reserved
;; You may use, distribute and modify this code under the terms of the
;; GNU Affero General Public License

(in-package :mold-desktop)

(defclass collection (object gcl:sequence)
  ;; TODO: rename collection-type to elements-type or members-type
  ;; I'm not doing it now cause I'm deserializing from cl-store with old data
  ((collection-type :initarg :type
                    :accessor elements-type
                    :initform t
                    :documentation "The type of the objects of the collection.")
   (members :writer (setf members)
            :initarg :members
            :initform nil
            :type (or function list)
            :documentation "The members of the collection. It can be either a list of members, or a function that returns a list of members.")
   (order :initarg :order
          :accessor collection-order
          :initform nil
          :documentation "The spec for sorting the members.
Order spec is: (direction slot &optional test)"))
  (:metaclass mold-object-class)
  (:icon . "fa-solid fa-list")
  (:documentation "A collection of objects."))

(defmethod gcl:length ((coll collection))
  (length (members coll)))

(defmethod gcl:elt ((coll collection) index)
  (gcl:elt (members coll) index))

(defmethod gcl:make-iterator ((coll collection) start end)
  (gcl:make-iterator (members coll) start end))

(defmethod gcl:make-reverse-iterator ((coll collection) start end)
  (gcl:make-reverse-iterator (members coll) start end))

(defun sort-collection-members (members order)
  (error "TODO"))

(defmethod members ((collection collection))
  (let* ((unsorted-members
           (with-slots (members) collection
             (if (functionp members)
                 (funcall members)
                 members)))
         (sorted-members unsorted-members))
    (when (collection-order collection)
      (setf sorted-members (sort-collection-members unsorted-members (collection-order collection))))
    sorted-members))

(defgeneric collection-add (collection object))
(defmethod collection-add ((collection collection) object)
  (push-end object (members collection))
  (event-emitter:emit :changed collection collection))

(defgeneric collection-push (object collection))
(defmethod collection-add (object (collection collection))
  (push-end object (members collection))
  (event-emitter:emit :changed collection collection))

(defgeneric collection-remove (collection object))
(defmethod collection-remove ((collection collection) object)
  (setf (members collection) (remove object (members collection)))
  (event-emitter:emit :changed collection collection))

(defmethod gcl:delete (item (coll collection) &key from-end test start end count key)
  (setf (members coll)
        (gcl:delete (members coll) :from-end from-end :test test :start start
                                   :end end :count count :key key)))

(defmethod gcl:delete :after (item (coll collection) &rest args)
  (event-emitter:emit :changed coll coll))

(defgeneric create-item-view (object li &rest args)
  (:documentation "A view for OBJECT as a list item of a collection."))

(defmethod create-item-view (object li &rest args &key context)
  (let ((view (create-span li :content (who:escape-string (princ-to-string object)))))
    (set-on-click li (lambda* (_)
                       (if (getf args :on-select)
                           (funcall (getf args :on-select) object)
                           (show object :context context))))
    view))

(defun get-favicon-url (url)
  "Get the favicon link of url."
  ;; We use duckduckgo service for now ...
  (let ((uri (quri:uri url)))
    (format nil "https://icons.duckduckgo.com/ip3/~a.ico" (quri:uri-host uri))))

(defmethod create-item-view ((object object) li &key context on-select &allow-other-keys)
  (with-ui-update li object :changed
    (cond
      ((object-url object)
       (create-img li :url-src (get-favicon-url (object-url object))
                      :style "height: 32px; width: 32px; margin-right: 5px;"))
      ((object-icon object)
       (create-child li (html (:i :class (format nil "fa fa-solid fa-~a"
                                                 (object-icon object))
                                  :style "margin-right: 3px;")))))
    (let ((link
            (create-a li :content (or (object-name object) (object-url object)))))
      (set-on-event link "click"
                    (lambda* (_)
                      (if on-select
                          (funcall on-select object)
                          (show object :context context)))
                    :cancel-event t))))

(defun draw-paginated (clog-obj drawing-function members page-size)
  "Render MEMBERS using DRAWING-FUNCTION in a paginated way.

Arguments:
- CLOG-OBJ - CLOG:CLOG-ELEMENT: main clog object where new elements are attached to.
- DRAWING-FUNCTION - function: a function that takes an element from the paginated collection and creates a clog element for it. The clog element needs to be returned from this function.
- MEMBERS - list: the list of elements to render.
- PAGE-SIZE - integer: the page size."
  (when (not members)
    (return-from draw-paginated))
  (let ((page-members (subseq members 0 (min (length members) page-size)))
        (next-members (and (> (length members) page-size)
                           (subseq members page-size)))
        last-item-view)
    (dolist (item page-members)
      (setf last-item-view (the clog-element (funcall drawing-function item))))
    (when next-members
      (js-execute clog-obj
                  (format nil
                          "let observer = new IntersectionObserver(function(entries) {
        // isIntersecting is true when element and viewport are overlapping
        // isIntersecting is false when element and viewport don't overlap
        if(entries[0].isIntersecting === true)
                $('#~a').trigger('visible') // Element has just become visible in screen
}, { threshold: [0] });

observer.observe(document.getElementById('~a'));"
                          (html-id last-item-view) (html-id last-item-view)))
      (set-on-event last-item-view "visible"
                    (lambda (&rest args)
                      (declare (ignore args))
                      (draw-paginated clog-obj drawing-function next-members page-size))))))

(defclass collection-list-view (clog:clog-unordered-list)
  ((collection :initarg :collection)
   (args :initarg :args)
   (context :initarg :context)
   (collection-configuration :initform (make-instance 'collection-configuration))))

(defmethod refresh-view ((view collection-list-view))
  (with-slots (collection args context collection-configuration) view
    (setf (text view) "")
    (draw-paginated
     view
     (lambda (item)
       (let* ((li (mold-desktop/clog-gui::create-halos-wrapper view item :context collection :create-function #'create-list-item))
              (content-div (create-div li)))
         (apply #'create-item-view item content-div :context context (getf args :item))
         li))
     (apply-collection-configuration collection-configuration collection)
     20)))

(defmethod create-object-view ((collection collection) clog-obj &rest args &key context &allow-other-keys)
  "Lazy loading paginated version"
  (let ((view
          (create-unordered-list clog-obj :class "w3-ul")))
    (change-class view 'collection-list-view
                  :collection collection
                  :args args
                  :context context)
    (refresh-view view)
    (event-emitter:on :changed collection
                      (lambda (&rest args)
                        (declare (ignore args))
                        (refresh-view view)))
    view))

;; The non paginated version:
#+nil
(defmethod create-object-view ((collection collection) clog-obj &rest args)
  (let ((ul (create-unordered-list clog-obj :class "w3-ul")))
    (flet ((redraw-list (&rest _)
             (declare (ignore _))
             (setf (text ul) "")
             (dolist (item (members collection))
               (let* ((li (create-list-item ul))
                      (wrapper (mold-desktop/clog-gui::create-thin-halos-wrapper li item :context collection)))
                 (apply #'create-item-view item wrapper (getf args :item))))))
      (redraw-list)
      (event-emitter:on :changed collection #'redraw-list)
      ul)))

(defview create-expanded-collection-view (collection clog-obj &rest args)
  (:documentation "Create a view for COLLECTION with items expanded.")
  (:label "Expanded collection view")
  (:icon "fa fa-image"))

(defmethod create-expanded-collection-view ((collection collection) clog-obj &rest args &key context &allow-other-keys)
  "Lazy loading paginated version"
  (let ((ul (create-unordered-list clog-obj :class "w3-ul")))
    (flet ((redraw-list (&rest _)
             (declare (ignore _))
             (setf (text ul) "")
             (draw-paginated ul
                             (lambda (item)
                               (let* ((li (mold-desktop/clog-gui::create-halos-wrapper ul item :context collection :create-function #'create-list-item))
                                      (content-div (create-div li)))
                                 (apply #'create-thumbnail-view item content-div :context context (getf args :item))
                                 li))
                             (members collection) 20)))
      (redraw-list)
      (event-emitter:on :changed collection #'redraw-list)
      ul)))

(defmethod create-thumbnails-view ((collection collection) clog-obj &rest args)
  "Paginated thumbnails view"
  (let ((thumbnails (create-div clog-obj :class "thumbnails w3-light-gray")))
    (draw-paginated thumbnails
                    (lambda (item)
                      (let ((thumbnail (create-div thumbnails :class (format nil "thumbnail ~a" (or (getf args :thumbnail-size) "")))))
                        (let ((halos (mold-desktop/clog-gui::create-halos-wrapper thumbnail item :context collection)))
                          (let ((tview (apply #'render-object item halos #'create-thumbnail-view (getf args :item))))
                            (set-styles tview '(("height" "200px"))))
                          thumbnail)))
                    (members collection)
                    20)
    thumbnails))

;; The non paginated version:
#+nil
(defmethod create-thumbnail-view ((collection collection) clog-obj &rest args)
  (let ((thumbnails (create-div clog-obj :class "thumbnails")))
    (setf (clog:attribute thumbnails "style") "background-color: black;")
    (dolist (item (members collection))
      (let ((thumbnail (create-div thumbnails)))
        (setf (clog:attribute thumbnail "style")
              "margin: 2px; width: 200px; height: 200px;display: inline-block;overflow: hidden;background-color: white;")
        (let ((halos (mold-desktop/clog-gui::create-halos-wrapper thumbnail item)))
          (apply #'create-thumbnail-view item halos (getf args :item)))))))

(md/settings:defcustom *icons-view-icon-size*
  32
  "Default size of icons in icon views, in pixels."
  :type integer
  :group mold-desktop/clog-gui::gui)

(defun create-icon-view (object clog-obj &rest args &key
                                                      (icon-size *icons-view-icon-size*))
  ;; Halo wrapper
  (let* ((icon-view (create-div clog-obj :class "icon-view")))

    (mold-desktop/clog-gui::attach-presentation-handlers
     object icon-view :on-click nil)

    ;; Icon/image
    (cond
      ((object-url object)
       (let ((img-src (get-favicon-url (object-url object))))
         (create-img icon-view :url-src img-src
                               :style (format nil "height: ~apx; width: ~apx;"
                                              icon-size icon-size))))
      ((object-icon object)
       (create-icon (object-icon object) icon-view))
      (t ;; default icon
       (create-icon "fa-regular fa-file" icon-view)))
    ;; Title
    (arrows:->
        (create-a icon-view :content (object-title object) :class "title")
        (set-on-double-click (ignoring-args (run-command-interactively #'mold-desktop/clog-gui::set-object-name object)) :cancel-event t))

    (set-on-double-click icon-view (ignoring-args (show object)) :cancel-event t)

    ;; Detail
    (let ((object-detail (with-output-to-string (s)
                           (format s "~a.~%Type: ~a."
                                   (object-title object)
                                   (type-of object)))))
      (setf (clog:advisory-title icon-view) object-detail))

    icon-view))

(defmethod create-icons-view ((collection collection) clog-obj &rest args)
  "Paginated icons view"
  (let ((icons-view (create-div clog-obj :class "icons w3-light-gray")))
    (draw-paginated icons-view
                    (lambda (obj)
                      (create-icon-view obj icons-view))
                    (members collection)
                    20)
    icons-view))

(defvar *glyphs-icons*
  (json:decode-json-from-source (asdf:system-relative-pathname :mold-desktop "assets/glyphs-icons.json")))

(defvar *fontawesome-icons*
  (json:decode-json-from-source (asdf:system-relative-pathname :mold-desktop "assets/fontawesome-icons.json")))

(defun search-icon (term &key (icon-sets '(:glyphs :fontawesome))
                           (test 'equalp)
                           (find-one t))
  (let ((test-function (cond
                         ((eql test :search)
                          (lambda (x)
                            (search term x :test 'string=)))
                         (t test))))
    (let (results)
      (flet ((add-result (result)
               (when find-one
                 (return-from search-icon result))
               (push result results)))
        (when (member :glyphs icon-sets)
          (dolist (icon-spec *glyphs-icons*)
            (when (or (funcall test-function term (alex:assoc-value icon-spec :name))
                      (some (lambda (x)
                              (funcall test-function term x))
                            (alex:assoc-value icon-spec :terms)))
              (add-result (format nil "core-~a" (string-downcase (json:camel-case-to-lisp (alex:assoc-value icon-spec :name))))))))
        (when (member :fontawesome icon-sets)
          (dolist (icon-spec *fontawesome-icons*)
            (when (funcall test-function term (json:lisp-to-camel-case (symbol-name (first icon-spec))))
              (add-result (format nil "fa fa-~a" (first icon-spec)))))))
      results)))

#+test
(search-icon "arrowUp")
#+test
(search-icon "film")
#+test
(search-icon "video")

(defun category-icon (category)
  (search-icon (princ-to-string category)))

(defmethod create-categorized-thumbnail-view ((collection collection) clog-obj &rest args)
  (let ((thumbnails (create-div clog-obj :class "thumbnails"))
        (categorized (mapcar (lambda (x)
                               ;; Create a class -> instances collection
                               (cons (find-class (first x))
                                     (cdr x)))
                             (categorize-collection collection 'type-of))))
    (dolist (category categorized)
      (let ((category-icon (class-icon (first category)))
            (category-heading (create-button thumbnails :class "w3-button w3-block w3-left-align w3-light-gray")))
        (when category-icon
          (create-icon category-icon category-heading))
        (create-span category-heading :content (humanized-symbol-name (class-name (car category))))
        (dolist (item (cdr category))
          (let ((thumbnail (create-div thumbnails)))
            (setf (clog:attribute thumbnail "style")
                  "margin: 2px; width: 200px; height: 200px;display: inline-block;overflow: hidden;background-color: white;")
            (let ((halos (mold-desktop/clog-gui::create-halos-wrapper thumbnail item)))
              (apply #'create-thumbnail-view item halos (getf args :item)))))))))

(defmethod object-view-types append ((collection collection) (mold-module (eql :desktop)))
  '(create-thumbnails-view
    create-icons-view
    create-expanded-collection-view
    create-categorized-thumbnail-view
    create-table-view
    create-master-detail-view
    create-replacing-view
    create-breadcrumb-view
    create-breadcrumb-detail-view))

(defclass filtered-collection (collection)
  ((collection :initarg :collection
               :accessor collection)
   (filter :initarg :filter
           :accessor filter))
  (:metaclass mold-object-class))

(defmethod members ((collection filtered-collection))
  (remove-if-not (filter collection)
                 (members (collection collection))))

(defclass mapped-collection (collection)
  ((collection :initarg :collection
               :accessor collection)
   (function :initarg :function
             :accessor map-function))
  (:metaclass mold-object-class))

(defmethod members ((collection mapped-collection))
  (mapcar (map-function collection)
          (members (collection collection))))

(defun filter-collection (collection filter &key name)
  (make-instance 'filtered-collection
                 :name (or name (format nil "Filtered ~a" (object-name collection)))
                 :collection collection
                 :filter filter))

#+example
(show
 (make-instance 'filtered-collection
                :name "My videos"
                :collection (make-instance 'collection :members *objects*)
                :filter (lambda (object)
                          (typep object 'video))))

(defun map-collection (collection function &key name)
  (make-instance 'mapped-collection
                 :name (or name (format nil "Mapped ~a" (object-name collection)))
                 :collection collection
                 :function function))

#+example
(show
 (make-instance 'mapped-collection
                :name "Video names"
                :collection
                (make-instance 'filtered-collection
                               :name "My videos"
                               :collection (make-instance 'collection :members *objects*)
                               :filter (lambda (object)
                                         (typep object 'video)))
                :function #'object-name))

(defcommand (search-collection
             :icon "fa-solid fa-magnifying-glass"
             :show-result t)
    ((coll collection "Collection to search.")
     (query string "Query."))
  (let (matches)
    (dolist (item (members coll))
      (when (or (str:contains? query (object-title item))
                (str:contains? query (object-description item)))
        (push-end item matches)))
    (make-instance 'collection
                   :name (format nil "~a matching: ~a" (object-title coll) query)
                   :members matches)))

;; Treatment of plain lists

(defmethod object-title ((list list))
  (str:fit 50 (princ-to-string list)))

(defmethod create-object-view ((collection list) clog-obj &rest args &key context &allow-other-keys)
  "Lazy loading paginated version"
  (let ((ul (create-unordered-list clog-obj :class "w3-ul")))
    (with-ui-update ul collection :changed
      (setf (text ul) "")
      (draw-paginated ul
                      (lambda (item)
                        (let* ((li (mold-desktop/clog-gui::create-halos-wrapper ul item :context collection :create-function #'create-list-item))
                               (content-div (create-div li)))
                          (apply #'create-item-view item content-div :context context (getf args :item))
                          li))
                      collection 20))
    ul))

;; Collections configuration
;; Provides sorting, filtering, etc for collections and views of collections

(defclass collection-configuration ()
  ((sorters :initarg :sorters
            :accessor collection-sorters
            :initform nil
            :documentation "An alist of (attribute-name . sorter-function).")
   (filters :initarg :filters
            :accessor collection-filters
            :initform nil
            :documentation "An alist of (attribute-name . filter-function)")))

(defun add-collection-filter (collection-configuration attribute-name filter-func)
  (push (cons attribute-name filter-func)
        (collection-filters collection-configuration)))

(defun add-collection-sorter (collection-configuration attribute-name sorter-func)
  (push (cons attribute-name sorter-func)
        (collection-sorters collection-configuration)))

(defun clear-collection-configuration (collection-configuration)
  (setf (collection-sorters collection-configuration) nil)
  (setf (collection-filters collection-configuration) nil))

(defparameter *type-sorters*
  '((string . (string< string>))
    (number . (< >))
    (local-time:timestamp . (local-time:timestamp< local-time:timestamp>))))

(defcommand substring-string-filter-builder ((substring string))
  (lambda (string)
    (str:containsp substring string)))

(defcommand prefix-string-filter-builderr ((prefix string))
  (lambda (string)
    (str:starts-with-p prefix string)))

(defcommand range-number-filter-builder ((min integer) (max integer))
  (lambda (number)
    (<= min number max)))

(defcommand greater-than-number-filter-builder ((min integer))
  (lambda (number)
    (<= min number)))

(defcommand less-than-number-filter-builder ((max integer))
  (lambda (number)
    (<= number max)))

(defparameter *type-filters*
  '((string  . (substring-string-filter-builder prefix-string-filter-builder))
    (text . (substring-string-filter-builder prefix-string-filter-builder))
    (number . (range-number-filter-builder greater-than-number-filter-builder less-than-number-filter-builder))))

(defun attribute-filters (attribute)
  (cdr (assoc (attribute-type attribute) *type-filters*)))

(defun attribute-sorters (attribute)
  "Get type sorters for ATTRIBUTE, or nil if there isn't one."
  (cdr (assoc (attribute-type attribute) *type-sorters*)))

(defun sortable-attributes (class)
  (remove-if-not #'attribute-sorters (attributes class)))

(defun filterable-attributes (class)
  (remove-if-not #'attribute-filters (attributes class)))

;; TODO: use transducers? what happens with pagination?
(defun apply-collection-configuration (collection-configuration collection)
  (let ((members (members collection)))
    (dolist (attr-and-filter (collection-filters collection-configuration))
      (let ((filter (lambda (obj)
                      (funcall (cdr attr-and-filter)
                               (slot-value obj (car attr-and-filter))))))
        (setq members (remove-if-not filter members))))
    (dolist (attr-and-sorter (collection-sorters collection-configuration))
      (let ((sorter (lambda (x y)
                      (funcall (cdr attr-and-sorter)
                               (slot-value x (car attr-and-sorter))
                               (slot-value y (car attr-and-sorter))))))
        (setq members (sort members sorter))))
    members))
