## An introduction to Mold Desktop project

Mold Desktop is a web desktop environment implemented in Common Lisp and CLOG for the user interface.

Gathers ideas from other environments like Emacs, CLIM, Smalltalk, and also introduces its own.

It features:

- An object oriented design.
- Interaction via commands that apply to specific types of objects.
- Manipulation of objects via halo controls.
- Multiple views.
- Programmability from the user interface.
- Infinitly extensible via Lisp. Dynamic loading of modules. 
- Access to the whole Lisp image from the environment, and to the environment from the Lisp listener.
