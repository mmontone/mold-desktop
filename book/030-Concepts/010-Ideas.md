## Core ideas

### Expose as much as possible from objects and have access to that from UI

- Icons.
- Documentation.
- Properties. Attributes. Settings.
- Interrelationships (links).
- Commands.

### Every object gets a view on the UI

Every type of object can be displayed on the UI.

### Uniform presentation and manipulation UI

Every object can be manipulated via "halos" controls and commands.
Every object is displayed via the specialization of views.

### Every object can be combined with any other to perform some action

Every object can be used with any other, via the definition of commands.
They don't even need to know each other.
For example, a map object can work with any other object that has geographical information properties.

### No applications

There are no applications, only objects displayed with different views, that can be combined via commands.

### Uniform presentation framework

Every object is presented using some common specialized views. Also, every object can be manipulated using "halos" controls.

### Uniform behaviour framework

Everything goes through commands. Commands can be specialized on the type of objects involved.

### Uniform model editing

Every objects has properties and settings that can be edited from the UI.
Also, users and implementors can meta-model its own objects and its relationships, and get a uniform editing support from the environment.

### Linking

Objects can be linked to other objects, without restrictions, using semantic triples.

### Programmability

There are several means for programing the desktop:

- Write Lisp source code.
- Events and status of objects are exposed. The user can access them, and setup handlers for changes and events, using the user interface capabilities and commands.
