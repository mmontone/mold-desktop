<h2>Views</h2>

<p>
  Every Lisp object gets a view on the UI.
  The main view is created with MOLD-DESKTOP::CREATE-OBJECT-VIEW, which can be specialized on the type of object.
</p>

<p>
  For example, click on the following Lisp objects and see their representation in the GUI:
</p>

<p>
  <ul>
    <li>A string: (:show "hello world")</li>
    <li>A list:  (:show (list 1 2 3))</li>
    <li>A number:  (:show 123456)</li>
    <li>A package:  (:show (find-package :cl))</li>
  </ul>
</p>

<p>Try displaying your own objects via the (:command mold-desktop/commands::evaluate) command.</p>
