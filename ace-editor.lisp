(in-package :mold-desktop)

(pushnew 'init-ace-editor *init-functions*)

(defclass ace-editor (object)
  ((contents :initarg :contents
	     :accessor contents
	     :initform ""
	     :type string
	     :attribute t))
  (:metaclass mold-object-class)
  (:icon . "fa-regular fa-file")
  (:default-initargs
   :icon "fa-regular fa-regular fa-file-lines")
  (:documentation "Ace text editor"))

(defun init-ace-editor (body)
  (add-script (html-document body) "https://cdnjs.cloudflare.com/ajax/libs/ace/1.15.0/ace.min.js")
  (add-script (html-document body) "https://cdnjs.cloudflare.com/ajax/libs/ace/1.15.0/mode-lisp.min.js"))

(defmethod create-object-view ((object ace-editor) clog-obj &rest args)
  (declare (ignore args))
  (let ((editor-el (clog:create-div clog-obj
				    :content (contents object))))
    (setf (clog:height editor-el) "500px")
    (clog:js-execute *body*
		     (with-output-to-string (s)
		       (format s "var editor = ace.edit(\"~a\");" (html-id editor-el))
		       ;;(write-string "editor.setTheme(\"ace/theme/monokai\");" s)
		       ))
    editor-el))

(defcommand open-file-contents
    ((file filesystem-file))
  "Open the file contents using Ace editor component."
  (make-instance 'ace-editor
		 :name (princ-to-string (file-pathname file))
		 :contents (alexandria:read-file-into-string (file-pathname file))))

(defcommand open-pathname-contents
    ((pathname pathname))
  "Open the pathname contents using Ace editor component."
  (make-instance 'ace-editor
		 :name (princ-to-string pathname)
		 :contents (alexandria:read-file-into-string pathname)))

(pushnew :ace-editor *mold-modules*)
