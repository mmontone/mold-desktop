(asdf:defsystem #:mold-desktop-bookmarks
  :description "Bookmarks application for Mold Desktop"
  :version "0.1"
  :author "Mariano Montone <marianomontone@gmail.com>"
  :license "AGPL"
  :serial t
  :components
  ((:module "apps/bookmarks"
    :components ((:file "bookmarks"))))
  :depends-on (:mold-desktop))
