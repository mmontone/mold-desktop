(in-package :mold-desktop)

(defclass filesystem-directory (collection)
  ((directory :initarg :directory
              :accessor directory-name))
  (:default-initargs
   :icon "fa fa-folder")
  (:metaclass mold-object-class))

(defclass filesystem-file (object)
  ((pathname :initarg :pathname
             :accessor file-pathname)
   (mime-type :initarg :mime-type
              :accessor mime-type)
   (file-type :initarg :file-type
              :accessor file-type
              :type symbol
              :documentation "Used for dispatching based on type of file."))
  (:default-initargs
   :icon "fa fa-file")
  (:metaclass mold-object-class))

(defmethod members ((collection filesystem-directory))
  (with-slots (directory) collection
    (mapcar (lambda (pathname)
              (if (pathname-utils:directory-p pathname)
                  (make-instance 'filesystem-directory
                                 :name (princ-to-string pathname)
                                 :directory pathname)
                  (make-instance 'filesystem-file
                                 :name (pathname-utils:file-name pathname)
                                 :mime-type (trivial-mimes:mime pathname)
                                 :pathname pathname)))
            (append
             (uiop/filesystem:subdirectories directory)
             (uiop:directory-files directory)))))

(defmethod create-object-view ((file filesystem-file) clog-obj &rest args)
  ;; Create view for file based on its mime-type
  (let ((pathname-object (create-object-from-pathname (mime-type file)
                                                      (file-pathname file))))
    (if pathname-object
        (apply #'create-object-view pathname-object clog-obj args)
        (create-file-view (mime-type file) file clog-obj args))))

(defmethod create-item-view ((object filesystem-directory) li &rest args)
  (when (object-icon object)
    (create-child li (html (:i :class (format nil "fa fa-solid fa-~a"
                                              (object-icon object))
                               :style "margin-right: 3px;"))))
  (with-slots (directory) object
    (let* ((dirname (car (last (pathname-directory directory))))
           (link
             (create-a li :content (concatenate 'string dirname "/"))))
      (set-on-click link (lambda (&rest _)
                           (declare (ignore _))
                           (if (getf args :on-select)
                               (funcall (getf args :on-select) object)
                               (show object)))))))

(defun %file-size (file)
  (with-open-file (stream file :element-type '(unsigned-byte 8))
    (file-length stream)))

(defun file-size (obj)
  (typecase obj
    (pathname (%file-size obj))
    (filesystem-file (%file-size (file-pathname obj)))
    (filesystem-directory (%file-size (directory-name obj)))
    (t (error "Not a file object: ~s" obj))))

(defcommand (open-directory :show-result t)
    ((pathname string "The directory pathname"))
  (make-instance 'filesystem-directory
                 :name (princ-to-string pathname)
                 :directory pathname))

#+nil(mold-desktop::run-command 'open-directory #p"~/")

(defcommand (open-directory-pathname :show-result t)
    ((pathname pathname "The directory pathname"))
  (make-instance 'filesystem-directory
                 :name (princ-to-string pathname)
                 :directory pathname))
