(defpackage :mold-desktop/fulltext-search
  (:use :cl :mold-desktop))

(in-package :mold-desktop/fulltext-search)

(defparameter *index* (make-instance 'montezuma:index))

;; Persistent index
#+nil(defparameter *index* (make-instance 'montezuma:index
					  :path "/path/to/index"))

(defun index-objects ()
  (dolist (object mold-desktop::*objects*)
    (montezuma:add-document-to-index *index*
				     (list
				      (cons "id" (princ-to-string (mold-desktop::object-id object)))
				      (cons "title" (mold-desktop::object-name object))
                                      (cons "content" (mold-desktop::object-description object))))))

(defun search-object (search-term)
  (montezuma:search-each *index* (format nil "content:\"~a\"" search-term)
                       #'(lambda (doc score)
                           (format T "~&Document ~S found with score of ~S." doc score))))

#+test(search-object "Programming")
