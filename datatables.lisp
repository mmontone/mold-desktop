(in-package :mold-desktop)

(pushnew 'init-datatables *init-functions*)

(defun init-datatables (body)
  (load-css (html-document body) "https://cdn.datatables.net/v/dt/dt-1.13.2/datatables.min.css")
  (add-script (html-document body) "https://cdn.datatables.net/v/dt/dt-1.13.2/datatables.min.js"))

(defview create-datatable-view (object clog-obj &rest args)
  (:documentation "Create a datatable view for OBJECT.")
  (:label "Datatable view")
  (:icon "fa fa-table"))

(defmethod create-datatable-view (object clog-obj &rest args)
  "OBJECT can be any object respecting the table interface."
  (declare (ignore args))
  (let ((html-table
          (create-table clog-obj)))
    ;; Head
    (let* ((thead (create-table-head html-table))
	   (row (create-table-row thead)))
      (dolist (column (table-columns object))
	(create-table-heading row
			      :content
			      (str:sentence-case (princ-to-string column)))))
    ;; Body
    (let ((tbody (create-table-body html-table)))
      (dolist (row (table-rows object))
	(let ((tr (create-table-row tbody)))
          (dolist (column (table-columns object))
            (let ((colval (table-cell-value row column)))
              (create-table-column tr :content (princ-to-string colval))))
          (when (typep row 'object)
            (set-on-click tr (lambda (obj)
                               (open-object-in-window row obj)))))))
    ;; Invoke datatables
    (clog:js-execute html-table (format nil "$('#~a').DataTable()" (html-id html-table)))
    html-table))

(defmethod object-view-types append ((coll collection) (module (eql :datatables)))
  '(create-datatable-view))

(pushnew :datatables *mold-modules*)
