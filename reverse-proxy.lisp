(require :yxorp)

(defpackage :mold-desktop/proxy
  (:use :cl))

(in-package :mold-desktop/proxy)

(defmacro endpoint-path-redirect (&body endpoints)
  `(cond
     ,@(mapcar (lambda (endpoint)
                 `(,(if (stringp (first endpoint))
			`(str:starts-with? ,(first endpoint) (yxorp:header :uri))
			(first endpoint))
		   ,(when (stringp (first endpoint))
		      `(setf (yxorp:header :uri) (subseq (yxorp:header :uri) (length ,(first endpoint)))))
		   ,(second endpoint)))
               endpoints)))

(defmacro subdomain-redirect (&body endpoints)
  `(cond
     ,@(mapcar (lambda (endpoint)
                 `(,(if (stringp (first endpoint))
			`(str:starts-with? ,(concatenate 'string (first endpoint) ".") (yxorp:header :host))
			(first endpoint))
		   ,(second endpoint)))
               endpoints)))

(defun proxy-request ()
  (endpoint-path-redirect
    ("/api" 8081)
    (t 8080)))

(defun start-proxy (&optional (port 8082))
  (yxorp:start 
   (yxorp:config :port port
		 :destinator 'proxy-request))
  (format t "Proxy started on port: ~a" port))

(export 'start-proxy)

(defun stop-proxy ()
  (yxorp:stop))

(export 'stop-proxy)
