;; background jobs support

(in-package :mold-desktop)

(defvar *running-jobs* (make-instance 'collection
                                      :name "Running jobs"))

(defclass job (event-emitter:event-emitter)
  ((id :initarg :id
       :accessor job-id)
   (name :initarg :name
         :accessor object-name
         :type string)
   (status :accessor job-status
           :initform :created
           :type keyword)
   (progress :accessor job-progress
             :initarg :progress
             :initform nil
             :type (or null integer)
             :documentation "The job progress. Either NIL or a number from 0 to 100. If NIL, the the job progress is considered indeterminate.")
   (progress-max :initarg :progress-max
                 :accessor progress-max
                 :initform 100
                 :type integer
                 :documentation "Maximum of progress.")
   (display-progress-details :initarg :display-progress-details
                             :accessor display-progress-details-p
                             :initform nil
                             :type boolean
                             :documentation "Wether to display progress details.")
   (display-progress-time :initarg :display-progress-time
                          :accessor display-progress-time-p
                          :initform nil
                          :type boolean
                          :documentation "Wether to display progress time details.")
   (start-time :accessor job-start-time)
   (output :accessor job-output
           :initform (make-string-output-stream)
           :type stream)
   (description :initarg :description
                :accessor object-description
                :type string
                :initform "No description")
   (icon :initarg :icon
         :accessor object-icon
         :initform "fa fa-cog"))
  (:documentation "A job"))

(defun create-job (job-class &rest args)
  (let ((job (apply #'make-instance (or job-class 'job) args)))
    (start-job job)
    job))

(defgeneric start-job (job &rest args))
(defgeneric stop-job (job))
(defgeneric run-job (job &rest args))

(defmethod stop-job ((job job))
  (setf (job-status job) :finished)
  (trigger-notification (format nil "Finished: ~a" (object-name job))))

(defmethod start-job :before ((job job) &rest args)
  (collection-add *running-jobs* job)
  (start-spinner *loading-indicator*)
  (apply #'run-job job args))

(defmethod start-job ((job job) &rest args)
  (setf (job-status job) :started)
  (setf (job-start-time job) (get-internal-real-time)))

(defmethod (setf job-status) :after (value (job job))
  (when (eq value :finished)
    (collection-remove *running-jobs* job)
    (when (gcl:emptyp *running-jobs*)
      (stop-spinner *loading-indicator*)))
  (event-emitter:emit :changed job job))

(defmethod (setf job-progress) :after (value (job job))
  (event-emitter:emit :changed job job))

(defmethod object-title ((job job))
  (object-name job))

(defun show-running-jobs ()
  (show *running-jobs*))

(defclass pluggable-job (job)
  ((function :initarg :function
             :accessor job-function
             :initform (error "Provide a function for the job."))
   (in-background :initarg :in-background
                  :initform t
                  :accessor in-background-p)))

(defmethod run-job ((job pluggable-job) &rest args)
  (flet ((run ()
           (let ((*standard-output* (setf (job-output job) (make-string-output-stream))))
             (apply (job-function job) job args)
             (stop-job job))))
    (if* (in-background-p job)
       then (bt:make-thread #'run)
       else (run))))

(defmethod create-object-view ((job job) clog-obj &rest args
                               &key display-progress-details display-progress-time)
  (declare (ignore args))
  ;; Icon
  (create-icon (or (object-icon job)
                   "fa-solid fa-cog")
               clog-obj)
  ;; Title
  (arrows:->
   (create-a clog-obj :content (object-title job)
                      :style "margin-left: 10px;")
   (set-on-click (ignoring-args (show job))
                 :cancel-event t))
  ;; Progress bar
  (let ((progress-value (value-models:make-aspect-adaptor job 'job-progress)))
    (arrows:-> (create-div clog-obj)
               (mold-desktop/clog-gui::create-updatable-progress-bar progress-value :maximum (progress-max job))))
  ;; Progress time details
  (let (progress-detail
        progress-time)
    (when (or display-progress-details
              (display-progress-details-p job))
      (setf progress-detail (clog:create-span clog-obj)))
    (when (or display-progress-time
              (display-progress-time-p job))
      (setf progress-time (clog:create-span clog-obj)))
    ;; Description
    (create-p clog-obj :content (object-description job))
    ;; Output
    (let ((output-area (create-text-area clog-obj :value (get-output-stream-string (job-output job)))))
      (event-emitter:on :changed job
                        (lambda (&rest args)
                          (declare (ignore args))
                          ;; Update output
                          (setf (clog:value output-area)
                                (concatenate 'string
                                             (clog:value output-area)
                                             (get-output-stream-string (job-output job))))
                          ;; Update progress details
                          (when (or display-progress-details
                                    (display-progress-details-p job))
                            (with-slots (progress progress-max) job
                              (setf (clog:text progress-detail)
                                    (format nil "~a of ~a (~a%)"
                                            progress
                                            progress-max
                                            (truncate
                                             (/ (* progress 100)
                                                progress-max))))))
			  ;; Update progress time
                          (when (or display-progress-time
                                    (display-progress-time-p job))
                            (let ((current-time (- (get-internal-real-time)
						   (job-start-time job))))
                              (setf (clog:text progress-time)
                                    (with-output-to-string (s)
                                      (write-string "Elapsed: " s)
                                      (format-time-in-seconds-minutes-hours s (/ current-time 1000000))
                                      (let ((estimated-seconds (/ (- (/ (* (progress-max job) current-time)
                                                                        (job-progress job))
                                                                     current-time)
                                                                  1000000)))
                                        (write-string ". Remaining: " s)
                                        (format-time-in-seconds-minutes-hours s estimated-seconds))))))
                          )))))

(defmethod create-item-view ((job job) li &rest args &key context &allow-other-keys)
  (declare (ignore args))
  (create-child li (html (:i :class (format nil "fa fa-solid fa-~a"
                                            (or (object-icon job) "cog"))
                             :style "margin-right: 3px;")))
  (let ((link
          (create-a li :content (object-title job))))
    (set-on-event link "click"
                  (ignoring-args (show job :context context))
                  :cancel-event t))
  (let ((progress-value (value-models:make-aspect-adaptor job 'job-progress)))
    (arrows:-> (create-div li)
               (mold-desktop/clog-gui::create-updatable-progress-bar
		progress-value
		:maximum (progress-max job)))))

(defun call-with-job (args function)
  (let ((job (apply #'make-instance 'pluggable-job
                    :function function args)))
    (start-job job)
    job))

(defmacro with-job ((var &rest args) &body body)
  `(call-with-job (list ,@args)
                  (lambda (,var)
                    ,@body)))

#+test
(show (create-job 'pluggable-job
                  :name "My job"
                  :description "A cool job"
                  :progress 0
                  :progress-max 29
                  :function (lambda (job)
                              (print "Sleeping 30 seconds")
                              (loop for i from 0 to 29 do
                                (print "Working ...")
                                (incf (job-progress job))
                                (sleep 1))
                              (print "Done"))))

#+test
(show (create-job 'pluggable-job
                  :name "Indeterminate job"
                  :description "An indeterminate job"
                  :function (lambda (job)
                              (sleep 20))))

#+test
(with-job (job :name "Some job" :progress 0
               :progress-max 30)
  (print "Sleeping 30 seconds")
  (loop for i from 0 to 29 do
    (incf (job-progress job))
    (sleep 1))
  (print "Done"))

#+test
(show (create-job 'pluggable-job
                  :name "Progress details"
                  :description "A cool job"
                  :progress 0
                  :progress-max 30
                  :display-progress-details t
                  :function (lambda (job)
                              (print "Sleeping 30 seconds")
                              (loop for i from 0 to 29 do
                                (print "Working ...")
                                (incf (job-progress job))
                                (sleep 1))
                              (print "Done"))))

#+test
(show (create-job 'pluggable-job
                  :name "Progress time details"
                  :description "A cool job"
                  :progress 0
                  :progress-max 30
                  :display-progress-details t
                  :display-progress-time t
                  :function (lambda (job)
                              (print "Sleeping 30 seconds")
                              (loop for i from 0 to 29 do
                                (print "Working ...")
                                (incf (job-progress job))
                                (sleep 1))
                              (print "Done"))))
