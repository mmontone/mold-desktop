(in-package :mold-desktop)

;; Tests

;; Reset user history
(setf *user-history* (make-hash-table))
(setf *user-classifier-init* `(levenshtein-collection-classifier :history ,*user-history*))

(defparameter *main* (make-instance 'user-collection :name "Test collection"))
(defparameter *books* (make-instance 'user-collection :name "Books"))
(defparameter *images* (make-instance 'user-collection :name "Images"))
(defparameter *videos* (make-instance 'user-collection :name "Videos"))
(defparameter *goodreads* (make-instance 'user-collection :name "Goodreads"))
(defparameter *amazon* (make-instance 'user-collection :name "Amazon"))

(collection-add *main* *books*)
(collection-add *books* *goodreads*)
(collection-add *books* *amazon*)
(collection-add *main* *images*)
(collection-add *main* *videos*)

(collection-add *images* (make-instance 'image :url "https://i.sstatic.net/mPYaC.png"))
(collection-add *images* (make-instance 'image :url "https://i.sstatic.net/VB5JX.png"))
(collection-add *videos* (make-instance 'video :url "https://archive.org/download/big-bunny-sample-video/SampleVideo.mp4"))
(collection-add *goodreads* (make-instance 'ebook :url "https://www.goodreads.com/book/show/8051458-the-reapers-are-the-angels"))
(collection-add *amazon* (make-instance 'ebook :url "https://www.amazon.com/Let-Them-Theory-Life-Changing-Millions/dp/1401971369/"))

;; (classify-object (collection-classifier *main*) (make-instance 'image :url "https://lala.png") *main*)
;; (classify-object (collection-classifier *main*) (make-instance 'video :url "https://nonono.png") *main*)
;; Test same type, different url
;; (classify-object (collection-classifier *main*) (make-instance 'ebook :url "https://www.goodreads.com/book/show/7157310-rot-ruin") *main*)
;; (classify-object (collection-classifier *main*) (make-instance 'ebook :url "https://www.amazon.com/INNER-EXCELLENCE-Extraordinary-Performance-Possible/dp/1734654805") *main*)

;; Test the CLASSIFY-AND-ADD command

;; Drag and drop from collection of objects to main collection, and select CLASSIFY-AND-ADD command
;; (show *main* :view 'create-breadcrumb-view)
#+test
(show (make-instance 'collection :name "Drag items to Test collection and classify" :members (mapcar (lambda (x) (apply #'make-instance x))
                                                                                                     '((video :url "https://archive.org/download/big-bunny-sample-video/SampleVideo.ia.mp4")
                                                                                                       (image :url "https://i.sstatic.net/mPYaC.png")
                                                                                                       (ebook :url "https://www.goodreads.com/book/show/6953508-some-we-love-some-we-hate-some-we-eat")))))

;; Test bfs

(defparameter *main* (make-instance 'classifier-collection :name "Test collection"
                                                           :classifier (make-instance 'bfs-levenshtein-collection-classifier)))
(defparameter *books* (make-instance 'collection :name "Books"))
(defparameter *images* (make-instance 'collection :name "Images"))
(defparameter *videos* (make-instance 'collection :name "Videos"))
(defparameter *goodreads* (make-instance 'collection :name "Goodreads"))
(defparameter *amazon* (make-instance 'collection :name "Amazon"))

(collection-add *main* *books*)
(collection-add *books* *goodreads*)
(collection-add *books* *amazon*)
(collection-add *main* *images*)
(collection-add *main* *videos*)

(collection-add *images* (make-instance 'image :url "https://i.sstatic.net/mPYaC.png"))
(collection-add *images* (make-instance 'image :url "https://i.sstatic.net/VB5JX.png"))
(collection-add *videos* (make-instance 'video :url "https://archive.org/download/big-bunny-sample-video/SampleVideo.mp4"))
(collection-add *goodreads* (make-instance 'ebook :url "https://www.goodreads.com/book/show/8051458-the-reapers-are-the-angels"))
(collection-add *amazon* (make-instance 'ebook :url "https://www.amazon.com/Let-Them-Theory-Life-Changing-Millions/dp/1401971369/"))

;; (classify-object (collection-classifier *main*) (make-instance 'image :url "https://lala.png") *main*)
;; (classify-object (collection-classifier *main*) (make-instance 'video :url "https://nonono.png") *main*)
;; Test same type, different url
;; (classify-object (collection-classifier *main*) (make-instance 'ebook :url "https://www.goodreads.com/book/show/7157310-rot-ruin") *main*)
;; (classify-object (collection-classifier *main*) (make-instance 'ebook :url "https://www.amazon.com/INNER-EXCELLENCE-Extraordinary-Performance-Possible/dp/1734654805") *main*)

;; Test the CLASSIFY-AND-ADD command

;; Drag and drop from collection of objects to main collection, and select CLASSIFY-AND-ADD command
;; (show *main* :view 'create-breadcrumb-view)
#+test
(show (make-instance 'collection :name "Drag items to Test collection and classify" :members (mapcar (lambda (x) (apply #'make-instance x))
                                                                                                     '((video :url "https://archive.org/download/big-bunny-sample-video/SampleVideo.ia.mp4")
                                                                                                       (image :url "https://i.sstatic.net/mPYaC.png")
                                                                                                       (ebook :url "https://www.goodreads.com/book/show/6953508-some-we-love-some-we-hate-some-we-eat")))))
