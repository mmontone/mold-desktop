(in-package :mold-desktop)

(defcommand http-request
    ((uri string)
     &optional (accept string "*/*"))
  (multiple-value-bind (content status headers)
      (let ((drakma:*text-content-types*
	      (cons (cons "application" "json")
		    drakma:*text-content-types*)))
	(drakma:http-request uri :accept accept))
    (let* ((content-type (cdr (assoc :content-type headers)))
	   (parsed-content
	     (cond
	       ((search "json" content-type :test #'string=)
		(yason:parse content))
	       (t content))))
      (sera:dict "status" status
		 "content" parsed-content
		 "headers" (alexandria:alist-hash-table headers)))))

#+test
(show (http-request "https://petstore.swagger.io/v2/pet/findByStatus?status=available"))

#+test
(show (http-request "https://petstore.swagger.io/v2/pet/findByStatus?status=available")
      :view #'create-tree-detail-view)

