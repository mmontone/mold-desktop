;;;; https://open-meteo.com weather api

(defpackage mold-desktop/open-meteo
  (:use :cl :mold-desktop))

(in-package :mold-desktop/open-meteo)

(defcommand open-meteo-forecast
    ((lat integer "Latitude")
     (long integer "Longitude"))
  "Open meteo api access"
  (let ((uri "https://api.open-meteo.com/v1/forecast?")
	(query-params (list (cons "latitude" (princ-to-string lat))
			    (cons "longitude" (princ-to-string long))
			    (cons "timezone" "GMT")
			    (cons "daily" "temperature_2m_max,temperature_2m_min"))))
    (setf uri (str:concat uri (str:join #\&
					(mapcar (lambda (query-param)
						  (format nil "~a=~a" (car query-param)
							  (cdr query-param)))
						query-params))))
    (let ((data (json:decode-json-from-source
		 (drakma:http-request
		  (princ-to-string uri) :want-stream t))))
      data)))

#+test
(open-meteo-forecast 13.0 13.0)


