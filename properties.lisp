;; Copyright (C) 2022 Mariano Montone - All Rights Reserved
;; You may use, distribute and modify this code under the terms of the
;; GNU Affero General Public License

(in-package :mold-desktop)

(defclass property ()
  ((name :initarg :name :accessor property-name)
   (type :initarg :type :accessor property-type)
   (label :initarg :label :accessor property-label)
   (documentation :initarg :documentation :accessor property-documentation)
   (default-value :initarg :default-value :accessor default-value)))

(defclass customization-group ()
  ((name :initarg :name :accessor group-name)
   (properties :initarg :properties :accessor properties)
   (documentation :initarg :documentation :accessor group-documentation)))

(defun propagate-changes (clog-obj model &key
					   (property 'mold-desktop::element-value)
					   (ui-to-model #'identity)
					   (model-to-ui #'object-title))
  "Propagate changes between CLOG-OBJ and MODEL, bidirectionally."
  ;; ui -> model
  (set-on-change clog-obj
		 (lambda (&rest args)
		   (declare (ignore args))
		   (let ((*register-status-changes* t))
		     (setf (value-models:value model)
			   (funcall ui-to-model (funcall property clog-obj))))))
  ;; model -> ui
  (event-emitter:on :changed model
		    (lambda (val)
		      (funcall (fdefinition `(setf ,property))
			       (funcall model-to-ui (value-models:value model))
			       clog-obj))))

(defgeneric create-property-editor (property-type property value clog-obj &rest args)
  (:documentation "Create an editor component for PROPERTY."))

(defmethod create-property-editor ((property-type symbol) property model clog-obj &rest args)
  (declare (ignore args))
  ;; An object input
  (create-widget clog-obj 'object-input-widget
		 :model model
		 :object-type property-type))

(defmethod create-property-editor ((property-type (eql 'string)) property model clog-obj &rest args)
  (declare (ignore args))
  (create-widget clog-obj 'string-input-widget :model model))

(defmethod create-property-editor ((property-type (eql 'text)) property model clog-obj &rest args)
  (declare (ignore args))
  (let ((textarea (create-child clog-obj
				(html (:textarea
				       :class "w3-input w3-border"
				       :name (property-name property)
				       (who:str (value-models:value model)))))))
    (propagate-changes textarea model)
    textarea))

(defmethod create-property-editor ((property-type (eql 'boolean)) property model clog-obj &rest args)
  (declare (ignore args))
  (let ((checkbox
	  (create-child clog-obj (html (:input :type "checkbox"
					       :class "w3-check"
					       :name (property-name property)
					       :checked (value-models:value model))))))
    (propagate-changes checkbox model
		       :ui-to-model (lambda (val) (equalp val "on"))
		       :model-to-ui (lambda (val) (if val "on" "off")))
    checkbox))

(defmethod create-property-editor ((property-type list) property model clog-obj &rest args)
  "A list is assumed to be a choice type."
  (declare (ignore args))
  (let ((select (create-select clog-obj :name (property-name property)
                                        :class "w3-select"))
        (value (value-models:value model)))
    (dolist (option property-type)
      (create-option select :value option
                            :selected (equalp option value)
                            :content option))
    (propagate-changes select model)
    select))

(defun create-properties-editor-form (properties clog-obj &key on-submit)
  (declare (ignore on-submit))
  (let* ((form (create-form clog-obj))
         (table (create-table form :class "w3-table w3-small w3-striped w3-bordered")))
    (dolist (property-and-value properties)
      (let ((tr (create-table-row table)))
        (destructuring-bind (property . value) property-and-value
          (arrows:as-> (create-table-column tr) $
             (create-child $ (html (:label (who:str (or (property-label property)
							(humanized-symbol-name (property-name property)))))))
	     (setf (advisory-title $) (property-documentation property)))
          (let ((td (create-table-column tr)))
            (create-property-editor (property-type property) property value td)))))))

(defun make-property (name type &rest args)
  (apply #'make-instance 'property :name name :type type args))

;; ---------- Adaptors for object metamodel ---------------------------

(defmethod property-name ((slot mold-object-slot-definition))
  (closer-mop:slot-definition-name slot))

(defmethod property-label ((slot mold-object-slot-definition))
  (humanized-symbol-name (closer-mop:slot-definition-name slot)))

(defmethod property-type ((slot mold-object-slot-definition))
  "Use type specified in :attribute, or :type otherwise."
  (or (and (not (eq (slot-definition-attribute slot) t))
           (slot-definition-attribute slot))
      (closer-mop:slot-definition-type slot)))

(defmethod property-documentation ((slot mold-object-slot-definition))
  (documentation slot t))

#+example
(let ((properties (list
                   (cons (make-property "my string" 'string)
                         (value-models:as-value "this is a string"))
                   (cons (make-property "my-boolean" 'boolean)
                         (value-models:as-value t))
                   (cons (make-property "my-choice" '("foo" "bar" "baz"))
                         (value-models:as-value "foo")))))
  (let ((wnd (create-gui-window *body* :title "Properties editor")))
    (create-properties-editor-form properties (window-content wnd))))

#+example
(progn
  (setf *properties* (list
                   (cons (make-property "my string" 'string)
                         (value-models:as-value "this is a string"))
                   (cons (make-property "my-boolean" 'boolean)
                         (value-models:as-value t))
                   (cons (make-property "my-choice" '("foo" "bar" "baz"))
                         (value-models:as-value "foo"))))
  (let ((wnd (create-gui-window *body* :title "Properties editor")))
    (create-properties-editor-form *properties* (window-content wnd))))
