(in-package :mold-desktop)

;; Utilities

;; html

(defvar *html*)
(defmacro html* (&body body)
  `(who:with-html-output (*html*)
     ,@body))
(defmacro html (&body body)
  `(who:with-html-output-to-string (*html*)
     ,@body))

;; arrows

(defmacro -> (first &rest rest)
  (let ((binding (gensym)))
    `(let ((,binding ,first))
       ,@(loop for r in rest
               collect (list* (first r)
                              binding
                              (rest r))))))

(defmethod element-value ((obj clog-obj))
  (clog-connection:query (clog::connection-id obj)
                         (format nil "$('#~a').val()" (html-id obj))))

(defmethod (setf element-value) (value (obj clog-obj))
  (clog-connection:execute (clog::connection-id obj)
                           (format nil "$('#~a').val(~a)" (html-id obj) (prin1-to-string value))))

(defun clear-element (clog-obj)
  (setf (clog:text clog-obj) ""))

(ps:defpsmacro clog-elem (elem)
  `($ (ps:lisp (format nil "#~a" (html-id ,elem)))))

(defun element-extent (el)
  "Return extent of ELement in a list. (LEFT TOP WIDTH HEIGHT)"
  (arrows:->>
   (js-query el
             (ps:ps (let (($el (clog-elem el)))
                      (list (ps:chain $el (position) left)
                            (ps:chain $el (position) top)
                            (ps:chain $el (width))
                            (ps:chain $el (height))))))
   (str:split #\,)
   (mapcar #'parse-integer)))

(defun element-dimensions (el)
  "Return extent of ELelement in a list. (LEFT TOP RIGHT BOTTOM)"
  (arrows:->>
   (js-query el
             (ps:ps (let* (($el (clog-elem el))
                           (pos (ps:chain $el (position))))
                      (list (ps:chain pos left)
                            (ps:chain pos top)
                            (+ (ps:chain pos left) (ps:chain $el (width)))
                            (+ (ps:chain pos top) (ps:chain $el (height)))))))
   (str:split #\,)
   (mapcar #'parse-integer)))

(defmacro with-open-window (var args &body body)
  `(let ((,var (create-window *window-manager* ,@args)))
     ,@body))

(defmacro create-child* (clog-obj html)
  `(create-child ,clog-obj (html ,html)))

(defun newline-to-br (string)
  (cl-ppcre:regex-replace-all
   (ppcre:create-scanner '(:sequence (:greedy-repetition 0 nil (:alternation #\space #\tab #\return))
                           #\newline))
   string
   "<br/>"))

(defmacro lambda* (args &body body)
  "Lambda expression with ignorable arguments."
  (let* ((ignore '())
         (lambda-args (loop for arg in args
                            collect (if (string= (symbol-name arg) "_")
                                        (let ((ignore-arg (gensym)))
                                          (push ignore-arg ignore)
                                          ignore-arg)
                                        arg))))
    `(lambda ,lambda-args
       ,@(when ignore
           `((declare (ignore ,@ignore))))
       ,@body)))

(defmacro push-end (obj place)
  "Push OBJ to the end of PLACE."
  `(if (null ,place)
       (setf ,place (list ,obj))
       (setf (cdr (last ,place))
             (cons ,obj nil))))

(defun all-subclasses (class)
  (let ((direct-subclasses (closer-mop:class-direct-subclasses class)))
    (append direct-subclasses
            (loop for subclass in direct-subclasses
                  appending (all-subclasses subclass)))))

(defun add-script (clog-document script-url &key on-load)
  "Add script with on-load support."
  (let ((script
          (create-child (clog::head-element clog-document)
                        (format nil "<script>")
                        :auto-place  nil)))
    (when on-load
      (clog::set-event script "load" on-load))
    (clog::place-inside-bottom-of (clog::head-element clog-document) script)
    (setf (clog:attribute script "src") (escape-string script-url))
    script))

(defun icon-html (icon-spec &optional error-p)
  (cond
    ;; Fontawesome
    ((str:starts-with-p "fa" icon-spec)
     (who:with-html-output-to-string (html)
       (:i :class (format nil "mold-icon ~a" icon-spec))))
    ;; Remix icons
    ((str:starts-with-p "ri" icon-spec)
     (who:with-html-output-to-string (html)
       (:i :class icon-spec)))
    ;; glyphs.fyi
    ((str:starts-with-p "core" icon-spec)
     (format nil "<~a variant=\"poly\"></~a>"
             icon-spec icon-spec))
    (t (or (and error-p (error "Invalid icon spec: ~s" icon-spec))
           ""))))

;; FIXME: clog-obj argument should be before icon-spec argument
;; to follow CLOG create functions and be able to use with-clog-create.
(defun create-icon (icon-spec clog-obj &rest args)
  "Create an icon element from an ICON-SPEC string."
  (check-type icon-spec string)
  (cond
    ;; Fontawesome
    ((str:starts-with-p "fa" icon-spec)
     (apply #'create-child clog-obj (html (:i :class (format nil "mold-icon ~a" icon-spec))) args))
    ;; Remix icons
    ((str:starts-with-p "ri" icon-spec)
     (apply #'create-child clog-obj (html (:i :class icon-spec)) args))
    ;; glyphs.fyi
    ((str:starts-with-p "core" icon-spec)
     (apply #'create-child clog-obj (format nil "<~a variant=\"poly\"></~a>"
                                            icon-spec icon-spec)
            args))
    (t (apply #'create-child clog-obj (html (:i :class icon-spec)) args))))

(defun create-icon-button (clog-obj icon &optional action &rest args)
  (let ((btn (apply #'create-button clog-obj :class (or (getf args :class) "w3-button")
                    (alex:remove-from-plist args :class))))
    (create-icon icon btn)
    (when action
      (set-on-click btn action))
    btn))

(defun create-buttons-bar (clog-obj buttons-spec)
  (let ((bar (clog:create-div clog-obj :class "w3-bar")))
    (dolist (button-spec buttons-spec)
      (let ((btn (create-icon-button bar (getf button-spec :icon)
                                     (let ((command (getf button-spec :command)))
                                       (lambda (&rest args)
                                         (declare (ignore args))
                                         (run-command command))))))
        ;; The following is to avoid the button getting the focus after click
        (clog:set-on-mouse-down btn (lambda (&rest args) (declare (ignore args))) :cancel-event t)
        (anaphora:awhen (getf button-spec :description)
          (setf (clog:advisory-title btn) anaphora:it))))
    bar))

(defun toggle-hiddenp (clog-obj)
  (setf (hiddenp clog-obj)
        (not (hiddenp clog-obj))))

(defun humanized-symbol-name (symbol-name)
  (str:capitalize (serapeum:string-join (serapeum:split-sequence #\- (string symbol-name)) #\space)))

(defun format-universal-time (universal-time)
  (local-time:format-timestring nil (local-time:universal-to-timestamp universal-time) :format local-time:+rfc-1123-format+))

(defmacro ignoring-args (&body body)
  `(lambda* (&rest _)
     ,@body))

(defun condition-message (condition)
  "Get the message of CONDITION."
  (with-output-to-string (s)
    (write condition :stream s :escape nil)))

(defmacro with-ui-update (clog-obj model event &body body)
  "Render CLOG-OBJ evaluating BODY, and refresh CLOG-OBJ when MODEL triggers EVENT."
  (let ((args (gensym "ARGS-")))
    `(prog1
         (progn ,@body)
       (when (typep ,model 'event-emitter:event-emitter)
         (event-emitter:on ',event ,model
                           (lambda (&rest ,args)
                             (declare (ignore ,args))
                             (setf (text ,clog-obj) "") ;; delete UI elem contents
                             ,@body))))))

(defun shallow-copy-object (original)
  (let* ((class (class-of original))
         (copy (allocate-instance class)))
    (dolist (slot (mapcar #'closer-mop:slot-definition-name (closer-mop:class-slots class)))
      (when (slot-boundp original slot)
        (setf (slot-value copy slot)
              (slot-value original slot))))
    copy))

(defun class-ascendants (class)
  "Return CLASS ascendants chain."
  (let ((direct-superclasses (closer-mop:class-direct-superclasses class)))
    (cons class
          (loop for superclass in direct-superclasses
                appending (class-ascendants superclass)))))

(defun alist-elem-p (elem)
  (and (consp elem) (atom (car elem)) (atom (cdr elem))))

(defun alistp (alist)
  (when (listp alist)
    (dolist (elem alist)
      (unless (alist-elem-p elem)
        (return-from alistp nil)))
    t))

(defmethod json:encode-json ((object (eql :false)) &optional stream)
  (write-string "false" stream))

;; CLOG HACK!!
;; Modified from clog:set-on-event

(defmethod make-clog-callback
    ((obj clog::clog-obj) event handler
     &key (call-back-script "")
       (eval-script "")
       (post-eval "")
       (cancel-event nil)
       (one-time nil))
  (let ((hook (format nil "~A:~A" (clog::html-id obj) event))
        (cd   (clog::connection-data obj)))
    (if cd
        (cond (handler
               (let ((callback (format nil "~Aws.send(\"E:~A \"~A)~A~@[~A~]~@[~A~]"
                                       eval-script
                                       hook
                                       call-back-script
                                       post-eval
                                       (when one-time
                                         (format nil "; ~A.off(\"~A\")"
                                                 (clog::jquery obj)
                                                 event))
                                       (when cancel-event "; return false"))))
                 (clog::bind-event-script obj event callback)
                 (setf (gethash hook cd) handler)
                 ;; we return the callback js script
                 callback))
              (t
               (clog::unbind-event-script obj event)
               (remhash hook cd)))
        (format t "Attempt to set event on non-existant connection.~%"))))

;; interpolation macro
(defmacro interpol (string)
  (with-input-from-string (s (prin1-to-string string))
    (cl-interpol:interpol-reader s nil nil :recursive-p nil)))

(defun format-timestring (timestamp &rest args)
  (let ((local-time
          (etypecase timestamp
            (integer (local-time:universal-to-timestamp timestamp))
            (lt:timestamp timestamp))))
    (apply #'local-time:format-timestring nil local-time args)))

(defun create-link (clog-obj url &optional (target "_self") &rest args)
  (arrows:->
      (apply #'create-a clog-obj
             :link url
             :target target
             :style "color: blue;text-decoration:underline;"
             args)
      (clog:set-on-click (ignoring-args (js-execute *body*
                                                    (format nil "window.open('~a', '~a')" url target))))))

(defun make-accessing-value (object key)
  "Create a VALUE-MODEL:VALUE-MODEL for accessing OBJECT at KEY."
  (make-instance 'value-models:aspect-adaptor
                 :subject object
                 :getter (lambda (subject) (access subject key))
                 :setter (lambda (val subject) (setf (access subject key) val))))

#+test(let* ((obj '((x . 22) (y . 33)))
             (val (make-accessing-value obj 'x)))
        (print (value-models:value val))
        (setf (value-models:value val) 44)
        obj)

(defun read-csv-file (pathname &optional format)
  (check-type format (or null (member :alist)))
  (let ((lines (fare-csv:read-csv-file pathname)))
    (when (null format)
      (return-from read-csv-file lines))
    (ecase format
      (:alist
       (let ((columns (car lines))
             (rows (cdr lines)))
         (mapcar (lambda (row)
                   (map 'list #'cons columns row))
                 rows))))))

;; the if* macro used in Allegro:
;;
;; This is in the public domain... please feel free to put this definition
;; in your code or distribute it with your version of lisp.

(defvar if*-keyword-list '("then" "thenret" "else" "elseif"))

(defmacro if* (&rest args)
  (do ((xx (reverse args) (cdr xx))
       (state :init)
       (elseseen nil)
       (totalcol nil)
       (lookat nil nil)
       (col nil))
      ((null xx)
       (cond ((eq state :compl)
              `(cond ,@totalcol))
             (t (error "if*: illegal form ~s" args))))
    (cond ((and (symbolp (car xx))
                (member (symbol-name (car xx))
                        if*-keyword-list
                        :test #'string-equal))
           (setq lookat (symbol-name (car xx)))))

    (cond ((eq state :init)
           (cond (lookat (cond ((string-equal lookat "thenret")
                                (setq col nil
                                      state :then))
                               (t (error
                                   "if*: bad keyword ~a" lookat))))
                 (t (setq state :col
                          col nil)
                    (push (car xx) col))))
          ((eq state :col)
           (cond (lookat
                  (cond ((string-equal lookat "else")
                         (cond (elseseen
                                (error
                                 "if*: multiples elses")))
                         (setq elseseen t)
                         (setq state :init)
                         (push `(t ,@col) totalcol))
                        ((string-equal lookat "then")
                         (setq state :then))
                        (t (error "if*: bad keyword ~s"
                                  lookat))))
                 (t (push (car xx) col))))
          ((eq state :then)
           (cond (lookat
                  (error
                   "if*: keyword ~s at the wrong place " (car xx)))
                 (t (setq state :compl)
                    (push `(,(car xx) ,@col) totalcol))))
          ((eq state :compl)
           (cond ((not (string-equal lookat "elseif"))
                  (error "if*: missing elseif clause ")))
           (setq state :init)))))

(defconstant +seconds-in-one-hour+ 3600)
(defconstant +seconds-in-one-minute+ 60)

(defun format-time-in-seconds-minutes-hours (stream in-seconds)
  (when (>= in-seconds +seconds-in-one-hour+)
    (let* ((hours (floor (/ in-seconds +seconds-in-one-hour+))))
      (decf in-seconds (* hours +seconds-in-one-hour+))
      (format stream " ~a hour~p " hours hours)))
  (when (>= in-seconds +seconds-in-one-minute+)
    (let* ((minutes (floor (/ in-seconds +seconds-in-one-minute+))))
      (decf in-seconds (* minutes +seconds-in-one-minute+))
      (format stream "~a minute~p " minutes minutes)))
  (unless (zerop in-seconds)
    (format stream "~d seconds" (truncate in-seconds))))

(defmacro break-val (value)
  (let ((val (gensym)))
    `(let ((,val ,value))
       (break "~s" ,val)
       ,val)))

(defun toggle-debugging (&optional (debug t))
  (setf hunchentoot:*catch-errors-p* (not debug))
  (setf *handle-command-errors* (not debug))
  (setf *handle-view-errors* (not debug))
  (format t "Debugging ~a" (if debug "enabled" "disabled")))

(defmacro with-callback (callback &body body)
  "If CALLBACK is null, then work with promises."
  (alexandria:with-unique-names (reject err)
    `(if ,callback (progn ,@body)
         (blackbird:create-promise
          (lambda (,callback ,reject)
            (handler-case
                (progn ,@body)
              (error (,err)
                (funcall ,reject ,err))))))))

(defun css-classes (&rest classes)
  (str:join #\space (mapcar (alexandria:compose #'string-downcase #'princ-to-string) (remove-if #'null classes))))

#+test
(string=
 (css-classes "foo" 'bar)
 "foo bar")

#+test
(string=
 (css-classes "foo" (when nil 'bar))
 "foo")

#+test
(apply #'css-classes (list "foo" nil "bar"))

(defun function-designator-p (thing)
  (if (symbolp thing)
      (fboundp thing)
      (functionp thing)))

;; (function-designator-p 'print)
;; (function-designator-p #'print)
