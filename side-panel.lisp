(in-package :mold-desktop)

(defclass mold-desktop-side-panel (clog-div)
  ((object :initarg :object
           :accessor side-panel-object)
   (content :accessor side-panel-content)
   (opened :initform nil
           :accessor side-panel-opened)))

(defun create-side-panel (clog-obj &rest initargs)
  "Create a side panel."
  (let ((side-panel (create-div clog-obj :class "mold-side-panel w3-bar w3-lightgray w3-card-4")))
    (set-styles side-panel '(("width" "0px")))
    (apply #'change-class side-panel 'mold-desktop-side-panel initargs)

    ;; Toggle button
    (let ((toggle-button
            (create-child side-panel (html (:a :href "#" :class "side-panel-toggle"
                                               (:i :class "fa fa-cog"))))))
      (set-on-click toggle-button (lambda (obj)
                                    (declare (ignore obj))
                                    (toggle-side-panel side-panel))))

    ;; Content
    (setf (side-panel-content side-panel) (create-div side-panel))
    (set-styles (side-panel-content side-panel)
                '(("overflow" "scroll")))

    side-panel))

(defun side-panel-open (side-panel)
  (set-styles side-panel '(("width" "300px")))
  (setf (side-panel-opened side-panel) t))

(defun side-panel-close (side-panel)
  (set-styles side-panel '(("width" "0px")))
  (setf (side-panel-opened side-panel) nil))

(defun side-panel-open-object-settings (side-panel object &optional (tab :info))
  (setf (side-panel-object side-panel) object)
  (setf (text (side-panel-content side-panel)) "")
  (create-object-settings-view object (side-panel-content side-panel) tab)
  (side-panel-open side-panel))

(defun toggle-side-panel (side-panel)
  (if (side-panel-opened side-panel)
      (side-panel-close side-panel)
      (side-panel-open side-panel)))

#+example
(side-panel-open-object-settings *side-panel* (first *objects*))
