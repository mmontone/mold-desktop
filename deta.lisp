(defpackage :deta
  (:use :cl)
  (:export :deta-store))

(in-package :deta)

(defun deta-request (store endpoint &rest args)
  (let ((base-url (format nil "https://drive.deta.sh/v1/~a/~a"
			  (project-id store)
			  (drive-name store))))
    (apply #'drakma:http-request
	   (str:concat base-url endpoint)
	   :additional-headers (list (cons "X-Api-Key" (project-key store)))
	   args)))

(defun put-file (deta-store name pathname)
  (json:decode-json-from-source
   (deta-request deta-store "/files"
		 :method :post
		 :parameters (list (cons "name" name))
		 :content pathname
		 :want-stream t)))

(defun download-file (store name)
  (deta-request store "/files/download"
		:parameters (list (cons "name" name))))

(defclass deta-store (mold-desktop/store::fs-store)
  ((project-id :initarg :project-id
	       :accessor project-id
	       :initform (error "Provide deta project id"))
   (project-key :initarg :project-key
		:accessor project-key
		:initform (error "Provide deta project-key"))
   (drive-name :initarg :drive-name
	       :accessor drive-name)
   (filename :initarg :filename
	     :accessor filename)))

(defmethod mold-desktop/store:load-database :before ((store deta-store))
  (alexandria:write-byte-vector-into-file
   (download-file (drive-name store) (filename store))
   (mold-desktop/store::filepath store)
   :if-exists :supersede
   :if-does-not-exist :create))

(defmethod mold-desktop/store::save-database :after ((store deta-store))
  (put-file store (filename store) (mold-desktop/store::filepath store)))
