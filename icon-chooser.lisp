(in-package :mold-desktop)

(defparameter *icons-spec*
  (json:decode-json-from-source (asdf:system-relative-pathname :mold-desktop "assets/glyphs-icons.json")))

(defun create-icon-chooser (clog-obj)
  (let ((icon-chooser (create-div clog-obj :style "width: 500px; height: 200px;")))
    (dolist (icon *icons-spec*)
      (let ((icon-button (create-div icon-chooser))
	    (icon-html-tag (format nil "core-~a" (string-downcase (json:camel-case-to-lisp (alex:assoc-value icon :name))))))
	(create-child icon-button (format nil "<~a variant=\"poly\"></~a>"
					  icon-html-tag icon-html-tag))
	(setf (clog:attribute icon-button "style")
              "margin: 2px; width: 50px; height: 50px;display: inline-block; font-size: 50px;")))
    icon-chooser))

#+test
(open-in-window *body* (lambda (content)
			 (create-icon-chooser content)))
