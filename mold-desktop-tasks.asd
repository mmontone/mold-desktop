(asdf:defsystem #:mold-desktop-tasks
  :description "Task management application for Mold Desktop."
  :version "0.1"
  :author "Mariano Montone <marianomontone@gmail.com>"
  :license "AGPL"
  :serial t
  :components
  ((:module "apps/tasks"
    :components ((:file "tasks"))))
  :depends-on (:mold-desktop))
