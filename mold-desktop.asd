(asdf:defsystem #:mold-desktop
  :description "A programmable desktop"
  :version "0.1"
  :author "Mariano Montone <marianomontone@gmail.com>"
  :license "AGPL"
  :serial t
  :components
  ((:file "mime-functions")
   (:file "package")
   (:file "types")
   (:file "value-models")
   (:file "gui")
   (:file "semantic-ui")
   (:file "util")
   (:file "view")
   (:file "default-views")
   (:file "window-manager")
   (:file "object")
   (:file "store")
   (:file "triple-store")
   (:file "commands")
   (:file "commands-core")
   (:file "settings")
   (:file "collections")
   (:file "classifier-collection")
   (:file "colors")
   (:file "command-bar")
   (:file "menu")
   (:file "notifications")
   (:file "jobs")
   (:file "cron")
   (:file "modules")
   (:file "halos")
   (:file "links")
   (:file "object-repository")
   (:file "object-settings")
   (:file "help")
   (:file "side-panel")
   (:file "workspaces")
   (:file "desktop")
   (:file "widgets")
   (:file "properties")
   (:file "naked")
   (:file "output-stream")
   (:file "core-objects")   
   (:file "views")
   (:file "rss")
   (:file "filesystem")
   (:file "dashboard")
   (:file "search")
   (:file "tree")
   (:file "table")
   (:file "graph")
   (:file "richtext")
   (:file "models")
   (:file "tree-model-editor")
   (:file "lispdoc")
   (:file "doc")
   (:file "book")
   (:file "tasks")
   (:file "keymaps")
   (:file "system-log")
   (:file "repl")
   (:file "contact")
   (:file "calendar")
   (:file "email")
   (:file "deta")
   (:file "leaflet")
   (:file "charts")
   (:file "ace-editor")
   (:file "datatables")
   (:file "viewerjs")
   (:file "quill")
   (:file "neomacs")
   (:file "asdf")
   (:file "pathname"))
  :depends-on (:cl-who
	       :clog
	       :clog-callbacks
	       :cl-markdown
	       :drakma
	       :plump
	       :lquery :str
	       :access :rss
	       :cl-json :cl-css
	       :cl-markdown :pathname-utils
	       :cl-store :alexandria :serapeum
	       :closer-mop :uuid :local-time
	       :clack-handler-hunchentoot :salza2 :chipz
	       :event-emitter :lparallel
	       :cxml :xpath :net-telent-date
	       :generic-cl
	       :nkeymaps
	       :cl-smtp
	       :cl-cron
	       :parse-float
	       :cl-colors2
	       :cl-interpol
	       :plump :lquery
	       :secret-values
	       :mel-base
	       :fare-csv
               :blackbird
               :parenscript
               :neomacs-js
               :cl-mimeparse
               :uuid
               :trivia
               :trivia.ppcre
               :mk-string-metrics))

;; qbook

;;;;@include "desktop.lisp"

;;;;@include "commands.lisp"

;;;;@include "halos.lisp"
