;; Copyright (C) 2022 Mariano Montone - All Rights Reserved
;; You may use, distribute and modify this code under the terms of the
;; GNU Affero General Public License

(in-package :mold-desktop)

(defvar *register-status-changes* nil
  "When enabled, changes to objects are registered.
When set to a particular object instance, only changes to that object are registered.
This variable is usually bound dynamically from an UI operation.")

;; Mark attributes with :attribute t, settings with :setting t. Type of attribute or setting can be inferred from :type. For custom types, include the custom type spec in :attribute or :setting, like:  :attribute (choose "foo" "bar" "baz")

(defmacro defobject (name superclasses slots &rest options)
  `(defclass ,name ,(cons 'object (remove 'object superclasses))
     ,slots
     (:metaclass mold-object-class)
     ,@options))

;; subclass of standard-class
(defclass mold-object-class (standard-class)
  ((icon :initarg :icon
         :accessor class-icon
         :initform nil
         :type (or null string)
         :documentation "Icon spec")
   (events :initarg :events
           :accessor class-events
           :initform nil
           :documentation "The list of events triggered by this object.
Items are a property list with keys (:event :installer :documentation &optional :class)")))

;; yup, you know what you're doing
(defmethod closer-mop:validate-superclass
    ((sub mold-object-class) (sup standard-class))
  t)

(defmethod class-icon ((class t))
  nil)

;; subclass of standard-direct-slot-definition
(defclass mold-object-slot-definition ()
  ((attribute
    :initarg :attribute
    :accessor slot-definition-attribute
    :initform nil
    :documentation "When set, the slot is considered an attribute.")
   (setting
    :initarg :setting
    :accessor slot-definition-setting
    :initform nil
    :documentation "When set, the slot is considered a setting.")
   (status :initarg :status
           :accessor slot-definition-status
           :initform nil
           :documentation "When set, the slot is part of the status of the object.")
   (notify-change :initarg :notify-change
                  :initform t
                  :accessor slot-definition-notify-change
                  :documentation "For attributes, emit a change event when slot is changed, when this is enabled.")))

(defclass mold-object-direct-slot-definition (mold-object-slot-definition closer-mop:standard-direct-slot-definition)
  ())

(defclass mold-object-effective-slot-definition (mold-object-slot-definition closer-mop:standard-effective-slot-definition)
  ())

;; letting CLOS know that you want to use your custom
;; class whenever :monitored is non null
;; otherwise, you'll just use the standard slot definition class.
(defmethod closer-mop:direct-slot-definition-class
    ((class mold-object-class) &rest initargs)
  (declare (ignore initargs))
  'mold-object-direct-slot-definition)

(defmethod closer-mop:effective-slot-definition-class
    ((class mold-object-class) &rest initargs)
  (declare (ignore initargs))
  'mold-object-effective-slot-definition)

;; specialize on mold-object-class
(defmethod closer-mop:compute-effective-slot-definition
    ((class mold-object-class) name direct-slots)
  (let ((effective-slot (call-next-method)))
    ;; Set attribute property
    (let ((attribute-slot (find-if (lambda (slot)
                                     (and (typep slot 'mold-object-direct-slot-definition)
                                          (slot-value slot 'attribute)))
                                   direct-slots)))
      (when attribute-slot
        (setf (slot-value effective-slot 'attribute)
              (slot-value attribute-slot 'attribute))))
    ;; Set setting property
    (let ((setting-slot (find-if (lambda (slot)
                                   (and (typep slot 'mold-object-direct-slot-definition)
                                        (slot-value slot 'setting)))
                                 direct-slots)))
      (when setting-slot
        (setf (slot-value effective-slot 'setting)
              (slot-value setting-slot 'setting))))
    ;; Set status property
    (let ((status-slot (find-if (lambda (slot)
                                  (and (typep slot 'mold-object-direct-slot-definition)
                                       (slot-value slot 'status)))
                                direct-slots)))
      (when status-slot
        (setf (slot-value effective-slot 'status)
              (slot-value status-slot 'status))))
    ;; Set notify change property
    (setf (slot-value effective-slot 'notify-change)
          (some (lambda (slot)
                  (and (typep slot 'mold-object-direct-slot-definition)
                       (slot-value slot 'notify-change)))
                direct-slots))
    effective-slot))

(defmethod (setf closer-mop:slot-value-using-class) :around (new-value (class mold-object-class) object slot-definition)
  (let* ((slot-name (closer-mop:slot-definition-name slot-definition))
         (slot-boundp (slot-boundp object slot-name))
         (current-value (when slot-boundp (slot-value object slot-name))))
    (prog1
        (call-next-method)

      ;; Register a status change
      (when (and (slot-boundp object 'status-changes)
                 (or (eq t *register-status-changes*)
                     (eq object *register-status-changes*)))
        (let ((*register-status-changes* nil))
          (push (make-status-change object
                                    (list
                                     (list (closer-mop:slot-definition-name slot-definition)
                                           :from current-value
                                           :to new-value)))
                (status-changes object))))
      (when (and (slot-boundp object 'event-emitter::silo)
                 (or
                  ;; Trigger change event when notify-change is enabled
                  (and (typep slot-definition 'mold-object-slot-definition)
                       (slot-definition-attribute slot-definition)
                       (slot-definition-notify-change slot-definition)
                       (not (equalp new-value current-value)))
                  ;; Trigger change event when object status changed
                  (and (typep slot-definition 'mold-object-slot-definition)
                       (slot-definition-status slot-definition)
                       (not (equalp new-value current-value)))))
        (event-emitter:emit :changed object object)))))

(defun attributes (class)
  (remove-if-not (lambda (slot)
                   (and (typep slot 'mold-object-slot-definition)
                        (slot-definition-attribute slot)))
                 (closer-mop:class-slots class)))

(defun attribute-slots (class)
  (remove-if-not (lambda (slot)
                   (and (typep slot 'mold-object-slot-definition)
                        (slot-definition-attribute slot)))
                 (closer-mop:class-slots class)))

(defun settings (class)
  (remove-if-not (lambda (slot)
                   (and (typep slot 'mold-object-slot-definition)
                        (slot-definition-setting slot)))
                 (closer-mop:class-slots class)))

(defun setting-slots (class)
  (remove-if-not (lambda (slot)
                   (and (typep slot 'mold-object-slot-definition)
                        (slot-definition-setting slot)))
                 (closer-mop:class-slots class)))

(defun status-slots (class)
  "Return status slots of CLASS."
  (remove-if-not (lambda (slot)
                   (and (typep slot 'mold-object-slot-definition)
                        (slot-definition-status slot)))
                 (closer-mop:class-slots class)))

(defun status (object)
  "The status of OBJECT."
  (mapcar (lambda (slot)
            (slot-value object (c2mop:slot-definition-name slot)))
          (status-slots (class-of object))))

(defmethod attribute-type (attribute-slot)
  "Return the type of the ATTRIBUTE-SLOT.
Should return the most specific type."
  ;; If a type was specified via :attribute, return it.
  (if (not (eq (slot-definition-attribute attribute-slot) t))
      (slot-definition-attribute attribute-slot)
      ;; else, return the type of the slot
      (c2mop:slot-definition-type attribute-slot)))

(defclass object (event-emitter:event-emitter)
  ((name :initarg :name :accessor object-name
         :initform nil
         :attribute string
         :documentation "The name of the object.")
   (id :initarg :id :accessor object-id :initform (uuid:make-v4-uuid))
   (url :initarg :url :accessor object-url
        :initform nil
        :attribute string
        :documentation "The url of the object.")
   (description :initarg :description
		:accessor object-description
                :initform ""
                :attribute text
                :documentation "The description of the object.")
   (metadata :initarg :metadata :accessor metadata
             :initform nil)
   (type :initarg :type :accessor object-type
         :initform nil)
   (image :initarg :image :accessor object-image :initform nil)
   (icon :initarg :icon :accessor object-icon :initform nil)
   (category :initarg :category :accessor object-category :initform nil
             :documentation "Category")
   (tags :initarg :tags :accessor tags :initform nil
         :documentation "List of tags")
   (status-changes :accessor status-changes
                   :initform nil
                   :documentation "The list of status changes to the object.")
   (created-at :accessor created-at
               :initform (get-universal-time)
               :type integer)
   (updated-at :accessor updated-at
               :initform (get-universal-time)
               :type (or null integer))
   (accessed-at :accessor accessed-at
                :initform nil
                :type (or null integer)
                :documentation "When the object was last accessed by the user"))
  (:metaclass mold-object-class))

(defmethod print-object ((object object) stream)
  (print-unreadable-object (object stream :type t :identity t)
    (format stream "~a: ~a @ ~a"
            (object-id object)
            (object-name object)
            (object-url object))))

(defgeneric members (object)
  (:documentation "Returns a list of members of OBJECT. Generic interface for composite objects."))

(defmethod members (object)
  nil)

(defgeneric copy-object (object)
  (:documentation "Obtain a copy of OBJECT."))

(defmethod copy-object ((object object))
  (shallow-copy-object object))

(defmethod object-icon :around ((object object))
  ;; The icon of an OBJECT instance, is either the specific icon
  ;; specified for that instance, or the icon of the OBJECT class
  (or (call-next-method)
      (class-icon (find-if #'class-icon (class-ascendants (class-of object))))))

(defgeneric object-attributes (object))

(defmethod object-attributes ((object object))
  (attribute-slots (class-of object)))

(defun find-object-attribute (object name &key (error-p t))
  (or (find name (object-attributes object)
            :key #'c2mop:slot-definition-name)
      (and error-p (error "Not an attribute of ~a: ~s" object name))))

(defgeneric object-title (object))

(defmethod object-title (object)
  (princ-to-string object))

(defmethod object-title ((class class))
  (princ-to-string (class-name class)))

(defmethod object-title ((object object))
  "Get a title for OBJECT.
Uses its name, url and id."
  (princ-to-string
   (or (object-name object)
       (object-url object)
       (object-id object))))

(defmethod object-description (object)
  nil)

;; Example

(defclass document (object)
  ((title :attribute string
          :type string)
   (content :attribute text
            :type string))
  (:metaclass mold-object-class))

#+todo
(defobject youtube-video (video)
  ((youtube-api-key :type string :setting t))
  (:url-parser parse-youtube-url)
  (:create-view (video clog-obj &rest args))
  (:create-item (video clog-obj &rest args)))

;; Generic api for Lisp objects that are not of type OBJECT

(defmethod object-name (lisp-obj)
  (princ-to-string lisp-obj))

(defmethod object-id (lisp-obj)
  (sxhash lisp-obj))

(defmethod object-icon (lisp-obj)
  nil)

(defmethod object-url (lisp-obj)
  nil)

;; ------------- Object status -----------------------------------------

(defclass status-change ()
  ((object :initarg :object
           :accessor object
           :initform (error "Provide object")
           :documentation "The object that changed its status.")
   (object-copy :accessor object-copy
                :initarg :object-copy
                :initform nil
                :documentation "A copy of the object that changed at this point.")
   (changes :initarg :changes
            :accessor changes
            :initform (error "Provide changes")
            :documentation "The list of changes applied.")
   (timestamp :initarg :timestamp
              :accessor timestamp
              :initform (get-universal-time)
              :documentation "The timestamp of the status change")))

(defclass standard-status-change (status-change)
  ())

(defgeneric status-change-class (object)
  (:documentation "The status class for OBJECT.")
  (:method ((object object))
    'standard-status-change))

(defgeneric make-status-change (object changes)
  (:documentation "Create a status change with CHANGES for OBJECT."))

(defmethod make-status-change ((object object) changes)
  (make-instance (status-change-class object)
                 :object object
                 :object-copy (let ((*register-status-changes* nil))
                                (copy-object object))
                 :changes changes))

;; ----------- Object events ---------------------------------------

(defclass event-object ()
  ((event :initarg :event
          :accessor event
          :initform (error "Provide the event"))
   (object :initarg :object
           :accessor event-object
           :initform (error "Provide the event object"))
   (documentation :initarg :documentation
                  :accessor event-documentation
                  :initform nil)
   (installer :initarg :installer
              :accessor event-installer
              :documentation "The installer function. The installer function takes a handler function and an OBJECT, and installs it as the EVENT handler on OBJECT."))
  (:documentation "An object for representing an OBJECT's events.
Thus events can be used as targets from the user interface via commands."))

(defmethod print-object ((event event-object) stream)
  (print-unreadable-object (event stream :type t :identity t)
    (format stream "~a" (event event))))

(defun make-event-object-from-spec (spec object)
  "Create an event from SPEC.
SPEC is a property list with keys (:event :installer :documentation &optional :class)."
  (let ((args (alexandria:remove-from-plist spec :class)))
    (apply #'make-instance (or (getf spec :class) 'event-object)
           :object object
           args)))

(defgeneric object-events (object)
  (:documentation "Return the list of EVENT-OBJECTs for OBJECT."))

(defun default-object-events (object)
  "Return a list of common EVENT-OBJECTs."
  (mapcar (alex:rcurry #'make-event-object-from-spec object)
          `((:event :changed
             :documentation "Event triggered when the object changes."
             :installer ,(lambda (handler obj)
                           (event-emitter:on :changed obj handler))))))

(defmethod object-events ((object object))
  (append (mapcar (alex:rcurry #'make-event-object-from-spec object)
                  (class-events (class-of object)))
          (default-object-events object)))

(defun install-event-handler (handler event-object)
  (let ((object (event-object event-object)))
    (funcall (event-installer event-object)
             handler object)))

;;--- Instances of type

(defgeneric instances-of (type)
  (:documentation "Return the instances of TYPE."))
