(defpackage :custom-types
  (:use :cl)
  (:shadow #:typep)
  (:export #:typep
           #:custom-typep
           #:type-members
           #:member-of-list
           #:native-type
           #:custom-type
           #:parse-type-spec))

(in-package :custom-types)

(defclass custom-type ()
  ((name :initarg :name
         :accessor type-name)))

(defclass native-type (custom-type)
  ((params :initarg :params
           :accessor type-params)))

(defgeneric type-members (type))

(defun parse-type-spec (type-spec)
  (when (cl:typep type-spec 'custom-type)
    (return-from parse-type-spec type-spec))
  (let ((type-name (if (listp type-spec)
                       (car type-spec)
                       type-spec))
        (type-params (if (listp type-spec)
                         (rest type-spec)
                         nil)))
    (apply #'%parse-type-spec type-name type-params)))

(defgeneric %parse-type-spec (type-name &rest params))

(defmethod %parse-type-spec (type-name &rest params)
  (make-instance 'native-type :name type-name :params params))

(defgeneric custom-typep (object type))
(defmethod custom-typep (object type)
  (custom-typep object (parse-type-spec type)))
(defmethod custom-typep (object (type native-type))
  (typep object (list* (type-name type)
                       (type-params type))))

(custom-typep "foo" (parse-type-spec 'string))
(custom-typep "foo" 'string)
(custom-typep "foo" (parse-type-spec 'integer))
(custom-typep "foo" 'integer)

(defclass member-of-list (custom-type)
  ((list-spec :initarg :list-spec
              :accessor list-spec) ;; should support literal lists, variable values and functions
   (key :initarg :key) ;; TODO
   (test :initarg :test)) ;; TODO
  (:default-initargs
   :name 'member-of-list))

(defmethod %parse-type-spec ((type-name (eql 'member-of-list)) &rest params)
  (destructuring-bind (list-spec &key key test) params
    (check-type list-spec (or cons symbol))
    (make-instance 'member-of-list
                   :list-spec list-spec
                   :key key
                   :test test)))

(defmethod type-members ((type member-of-list))
  (cond
    ((listp (list-spec type))
     (list-spec type))
    ((symbolp (list-spec type))
     (symbol-value (list-spec type)))))

(defmethod custom-typep (object (type member-of-list))
  (and (member object (type-members type)) t))

(parse-type-spec '(member-of-list (foo bar baz)))
(custom-typep 'foo '(member-of-list (foo bar baz)))
(custom-typep 'zoo '(member-of-list (foo bar baz)))
(type-members (parse-type-spec '(member-of-list (foo bar baz))))
