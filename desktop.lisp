;; Copyright (C) 2022 Mariano Montone - All Rights Reserved
;; You may use, distribute and modify this code under the terms of the
;; GNU Affero General Public License

;;;; * Mold Desktop

;;;; A programmable web desktop.

(in-package :mold-desktop)

(defvar *config* nil
  "Configuration")
(defvar *app-directory* (asdf:system-source-directory :mold-desktop)
  "Application directory")
(defvar *store* (make-instance 'mold-desktop/store::fs-store
                               :filepath (merge-pathnames "mold-desktop.db"
                                                          (asdf:system-source-directory :mold-desktop)))
  "The store instance with user objects.")
(defvar *triple-store* (naive-triple-store:make-triple-store)
  "The triple store")
(defvar *objects* nil
  "The global list of objects.")
(defvar *object* nil
  "This variable is always bound to the current selected object in the desktop interface.")
(defvar *workspaces* nil
  "The list of workspaces")
(setf (get '*workspaces* :events)
      (make-instance 'event-emitter:event-emitter))
(defvar *workspace* nil
  "The current workspace")
(defvar *url-parsers* nil
  "The registered url parser functions.")
(defvar *collections* (make-instance 'collection :name "My collections"
                                                 :description "User collections")
  "The user collections.")

(defvar *init-functions* nil
  "Functions in this list are called at initialization time.
They are used by plugins and components to add resources to the HTML document, like Javascript scripts and CSS.")

(defvar *ui-state* :initial
  "The state of the user interface.")

(defvar *handle-view-errors* t
  "When enabled handle errors when rendering a view.
Set to NIL to debug view errors from Lisp listener.")

(defvar *handle-command-errors* t
  "When enabled handle errors when running a command.
Set to NIL to debug command errors from Lisp listener.")

;; App UI components
(defvar *command-bar*)
(defvar *loading-indicator*)
(defvar *app-body*)
(defvar *side-panel*)
(defvar *workspaces*)
(defvar *body* nil
  "CLOG user interface main object.")

;;; Demostrate a virtual desktop using CLOG GUI
(defun on-file-count (obj)
  (let ((win (create-gui-window obj :title "Count")))
    (dotimes (n 100)
      ;; window-content is the root element for the clog-gui
      ;; windows
      (create-div (window-content win) :content n))))

(defun browse-in-window (obj title url)
  (let ((win (create-gui-window obj :title title)))
    (create-child (window-content win)
                  (html (:iframe :style "width:100%;height:97%;"
                                 :src url)))))

(defun about-common-lisp (obj)
  (browse-in-window obj "About Common Lisp" "https://common-lisp.net/"))

(defun open-clog-manual (obj)
  (browse-in-window obj "CLOG manual" "https://rabbibotton.github.io/clog/clog-manual.html"))

(defun open-markdown-in-window (obj title markdown-file)
  (let ((win (create-gui-window obj :title title))
        (html (with-output-to-string (s)
                (cl-markdown:markdown markdown-file :stream s))))
    (create-div (window-content win) :content html)))

(defun open-learn-clog-window (obj)
  ;;(browse-in-window obj "Learn CLOG" "https://github.com/rabbibotton/clog/blob/main/LEARN.md")
  (open-markdown-in-window obj "Learn CLOG" (asdf:system-relative-pathname :clog "LEARN.md")))

(defun open-readme-window (obj)
  (open-markdown-in-window obj "README" (asdf:system-relative-pathname :clog "README.md")))

(defun open-clog-concept-window (obj)
  (open-markdown-in-window obj "CONCEPT" (asdf:system-relative-pathname :clog "CONCEPT.md")))

(defun on-file-drawing (obj)
  (let* ((win (create-gui-window obj :title "Drawing"))
         (canvas (create-canvas (window-content win) :width 600 :height 400))
         (cx     (create-context2d canvas)))
    (set-border canvas :thin :solid :black)
    (fill-style cx :green)
    (fill-rect cx 10 10 150 100)
    (fill-style cx :blue)
    (font-style cx "bold 24px serif")
    (fill-text cx "Hello World" 10 150)
    (fill-style cx :red)
    (begin-path cx)
    (ellipse cx 200 200 50 7 0.78 0 6.29)
    (path-stroke cx)
    (path-fill cx)))

(defun on-file-movies (obj)
  (let* ((win  (create-gui-window obj :title "Movie"))
         (movie (create-video (window-content win)
                              :source "https://www.w3schools.com/html/mov_bbb.mp4")))
    (set-geometry movie :units "%" :width 100 :height 100)))

(defun on-help-about (obj)
  (let* ((about (create-gui-window *app-body*
                                   :title   "About"
                                   :content "<div class='w3-black'>
                                         <center><img src='/img/icons8-desktop-64.png'></center>
                                         <center>Mold Desktop</center>
                                         <center>The Programmable Desktop</center></div>
                                         <center>(c) 2022 - Mariano Montone</center></p></div>"
                                   :hidden  t
                                   :width   200
                                   :height  215)))
    (window-center about)
    (setf (visiblep about) t)
    (set-on-window-can-size about (lambda (obj)
                                    (declare (ignore obj))()))))

(defun create-object-from-url (url)
  "Create an object from URL."
  (flet ((create-webpage ()
           (multiple-value-bind (title description image-src)
               (fetch-preview-data url)
             (make-instance 'web-page :url url
                                      :name (or title url)
                                      :description description
                                      :image-src image-src))))
    ;; Access the headers
    (handler-case
        (multiple-value-bind (content status headers)
            (drakma:http-request url :method :head)
          (declare (ignore content))
          (if (= status 200)
              (progn
                ;; First try to find a parser and parse the url with it
                (dolist (url-parser *url-parsers*)
                  (let ((object (funcall url-parser url headers)))
                    (when object
                      (return-from create-object-from-url object))))
                ;; If no parser found, create a generic WEB-PAGE object
                (create-webpage))
              ;; If error status, we could return NIL or create the web-page object anyways.
              (make-instance 'web-page :url url)))
      (USOCKET:NS-HOST-NOT-FOUND-ERROR ()
        ;; The host could not be accessed. We could return NIL or create the webpage anyway.
        (make-instance 'web-page :url url)))))

(defmethod create-object-view (object clog-obj &rest args)
  "The default view for objects with no specialized view."
  (declare (ignore args))
  (create-div clog-obj :content (who:escape-string (object-title object))))

(defmethod create-object-view ((object object) clog-obj &rest args)
  "The default view for objects with no specialized view."
  (declare (ignore args))
  (create-div clog-obj :content (or (object-name object)
                                    (format nil "~a::~a"
                                            (object-id object)
                                            (class-name (class-of object))))))

(defmethod object-view-types append ((object object) (mold-module (eql :desktop)))
  '(create-object-view create-object-settings-view))

(defclass web-page (object)
  ((image-src :initarg :image-src
              :accessor image-src
              :initform nil))
  (:icon . "core-laptop-code")
  (:documentation "A web page")
  (:metaclass mold-object-class))

(defun fetch-preview-data (url)
  ;; Try to build a preview of the web page
  ;; https://andrejgajdos.com/how-to-create-a-link-preview/

  (multiple-value-bind (html status headers) (drakma:http-request url :method :head)
    (declare (ignore html))
    (when (>= status 400)
      (return-from fetch-preview-data))
    ;; The content type needs to be HTML
    (when (not (search "text/html" (alexandria:assoc-value headers :content-type)
                       :test 'string=))
      (return-from fetch-preview-data)))

  (multiple-value-bind (html status) (drakma:http-request url)
    (when (>= status 400)
      (return-from fetch-preview-data))
    (let ((dom (ignore-errors (lquery:$ (lquery:initialize html)))))
      (when dom
        (flet ((meta (what name)
                 (let ((selector (format nil "meta[~a=~s]" what name)))
                   (alex:when-let (el (lquery:$1 dom selector))
                     (plump:attribute el "content")))))
          (let ((title (or (meta "property" "og:title")
                           (meta "name" "twitter:title")
                           (alex:when-let (el (lquery:$1 dom "title")) (plump:text el))
                           (alex:when-let (el (lquery:$1 dom "h1")) (plump:text el))
                           (alex:when-let (el (lquery:$1 dom "h2")) (plump:text el))))
                (description (or (meta "property" "og:description")
                                 (meta "name" "twitter:description")
                                 (meta "name" "description")
                                 (alex:when-let (el (lquery:$1 dom "p")) (plump:text el))))
                (image (or (meta "property" "og:image")
                           (alex:when-let (el (lquery:$1 dom "link[rel=\"image_src\"]"))
                             (plump:attribute el "href"))
                           (meta "name" "twitter:image")
                           (alex:when-let (img (lquery:$1 dom "img")) (plump:attribute img "src")))))
            (when (and image (eql (aref image 0) #\/))
              ;; Convert relative url to absolute
              (let* ((uri (quri:uri url)))
                (setf image (str:concat (quri:uri-scheme uri) "://" (quri:uri-domain uri) image))))
            (values title description image)))))))

(defmethod create-object-view ((object web-page) clog-obj &rest args)
  (declare (ignore args))
  (create-child clog-obj
                (html
                  (:div
                   (:h3
                    (when (object-url object)
                      (who:htm
                       (:img :src (get-favicon-url (object-url object))
                             :style "height: 32px; width: 32px; margin-right: 5px;")))
                    (who:str (object-title object)))

                   (when (image-src object)
                     (who:htm (:img :src (image-src object)
                                    :style "margin: 5px; max-width: 200px; max-height: 200px; float: left;")))
                   (:p (who:str (object-description object)))))))

(defmethod create-thumbnail-view ((object object) clog-obj &rest args &key context &allow-other-keys)
  (let* ((view (create-div clog-obj :class "thumbnail"))
         (title (create-child view
                              (html
                                (:h5 :style "cursor:pointer;"
                                     (when (object-url object)
                                       (who:htm (:img :src (get-favicon-url (object-url object))
                                                      :style "height: 32px; width: 32px; margin-right: 5px;")))
                                     (who:str (object-title object)))))))
    (create-child view (html (:p (:small (who:str (str:shorten 500 (object-description object)))))))
    (create-div view :style "clear:both;")
    (let ((click-handler (or (and (getf args :on-click)
                                  (lambda* (&rest rs)
                                    ;; Pass the clicked object to the click handler
                                    (apply (getf args :on-click) object rs)))
                             (lambda* (_)
                               (show object :context context)))))

      (set-on-event title "click" click-handler :cancel-event t))
    view))

(defmethod create-thumbnail-view ((object web-page) clog-obj &rest args &key context &allow-other-keys)
  (let* ((view (create-div clog-obj :class "thumbnail"))
         (title (create-child view (html (:h5 :style "cursor:pointer;"
                                              (:img :src (get-favicon-url (object-url object))
                                                    :style "height: 32px; width: 32px; margin-right: 5px;")
                                              (who:str (object-title object)))))))
    (when (image-src object)
      (create-img view :url-src (image-src object)
                       :style "margin: 5px; max-width: 200px; max-height: 200px; float: left;"))

    (create-child view (html (:p (:small (who:str (str:shorten 500 (object-description object)))))))
    (create-div view :style "clear:both;")
    (let ((click-handler (or (and (getf args :on-click)
                                  (lambda* (&rest rs)
                                    ;; Pass the clicked object to the click handler
                                    (apply (getf args :on-click) object rs)))
                             (lambda* (_)
                               (show object :context context)))))

      (set-on-event title "click" click-handler :cancel-event t))
    view))

(mold-desktop/settings:defcustom mold-desktop::*desktop-background-color*
  cl-colors2:+blue+
  "Color for the desktop background."
  :type mold-desktop::color
  :on-change (lambda (var)
               (mold-desktop::set-background-color
                (cl-colors2:print-css-rgb/a (mold-desktop/settings:custom-var-value var)))))

(mold-desktop/settings:defcustom *check-iframe-for-web-pages*
  t
  "If enabled use iframe view for WEB-PAGE objects when the site allows it."
  :type boolean
  :group mold-desktop/settings::desktop)

(defmethod object-default-view ((object web-page))
  (when *check-iframe-for-web-pages*
    (multiple-value-bind (content status headers)
        (drakma:http-request (object-url object) :method :head)
      (declare (ignore content))
      (when (= status 200)
        (when (not (equalp (cdr (assoc :x-frame-options headers)) "DENY"))
          (return-from object-default-view 'create-iframe-view)))))
  (call-next-method))

(defun open-in-window (clog-obj element-creator-func &rest args)
  (let ((wnd (apply #'create-gui-window clog-obj args)))
    (funcall element-creator-func (window-content wnd))))

(defun render-object (object clog-obj view &rest args)
  (if *handle-view-errors*
      (handler-case
          (apply view object clog-obj args)
        (error (e)
          (create-p clog-obj :content (hunchentoot:escape-for-html (condition-message e)))
          (format *error-output* "Error rendering view ~a: ~a" view e)
          (trivial-backtrace:print-backtrace e)))
      (apply view object clog-obj args)))

(defun create-spinner (clog-obj &key (size 32))
  (create-child clog-obj
                (html (:i :class "fa fa-spinner w3-spin"
                          :style (format nil "font-size:~apx" size)))))

(defun start-spinner (spinner)
  (clog:add-class spinner "w3-spin"))

(defun stop-spinner (spinner)
  (clog:remove-class spinner "w3-spin"))

(defun open-object-in-window (object clog-obj &optional view &rest args)
  (let ((wnd (create-window *window-manager*
                            clog-obj
                            :title (object-title object)
                            :object object)))
    ;; Show a spinner while the object view loads
    (let* ((spinner (create-spinner wnd))
           (view (or view (object-default-view object))))
      (apply #'render-object object
             (mold-desktop/clog-gui:window-content wnd)
             view args)
      (setf (mold-desktop/clog-gui::object-view wnd) view)
      (remove-from-dom spinner))))

(defgeneric show-object (object context view workspace))

(defmethod show-object (object context view workspace)
  (open-object-in-window object *app-body* view))

(defmethod show-object (object context view (workspace default-workspace))
  (workspace-add-object workspace object)
  (open-object-in-window object *app-body* view))

(defun show (object &key view context)
  "Show OBJECT in desktop."
  (show-object object context view *workspace*))

(defun edit (object)
  (with-open-window wnd (*app-body* :title (format nil "Edit ~a" (object-title object)))
    (create-object-editor-view object wnd)))

(defun add-and-show-object (object clog-obj)
  (push object *objects*)
  (open-object-in-window object clog-obj))

(defun select-object (object &optional (open-command-bar t))
  (command-bar-select-object *command-bar* object)
  (when open-command-bar
    (open-command-bar *command-bar*)))

(defun unselect-object (object &optional (open-command-bar t))
  (command-bar-unselect-object *command-bar* object)
  (when open-command-bar
    (open-command-bar *command-bar*)))

(defun toggle-object (object &optional (open-command-bar t))
  (command-bar-toggle-object *command-bar* object)
  (when open-command-bar
    (open-command-bar *command-bar*)))

(defun object-selected? (object)
  (member object (selected-arguments (command-bar-status *command-bar*))))

(defun show-commands-for-object (object)
  "Open a command bar listing available commands for OBJECT.
The command bar status is reseted, OBJECT selected and commands listed.
The command bar is opened if it was not opened already."
  (reset-command-bar *command-bar*)
  (command-bar-select-object *command-bar* object)
  (open-command-bar *command-bar*))

(defun pretty-object-type-name (class)
  "Make a good looking name for object type from CLASS name."
  (str:join #\space
            (mapcar (alex:compose #'string-capitalize #'string-downcase)
                    (str:split #\- (princ-to-string (class-name class))))))

(defun open-collection-with-all-objects (clog-obj &optional (view 'create-object-view))
  (open-object-in-window
   (make-instance 'collection
                  :name "All objects"
                  :members *objects*
                  :description "The collection of all the objects")
   clog-obj view))

(defcommand (list-all-objects :icon "fa fa-list"
                              :show-result t)
    ()
  "Open collection with all objects"
  (make-instance 'collection
                 :name "All objects"
                 :members *objects*
                 :description "The collection of all the objects"))

(defcommand (list-my-collections :icon "fa fa-list")
    ()
  "Open user collections"
  (show *collections* :view 'create-tree-view))

(defcommand (set-background-color :icon "fa fa-cog")
    ((color color-name))
  "Set the desktop background color."
  (setf (clog:style (workspace-elem *workspace*) "background-color")
        color))

(defun start-desktop (body)
  (setf *body* body)
  (setf *workspaces* (list (make-instance 'default-workspace)))

  ;; Theme
  (load-css (html-document body) "https://cdnjs.cloudflare.com/ajax/libs/fomantic-ui/2.8.8/semantic.min.css")
  (load-script (html-document body) "https://cdnjs.cloudflare.com/ajax/libs/fomantic-ui/2.8.8/semantic.min.js" :wait-for-load nil)

  ;; Application assets
  (load-css (html-document body) (format nil "css/app.css?t=~a" (get-universal-time)))
  (load-script (html-document body) (format nil "/js/app.js?t=~a" (get-universal-time)))

  ;; Icons
  (load-css (html-document body) "https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.1.1/css/all.min.css")
  (load-css (html-document body) "https://cdn.jsdelivr.net/npm/remixicon@2.5.0/fonts/remixicon.css")

  ;; For tags input
  (load-script (html-document body) "https://cdn.jsdelivr.net/npm/suggestags@1.27.0/js/jquery.amsify.suggestags.min.js" :wait-for-load nil)


  ;; Libraries
  (load-script (html-document body) (format nil "/js/dnd.js?t=~a" (get-universal-time)))
  (load-script (html-document body) "https://cdnjs.cloudflare.com/ajax/libs/split.js/1.6.0/split.min.js")

  (setf (title (html-document body)) "Mold Desktop - The Programmable Desktop")

  ;; For web oriented apps consider using the :client-movement option.
  ;; See clog-gui-initialize documentation.
  (clog-gui-initialize body)
  (add-class body (or (acc:accesses *config* :desktop :background-color) "w3-light-blue"))

  (setf (text body) "")
  (setf *side-panel* (apply #'create-side-panel body (alexandria:assoc-value *config* :settings-panel)))

  (setf *command-bar* (apply #'create-command-bar body (alexandria:assoc-value *config* :command-bar)))

  (open-workspace (first *workspaces*) body)

  (init-window-manager *window-manager* *app-body*)

  (dolist (init-function *init-functions*)
    (funcall init-function body))

  ;; Setup key-bindings
  ;; FIXME: we have a problem when trying to bind key-down events to the same clog object (body) from different places.
  ;; Only the last call to set-on-key-down seems to work.
  (set-on-key-down body
                   (lambda* (_ keyboard-event)
                     (let ((key-binding (keyboard-event-key-binding keyboard-event)))
                       (when key-binding
                         (run-key-binding key-binding)))))

  (init-command-bar *command-bar* body *app-body*)

  ;; Open the welcome book on start

  (run-command 'open-mold-desktop-book)

  ;; Scripts
  ;; TODO: make these optional?
  (create-child body (html (:script :src "https://unpkg.com/@glyphs/wc-core" )))
  (create-child body (html (:script :src "https://unpkg.com/@glyphs/wc-brands" )))
  (create-child body (html (:script :src "https://unpkg.com/@glyphs/wc-flags" )))
  )

;; Bookmarklet:

;; function post (url, formData) {
;;   const h = (tag, props) => Object.assign(document.createElement(tag), props)
;;   const iframe = h('iframe', {id: 'mold-desktop', name:'mold-desktop',hidden: true})
;;   const form = h('form', { action: url, method: 'post', hidden: true, target: 'mold-desktop' })
;;   for (const [name, value] of Object.entries(formData)) {
;;     form.appendChild(h('input', { name, value }))
;;   }
;;   document.body.appendChild(iframe)
;;   document.body.appendChild(form)
;;   form.submit()
;; }

;; For adding url:
;; post('http://127.0.0.1:8081/add', { url: window.location })

;; For parsing calendar event:
;; post('http://127.0.0.1:8081/calevent', {url: window.location, source: window.getSelection().toString()})

;; For adding a note:
;; post('http://127.0.0.1:8081/addnote', {url: window.location, content: window.getSelection().toString()})

;; https://www.yourjs.com/bookmarklet/

;; Bookmarks

(defun init-bookmarks-lp-kernel ()
  (when (not lparallel:*kernel*)
    (setf lparallel:*kernel* (lparallel:make-kernel 3))))

(defcommand import-firefox-bookmarks
    ((file string "The file to import"))
  "Imports bookmarks from Firefox JSON file."
  (init-bookmarks-lp-kernel)
  (let* ((bookmarks
           (loop for category in
                              (accesses (json:decode-json-from-source (pathname file)) :children)
                 appending (access:accesses category :children))))
    (with-job (job :name "Firefox bookmarks import"
                   :progress 0
                   :progress-max (length bookmarks)
                   :display-progress-details t
		   :display-progress-time t)
      (lparallel:pmapc
       (lambda (bookmark)
         (handler-case
             (progn
               (format (job-output job) "Fetching: ~a..." (access  bookmark :uri))
               (let ((imported-object (create-object-from-url (access  bookmark :uri))))
                 (push imported-object *objects*)
                 (format (job-output job) "Imported: ~a~%" imported-object)))
           (error (e)
             (print e)))
         (incf (job-progress job)))
       bookmarks)
      (format (job-output job) "Import finished~%"))))

(defun on-load-system (clog-obj)
  (run-command 'mold-desktop/commands::load-system))

(defun on-save-system (clog-obj)
  (run-command 'mold-desktop/commands::save-system))

(defun load-config (&optional pathname)
  (let ((pathname (or pathname
                      (asdf:system-relative-pathname :mold-desktop "config.lisp"))))
    (setf *config* (read-from-string (alexandria:read-file-into-string pathname)))))

(defun config-get (&rest keys)
  (apply #'access:accesses *config* keys))

(defun config-ensure-get (&rest keys)
  (or (apply #'config-get keys)
      (error "Configuration not set: ~s" keys)))

(hunchentoot:define-easy-handler (add-object-handler :uri "/add")
    (url)
  (let ((object (mold-desktop/commands::add-object-from-url url)))
    (show object))
  "Done")

(hunchentoot:define-easy-handler (add-calendar-event :uri "/calevent")
    (url source)
  (let* ((data (parse-calendar-event source))
         (calendar-event
           (acc:with-access (title time date people duration urls)
                            data
             (make-instance 'calendar-event
                            :name title
                            :time time
                            :date date
                            :people people
                            :duration duration
                            :url (or (first urls)
                                     url)))))
    (add-and-show-object calendar-event *body*)
    "Done"))

(hunchentoot:define-easy-handler (add-note :uri "/addnote")
    (url content)
  (let ((doc (make-instance 'remote-document
                            :url url
                            :name (str:shorten 80 content)
                            :format :text
                            :content content)))
    (add-and-show-object doc *app-body*)
    "Done"))

(hunchentoot:define-easy-handler (render-object-handler :uri "/render")
    (url)
  (setf (hunchentoot:header-out "Access-Control-Allow-Origin") "*")
  (setf (hunchentoot:header-out "Access-Control-Allow-Methods") "GET, OPTIONS")
  (let* ((object (create-object-from-url url))
         (view (create-object-view object *body*)))
    (clog:outer-html view)))

(defvar *lack-middlewares* nil)

(defun start (&key (load-database t)
                (open-browser t)
                (host "0.0.0.0")
                (port 8080)
                app-directory)
  "Start desktop."
  (setf *random-state* (make-random-state t))
  (setf *window-manager* (make-instance 'halos-window-manager))
  (when load-database
    (mold-desktop/store:load-database *store*))
  (initialize 'start-desktop
              :host host :port port
              :static-root (merge-pathnames "static/" (or app-directory *app-directory*))
	      :lack-middleware-list *lack-middlewares*)
  (when open-browser
    (open-browser :url (format nil "http://127.0.0.1:~a" port)))
  (format t "Starting Hunchentoot api: ~a ~%"
          (hunchentoot:start (make-instance 'hunchentoot:easy-acceptor :port 8081))))
