(in-package :mold-desktop)

;; https://powerplatform.microsoft.com/en-us/common-data-model/
;; https://docs.microsoft.com/es-es/common-data-model/schema/core/applicationcommon/contact
;; See vcard format and vcard-parser.lisp

(defobject contact ()
  ((fullname :accessor fullname
             :attribute t
             :type string)
   (birth-date :accessor birth-date
               :attribute date)
   (telephone :accessor telephone
              :attribute string)
   (email :accessor email
          :attribute string))
  (:documentation "A contact object."))
