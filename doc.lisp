;; Documentation system for Mold Desktop
;; Basic design is:
;; DOCNODE objects, that can be formatted in different ways,
;; depending on its formatter.
;; A collection of subnodes, for sections, subsections, etc.
;; Similar model than TexInfo.

(in-package :mold-desktop)

(defclass doc-node (object)
  ((parent :initarg :parent
           :initform nil
           :accessor parent-node)
   (subnodes :initarg :subnodes
             :accessor subnodes
             :initform (make-instance 'collection :type 'doc-node)
	     :type collection
	     :attribute t)
   (contents :initarg :contents
             :accessor contents
             :initform ""
	     :type text
	     :attribute t))
  (:metaclass mold-object-class)
  (:icon . "core-file"))

(defmethod members ((node doc-node))
  (subnodes node))

(defun doc-node-siblings (doc-node)
  (let ((parent (parent-node doc-node)))
    (when parent
      ;; Return left and right siblings
      (apply #'values
             (split-sequence:split-sequence
              doc-node
              (members (subnodes parent)))))))

(defmethod tree-node-children ((obj doc-node))
  (subnodes obj))

(defclass text-doc-node (doc-node)
  ()
  (:metaclass mold-object-class)
  (:documentation "A plain text document node."))

(defun split-in-paragraphs (text)
  (str:split (coerce (list #\newline #\newline) 'string) text))

(defmethod create-object-view ((obj text-doc-node) clog-obj &rest args &key (show-title t))
  (declare (ignore args))
  (when show-title (create-section clog-obj :h1 :content (object-title obj)))
  (dolist (par (split-in-paragraphs (contents obj)))
    (create-p clog-obj :content par)))

#+test(show (make-instance 'text-doc-node :name "Hello world" :contents "Hello world!!"))

#+test(show (make-instance 'text-doc-node :name "Hello world" :contents "Hello world!!

This is a paragraph"))

(defclass markdown-doc-node (doc-node)
  ()
  (:metaclass mold-object-class)
  (:documentation "A documentation node with contents in Markdown format."))

(defmethod create-object-view ((obj markdown-doc-node) clog-obj &rest args)
  (declare (ignore args))
  (create-section clog-obj :h1 :content (object-title obj))
  (create-child clog-obj
                (with-output-to-string (s)
                  (write-string "<div>" s)
                  (cl-markdown:markdown (contents obj) :stream s)
                  (write-string "</div>" s))))

#+test(show (make-instance 'markdown-doc-node
                           :name "README"
                           :contents (alex:read-file-into-string (asdf:system-relative-pathname :mold-desktop "README.md"))))

#+test(show (make-instance 'markdown-doc-node
                           :name "IDEAS"
                           :contents (alex:read-file-into-string (asdf:system-relative-pathname :mold-desktop "docs/ideas.md"))))

(defclass lisp-doc-node (doc-node)
  ()
  (:metaclass mold-object-class)
  (:documentation "A documentation node with contents in Lisp doc format."))

(defmethod create-object-view ((obj lisp-doc-node) clog-obj &rest args)
  (declare (ignore args))
  (create-section clog-obj :h1 :content (object-title obj))
  (lispdoc:create-html obj clog-obj))

#+test(show (make-instance 'lisp-doc-node
                           :name "Test"
                           :contents "(:h1 \"header1\")

lalal
sdfsf

dfdf"))

(defvar *doc-node-extensions*
  '(("txt" . text-doc-node)
    ("lispdoc" . lisp-doc-node)
    ("ldoc" . lisp-doc-node)
    ("md" . markdown-doc-node)
    ("markdown" . markdown-doc-node))
  "An alist map of (file-extension . doc-node-type). Used by READ-DOC-NODE-FROM-FILE to load files as DOC-NODEs.")

(defun read-doc-node-from-file (pathname &rest args)
  "Read a DOC-NODE from a file.
The file extension is used to determine the type of DOC-NODE to create."
  (let* ((file-extension (pathname-type pathname))
	 (doc-node-class (or (sera:assocdr file-extension *doc-node-extensions* :test #'string=)
			     (error "Don't know how to create a documentation node for file extension: ~a" file-extension)))
	 (contents (alexandria:read-file-into-string pathname)))
    (apply #'make-instance doc-node-class :contents contents args)))
