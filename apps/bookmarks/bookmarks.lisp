(defpackage :md/bookmarks
  (:local-nicknames
   (:md :mold-desktop)
   (:alex :alexandria))
  (:use :cl :clog :clog-gui))

(in-package :md/bookmarks)

(defmacro with-ui-update (parent model event &body body)
  "Render PARENT evaluating BODY, and refresh the PARENT when MODEL triggers EVENT."
  `(progn
     ,@body
     (event-emitter:on ',event ,model
                       (lambda (&rest args)
                         (declare (ignore args))
                         (setf (text ,parent) "") ;; delete parent contents
                         ,@body))))

(defvar *details-panel*)

(defparameter *default-collections*
  (list (make-instance 'md::collection
                       :name "Most recent"
                       :members (lambda () md::*objects*)
                       :icon "fa-solid fa-clock")
        (make-instance 'md::collection
                       :name "Not seen"
                       :members (lambda () (remove-if #'md::accessed-at md::*objects*))
                       :icon "fa fa-eye")
        (make-instance 'md::collection
                       :name "Favourites"
                       :members nil
                       :icon "fa fa-star")))

(defclass mold-space (md::collection)
  ()
  (:metaclass md::mold-object-class))

(defparameter *default-spaces*
  (list
   (make-instance 'mold-space
                  :name "My space 1"
                  :icon "fa-solid fa-earth-americas")
   (make-instance 'mold-space
                  :name "My space 2"
                  :icon "fa-solid fa-earth-americas")))

(defclass bookmarks-workspace (md::mold-workspace)
  ((collections :initform *default-collections*
                :accessor collections)
   (spaces :initform *default-spaces*
           :accessor spaces))
  (:default-initargs
   :name "Bookmarks"
   :icon "fa-regular fa-bookmark")
  (:documentation "Workspace with user interface for managing bookmarks."))

(defmethod md::open-workspace ((workspace bookmarks-workspace) body)
  (let* ((app-main (create-div body :class "app-main workspace side-bar-workspace"))
         (navigation-bar (apply #'create-navigation-bar app-main (alexandria:assoc-value md::*config* :navigation-bar)))
         (app-body (create-div app-main :class "app-body"))
         (details-panel (create-div app-body :class "details-panel w3-hide")))

    (let ((app (clog-gui::connection-data-item body "clog-gui")))
      (setf (mold-desktop/clog-gui::app-body app) app-body)
      (setf (connection-data-item body "clog-app-body") app-body))

    (setf (md::workspace-elem workspace) app-main)

    (setf md::*app-body* app-body
          (md::workspace-body workspace) app-body)

    (setf *details-panel* details-panel)

    ;; Handle drag and drop urls from browser's windows/tabs
    (setf (attribute app-body "ondragover") "return false")
    (set-on-drag-over app-body (md::lambda* (_) nil))
    (set-on-drop app-body (lambda (obj event)
                            (declare (ignore obj))
                            (let ((maybe-url (getf event :drag-data)))
                              (when (or (str:starts-with? "http://" maybe-url)
                                        (str:starts-with? "https://" maybe-url))
                                (md::show (mold-desktop/commands::add-object-from-url maybe-url))))))

    (navigate-to-collection (first (collections workspace)))

    (set-on-before-unload (window body) (lambda(obj)
                                          (declare (ignore obj))
                                          ;; return empty string to prevent nav off page
                                          ""))
    app-main))

(defmethod mold-desktop::workspace-remove-object ((ws bookmarks-workspace) object)
  ;; Do something here?
  )

(defclass navigation-bar (clog-element event-emitter:event-emitter)
  (;; Model
   (search-input :accessor search-input)
   (categories :initarg :categories
               :accessor categories
               :documentation "Objects categories tree.")
   (collections :initarg :collections
                :accessor collections
                :documentation "User's collections.")
   (spaces :initarg :spaces
           :accessor spaces
           :documentation "User's spaces")
   ;; UI
   (categories-view :accessor categories-view)
   (collections-view :accessor collections-view)
   (spaces-view :accessor spaces-view)))

(defun render-collections-list (collections-view)
  (dolist (col (collections md::*workspace*))
    (let ((li (create-list-item collections-view)))
      (when (md::object-icon col)
        (md::create-icon (md::object-icon col) li))
      (create-span li :content (md::object-name col) :style "margin-left:8px;")
      (set-on-click li (md::lambda* (_)
                         (navigate-to-collection col)))
      (when (listp (slot-value col 'md::members))
        ;; We assume collection is mutable looking at the ITEMS slot

        ;; To enable drop, drag-over event handler needs to be set:
        (set-on-drag-over li (lambda (obj)
                               (declare (ignore obj))
                               (add-class li "drop-allowed")))
        (set-on-drag-leave li
                           (lambda (obj)
                             (declare (ignore obj))
                             (remove-class li "drop-allowed")))
        (set-on-drop li (lambda (&rest args)
                          (declare (ignore args))
                          (push mold-desktop/clog-gui::*dragging-object* (md::members col))
                          (setf mold-desktop/clog-gui::*dragging-object* nil)
                          (remove-class li "drop-allowed")))
        ))))

(defun render-spaces-list (spaces-view)
  (dolist (space (spaces md::*workspace*))
    (let ((li (create-list-item spaces-view)))
      (md::create-icon (or (md::object-icon space) "fa-solid fa-earth-americas")
                       li)
      (create-span li :content (md::object-name space) :style "margin-left: 8px;")
      (set-on-click li (md::lambda* (&rest _)
                         (show-space space)))

      ;; To enable drop, drag-over event handler needs to be set:
      (set-on-drag-over li (lambda (obj)
                             (declare (ignore obj))
                             (add-class li "drop-allowed")))
      (set-on-drag-leave li
                         (lambda (obj)
                           (declare (ignore obj))
                           (remove-class li "drop-allowed")))
      (set-on-drop li (md::lambda* (&rest _)
                        (push mold-desktop/clog-gui::*dragging-object* (md::members space))
                        (setf mold-desktop/clog-gui::*dragging-object* nil)
                        (remove-class li "drop-allowed"))))))

(defun show-space (space)
  (setf (text md::*app-body*) "")
  (dolist (object (md::members space))
    (md::show object)))

(defun create-navigation-bar (clog-obj &rest initargs)
  (let ((navigation-bar (create-div clog-obj :class "mold-navigation-bar w3-bar w3-card-4")))
    (apply #'change-class navigation-bar 'navigation-bar initargs)
    (setf (search-input navigation-bar)
          (create-child navigation-bar
			(md::html (:input :type "text"
                                          :class "w3-input w3-border"
                                          :placeholder "Search ..."))))
    ;; Create search collection on search
    (set-on-key-press (search-input navigation-bar)
                      (md::lambda* (_ event)
                        (when (string= (getf event :key) "Enter")
                          (let* ((search-term (md::element-value (search-input navigation-bar)))
                                 (collection (make-instance 'md::collection
                                                            :name (format nil "Search: ~a" search-term)
                                                            :icon "fa-solid fa-magnifying-glass"
                                                            :members (lambda ()
                                                                     (remove-if-not (lambda (obj)
                                                                                      (or (search search-term (md::object-name obj) :test #'equalp)
                                                                                          (search search-term (md::object-description obj) :test #'equalp)))
                                                                                    md::*objects*)))))
                            (md::push-end collection (collections md::*workspace*))
                            (event-emitter:emit :collections-changed navigation-bar)
                            (navigate-to-collection collection)
                            ))))

    (let ((toggle-categories
            (create-button navigation-bar :class "w3-button w3-block w3-left-align w3-light-gray")))
      (md::-> (md::create-icon "core-grid-sm" toggle-categories)
              (md::set-styles '(("font-size" "2rem"))))
      (create-span toggle-categories :content "Categories" :style "vertical-align:super;")
      (let ((categories (create-top-categories-list navigation-bar)))
        (set-on-click toggle-categories (lambda (&rest args)
                                          (declare (ignore args))
                                          (toggle-class categories "w3-hide")))))

    (let ((toggle-collections
            (create-button navigation-bar :class "w3-button w3-block w3-left-align w3-light-gray")))

      (md::-> (md::create-icon "core-grid-list" toggle-collections)
              (md::set-styles '(("font-size" "2rem"))))
      (create-span toggle-collections :content "Collections" :style "vertical-align:super;")

      ;; Collections

      (let ((collections (create-unordered-list navigation-bar :class "w3-ul w3-hoverable")))
        (setf (collections-view navigation-bar) collections)

        (with-ui-update collections navigation-bar :collections-changed
          (render-collections-list collections))

        (set-on-click toggle-collections (lambda (&rest args)
                                           (declare (ignore args))
                                           (toggle-class collections "w3-hide")))))

    (let ((toggle-spaces (create-button navigation-bar :class "w3-button w3-block w3-left-align w3-light-gray")))

      (md::-> (md::create-icon "core-globe-middle" toggle-spaces)
              (md::set-styles '(("font-size" "2rem"))))
      (create-span toggle-spaces :content "Spaces" :style "vertical-align:super;")

      (let ((spaces (create-unordered-list navigation-bar :class "w3-ul w3-hoverable")))
        (with-ui-update spaces navigation-bar :spaces-changed
          (render-spaces-list spaces))

        (set-on-click toggle-spaces (lambda (&rest args)
                                      (declare (ignore args))
                                      (toggle-class spaces "w3-hide")))))

    navigation-bar))

(defun show-object-details (object)
  (setf (text *details-panel*) "")
  ;; REVIEW. Mark OBJECT as accessed when details shown, or not?
  (setf (md::accessed-at object) (get-universal-time))
  (when (not (typep object 'md::collection))
    (md::create-object-view object *details-panel*)
    (create-div *details-panel* :style "clear:both;"))
  (mold-desktop/clog-gui::create-halos-bar object *details-panel*)
  (md::-> (md::create-object-settings-view object *details-panel*)
          (md::set-styles '(("border-top" "1px solid gray")
                            ("margin-top" "20px"))))

  (remove-class *details-panel* "w3-hide"))

(defun navigate-to-objects-with-type (type)
  (let ((objects (make-instance 'md::collection
                                :name (format nil "Objects of type ~a" type)
                                :members (remove-if-not (lambda (obj)
                                                        (typep obj type))
                                                      md::*objects*))))
    (setf (text md::*app-body*) "")
    (create-div md::*app-body* :style "width:25vw;float:right;height:100%;" :content "&nbsp;") ;; hack
    (setf *details-panel* (create-div md::*app-body* :class "details-panel w3-hide w3-card w3-padding"))
    (show-object-details objects)
    (md::->
     (md::create-thumbnails-view objects md::*app-body*
                                :item (list :on-click
                                            (lambda (obj &rest args)
                                              (declare (ignore args))
                                              (show-object-details obj)))
                                :thumbnail-size "wide")
     (add-class "w3-padding"))))

(defun navigate-to-collection (collection)
  (setf (text md::*app-body*) "")
  (create-div md::*app-body* :style "width:25vw;float:right;height:100%;" :content "&nbsp;") ;; hack
  (setf *details-panel* (create-div md::*app-body* :class "details-panel w3-hide w3-card w3-padding"))
  (show-object-details collection)
  (md::->
   (md::create-thumbnails-view collection md::*app-body*
                              :item (list :on-click
                                          (lambda (obj &rest args)
                                            (declare (ignore args))
                                            (show-object-details obj)))
                              :thumbnail-size "wide")
   (add-class "w3-padding")))

(defun create-top-categories-list (clog-obj)
  (let ((ul (create-unordered-list clog-obj :class "w3-ul w3-hoverable")))
    (set-styles ul '(("max-height" "200px") ("overflow-y" "scroll")))
    (dolist (class (sort (closer-mop:class-direct-subclasses (find-class 'md::object))
                         'string< :key (alex:compose 'symbol-name 'class-name)))
      (let ((li (create-list-item ul)))
        (when (md::class-icon class)
          (let ((icon (md::create-icon (md::class-icon class) li)))
            (when icon
              (set-styles icon '(("margin-right" "5px"))))))
        (create-span li :content (md::humanized-symbol-name (class-name class)))
        (set-on-click li (md::lambda* (_)
                           (navigate-to-objects-with-type (class-name class))))))
    ul))
