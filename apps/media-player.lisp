(defpackage :md/media-player
  (:local-nicknames
   (:md :mold-desktop)
   (:alex :alexandria))
  (:use :cl :clog :clog-gui))

(in-package :md/media-player)

(defvar *details-panel*)
(defvar *collections* nil
  "The user's collections.")

(defun initialize-collections ()
  (setf *collections* nil)
  (md::push-end (make-instance 'md::collection
                               :name "Most recent"
                               :members (lambda () md::*objects*)
                               :icon "fa-solid fa-clock")
                *collections*)
  (md::push-end (make-instance 'md::collection
                               :name "Not seen"
                               :members (lambda () (remove-if #'md::accessed-at md::*objects*))
                               :icon "fa fa-eye")
                *collections*)
  (md::push-end (make-instance 'md::collection
                               :name "Favourites"
                               :members nil
                               :icon "fa fa-star")
                *collections*))

(defclass media-player-workspace (md::mold-workspace)
  ()
  (:default-initargs
   :name "Media Player"
   :icon "fa-solid fa-play")
  (:documentation "Workspace with user interface for managing media."))

(defmethod md::open-workspace ((workspace media-player-workspace) body)
  (initialize-collections)
  (let* ((app-main (create-div body :class "app-main"))
         (settings-panel (apply #'md::create-side-panel app-main (alexandria:assoc-value md::*config* :settings-panel)))
         (navigation-bar (apply #'create-navigation-bar app-main (alexandria:assoc-value md::*config* :navigation-bar)))
         (app-body (create-div app-main :class "app-body"))
         (details-panel (create-div app-body :class "details-panel w3-hide"))
         (command-bar (apply #'md::create-command-bar body (alexandria:assoc-value md::*config* :command-bar))))

    (let ((app (clog-gui::connection-data-item body "clog-gui")))
      (setf (mold-desktop/clog-gui::app-body app) app-body)
      (setf (connection-data-item body "clog-app-body") app-body))

    (setf md::*app-body* app-body)
    (setf *details-panel* details-panel)

    ;; Handle drag and drop urls from browser's windows/tabs
    (setf (attribute app-body "ondragover") "return false")
    (set-on-drag-over app-body (md::lambda* (_) nil))
    (set-on-drop app-body (lambda (obj event)
                            (declare (ignore obj))
                            (let ((maybe-url (getf event :drag-data)))
                              (when (or (str:starts-with? "http://" maybe-url)
                                        (str:starts-with? "https://" maybe-url))
                                (md::show (mold-desktop/commands::add-object-from-url maybe-url))))))

    (dolist (init-function md::*init-functions*)
      (funcall init-function body))

    (md::init-window-manager md::*window-manager* app-body)

    (setf md::*side-panel* settings-panel)
    (md::init-side-panel settings-panel body app-body)

    (setf md::*command-bar* command-bar)
    (md::init-command-bar command-bar body app-body)

    (navigate-to-collection (first *collections*))

    (set-on-before-unload (window body) (lambda(obj)
                                          (declare (ignore obj))
                                          ;; return empty string to prevent nav off page
                                          ""))))

(defclass navigation-bar (clog-element)
  (;; Model
   (search-input :accessor search-input)
   (categories :initarg :categories
               :accessor categories
               :documentation "Objects categories tree.")
   (collections :initarg :collections
                :accessor collections
                :documentation "User's collections.")
   (spaces :initarg :spaces
           :accessor spaces
           :documentation "User's spaces")
   ;; UI
   (categories-view :accessor categories-view)
   (collections-view :accessor collections-view)
   (spaces-view :accessor spaces-view)))

(defun refresh-collections-view (navigation-bar)
  (setf (text (collections-view navigation-bar)) "")
  (dolist (col *collections*)
    (let ((li (create-list-item (collections-view navigation-bar))))
      (when (md::object-icon col)
        (md::create-icon (md::object-icon col) li))
      (create-span li :content (md::object-name col) :style "margin-left:8px;")
      (set-on-click li (md::lambda* (_)
                         (navigate-to-collection col)))
      (when (listp (slot-value col 'md::members))
        ;; We assume collection is mutable looking at the ITEMS slot

        ;; To enable drop, drag-over event handler needs to be set:
        (set-on-drag-over li (lambda (obj)
                               (declare (ignore obj))
                               (add-class li "drop-allowed")))
        (set-on-drag-leave li
                           (lambda (obj)
                             (declare (ignore obj))
                             (remove-class li "drop-allowed")))
        (set-on-drop li (lambda (&rest args)
                          (push mold-desktop/clog-gui::*dragging-object* (md::members col))
                          (setf mold-desktop/clog-gui::*dragging-object* nil)
                          (remove-class li "drop-allowed")))
        ))))

(defun show-space (space)
  (setf (text md::*app-body*) "")
  (dolist (object (md::members space))
    (md::show object)))

(defun create-navigation-bar (clog-obj &rest initargs)
  (let ((navigation-bar (create-div clog-obj :class "mold-navigation-bar w3-bar w3-card-4")))
    (apply #'change-class navigation-bar 'navigation-bar initargs)
    (setf (search-input navigation-bar)
          (create-child navigation-bar (md::html (:input :type "text"
                                                         :class "w3-input w3-border"
                                                         :placeholder "Search ..."))))
    ;; Create search collection on search
    (set-on-key-press (search-input navigation-bar)
                      (md::lambda* (_ event)
                        (when (string= (getf event :key) "Enter")
                          (let* ((search-term (md::element-value (search-input navigation-bar)))
                                 (collection (make-instance 'collection
                                                            :name (format nil "Search: ~a" search-term)
                                                            :icon "fa-solid fa-magnifying-glass"
                                                            :members (lambda ()
                                                                     (remove-if-not (lambda (obj)
                                                                                      (or (search search-term (md::object-name obj) :test #'equalp)
                                                                                          (search search-term (md::object-description obj) :test #'equalp)))
                                                                                    md::*objects*)))))
                            (md::push-end collection *collections*)
                            (refresh-collections-view navigation-bar)))))

    (let ((toggle-categories
            (create-button navigation-bar :class "w3-button w3-block w3-left-align w3-light-gray")))
      (md::-> (md::create-icon "core-grid-sm" toggle-categories)
              (md::set-styles '(("font-size" "2rem"))))
      (create-span toggle-categories :content "Categories" :style "vertical-align:super;")
      (let ((categories (create-top-categories-list navigation-bar)))
        (set-on-click toggle-categories (lambda (&rest args)
                                          (declare (ignore args))
                                          (toggle-class categories "w3-hide")))))

    (let ((toggle-collections
            (create-button navigation-bar :class "w3-button w3-block w3-left-align w3-light-gray")))

      (md::-> (md::create-icon "core-grid-list" toggle-collections)
              (md::set-styles '(("font-size" "2rem"))))
      (create-span toggle-collections :content "Collections" :style "vertical-align:super;")

      (let ((collections (create-unordered-list navigation-bar :class "w3-ul w3-hoverable")))
        (setf (collections-view navigation-bar) collections)

        (refresh-collections-view navigation-bar)

        (set-on-click toggle-collections (lambda (&rest args)
                                           (declare (ignore args))
                                           (toggle-class collections "w3-hide")))))

    navigation-bar))

(defun show-object-details (object)
  (setf (text *details-panel*) "")
  ;; REVIEW. Mark OBJECT as accessed when details shown, or not?
  (setf (md::accessed-at object) (get-universal-time))
  (md::create-object-view object *details-panel*)
  (create-div *details-panel* :style "clear:both;")
  (md::-> (md::create-object-settings object *details-panel*)
          (md::set-styles '(("border-top" "1px solid gray")
                            ("margin-top" "20px"))))

  (remove-class *details-panel* "w3-hide"))

(defun navigate-to-objects-with-type (type)
  (let ((objects (make-instance 'md::collection
                                :members (remove-if-not (lambda (obj)
                                                        (typep obj type))
                                                      md::*objects*))))
    (setf (text md::*app-body*) "")
    (create-div md::*app-body* :style "width:25vw;float:right;height:100%;" :content "&nbsp;") ;; hack
    (setf *details-panel* (create-div md::*app-body* :class "details-panel w3-hide w3-card w3-padding"))
    (md::->
     (md::create-thumbnail-view objects md::*app-body*
                                :item (list :on-click
                                            (lambda (obj &rest args)
                                              (declare (ignore args))
                                              (show-object-details obj)))
                                :thumbnail-size "wide")
     (add-class "w3-padding"))))

(defun navigate-to-collection (collection)
  (setf (text md::*app-body*) "")
  (create-div md::*app-body* :style "width:25vw;float:right;height:100%;" :content "&nbsp;") ;; hack
  (setf *details-panel* (create-div md::*app-body* :class "details-panel w3-hide w3-card w3-padding"))
  (md::->
   (md::create-thumbnail-view collection md::*app-body*
                              :item (list :on-click
                                          (lambda (obj &rest args)
                                            (declare (ignore args))
                                            (show-object-details obj)))
                              :thumbnail-size "wide")
   (add-class "w3-padding")))

(defun create-top-categories-list (clog-obj)
  (let ((ul (create-unordered-list clog-obj :class "w3-ul w3-hoverable")))
    (set-styles ul '(("max-height" "200px") ("overflow-y" "scroll")))
    (dolist (class (sort (closer-mop:class-direct-subclasses (find-class 'md::object))
                         'string< :key (alex:compose 'symbol-name 'class-name)))
      (let ((li (create-list-item ul)))
        (when (md::class-icon class)
          (let ((icon (md::create-icon (md::class-icon class) li)))
            (when icon
              (set-styles icon '(("margin-right" "5px"))))))
        (create-span li :content (md::humanized-symbol-name (class-name class)))
        (set-on-click li (md::lambda* (_)
                           (navigate-to-objects-with-type (class-name class))))))
    ul))
