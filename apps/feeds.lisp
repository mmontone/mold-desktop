(defpackage :md/feeds
  (:local-nicknames
   (:md :mold-desktop)
   (:alex :alexandria)
   (:acc :access))
  (:use :cl :clog :clog-gui))

(in-package :md/feeds)

(defvar *details-panel*)

(defparameter *default-collections*
  (list (make-instance 'md::collection
                       :name "Most recent"
                       :members (lambda () md::*objects*)
                       :icon "fa-solid fa-clock")
        (make-instance 'md::collection
                       :name "Not seen"
                       :members (lambda () (remove-if #'md::accessed-at md::*objects*))
                       :icon "fa fa-eye")
        (make-instance 'md::collection
                       :name "Favourites"
                       :members nil
                       :icon "fa fa-star")))

(defun all-available-feeds ()
  "The default list of feeds is every object in *OBJECTS* of type RSS-FEED."
  (remove-if-not (lambda (object)
                   (typep object 'md::rss-feed))
                 md::*objects*))


(defclass feeds-workspace (md::mold-workspace)
  ((feeds :accessor feeds
          :initform (all-available-feeds))
   (collections :accessor collections
                :initform *default-collections*))
  (:default-initargs
   :name "Feeds"
   :icon "fa-solid fa-rss")
  (:documentation "Workspace with user interface for managing feeds."))

(defmethod md::open-workspace ((workspace feeds-workspace) body)
  (let* ((app-main (create-div body :class "app-main side-bar-workspace workspace feeds-workspace"))
         (navigation-bar (apply #'create-navigation-bar app-main (alexandria:assoc-value md::*config* :navigation-bar)))
         (app-body (create-div app-main :class "app-body"))
         (details-panel (create-div app-body :class "details-panel w3-hide")))

    (let ((app (clog-gui::connection-data-item body "clog-gui")))
      (setf (mold-desktop/clog-gui::app-body app) app-body)
      (setf (connection-data-item body "clog-app-body") app-body))

    (setf (md::workspace-elem workspace) app-main)
    (setf md::*app-body* app-body)
    (setf *details-panel* details-panel)

    ;; Handle drag and drop urls from browser's windows/tabs
    (setf (attribute app-body "ondragover") "return false")
    (set-on-drag-over app-body (md::lambda* (_) nil))
    (set-on-drop app-body (lambda (obj event)
                            (declare (ignore obj))
                            (let ((maybe-url (getf event :drag-data)))
                              (when (or (str:starts-with? "http://" maybe-url)
                                        (str:starts-with? "https://" maybe-url))
                                (md::show (mold-desktop/commands::add-object-from-url maybe-url))))))

    ;;(navigate-to-collection (first *collections*))

    (set-on-before-unload (window body) (lambda(obj)
                                          (declare (ignore obj))
                                          ;; return empty string to prevent nav off page
                                          ""))
    app-main))

(defclass navigation-bar (clog-element)
  (;; Model
   (search-input :accessor search-input)
   (categories :initarg :categories
               :accessor categories
               :documentation "Objects categories tree.")
   (collections :initarg :collections
                :accessor collections
                :documentation "User's collections.")
   (spaces :initarg :spaces
           :accessor spaces
           :documentation "User's spaces")
   ;; UI
   (feeds-list-view :accessor feeds-list-view)
   (categories-view :accessor categories-view)
   (collections-view :accessor collections-view)
   (spaces-view :accessor spaces-view)))

(defun refresh-collections-view (navigation-bar)
  (setf (text (collections-view navigation-bar)) "")
  (dolist (col (collections md::*workspace*))
    (let ((li (create-list-item (collections-view navigation-bar))))
      (when (md::object-icon col)
        (md::create-icon (md::object-icon col) li))
      (create-span li :content (md::object-name col) :style "margin-left:8px;")
      (set-on-click li (md::lambda* (_)
                         (navigate-to-collection col)))
      (when (listp (slot-value col 'md::members))
        ;; We assume collection is mutable looking at the ITEMS slot

        ;; To enable drop, drag-over event handler needs to be set:
        (set-on-drag-over li (lambda (obj)
                               (declare (ignore obj))
                               (add-class li "drop-allowed")))
        (set-on-drag-leave li
                           (lambda (obj)
                             (declare (ignore obj))
                             (remove-class li "drop-allowed")))
        (set-on-drop li (lambda (&rest args)
                          (push mold-desktop/clog-gui::*dragging-object* (md::members col))
                          (setf mold-desktop/clog-gui::*dragging-object* nil)
                          (remove-class li "drop-allowed")))
        ))))

(defun show-space (space)
  (setf (text md::*app-body*) "")
  (dolist (object (md::members space))
    (md::show object)))

(defun create-navigation-bar (clog-obj &rest initargs)
  (let ((navigation-bar (create-div clog-obj :class "mold-navigation-bar w3-bar w3-card-4")))
    (apply #'change-class navigation-bar 'navigation-bar initargs)
    (setf (search-input navigation-bar)
          (create-child navigation-bar (md::html (:input :type "text"
                                                         :class "w3-input w3-border"
                                                         :placeholder "Search or RSS url..."))))
    ;; Create search collection on search
    (set-on-key-press (search-input navigation-bar)
                      (md::lambda* (_ event)
                        (when (string= (getf event :key) "Enter")
                          (let ((search-input (md::element-value (search-input navigation-bar))))
                            (cond
                              ((or (str:starts-with-p "http://" search-input )
                                   (str:starts-with-p "https://" search-input))
                               ;; Check it is an RSS feed and add as feed
                               (let ((rss-feed (md::parse-rss-url search-input)))
                                 (when rss-feed
                                   (push rss-feed md::*objects*)
                                   (push rss-feed (feeds md::*workspace*))
                                   (refresh-feeds-list-view navigation-bar)
                                   (navigate-to-feed rss-feed))))
                              (t ;; A search term
                               (let ((collection (make-instance 'md::collection
                                                                :name (format nil "Search: ~a" search-input)
                                                                :icon "fa-solid fa-magnifying-glass"
                                                                :members (lambda ()
                                                                           (remove-if-not (lambda (obj)
                                                                                            (or (search search-input (md::object-name obj) :test #'equalp)
                                                                                                (search search-input (md::object-description obj) :test #'equalp)))
                                                                                          md::*objects*)))))
                                 (md::push-end collection (collections md::*workspace*))
                                 (refresh-collections-view navigation-bar))))))))

    (let ((toggle-feeds
            (create-button navigation-bar :class "w3-button w3-block w3-left-align w3-light-gray")))
      (md::-> (md::create-icon "core-laptop-code" toggle-feeds)
              (md::set-styles '(("font-size" "2rem"))))
      (create-span toggle-feeds :content "Feeds" :style "vertical-align:super;")
      (with-slots (feeds-list-view) navigation-bar
        (setf feeds-list-view (create-feeds-list navigation-bar))
        (set-on-click toggle-feeds (lambda (&rest args)
                                     (declare (ignore args))
                                     (toggle-class feeds-list-view "w3-hide")))))

    (let ((toggle-collections
            (create-button navigation-bar :class "w3-button w3-block w3-left-align w3-light-gray")))

      (md::-> (md::create-icon "core-grid-list" toggle-collections)
              (md::set-styles '(("font-size" "2rem"))))
      (create-span toggle-collections :content "Collections" :style "vertical-align:super;")

      (let ((collections (create-unordered-list navigation-bar :class "w3-ul w3-hoverable")))
        (setf (collections-view navigation-bar) collections)

        (refresh-collections-view navigation-bar)

        (set-on-click toggle-collections (lambda (&rest args)
                                           (declare (ignore args))
                                           (toggle-class collections "w3-hide")))))

    navigation-bar))

(defun show-object-details (object &optional (view #'md::create-object-view))
  (setf (text *details-panel*) "")
  ;; REVIEW. Mark OBJECT as accessed when details shown, or not?
  (setf (md::accessed-at object) (get-universal-time))
  (when view
    (funcall view object *details-panel*))
  (create-div *details-panel* :style "clear:both;")
  (mold-desktop/clog-gui::create-halos-bar object *details-panel*)
  (md::-> (md::create-object-settings-view object *details-panel*)
          (md::set-styles '(("border-top" "1px solid gray")
                            ("margin-top" "20px"))))

  (remove-class *details-panel* "w3-hide"))

(defun navigate-to-feed (feed)
  (setf (text md::*app-body*) "")
  (create-div md::*app-body* :style "width:25vw;float:right;height:100%;" :content "&nbsp;") ;; hack
  (setf *details-panel* (create-div md::*app-body* :class "details-panel w3-hide w3-card w3-padding"))
  (show-object-details feed nil)
  (md::->
   (md::create-thumbnails-view feed md::*app-body*
                               :item (list :on-click
                                           (lambda (obj &rest args)
                                             (declare (ignore args))
                                             (show-object-details obj)))
                               :thumbnail-size "wide")
   (add-class "w3-padding")))

(defun navigate-to-collection (collection)
  (setf (text md::*app-body*) "")
  (create-div md::*app-body* :style "width:25vw;float:right;height:100%;" :content "&nbsp;") ;; hack
  (setf *details-panel* (create-div md::*app-body* :class "details-panel w3-hide w3-card w3-padding"))
  (md::->
   (md::create-thumbnails-view collection md::*app-body*
                               :item (list :on-click
                                           (lambda (obj &rest args)
                                             (declare (ignore args))
                                             (show-object-details obj)))
                               :thumbnail-size "wide")
   (add-class "w3-padding")))

(defun create-feeds-list (clog-obj)
  (let ((ul (create-unordered-list clog-obj :class "w3-ul w3-hoverable")))
    (set-styles ul '(("max-height" "200px") ("overflow-y" "scroll")))
    (dolist (feed (feeds md::*workspace*))
      (let ((li (create-list-item ul)))
        (when (md::object-icon feed)
          (let ((icon (md::create-icon (md::object-icon feed) li)))
            (when icon
              (set-styles icon '(("margin-right" "5px"))))))
        (create-span li :content (md::humanized-symbol-name (md::object-name feed)))
        (set-on-click li (md::lambda* (_)
                           (navigate-to-feed feed)))))
    ul))

(defun refresh-feeds-list-view (navigation-bar)
  (with-slots (feeds-list-view) navigation-bar
    (setf (text feeds-list-view) "")
    (dolist (feed (feeds md::*workspace*))
      (let ((li (create-list-item feeds-list-view)))
        (when (md::object-icon feed)
          (let ((icon (md::create-icon (md::object-icon feed) li)))
            (when icon
              (set-styles icon '(("margin-right" "5px"))))))
        (create-span li :content (md::humanized-symbol-name (md::object-name feed)))
        (set-on-click li (md::lambda* (_)
                           (navigate-to-feed feed)))))
    feeds-list-view))
