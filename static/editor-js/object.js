/**
 * Tool for creating image Blocks for Editor.js
 */
class MoldObject {
    /**
     * Tool class constructor
     * @param {ImageToolData} data — previously saved data
     * @param {object} api — Editor.js Core API {@link  https://editorjs.io/api}
     * @param {ImageToolConfig} config — custom config that we provide to our tool's user
     */
    constructor({data, api, config}){
	this.config = config || {};
        this.data = data;
        this.wrapper = undefined;
    }

    /**
     * Return a Tool's UI
     * @return {HTMLElement}
     */
    render () {
	this.wrapper = document.getElementById(this.data.htmlId);
	this.wrapper.parentNode.removeChild(this.wrapper);

	return this.wrapper;
    }

    /**
     * Extract data from the UI
     * @param {HTMLElement} blockContent — element returned by render method
     * @return {SimpleImageData}
     */
    save(blockContent){
        const image = blockContent.querySelector('img');
        const caption = blockContent.querySelector('[contenteditable]');

        return Object.assign(this.data, {
            url: image.src,
            caption: caption.innerHTML || ''
        });
    }    
}
