const Embed = Quill.import("blots/embed");
const Delta = Quill.import("delta");

const MODULE_SOURCE = "module/mold";


/**
 * Given a delta searchs if it contains an insert op and returns its value.
 * @param {Quill Delta} delta
 */
const getInsertFromDelta = ({ ops }) => {
    const insertOp = ops.find(op => op.insert && op.insert !== "");
    if (insertOp) return insertOp.insert;
    return null;
};

/**
 * Given a delta searchs if it contains an insert op and returns its value.
 * @param {Quill Delta} delta
 */
const getRetainFromDelta = ({ ops }) => {
    const insertOp = ops.find(op => op.retain);
    if (insertOp) return insertOp.retain;
    return 0;
};

/**
 * Given an array of delta ops, returns TRUE if its an insert op,
 * false otherwise.
 * @param {Quill Delta} ops
 */
const deltaIsInsert = ({ ops }) => {
    const opsLength = ops.length;

    let insertOp;

    if (opsLength === 1) {
        insertOp = ops[0].insert;
    } else if (opsLength > 1) {
        insertOp = ops[1].insert;
    }

    // Checks if insertOp exists and it's a string
    return insertOp && typeof insertOp === "string";
};

/**
 *
 */
const getDeltaInsertIndex = ({ ops }) => {
    if (ops.length === 1) return 0;
    if (ops.length > 1) return ops[0].retain;
};


//---------------------------------------------------------------------

class MoldObjectBlot extends Embed {
    static create(data) {
	const node = super.create(data);

	console.log('Node', node);
	console.log('Htmlid', data.htmlId);
	console.log('Obj', document.getElementById(data.htmlId));

	node.appendChild(document.getElementById(data.htmlId));
        return node;
    }

    static value(domNode) {
	// FIXME ?
        return domNode.getAttribute("id");
    }
}

MoldObjectBlot.blotName = "moldObject";
MoldObjectBlot.tagName = "div";
MoldObjectBlot.className = "mold-object";

Quill.register(MoldObjectBlot, true);

// Quill.on("text-change", onTextChange);

// function onTextChange (delta, oldDelta, source)  {
//     if (quill) {
//         console.log(JSON.stringify(quill.getContents().ops));
//         console.log(quill.getLength());
//     }
// }

// Insert a MoldObject view into quill component.
// data has id of mold object, and htmlId of the view element.
function quillInsertMoldObject (quill, data) {

    console.log(quill, data);
    
    var selection = quill.getSelection();
    var selectionIndex = selection ? selection.index : 0; 

    quill.insertEmbed(
        selectionIndex,
        "moldObject",
        {
            index: 0,
            id: data.id,
	    htmlId: data.htmlId
        },
        "api",
    );

    quill.setSelection(selectionIndex + 1,0 );
}
