function initCircularMenu(menu) {
    var radius = 35;
    let items = menu.querySelectorAll('.circle a');
    let circle = menu.querySelector('.circle');

    for(var i = 0, l = items.length; i < l; i++) {
        items[i].style.left = (50 - radius*Math.cos(-0.5 * Math.PI - 2*(1/l)*i*Math.PI)).toFixed(4) + "%";

        items[i].style.top = (50 + radius*Math.sin(-0.5 * Math.PI - 2*(1/l)*i*Math.PI)).toFixed(4) + "%";
    }

    let menubutton = menu.querySelector('.menu-button');
    let timeout = null;

    menubutton.onmouseover = function(e) {
        e.preventDefault();

        // Set timeout to be a timer which will invoke callback after 1s
        timeout = setTimeout(function () {
            circle.classList.add('open');
            circle.parentElement.style.zIndex = 999;
            circle.onmouseleave = function (e) {
                circle.parentElement.style.zIndex = null;
                circle.classList.remove('open')
            }
        }, 400);
    };
    menubutton.onmouseout = function (e) {
        clearTimeout(timeout);
    };

    let buttons = menu.querySelectorAll('.circle a');
    for(var i = 0; i < buttons.length; i++) {
        buttons[i].onmouseup = function(e) {
            circle.classList.remove('open');
        }
    }
}

/* Treeviews */

function initTreeView(el) {
    var toggler = el.getElementsByClassName("caret");
    var i;

    for (i = 0; i < toggler.length; i++) {
	toggler[i].addEventListener("click", function() {
	    this.parentElement.querySelector(".nested").classList.toggle("active");
	    this.classList.toggle("caret-down");
	});
    }
}

/* Halos */

function initHalos(halosWindow) {
    /*
    halosWindow.addEventListener('mouseover', function (ev) {
	if (ev.ctrlKey) {
	    halosWindow.classList.add('active');
	}
	else {
	    halosWindow.classList.remove('active');
	}	
	});
    */

    // halosWindow.addEventListener('contextmenu', function (ev) {
    // 	halosWindow.classList.add('active');
    // });

    // halosWindow.addEventListener('mouseleave', function (ev) {
    // 	halosWindow.classList.remove('active');
    // });
    
}
