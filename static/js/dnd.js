/* Drag an object using a "ghost" UI element */

function dragStartObject (e) {
    var elem = document.createElement("div");
    elem.id = "drag-ghost";
    elem.className = "drag-ghost";
    elem.textNode = "Dragging";
    elem.textContent = e.target.getAttribute('data-drag-title');
    document.body.appendChild(elem);
    e.dataTransfer.setDragImage(elem, 0, 0);
    e.dataTransfer.setData('text/plain',e.target.getAttribute('data-drag-obj'));
    console.log('Drag start', e, 'elem', elem);
}

document.addEventListener("dragstart", function (e) {
    dragStartObject(e);
});

document.addEventListener("dragend", function(e) {
    // Remove the created ghost elem on dragend
    console.log('Drag end', e);
    var ghost = document.getElementById("drag-ghost");
    if (ghost.parentNode) {
        ghost.parentNode.removeChild(ghost);
    }
});
