/* Communication */

var Neomacs = {};
var RemoteJS = {};

RemoteJS.send = function(data) {
    console.log('Neomacs Send', data);
    // Clog websocket
    ws.send('neomacs:', data);
};

/* Windows */

Neomacs.encodeKeyEvent = function (event, bufferID) {
    return JSON.stringify({inputEvent: {'type': 'keyDown',
                                        'key': event.key,
                                        'code': event.code,
                                        'isAutoRepeat': event.repeat,
                                        'isComposing': event.isComposing,
                                        'shift': event.shiftKey,
                                        'control': event.ctrlKey,
                                        'alt': event.altKey,
                                        'meta': event.metaKey,
                                        'location': event.location,
                                        'modifiers': event.modifiers
                                       },
                           buffer: bufferID
                          });
}

Neomacs.buffers = {};

Neomacs.generateId = function (dict) {
    for(let i = -1;;i--){
        const id = i.toString();
        if(!(id in dict)) return id;
    }
};

Neomacs.createBuffer = function(id, options) {
    let body = document.getElementsByTagName('body')[0];
    let buf = document.createElement('div');
    buf.setAttribute('neomacs-identifier', id);
    buf.setAttribute('contentEditable', 'true');
    // Hide the browser's text cursor. Show Neomacs cursor only.
    buf.setAttribute('style', 'caret-color:transparent;');
    body.appendChild(buf);
    buf.addEventListener('keydown', (event) => {
        // https://www.electronjs.org/docs/latest/api/web-contents#event-before-input-event
        // {"inputEvent":{"type":"keyDown","key":"Backspace","code":"Backspace","isAutoRepeat":false,"isComposing":false,"shift":false,"control":false,"alt":false,"meta":false,"location":0,"_modifiers":0,"modifiers":[]},"buffer":"3"}
        RemoteJS.send(JSON.stringify({inputEvent: {'type': 'keyDown',
                                                   'key': event.key,
                                                   'code': event.code,
                                                   'isAutoRepeat': event.repeat,
                                                   'isComposing': event.isComposing,
                                                   'shift': event.shiftKey,
                                                   'control': event.controlKey,
                                                   'alt': event.altKey,
                                                   'meta': event.metaKey,
                                                   'location': event.location,
                                                   'modifiers': event.modifiers
                                                  }, buffer: id}));
        event.preventDefault();
    });
    buf.addEventListener('focus',() => {
        // avoid dead loop: lisp => webContents.focus => on 'focus' => 'focus' event (lisp) => lisp
        RemoteJS.send(JSON.stringify({inputEvent: {type: "focus"}, buffer: id}));});
    Neomacs.buffers[id] = buf;
    RemoteJS.send(JSON.stringify({inputEvent: {type: "dom-ready"}, buffer: id}));
    return buf;
};

Neomacs.closeBuffer = function(id) {
    Neomacs.buffers[id].remove();
    Neomacs.buffers[id] = null;
};

/* Start up */

function setAction(elID, actionID) {
    let el = document.getElementById(elID);
    el.addEventListener('click', (ev) => {
        RemoteJS.send(JSON.stringify({inputEvent: {type: "ipc", details: {type: "action", id: actionID}}}));
    });
    // Prevent the button getting the focus
    el.addEventListener('mousedown', (ev) => ev.preventDefault());
}

/*
  window.addEventListener("load", function () {
  setAction('bold', 'bold');
  setAction('italic', 'italic');
  setAction('undo', 'undo');
  setAction('li', 'li');
  setAction('eval-last-expression', 'eval-last-expression');
  // Start the WebSockets client
  Neomacs.startWebSockets("localhost", 9091);
  });
*/
