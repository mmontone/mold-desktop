(in-package :mold-desktop)

(deftype color ()
  `(or cl-colors2:rgb cl-colors2:hsv))

(deftype color-name ()
  `string)

(defmethod mold-desktop::prompt-command-argument-type
    ((arg-type (eql 'color)) arg command-bar handler)
  (mold-desktop::command-bar-choose
   (format nil "~a: " (mold-desktop::format-argname (getf arg :name)))
   (mapcar (lambda (c)
             (list :label (car c)
                   :value (symbol-value (cdr c))
                   :description (format nil "~a color" (car c))))
           cl-colors2:*x11-colors-list*)
   handler command-bar
   :close nil))

#+test
(prompt-command-argument '(:name "Color" :type color)
			 *command-bar* #'show )

(defmethod mold-desktop::prompt-command-argument-type
    ((arg-type (eql 'color-name)) arg command-bar handler)
  (mold-desktop::command-bar-choose
   (format nil "~a: " (mold-desktop::format-argname (getf arg :name)))
   (mapcar (lambda (c)
             (list :label (car c)
                   :value (car c)
                   :description (format nil "~a color" (car c))))
           cl-colors2:*x11-colors-list*)
   handler command-bar
   :close nil))

#+test
(prompt-command-argument '(:name "Color" :type color-name)
			 *command-bar* #'show )
