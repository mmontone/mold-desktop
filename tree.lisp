;; Copyright (C) 2022 Mariano Montone - All Rights Reserved
;; You may use, distribute and modify this code under the terms of the
;; GNU Affero General Public License

(in-package :mold-desktop)

;; https://www.w3.org/TR/wai-aria-practices-1.1/examples/treeview/treeview-2/treeview-2b.html
;; https://www.w3schools.com/howto/tryit.asp?filename=tryhow_js_treeview

;;(defun init-tree-component (body))

;;(pushnew 'init-tree-component *init-functions*)

;; tree-node api
;; implement for every object so that any object can be used as a tree-object node.
(defgeneric tree-node-label (object)
  (:documentation "Label for OBJECT to use in a tree."))
(defgeneric tree-node-children (object)
  (:documentation "Children of OBJECT as a tree-node."))
(defgeneric tree-node-icon (object)
  (:documentation "Icon of OBJECT as a tree node."))
(defgeneric tree-node-add (tree-node object)
  (:documentation "Add OBJECT to children of TREE-NODE."))
(defgeneric tree-node-leaf-p (object)
  (:documentation "Returns if OBJECT is a leaf or not."))
(defgeneric tree-node-value (object)
  (:method (obj)
    obj)
  (:documentation "Returns the node value (for when working with tree-detail-views for example.)
No need to specialize for the common case."))

(defmethod tree-node-icon (object)
  nil)
(defmethod tree-node-children (object)
  (members object))
(defmethod tree-node-leaf-p (object)
  "Default implementation looks at OBJECT children.
For lazy loading, specialize this and return NIL."
  (gcl:emptyp (tree-node-children object)))
(defmethod tree-node-leaf-p ((coll collection))
  nil)

(defmethod tree-node-add (tree-node object)
  nil)

(defmethod tree-node-label ((tree-node cons))
  (first tree-node))
(defmethod tree-node-children ((tree-node cons))
  (rest tree-node))
(defmethod tree-node-add ((tree-node cons) obj)
  (push-end tree-node obj))

(defmethod tree-node-label (object)
  (princ-to-string object))

(defmethod tree-node-label ((object object))
  (object-title object))

(defmethod tree-node-children ((coll collection))
  (members coll))
(defmethod tree-node-add ((coll collection) obj)
  (mold-desktop::collection-add coll obj))
(defmethod tree-node-icon ((object object))
  (object-icon object))

;; -- tree api for hash-tables

(defstruct hash-table-entry
  key value)

(defmethod tree-node-label ((entry hash-table-entry))
  (princ-to-string (hash-table-entry-key entry)))
(defmethod tree-node-children ((entry hash-table-entry))
  (tree-node-children (hash-table-entry-value entry)))
(defmethod tree-node-value ((entry hash-table-entry))
  (hash-table-entry-value entry))
(defmethod object-icon ((entry hash-table-entry))
  "fa-solid fa-key")
(defmethod object-icon ((hash-table hash-table))
  "fa-solid fa-table")

(defmethod tree-node-children ((hash-table hash-table))
  (serapeum:maphash-return
   (lambda (key value)
     (make-hash-table-entry :key key :value value))
   hash-table))

(defclass tree-object (object)
  ((nodes :initarg :nodes
          :accessor tree-node-children
          :initform nil
          :documentation "The nodes of the tree. An alist with format
(object . children) "))
  (:metaclass mold-object-class)
  (:documentation "A object with nodes in a tree."))

(defmethod create-object-view ((object tree-object) clog-obj &rest args)
  (apply 'create-tree-view object clog-obj args))

#+example
(open-object-in-window
 (make-instance 'tree-object
                :nodes (list (cons "First 10"
                                   (subseq *objects* 0 10))
                             (cons "Second 10"
                                   (subseq *objects* 10 20))))
 *body*)

#+example
(open-object-in-window
 (make-instance 'tree-object
                :nodes (list (cons "First 10"
                                   (subseq *objects* 500 510))
                             (cons "Second 10"
                                   (subseq *objects* 510 520))))
 *body* #'mold-desktop::create-object-view
 :on-click (lambda (obj) (when (not (listp obj))
                      (open-object-in-window obj *body*))))

(defun make-typed-tree (types &optional collection)
  "Build a TREE-OBJECT with TYPES as categories."
  (let ((actual-collection (or collection
                               (make-instance 'collection
                                              :members *objects*)))
        (tree-nodes))
    (dolist (type types)
      (push (cons (humanized-symbol-name (symbol-name type))
                  (remove-if-not (lambda (object)
                                   (typep object type))
                                 (members actual-collection)))
            tree-nodes))
    (make-instance 'tree-object :nodes tree-nodes)))

#+example
(open-object-in-window (make-typed-tree '(book video chess-game))
                       *body*
                       #'create-object-view)

(defun categorize-collection (collection category-extractor)
  "Create a tree from COLLECTION, using CATEGORY-EXTRACTOR function to extract the categories."
  (let ((categories (make-hash-table)))
    (dolist (item (members collection))
      (let ((category (funcall category-extractor item)))
        (push item (gethash category categories))))
    (alexandria:hash-table-alist categories)))

#+example
(open-object-in-window
 (make-instance 'tree-object
                :nodes
                (categorize-collection (make-instance 'collection :members *objects*)
                                       'type-of))
 *body*)

#+example
(open-object-in-window
 (make-instance 'tree-object
                :nodes
                (categorize-collection (make-instance 'collection :members *objects*)
                                       'object-category))
 *body*)

(defclass type-category-tree-node (tree-node)
  ((category :initarg :category)
   (type-predicate :initarg :type-predicate
                   :initform #'typep)))

(defclass tree-model (object)
  ((nodes :initarg :nodes
          :accessor nodes))
  (:metaclass mold-object-class)
  (:documentation "A user editable tree made up of arbitrary objects."))

(defclass editable-paragraph (object)
  ((contents :initarg :contents
             :accessor contents))
  (:metaclass mold-object-class)
  (:documentation "An editable text paragraph"))

(defclass document-section (object)
  ((name :initarg :name
         :accessor section-name)
   (children :initarg :children
             :accessor children))
  (:metaclass mold-object-class)
  (:documentation "A document section."))

(defcommand toggle-tree-node
    ((tree-node object))
  "Toggle a tree node"
  )

(defun flat-tree-view (tree)
  "An accordion like flat view for trees."

  )

(defcommand (add-to-tree
             :icon "fa fa-plus")
    ((tree tree-object "Tree object.")
     (object object "Object to add to the tree."))
  (push-end object (tree-node-children tree))
  (event-emitter:emit :changed tree tree))

(defview create-tree-view (object clog-obj &rest args)
  (:documentation "Shows object in a tree.")
  (:icon "fa-solid fa-folder-tree")
  (:label "Tree view"))

(defmethod create-tree-view ((object t) clog-obj &rest args)
  (flet ((set-context-handler (html obj)
           (set-on-context-menu html
                                (lambda (&rest args)
                                  (declare (ignore args))
                                  (mold-desktop::show-commands-for-object obj))))
         (set-click-handler (html obj)
           (set-on-click html (lambda (&rest _)
                                (declare (ignore _))
                                (funcall (getf args :on-click) obj))
                         :cancel-event t)
           (set-styles html '(("cursor" "pointer"))))
         (set-drag-handler (html obj)
           (setf (clog:attribute html "data-drag-title")
                 (tree-node-label obj))
           (setf (clog:property html "draggable") "true")
           (set-on-drag-start html (lambda (_)
                                     (declare (ignore _))
                                     (mold-desktop/clog-gui::start-dragging-object obj))))
         (set-drop-handler (html obj)
           ;; To enable drop, drag-over event handler needs to be set:
           (clog:set-on-drag-over html (lambda (obj)
                                         (declare (ignore obj))
                                         (add-class html "drop-allowed")))
           (clog:set-on-drag-leave html
                                   (lambda (obj)
                                     (declare (ignore obj))
                                     (remove-class html "drop-allowed")))
           (clog:set-on-drop html (lambda (&rest args)
                                    (declare (ignore args))
                                    (mold-desktop::select-object obj)
                                    (mold-desktop::select-object md/clog-gui::*dragging-object*)
                                    (mold-desktop/clog-gui::stop-dragging-object)
                                    (remove-class html "drop-allowed")))))
    (let ((tree (create-unordered-list clog-obj :class "tree")))
      (setf (attribute tree "role") "tree")
      (labels ((add-tree-level (item li)
                 (cond
                   ;; a leaf
                   ((tree-node-leaf-p item)
                    (let ((span (create-span li :class "tree-leaf")))
                      (with-ui-update span item :changed
                        (setf (text span) (tree-node-label item))
                        (alex:when-let ((icon (tree-node-icon item)))
                          (create-icon icon span :auto-place :top)))
                      (when (getf args :on-click)
                        (set-click-handler span item))
                      (set-context-handler span item)
                      (set-drag-handler span item)
                      (set-drop-handler span item)))
                   ;; a node
                   ((not (tree-node-leaf-p item))
                    (let ((span (create-span li :class "caret tree-node")))
                      (with-ui-update span item :changed
                        (setf (text span) (tree-node-label item))
                        (alex:when-let ((icon (tree-node-icon item)))
                          (create-icon icon span :auto-place :top)))
                      (when (getf args :on-click)
                        (set-click-handler li item))
                      (set-context-handler li item)
                      (set-drag-handler li item)
                      (set-drop-handler li item)
                      (let ((ul (create-unordered-list li :class "nested"))
                            (loaded nil))
                        (setf (attribute ul "role") "group")
                        (set-on-click span (lambda (&rest args)
                                             (declare (ignore args))
                                             (toggle-class ul "active")
                                             (toggle-class span "caret-down")
                                             (when (not loaded)
                                               (with-ui-update ul item :changed
                                                 (gcl:doseq (child (tree-node-children item))
                                                   (let ((li (create-list-item ul)))
                                                     (setf (attribute li "role") "treeitem")
                                                     (setf (attribute li "aria-expanded") "false")
                                                     (add-tree-level child li))))
                                               (setf loaded t)))))))))
               (draw-tree (&rest args)
                 (declare (ignore args))
                 (setf (text tree) "")
                 (gcl:doseq (item (tree-node-children object))
                   (let ((li (create-list-item tree)))
                     (setf (attribute li "role") "treeitem")
                     (setf (attribute li "aria-expanded") "false")
                     (add-tree-level item li)))))
        (draw-tree)
        (when (typep object 'object)
          (event-emitter:on :changed object #'draw-tree))
        tree
        ))))

(defview create-tree-detail-view (object clog-obj &rest args)
  (:documentation "Tree plus a detail panel")
  (:icon "fa-solid fa-folder-tree")
  (:label "Tree detail view"))

(defmethod create-tree-detail-view (object clog-obj &key view)
  (let* ((tree-detail-view (create-div clog-obj :class "split" :style "height:100%;"))
         (tree-view (create-div tree-detail-view :style "overflow:auto;"))
         (detail-view (create-div tree-detail-view :class "w3-padding-small"
                                                   :style "overflow:auto;")))
    (create-tree-view object tree-view
                      :on-click (lambda (node)
                                  (let ((elem-view (or view (object-default-view node))))
                                    (clear-element detail-view)
                                    (let ((wrapper (mold-desktop/clog-gui:create-halos-wrapper detail-view node)))
                                      (funcall elem-view (tree-node-value node) wrapper)))))
    (clog:js-execute clog-obj (format nil "Split(['#~a','#~a'])"
                                      (clog:html-id tree-view)
                                      (clog:html-id detail-view)))
    tree-detail-view))

(defmethod object-view-types append ((object object) (mold-module (eql :tree)))
  '(create-tree-view create-tree-detail-view))

(defmethod object-view-types append
    ((hash-table hash-table) (mold-module (eql :tree)))
  '(create-tree-view create-tree-detail-view))

(pushnew :tree *mold-modules*)
