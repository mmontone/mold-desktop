;;;; * Commands

(in-package :mold-desktop)

;; Copied and modified from Nyxt

;;;; SPDX-FileCopyrightText: Atlas Engineer LLC
;;;; SPDX-License-Identifier: BSD-3-Clause

;; nyxt/source/command.lisp

(defvar *commands* '()
  "The list of known commands, for internal use only.")

(defclass command (standard-generic-function)
  ((condition :initform nil
              :accessor command-condition
              :initarg :when
              :documentation "When set, the COMMAND-CONDITION should return T for the command to be allowed to be executed.
The COMMAND-CONDITION should be a funcallable object that receives the currently-selected arguments.")
   (last-access :initform (local-time:now)
                :type local-time:timestamp
                :accessor command-last-access
                :documentation "Last time this command was called from prompt buffer.
Useful to sort the commands by most recent use.")
   (args-info :initarg :args-info
              :initform nil
              :accessor args-info
              :documentation "The command arguments.")
   ;; By specifying a return type for commands, as well for its arguments,
   ;; we get the basis for implementing user-level graphical composition of commands ala NodeRED.
   (return-type :initarg :return-type
                :accessor return-type
                :initform nil
                :documentation "The return type of the command.")
   (icon :initarg :icon
         :accessor command-icon
         :initform nil
         :documentation "The icon for the command.")
   (show-result :initarg :show-result
                :accessor show-result
                :initform nil
                :type boolean
                :documentation "When T, the result of the command is shown in the desktop using SHOW function. If result is NIL, then this has no effect."))
  (:metaclass closer-mop:funcallable-standard-class)
  (:documentation "Commands are interactive functions.
(As in Emacs.)

Commands are funcallable.

We need a `command' class for multiple reasons:
- Identify commands uniquely.

- Customize prompt buffer display value with properties.

- Last access: This is useful to sort command by the time they were last
  called.  The only way to do this is to persist the command instances.

Since they are generic functions, they can be specialize with `:before',
`:after' and `:around' qualifiers, effectively acting as hooks.
These specializations are reserved to the user."))

;; This is needed because defcommand macro needs to inspect command class
;; at compile time, for setting command options.
(closer-mop:finalize-inheritance (find-class 'command))

(defclass command-method (standard-method)
  ((args-info :initarg :args-info
              :initform nil
              :accessor args-info
              :documentation "The command arguments.")))

(c2mop:finalize-inheritance (find-class 'command-method))

(defun command-p (thing)
  "Return T if THING  is a command instance."
  (let ((func (if (symbolp thing)
                  (symbol-function thing)
                  thing)))
    (typep func 'command)))

(defmethod command-name ((command command))
  "A useful shortcut."
  (closer-mop:generic-function-name command))

(defun register-command (command)
  (setf *commands* (merge 'list (list command) *commands*
                          #'string<
                          :key (alex:compose #'string #'command-name)))
  command)

(defmethod initialize-instance :after ((command command) &key)
  ;; Overwrite previous command:
  (setf *commands* (delete (closer-mop:generic-function-name command) *commands* :key #'closer-mop:generic-function-name))
  (register-command command))

(defun find-command (name &optional (error-p t))
  (etypecase name
    (command name)
    (symbol (or (find name *commands* :key #'command-name)
                (and error-p
                     (error "Command not found: ~s" name))))))

(defun generalize-lambda-list (lambda-list)
  "Return a lambda-list compatible with generic-function definitions.
Generic function lambda lists differ from ordinary lambda list in some ways;
see HyperSpec '3.4.2 Generic Function Lambda Lists'."
  (multiple-value-bind (required optional rest keywords aok? aux key?)
      (alex:parse-ordinary-lambda-list lambda-list)
    (declare (ignore aux))
    (sera:unparse-ordinary-lambda-list required (mapcar #'first optional) rest (mapcar #'cadar keywords) aok? nil key?)))

(defun generalize-command-lambda-list (lambda-list)
  (flet ((arg-name (arg)
           (getf arg :name)))
    (multiple-value-bind (required optional rest keywords)
        (parse-command-lambda-list lambda-list)
      (sera:unparse-ordinary-lambda-list (mapcar #'arg-name required)
                                         (mapcar #'arg-name optional)
                                         rest
                                         (mapcar #'arg-name keywords)))))

#+nil(generalize-command-lambda-list '((x integer) &optional (y string)))

;; TODO: Can we use `alex:named-lambda'?  How do we get the name then?
(defun make-command (name lambda-expression)
  "Return an non-globally defined command named NAME."
  (let ((arglist (second lambda-expression))
        (doc (nth-value 2 (alex:parse-body (rest (rest lambda-expression)) :documentation t))))
    (let ((command (make-instance 'command
                                  :name name
                                  :lambda-list (generalize-lambda-list arglist)
                                  :documentation doc)))
      (closer-mop:ensure-method command lambda-expression)
      command)))

(defmacro lambda-command (name args &body body)
  "ARGS may only be a list of required arguments (optional and keyword argument
not allowed).

Example:

\(let ((source (make-my-source)))
  (lambda-command open-file* (files)
    \"Open files in some way.\"
    ;; Note that `source' is captured in the closure.
    (mapc (opener source) files)))"
  (alex:with-gensyms (closed-over-body)
    ;; Warning: `make-command' takes a lambda-expression as an unevaluated list,
    ;; thus the BODY environment is not that of the lexical environment
    ;; (closures would thus fail to close over).  To avoid this problem, we capture
    ;; the lexical environment in a lambda.
    ;;
    ;; Note that this relies on the assumption that ARGS is just a list of
    ;; _required arguments_, which is a same assumption for prompt buffer actions.
    ;; We could remove this limitation with some argument parsing.
    `(let ((,closed-over-body (lambda ,args ,@body)))
       (make-command ',name
                     (list 'lambda ',args (list 'apply ,closed-over-body  '(list ,@args)))))))

(defmacro lambda-mapped-command (function-symbol)
  "Define a command which `mapcar's FUNCTION-SYMBOL over a list of arguments."
  (let ((name (intern (str:concat (string function-symbol) "-*"))))
    `(lambda-command ,name (arg-list)
       ,(documentation function-symbol 'function)
       (mapcar ',function-symbol arg-list))))

(defmacro lambda-unmapped-command (function-symbol)
  "Define a command which calls FUNCTION-SYMBOL over the first element of a list
of arguments."
  (let ((name (intern (str:concat (string function-symbol) "-1"))))
    `(lambda-command ,name (arg-list)
       ,(documentation function-symbol 'function)
       (,function-symbol (first arg-list)))))

(defun parse-command-lambda-list (lambda-list)
  "Parse a command lambda list."
  ;; Commands lambda-lists contain type information, and documentation, of arguments.
  (let (required
        optional
        key
        rest
        (status :required))
    (dolist (elt lambda-list)
      (case status
        (:required
         (case elt
           (&optional (setf status :optional))
           (&key (setf status :key))
           (&rest (setf status :rest)))
         (when (eql status :required)
           (when (not (listp elt))
             (error "Required arg should be a list: ~s" elt))
           (destructuring-bind (arg-name arg-type &optional doc &rest options) elt
             (check-type arg-name symbol)
             (check-type doc (or null string))
             (push (list :name arg-name :type arg-type
                         :doc doc :lambda-list-type :required
                         :options options)
                   required))))
        (:optional
         (case elt
           (&optional (error "Misplaced: ~s" elt))
           (&key (setf status :key)
            (warn "&optional and &key in lambda-list"))
           (&rest (setf status :rest)))
         (when (eql status :optional)
           (destructuring-bind (arg-name arg-type &optional default-value doc &rest options) elt
             (check-type arg-name symbol)
             (check-type doc (or null string))
             (push (list :name arg-name :type arg-type
                         :default default-value :doc doc
                         :lambda-list-type :optional
                         :options options)
                   optional))))
        (:key
         (case elt
           ((&optional &key) (error "Misplaced: ~s" elt))
           (&rest (setf status :rest)))
         (when (eql status :key)
           (destructuring-bind (arg-name arg-type &optional default-value doc &rest options) elt
             (check-type arg-name symbol)
             (check-type doc (or null string))
             (push (list :name arg-name :type arg-type
                         :default default-value :doc doc
                         :lambda-list-type :keyword
                         :options options)
                   key))))
        (:rest
         (destructuring-bind (arg-name arg-type &optional doc) elt
           (check-type arg-name symbol)
           (check-type doc (or null string))
           (setf rest (list :name arg-name :type arg-type
                            :doc doc
                            :lambda-list-type :rest))))))
    (values (nreverse required)
            (nreverse optional)
            rest
            (nreverse key))))

;; (parse-command-lambda-list '())
;; (parse-command-lambda-list '((x integer "The X param")))
;; (parse-command-lambda-list '((x integer) &optional (y integer 22)))
;; (parse-command-lambda-list '((x integer) &key (y string "lala")))
;; (parse-command-lambda-list '((x integer) &rest (args string)))

;; (parse-command-lambda-list '((x integer "An integer" :prompt my-integer-prompter)))

(defun command-method-lambda-list (lambda-list)
  "Convert a command lambda list to a method lambda list."
  (flet ((arg-name (arg)
           (getf arg :name)))
    (multiple-value-bind (required optional rest keyword)
        (parse-command-lambda-list lambda-list)
      (append (mapcar #'arg-name required)
              (when optional
                (cons '&optional
                      (mapcar (lambda (arg)
                                (list (arg-name arg)
                                      (getf arg :default)))
                              optional)))
              (when rest
                (list '&rest (arg-name rest)))
              (when keyword
                (cons '&key
                      (mapcar (lambda (arg)
                                (list (arg-name arg)
                                      (getf arg :default)))
                              keyword)))))))

;;(command-method-lambda-list '((x integer) &key (y string 33)))
;;(command-method-lambda-list '((x integer) &optional (y string 33)))

(defmacro defcommand (name-and-options (&rest arglist) &body body)
  "Define new command NAME.
`defcommand' syntax is similar to `defmethod'.

Example:

\(defcommand play-video-in-current-page (&optional (buffer (current-buffer)))
  \"Play video in the currently open buffer.\"
  (uiop:run-program (list \"mpv\" (render-url (url buffer)))))"
  (let ((doc (or (nth-value 2 (alex:parse-body body :documentation t)) ""))
        (name (if (listp name-and-options) (first name-and-options) name-and-options))
        (options (and (listp name-and-options) (rest name-and-options))))
    (check-type doc string)
    `(progn
       ;;(export-always ',name (symbol-package ',name))
       (defgeneric ,name ,(generalize-command-lambda-list arglist)
         (:documentation ,doc)
         (:method ,(command-method-lambda-list arglist) ,@body)
         (:generic-function-class command)
         (:method-class command-method))
       (setf (args-info (symbol-function ',name))
             ',(multiple-value-list (parse-command-lambda-list arglist)))
       ,@(loop for key in options by #'cddr
               for val in (rest options) by #'cddr
               for slot := (or (find-if (lambda (initargs) (member key initargs))
                                        (closer-mop:class-slots (find-class 'command))
                                        :key #'closer-mop:slot-definition-initargs)
                               (error "Invalid command option: ~s" key))
               collect `(setf (slot-value (symbol-function ',name) ',(closer-mop:slot-definition-name slot)) ,val))
       (symbol-function ',name))))

(defmacro defgeneric-command (name arglist &rest options)
  "Define new command NAME.
`defgeneric-command' syntax is similar to `defgeneric'."
  (let ((class-options (remove-if-not (lambda (opt)
                                        (member opt '(:documentation)))
                                      options :key #'car))
        (initarg-options (remove-if (lambda (opt)
                                      (member opt '(:documentation)))
                                    options :key #'car)))
    ;;(export-always ',name (symbol-package ',name))
    `(progn
       (defgeneric ,name ,(generalize-command-lambda-list arglist)
         (:generic-function-class command)
         ,@class-options
         (:method-class command-method))
       (setf (args-info (symbol-function ',name))
             ',(multiple-value-list (parse-command-lambda-list arglist)))
       ,@(loop for opt in initarg-options
               for key := (car opt)
               for val := (rest opt)
               for slot := (or (find-if (lambda (initargs) (member key initargs))
                                        (closer-mop:class-slots (find-class 'command))
                                        :key #'closer-mop:slot-definition-initargs)
                               (error "Invalid command option: ~s" key))
               collect `(setf (slot-value (symbol-function ',name) ',(closer-mop:slot-definition-name slot)) ,val))
       (symbol-function ',name))))

(defmacro defcommand-method (name arglist &body body)
  `(let ((method
           (defmethod ,name ,arglist
             ,@body)))
     (setf (args-info method)
           ',(multiple-value-list (parse-command-lambda-list arglist)))
     method))

(defmacro defcommand-from-function (function-name)
  "Define a command from function named FUNCTION-NAME.
Uses introspection on functions, docstring and type signature."
  (error "TODO"))

(defun delete-command (name)
  "Remove command NAME, if any.
Any function or macro definition of NAME is also removed,
regardless of whether NAME is defined as a command."
  (setf *commands* (delete name *commands* :key #'command-name))
  (fmakunbound name))

(defun list-commands ()
  "List commands.
Commands are instances of the `command' class."
  *commands*)

(defun run-command (command &rest args)
  "Run COMMAND over ARGS and show and return its result."
  (flet ((run ()
           (let ((result (apply command args)))
             (when (and result (show-result (find-command command)))
               (show result))
             result)))
    (if* *handle-command-errors*
       then
           (handler-case (run)
             (error (e)
               (alert-toast *app-body* (princ-to-string (type-of e))
                            (who:escape-string (condition-message e)))
               (format *error-output* "Error running command ~a: ~a"
                       command e)
               (trivial-backtrace:print-backtrace e)))
       else
           (run))))

(defgeneric generic-typep (object type)
  (:documentation "A generic version of TYPEP")
  (:method (object type)
    (typep object type)))

(defgeneric command-matches-arguments-p (command arguments &key partial))

(defmethod command-matches-arguments-p ((command command) arguments &key partial)
  "Returns T if COMMAND can be applied to ARGUMENTS.
If PARTIAL is NIL, COMMAND matches ARGUMENTS iff all requiredarguments of COMMAND match and the number of ARGUMENTS is equal to the number of COMMAND required arguments."
  (destructuring-bind (required optional &rest _) (args-info command)
    (declare (ignore _))

    ;; Command condition passes?
    (when (and (command-condition command)
               (not (apply (command-condition command) arguments)))
      (return-from command-matches-arguments-p nil))

    ;; Are there enough args?
    (when (not
           (>= (+ (length required) (length optional))
               (length arguments)))
      (return-from command-matches-arguments-p nil))

    (when (not partial)
      (when (not (= (length required) (length arguments)))
        (return-from command-matches-arguments-p nil)))

    ;; The types of argument match
    ;; FIXME: we try in order, but we may want to consider matching
    ;; arguments independently of their order.
    (let ((arg-types (mapcar (lambda (arg)
                               (getf arg :type))
                             (append required optional))))
      (loop for arg in arguments
            for arg-type in arg-types
            when (not (generic-typep arg arg-type))
              do (return-from command-matches-arguments-p nil)))
    t))

(defun matching-commands (arguments &optional (commands *commands*))
  "Returns the list of commands that match the ARGUMENTS types."
  (remove-if-not
   (lambda (cmd)
     (command-matches-arguments-p cmd arguments))
   commands))

(defmethod object-title ((cmd command))
  (princ-to-string (command-name cmd)))

;;------------- Command application -------------------------------

(defclass partially-applied-command (object)
  ((command :initarg :command
            :initform nil
            :accessor command
            :attribute command
            :documentation "The command to apply")
   (arg-values :initarg :arg-values
               :accessor arg-values
               :initform nil
               :documentation "An alist with (arg-name . value)"))
  (:metaclass mold-object-class)
  (:documentation "A command with values in some or all of its arguments."))

(defmethod mold-desktop/clog-gui::halos-for-object append ((object partially-applied-command) context)
  (list (list :run :label "Run"
                   :icon "fa fa-play"
                   :position :right-bottom
                   :action (lambda (&rest args)
                             (declare (ignore args))
                             (mold-desktop::show
                              (apply (command object)
                                     (arg-values object)))))))

(defclass eval-command (object)
  ((lisp-expression :initarg :lisp-expression
                    :accessor lisp-expression
                    :initform nil
                    :attribute text))
  (:metaclass mold-object-class)
  (:documentation "A command with a lisp expression waiting to be evaluated."))

(defclass composed-command (object)
  (cmd1 cmd2 arg-name)
  (:metaclass mold-object-class)
  (:documentation "A composition of commands. After cmd1 is evaluated, its result is used as value for the argument named arg-name in cmd2."))

(defclass seq-command (object)
  (commands)
  (:metaclass mold-object-class)
  (:documentation "A list of commands that run in sequence."))

(defclass cond-command (object)
  (conditions)
  (:metaclass mold-object-class)
  (:documentation "A command that runs conditionally."))

(defclass repeat-command (object)
  (while command)
  (:metaclass mold-object-class)
  (:documentation "A command that runs repeatedly."))

;; --- UI
;; A command's UI should have name, description, and args descriptions.

(defmethod create-object-view ((object eval-command) clog-obj &rest args)
  (declare (ignore args))
  (flet ((evaluate (&rest args)
           (declare (ignore args))
           (let ((*package* (find-package 'mold-desktop)))
             (let ((result (eval (read-from-string (lisp-expression object)))))
               (show (if (listp result)
                         (make-instance 'collection :members result)
                         result))))))
    (let* ((div (create-div clog-obj :class "eval-command"))
           (link
             (create-a div :content (or (object-name object)
                                        (object-id object))))
           (button (create-button div :class "w3-button")))
      (create-icon "fa-solid fa-play" button)
      (set-on-event link "click" #'evaluate :cancel-event t)
      (set-on-event button "click" #'evaluate :cancel-event t)
      div)))

(defmethod instances-of ((type (eql 'command)))
  *commands*)

;; FIXME: the following needs more work. Optional and keyword args. Collection arguments, input for object types, etc.

(defmethod create-object-view ((command command) clog-obj &rest args)
  "Create a user interface for COMMAND.
The default interface presents user with a form for filling the command's argument and evaluating it."
  (declare (ignore args))

  (create-p clog-obj :content (documentation command 'function))

  (let ((command-args (loop for arg in (first (args-info command))
                            collect (cons (getf arg :name)
                                          ;; set value for optional arguments here:
                                          nil
                                          )))
        (table (create-table clog-obj :class "w3-table w3-bordered"))
        (event-emitter (make-instance 'event-emitter:event-emitter)))

    ;; Create a form for editing command arguments:
    (dolist (command-arg (first (args-info command)))
      (let ((tr (create-table-row table)))
        (let ((td (create-table-column tr :content (str:sentence-case (princ-to-string (getf command-arg :name))))))
          (setf (advisory-title td) (getf command-arg :doc)))
        (let* ((td (create-table-column tr))
               (value (access command-args (getf command-arg :name))))
          (if (subtypep (getf command-arg :type) 'collection)
              ;; FIXME:
              (create-model-editor-view value td)
              ;; else
              (let ((div (create-div td :style (format nil "cursor: url(~s), auto" +wrench-icon-img-data+))))
                (with-ui-update div event-emitter :changed
                  (setf (text div) (princ-to-string
                                    (access command-args (getf command-arg :name)))))
                (setf (clog:advisory-title div) "Double click to edit.")
                (set-on-double-click
                 div
                 (lambda (&rest args)
                   (declare (ignore args))
                   (prompt-command-argument-type
                    (getf command-arg :type)
                    (list :name (getf command-arg :name))
                    *command-bar*
                    (lambda (value)
                      (setf (access command-args (getf command-arg :name)) value)
                      (event-emitter:emit :changed event-emitter)
                      (close-command-bar *command-bar*))))))))))

    ;; Create a button for evaluating the command with the value of the arguments
    (create-icon-button clog-obj "fa fa-play"
                        (lambda (&rest _)
                          (declare (ignore _))
                          ;; FIXME: consider command keyword arguments here
                          (show (apply command (mapcar #'cdr command-args)))))
    clog-obj))

#+test(show #'mold-desktop/commands::add-numbers)
#+test(show #'mold-desktop/commands::add-object-from-url)
