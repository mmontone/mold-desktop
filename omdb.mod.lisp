;; https://www.omdbapi.com/

(mold-desktop::defmodule mold-desktop::file-mold-module
  open-movie-database
  :description "Open Movie Database api")

(in-package :mold-desktop)

;; http://www.omdbapi.com/?s=spider%20man&apikey=

(defun omdb-create-movie (data)
  (acc:with-access (|Title| |Type| |imdbID| |Poster|) data
    (make-instance 'movie
		   :name |Title|
		   :type |Type|
		   :imdb-id |imdbID|
		   :poster |Poster|)))

(defcommand (omdb-search-movies 
             :icon "fa-solid fa-magnifying-glass"
             :show-result t)
    ((query string "Search query"))
  "Search movies in omdbapi.com"
  (make-instance 'collection
		 :name (format nil "OMDB search: ~a" query)
		 :members
		 (arrows:as-> "http://www.omdbapi.com" $
			      (drakma:http-request $
						   :parameters `(("s" . ,query)
								 ("apikey" . ,(config-ensure-get :omdb :api-key)))
						   :want-stream t)
			      (yason:parse $)
			      (acc:access $ "Search")
			      (mapcar #'omdb-create-movie $)
			      (remove-if #'null $))))

(defmethod create-object-view ((object movie) clog-obj &rest args &key context &allow-other-keys)
  (let* ((view (create-div clog-obj :class "thumbnail"))
         (title (create-child view (html (:h5 :style "cursor:pointer;"
                                              (who:str (object-title object)))))))
    (create-img view :url-src (movie-poster object)
                     :style "margin: 5px; max-width: 200px; max-height: 200px; float: left;")

    (create-child view (html (:p (:small (who:str (object-description object))))))
    (let ((click-handler (or (and (getf args :on-click)
                                  (lambda* (&rest rs)
                                    ;; Pass the clicked object to the click handler
                                    (apply (getf args :on-click) object rs)))
                             (lambda* (_)
                               (show object :context context)))))

      (set-on-event title "click" click-handler :cancel-event t))
    (create-link view
		 (format nil "https://www.imdb.com/title/~a" (imdb-id object))
		 "_blank"
		 :content "IMDB page")
    view))

(defmethod create-item-view ((object movie) li &rest args &key context &allow-other-keys)
  (declare (ignore args))
  (flet ((redraw (&rest args)
           (declare (ignore args))
           (setf (text li) "")
           (when (movie-poster object)
             (create-img li :url-src (movie-poster object)
                            :style "height: 32px; width: 32px; margin-right: 5px;"))
           (let ((link
                   (create-a li :content (or (object-name object) (object-url object)))))
             (set-on-event link "click"
                           (lambda* (_)
                             (show object :context context))
                           :cancel-event t))))
    (prog1
        (redraw)
      (event-emitter:on :changed object #'redraw))))

(defun parse-omdb-url (url headers)
  (when (str:starts-with-p "https://www.omdb.org" url)
    ;; TODO
    ))

(pushnew 'parse-omdb-url *url-parsers*)
(pushnew :omdb *mold-modules*)
