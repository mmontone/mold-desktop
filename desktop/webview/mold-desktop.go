package main

import "github.com/webview/webview"

func main() {
	debug := true
	w := webview.New(debug)
	defer w.Destroy()
	w.SetTitle("Mold Desktop - The Programmable Desktop")
	w.SetSize(1000, 600, webview.HintNone)
	w.Navigate("http://127.0.0.1:8080")
	w.Run()
}
