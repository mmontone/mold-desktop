(defpackage :mold-desktop/leaflet
  (:use :cl :mold-desktop :clog))

(in-package :mold-desktop/leaflet)

(defun init-leaflet (body)
  (load-css (html-document body) "https://unpkg.com/leaflet@1.8.0/dist/leaflet.css")
  (load-script (html-document body) "https://unpkg.com/leaflet@1.8.0/dist/leaflet.js"))

(pushnew 'init-leaflet mold-desktop::*init-functions*)

(defclass leaflet-map (mold-desktop::map-object)
  ((map-div :accessor map-div)
   (coordinates :initarg :coordinates
		:accessor map-coordinates
		:initform (cons 0 0)
		:attribute string)
   (zoom-level :initarg :zoom-level
	       :accessor zoom-level
	       :initform 13
	       :attribute integer))
  (:metaclass mold-desktop::mold-object-class))

(defclass map-element ()
  ())

(defclass map-marker (map-element)
  ((position :initarg :position
	     :accessor marker-position)))

(defmethod mold-desktop::create-object-view ((map leaflet-map) clog-obj &rest args)
  (with-slots (map-div) map
    (setf map-div (create-div clog-obj))
    (setf (height map-div) "500px")
    (js-execute clog-obj (format nil "window.map~a = L.map('~a').setView([~a, ~a], ~a);"
				 (html-id map-div)
				 (html-id map-div)
				 (car (map-coordinates map))
				 (cdr (map-coordinates map))
				 (zoom-level map)))
    (js-execute clog-obj (format nil "L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
    maxZoom: 19,
    attribution: '© OpenStreetMap'
}).addTo(window.map~a);" (html-id map-div)))))

#+example
(mold-desktop::show (make-instance 'leaflet-map
				   :coordinates (cons "-15.194" "-56.369")
				   :zoom-level 1))

(defgeneric geographic-location (object)
  (:documentation "Return the geographic location of OBJECT."))

(defmethod geographic-location (object)
  "OBJECT does not have a geographic location by default."
  nil)

(mold-desktop::defcommand (set-map-marker
			   :icon "fa-solid fa-map-pin")
    ((map leaflet-map "The map")
     (object mold-desktop::object "The object to use for maker location"))
  "Add OBJECT as marker to the MAP"
  (when (geographic-location object)
    (js-execute mold-desktop::*body*
		(format nil "L.marker([~a, ~a]).addTo(window.map~a);"
			(car (geographic-location object))
			(cdr (geographic-location object))
			(html-id (map-div map))))))

(mold-desktop::defcommand (set-map-markers
			   :icon "fa-solid fa-map-pin")
    ((map leaflet-map "The map")
     (collection mold-desktop::collection "The collection of objects to use as markers"))
  (dolist (object (mold-desktop::members collection))
    (set-map-marker map object)))

(defclass object-with-address ()
  ((country :initarg :country
	    :accessor country)
   (state :initarg :state
	  :accessor state)
   (address :initarg :address
	    :accessor address)
   (geographic-location :initarg :geographic-location
			:accessor geographic-location
			:initform nil
			:documentation "A cons with lat and long as car and cdr."))
  (:documentation "Object mixin that fetches geographic location from address information using a service (nominatim.)"))

(defun fetch-geographic-location (object)
  (error "TODO"))

(defmethod geographic-location ((object object-with-address))
  (when (slot-value object 'geographic-location)
    (return-from geographic-location (slot-value object 'geographic-location)))
  (setf (geographic-location object)
	(fetch-geographic-location object))
  (slot-value object 'geographic-location))

(defclass open-address (mold-desktop::object)
  ((data :initarg :data
	 :accessor address-data))
  (:metaclass mold-desktop::mold-object-class)
  (:documentation "openaddresses.io"))

(defclass city (object-with-address)
  ())

(defvar *example-addresses* nil)

#+example
(let ((addresses-path (asdf:system-relative-pathname :mold-desktop "assets/br_addresses.geojson")))
  (setf *example-addresses*
	(with-open-file (file addresses-path)
	  (loop for i from 1 to 50 
		collect (let ((data (json:decode-json-from-source file)))
			  (make-instance 'open-address :data data
						       :name (format nil "~a@~a"
								     (access:accesses data :properties :street)
								     (access:accesses data :properties :city))))))))

(defmethod geographic-location ((address open-address))
  (let ((coordinates
	  (access:accesses (address-data address)
			   :geometry :coordinates)))
    (cons (second coordinates) (first coordinates))))

(defvar *capitals-collection*
  (make-instance 'mold-desktop::collection
		 :name "Country capitals"
		 :type 'city
		 :members (mapcar
			 (lambda (data)
			   (make-instance 'city
					  :country (access:access data :*country-name)
					  :state (access:access data :*capital-name)
					  :geographic-location (cons
								(parse-float::parse-float (access:access data :*capital-latitude))
								(parse-float:parse-float (access:access data :*capital-longitude)))))
			 (json:decode-json-from-source
			  (asdf:system-relative-pathname :mold-desktop "assets/country-capitals.json")))))

#+example
(mold-desktop::show *capitals-collection*)
