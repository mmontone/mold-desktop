(in-package :mold-desktop)

(defclass mold-workspace ()
  ((name :accessor workspace-name
         :initarg :name)
   (description :accessor workspace-description
                :initform nil
                :initarg :description)
   (icon :accessor workspace-icon
         :initform nil
         :initarg :icon)
   (elem :accessor workspace-elem
         :initform nil)
   (body :accessor workspace-body
         :initform nil)))

(defmethod print-object ((workspace mold-workspace) stream)
  (print-unreadable-object (workspace stream :type t :identity t)
    (format stream "~a" (workspace-name workspace))))

(defgeneric workspace-status-label (workspace)
  (:documentation "Label used in workspaces area"))

(defmethod workspace-status-label ((workspace mold-workspace))
  (workspace-name workspace))

(defgeneric open-workspace (workspace body))
(defgeneric close-workspace (workspace))

(defgeneric workspace-accepts-object (workspace object)
  (:documentation "Returns T if WORKSPACE allows OBJECT to be dropped into it."))
(defmethod workspace-accepts-object (workspace object)
  nil)

(defgeneric workspace-add-object (workspace object)
  (:documentation "What is done for adding OBJECT to WORKSPACE"))

(defgeneric workspace-remove-object (workspace object))

(defclass default-workspace (mold-workspace)
  ((objects :accessor workspace-objects
            :initform nil))
  (:default-initargs
   :name "Home"
   :icon "fa-solid fa-house")
  (:documentation "The default type of workspace"))

(defmethod workspace-accepts-object ((workspace default-workspace) object)
  t)

(defmethod workspace-add-object ((workspace default-workspace) object)
  (push object (workspace-objects workspace)))

(defmethod workspace-remove-object ((workspace default-workspace) object)
  (setf (workspace-objects workspace) (remove object (workspace-objects workspace))))

(defmethod open-workspace :around ((workspace mold-workspace) body)
  (when (eq workspace *workspace*)
    (return-from open-workspace))
  ;;(setf (text body) "")
  (when (and *workspace* (workspace-elem *workspace*))
    (hide-workspace *workspace*))
  (setf *workspace* workspace)
  (let ((elem (call-next-method)))
    (setf (workspace-elem workspace) elem)))

(defun create-menu-full-screen (clog-obj &key (html-id nil))
  (create-child clog-obj
                (html
                  (:span :class "w3-bar-item"
                         :style "user-select:none;cursor:pointer;"
                         :title "Toggle full screen"
                         :onclick "if (document.fullscreenElement==null) {
                                      documentElement.requestFullscreen()
                                    } else {document.exitFullscreen();}"
                         (who:str "⤢")))
                :html-id html-id
                :clog-type 'clog-gui-menu-item))

(defun create-menu-icon (clog-obj icon-spec &key
                                              (on-click nil)
                                              (class "w3-button w3-bar-item"))
  (let* ((button (create-button clog-obj :class class)))
    (create-icon icon-spec button)
    (set-on-click button on-click)
    button))

(defun create-notifications-button (clog-obj)
  (let ((button (create-menu-icon clog-obj "fa-solid fa-bell")))
    (set-on-click button (ignoring-args (show-notifications)))
    ;; Change the color of the bell when notifications change
    (flet ((update-notifications-button-color ()
	     (setf (clog:color button)
		   (if* (gcl:emptyp *notifications*)
		      then "white"
		      else "tomato"))))
      (update-notifications-button-color)
      (event-emitter:on :changed *notifications*
			(ignoring-args (update-notifications-button-color))))
    button))

(defun render-default-workspace-menu (clog-obj)
  (with-clog-create clog-obj
      (gui-menu-bar (:class "w3-bar w3-black w3-card-4 workspace-menu-bar")
                    (gui-menu-icon (:image-url "/img/icons8-desktop-64.png"
                                    :on-click 'on-help-about))
                    (gui-menu-drop-down (:content "Objects")
                                        (gui-menu-item (:content "New ..."
                                                        :on-click (ignoring-args (run-command-interactively 'mold-desktop/commands::create-new-object))))
                                        (gui-menu-item (:content "My collections"
                                                        :on-click (ignoring-args (run-command 'list-my-collections))))
                                        (gui-menu-item (:content "Browse all"
                                                        :on-click (ignoring-args (run-command 'list-all-objects)))))
                    (gui-menu-drop-down (:content "Workspaces")
                                        (gui-menu-item (:content "New ..."
                                                        :on-click (ignoring-args (run-command-interactively 'add-workspace))))
                                        (gui-menu-item (:content "Switch to ..."
                                                        :on-click (ignoring-args (run-command-interactively 'switch-to-workspace)))))
                    #+nil(gui-menu-drop-down (:content "Window")
                                             (gui-menu-item (:content "Maximize All" :on-click 'maximize-all-windows))
                                             (gui-menu-item (:content "Normalize All" :on-click 'normalize-all-windows))
                                             (gui-menu-window-select ()))
                    (gui-menu-drop-down (:content "System")
                                        (gui-menu-item (:content "Settings"
                                                        :on-click (ignoring-args (run-command 'mold-desktop/settings::customize-system))))
                                        (gui-menu-item (:content "Load module"
                                                        :on-click (ignoring-args (run-command-interactively 'mold-desktop::load-module))))
                                        (gui-menu-item (:content "Load user data"
                                                        :on-click (ignoring-args (command-bar-confirm-and-do "Load data from disk?"
                                                                                                             (ignoring-args (mold-desktop/commands::load-system))
                                                                                                             *command-bar*))))
                                        (gui-menu-item (:content "Save user data"
                                                        :on-click (ignoring-args (command-bar-confirm-and-do "Save current data to disk?"
                                                                                                             (ignoring-args (mold-desktop/commands::save-system))
                                                                                                             *command-bar*)))))
                    (gui-menu-drop-down (:content "Help")
                                        (gui-menu-item (:content "Manual" :on-click (ignoring-args (run-command 'open-mold-desktop-book))))
                                        (gui-menu-item (:content "About" :on-click 'on-help-about)))
                    (notifications-button ())
                    (menu-full-screen ()))))

(defmethod open-workspace ((workspace default-workspace) body)
  (let* ((app-main (create-div body :class "app-main workspace default-workspace"))
         (menu (render-default-workspace-menu app-main))
         (app-body (create-div app-main :class "app-body")))
    (declare (ignorable menu))

    (let ((app (clog-gui::connection-data-item body "clog-gui")))
      (setf (mold-desktop/clog-gui::app-body app) app-body)
      (setf (connection-data-item body "clog-app-body") app-body))

    (setf *app-body* app-body
          (workspace-body workspace) app-body)

    ;; Handle drag and drop urls from browser's windows/tabs
    ;; TODO: add support for drag and drop of files from user's desktop
    ;; https://developer.mozilla.org/en-US/docs/Web/API/DataTransfer/files
    ;; https://developer.mozilla.org/en-US/docs/Web/API/HTML_Drag_and_Drop_API/File_drag_and_drop
    ;; A custom set-on-drop-files function needs to be implemented
    (setf (attribute app-body "ondragover") "return false")
    (set-on-drag-over app-body (lambda* (_) nil))
    (set-on-drop app-body (lambda (obj event)
                            (declare (ignore obj))
                            ;; Create object from url
                            (let ((maybe-url (getf event :drag-data)))
                              (when (or (str:starts-with? "http://" maybe-url)
                                        (str:starts-with? "https://" maybe-url))
                                (workspace-add-object workspace mold-desktop/clog-gui::*dragging-object*)
                                (show (mold-desktop/commands::add-object-from-url maybe-url))))
                            ;; Open the dragged object, or a shallow-copy of it,
                            ;; depending keyboard modifiers.
                            (when mold-desktop/clog-gui::*dragging-object*
                              (let ((dragging-object
                                      (if (getf event :ctrl-key)
                                          (shallow-copy-object mold-desktop/clog-gui::*dragging-object*)
                                          mold-desktop/clog-gui::*dragging-object*)))
                                (workspace-add-object workspace dragging-object)
                                (show dragging-object)
                                (mold-desktop/clog-gui::stop-dragging-object)))))

    ;; Open the workspace objects
    (dolist (object (workspace-objects workspace))
      (show object))

    (set-on-before-unload (window body)
                          (lambda(obj)
                            (declare (ignore obj))
                            ;; return empty string to prevent nav off page
                            ""))

    ;; We need to return the workspace CLOG object
    app-main))

(defmethod close-workspace ((workspace mold-workspace))
  (when (> (length *workspaces*) 1)
    (setf *workspaces* (delete workspace *workspaces*))
    (event-emitter:emit :changed (get '*workspaces* :events))
    (open-workspace (first *workspaces*) *body*)))

(defun hide-workspace (workspace)
  (when (workspace-elem workspace)
    (setf (hiddenp (workspace-elem workspace)) t)))

(defun show-workspace (workspace)
  (setf (hiddenp (workspace-elem workspace)) nil))

(defun activate-workspace (workspace)
  (setf *app-body* (workspace-body workspace)))

(defun deactivate-workspace (workspace)
  (setf *app-body* nil))

(defcommand add-workspace
    ((type (subclass-of mold-workspace) "The type of workspace"))
  "Add a workspace"
  (let ((workspace (make-instance type)))
    (push-end workspace *workspaces*)
    (event-emitter:emit :changed (get '*workspaces* :events))
    (open-workspace workspace *body*)))

(defcommand switch-workspace ()
  "Switch to another workspace"
  (let* ((workspace-pos (position *workspace* *workspaces*))
         (next-workspace-pos (mod (1+ workspace-pos) (length *workspaces*))))
    (switch-to-workspace (nth next-workspace-pos *workspaces*))))

(defcommand switch-to-workspace
    ((workspace (member-of-list *workspaces* :label workspace-name) "The workspace to switch to"))
  "Switch to WORKSPACE"
  (hide-workspace *workspace*)
  (deactivate-workspace *workspace*)
  (if (workspace-elem workspace)
      (progn
        (activate-workspace workspace)
        (show-workspace workspace))
      (open-workspace workspace *body*))
  (setf *workspace* workspace))
