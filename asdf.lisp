(in-package :mold-desktop)

(defmethod create-object-view ((system asdf:system) clog-obj &rest args)
  (declare (ignore args))
  )

(defmethod create-asdf-system-dependencies-graph-view ((system asdf:system) clog-obj &rest args)
  (declare (ignore args)))

(defmethod object-view-types append ((system asdf:system) module)
  '(create-asdf-system-dependencies-graph-view))
