;; Using this technique for the Heroku deploy: https://medium.com/ki-labs-engineering/deploying-a-native-go-binary-on-heroku-6d4c955819d8

(require :sb-cltl2)
(require :sb-concurrency)

;;; The following lines added by ql:add-to-init-file:
#-quicklisp
(let ((quicklisp-init (merge-pathnames "quicklisp/setup.lisp"
                                       (user-homedir-pathname))))
  (when (probe-file quicklisp-init)
    (load quicklisp-init)))

(declaim (optimize (speed 0) (compilation-speed 0) (safety 3) (debug 3)))

(setf sb-impl::*default-external-format* :utf-8)

(require :swank)

(load "./load.lisp")
(load "./heroku.lisp")

;; Choose one of reverse proxies
(load "./reverse-proxy.lisp")
;; (load "./hunchentoot-proxy.lisp")

;; load config

(setf mold-desktop::*config* (read-from-string (alexandria:read-file-into-string "config.lisp")))

(setf mold-desktop::*store* (make-instance 'deta::deta-store
					   :filename "desktop.db"
					   :drive-name "desktop"
					   :filepath #p"/tmp/desktop.db"))

(defpackage :video-server/build
  (:nicknames :vs/build)
  (:use :cl))

(in-package :video-server/build)

(defun start ()
  (heroku::start-self-ping "https://mold-desktop.herokuapp.com/")
  (mold-desktop:start :load-database t
		      :open-browser nil
		      :app-directory "./")
  (mold-desktop/proxy:start-proxy (parse-integer (second sb-ext:*posix-argv*)))
  (swank:create-server :dont-close t)
  (loop (sleep 2))
  ;;(sb-impl::toplevel-init)
  )

(hunchentoot:define-easy-handler (root-handler :uri "/")
    ()
  "Hello world")

(defun start-hunchentoot ()
  (let ((acceptor (hunchentoot:start (make-instance 'hunchentoot:easy-acceptor
						    :address "0.0.0.0"
						    :port (parse-integer (second sb-ext:*posix-argv*))))))
    (format t "Started: ~a ~%" acceptor)
    (loop (sleep 2))
  ;;(sb-impl::toplevel-init)
  ))

(sb-ext:save-lisp-and-die #p"./bin/heroku-mold-desktop"
			  :toplevel #'start
			  :executable t
			  :compression t)
