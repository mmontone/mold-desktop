;;; The following lines added by ql:add-to-init-file:
#-quicklisp
(let ((quicklisp-init (merge-pathnames "quicklisp/setup.lisp"
                                       (user-homedir-pathname))))
  (when (probe-file quicklisp-init)
    (load quicklisp-init)))

(declaim (optimize (speed 0) (compilation-speed 0) (safety 3) (debug 3)))

(load "./load.lisp")

;; Choose one of reverse proxies
;; (load "./proxy.lisp")
;; (load "./hunchentoot-proxy.lisp")
;; Both lisp disabled for now, we use external reverse proxy

(defpackage :video-server/build
  (:nicknames :vs/build)
  (:use :cl))

(defun start ()
  (mold-desktop:start)
  (loop (sleep 2)))

(ccl:save-application #p"./bin/mold-desktop-ccl"
		      :toplevel-function #'start
		      :prepend-kernel t)
