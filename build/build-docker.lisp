#+sbcl(require :sb-cltl2)
#+sbcl(require :sb-concurrency)

;;; The following lines added by ql:add-to-init-file:
#-quicklisp
(let ((quicklisp-init (merge-pathnames "quicklisp/setup.lisp"
                                       (user-homedir-pathname))))
  (when (probe-file quicklisp-init)
    (load quicklisp-init)))

(declaim (optimize (speed 0) (compilation-speed 0) (safety 3) (debug 3)))

#+sbcl(setf sb-impl::*default-external-format* :utf-8)

(load "./load.lisp")

;; (ensure-directories-exist "~/.config/mold-desktop/")

(setf mold-desktop::*store*
      (make-instance 'mold-desktop/store::fs-store
                     :filepath "/root/mold-desktop.db"))

;; Choose one of reverse proxies
;; (load "./proxy.lisp")
;; (load "./hunchentoot-proxy.lisp")
;; Both lisp disabled for now, we use external reverse proxy

(defpackage :video-server/build
  (:nicknames :vs/build)
  (:use :cl))

(defun start ()
  (mold-desktop:start :port 8080
		      :open-browser nil
		      :app-directory "/root/")
  (loop (sleep 2)))

#+ccl(ccl:save-application #p"./bin/mold-desktop-docker"
			   :toplevel-function #'start
			   :prepend-kernel t)

#+sbcl(sb-ext:save-lisp-and-die #p"./bin/mold-desktop-docker"
				:toplevel #'start
				:executable t
				:compression t)
