;;; The following lines added by ql:add-to-init-file:
#-quicklisp
(let ((quicklisp-init (merge-pathnames "quicklisp/setup.lisp"
                                       (user-homedir-pathname))))
  (when (probe-file quicklisp-init)
    (load quicklisp-init)))

(load "./load.lisp")

(require :unix-opts)

;; Choose one of reverse proxies
;; (load "./proxy.lisp")
;; (load "./hunchentoot-proxy.lisp")
;; Both lisp disabled for now, we use external reverse proxy

(defpackage :mold-desktop/build
  (:nicknames :vs/build)
  (:use :cl))

(in-package :mold-desktop/build)

(defmacro when-option ((options opt) &body body)
  `(let ((it (getf ,options ,opt)))
     (when it
       ,@body)))

(defun parse-boolean (string)
  (cond
    ((member string '("yes" "true" "on") :test #'string=)
     t)
    (t nil)))

(opts:define-opts
  (:name :help
   :description "print this help text"
   :short #\h
   :long "help")
  (:name :verbose
   :description "verbose output"
   :short #\v
   :long "verbose")
  (:name :level
   :description "the program will run on LEVEL level"
   :short #\l
   :long "level"
   :required nil
   :arg-parser #'parse-integer
   :meta-var "LEVEL")
  (:name :host
   :description "host"
   :short #\h
   :long "host"
   :default "0.0.0.0"
   :meta-var "HOST")
  (:name :port
   :description "run on port"
   :short #\p
   :long "port"
   :required nil
   :default 8080
   :arg-parser #'parse-integer
   :meta-var "PORT")
  (:name :open-browser
   :description "open browser on start"
   :long "open-browser"
   :default t
   :arg-parser #'parse-boolean)
  (:name :load-database
   :description "load database on start"
   :long "load-database"
   :default t
   :arg-parser #'parse-boolean))

(defun start ()
  (let ((options (opts:get-opts)))
    (when-option (options :help)
      (opts:describe
       :prefix "Mold Desktop - A programmable desktop in Common Lisp."
       :suffix "so that's how it works…"
       :usage-of "mold-desktop"
       :args     "[FREE-ARGS]")
      (sb-posix:exit 0))
    (when-option (options :verbose)
      (format t "OK, running in verbose mode…~%"))
    (when-option (options :level)
      (format t "I see you've supplied level option, you want ~a level!~%" it))
    (mold-desktop:start :host (getf options :host)
			:port (getf options :port)
			:open-browser (getf options :open-browser)
			:load-database (getf options :load-database)))
  (sb-impl::toplevel-init))

(sb-ext:save-lisp-and-die #p"./bin/mold-desktop"
                          :toplevel #'start
                          :executable t
                          :compression t)
