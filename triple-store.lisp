;; TODO: consider using https://github.com/facts-db/cl-facts

(defpackage :naive-triple-store
  (:use :cl)
  (:export #:put-triple
           #:find-triples
           #:store-all-mold-objects
           #:make-triple-store))

(in-package :naive-triple-store)

(defstruct triple-store
  triples)

(defun put-triple (subject predicate object store)
  (check-type subject (or integer string))
  (push (list subject predicate object) (triple-store-triples store)))

(defun find-triples (subject predicate object store)
  (remove-if-not (lambda (triple)
                   (and (or (not subject)
                            (equal (first triple) subject))
                        (or (not predicate)
                            (equalp predicate (second triple)))
                        (or (not object)
                            (eql object (third triple)))))
                 (triple-store-triples store)))

(defun store-all-mold-objects (store)
  (dolist (object mold-desktop::*objects*)
    ;; Store tags
    (dolist (tag (mold-desktop::tags object))
      (put-triple (mold-desktop::object-id object)
                  "tag" tag store))
    ;; Store type
    (put-triple (mold-desktop::object-id object)
                "type" (class-of object)
                store)
    ;; Store categories?
    ))
