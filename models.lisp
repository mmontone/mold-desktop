;; Copyright (C) 2022 Mariano Montone - All Rights Reserved
;; You may use, distribute and modify this code under the terms of the
;; GNU Affero General Public License

;; TODO:
;; What I would like to try for user defined models:
;; Make them instances of MOLD-OBJECT-CLASS and install them as normal Lisp classses.
;; So they can be instantiated and defined functions and commands for them transparenently, as if they were another class defined via defclass.
;; Looks possible:
;; MOLD-DESKTOP> (setf (find-class 'my-class)
;; 		    (make-instance 'mold-object-class :name 'my-class))
;; #<MOLD-OBJECT-CLASS MOLD-DESKTOP::MY-CLASS>
;; MOLD-DESKTOP> (make-instance 'my-class)
;; #<MY-CLASS {1006224723}>

;; (setf (find-class 'foo-class)
;;       (make-instance 'standard-class :name 'foo-class
;; 				     :documentation "foo class"
;; 				     :direct-superclasses (mapcar #'find-class '(standard-object))
;; 				     :direct-slots '((:name x :initargs (:x)))))

(in-package :mold-desktop)

(defclass model-object (object)
  ((attributes :initarg :attributes
               :accessor model-attributes))
  (:metaclass mold-object-class))

(defclass model-attribute ()
  ((name :initarg :name :accessor attribute-name
         :initform nil)
   (type :initarg :type :accessor attribute-type
         :initform nil)))

;; Implement properties interface so we can use properties gui
(defmethod property-name ((attribute model-attribute))
  (attribute-name attribute))

(defmethod property-type ((attribute model-attribute))
  (attribute-type attribute))

(defclass model-instance (object)
  ((model :initarg :model
          :accessor model)
   (attribute-values :initarg :attribute-values
                     :accessor attribute-values))
  (:metaclass mold-object-class))

(defclass model-collection (collection)
  ((model-type :initarg :model-type
               :accessor model-type))
  (:metaclass mold-object-class))

(defclass filtered-model-collection (collection)
  ((collection :initarg :collection
               :accessor collection)
   (attribute-values :initarg :attribute-values
                     :accessor attribute-values))
  (:metaclass mold-object-class))

(defmethod attribute-value ((model-instance model-instance) name)
  (alex:assoc-value (attribute-values model-instance) name))

;; Implement table interface for model-collection
(defmethod columns ((collection model-collection))
  (mapcar 'attribute-name (model-attributes (model-type collection))))

(defmethod column-value ((row model-instance) column)
  (attribute-value row column))

(defmethod create-object-view ((model-collection model-collection) clog-obj &rest args)
  (apply #'create-table-view model-collection clog-obj args))

(defview create-object-editor-view (object clog-obj &rest args)
  (:documentation "Editing view using a web form.")
  (:icon "fa-regular fa-pen-to-square"))

(defmethod create-object-editor-view ((model-instance model-instance) clog-obj &rest args)
  (declare (ignore args))
  (create-properties-editor-form
   (loop for attr in (model-attributes (model model-instance))
         collect (cons attr (attribute-value model-instance (attribute-name attr))))
   clog-obj))

(defmethod create-object-editor-view ((object object) clog-obj &rest args)
  (declare (ignore args))
  (create-properties-editor-form
   (loop for slot in (object-attributes object)
	 collect (cons slot (value-models:make-slot-adaptor object (closer-mop:slot-definition-name slot))))
   clog-obj))

(defmethod create-object-view ((model-instance model-instance) clog-obj &rest args)
  (apply #'create-object-editor-view model-instance clog-obj args))

(defmethod object-view-types append ((object object) (mold-module (eql :models)))
  '(create-object-editor-view))

(defparameter *model-attribute-types*
  '(string boolean text choice list))

(defmethod create-object-editor-view ((model model-object) clog-obj &rest args)
  (declare (ignore args))
  (let* ((form (create-form clog-obj))
         (attributes-panel (create-div form :class "w3-panel"))
         (tmp (create-child attributes-panel (html (:header :class "w3-green" (:h5 (who:str "Attributes"))))))
         (attributes-table (create-table attributes-panel :class "w3-table w3-tiny w3-border"))
         (current-attribute (make-instance 'model-attribute))
         attribute-name-input attribute-type-input)

    (clog:set-styles attributes-table '(("max-height" "50px")
                                        ("overflow-y" "scroll")))

    (flet ((update-attributes-table ()
             (setf (text attributes-table) "")
             (-> (create-table-row attributes-table)
                (create-table-heading :content "attribute")
                (create-table-heading :content "type"))
             (dolist (attribute (model-attributes model))
               (let ((tr (create-table-row attributes-table
                                           :class "w3-hover-light-gray")))
                 (create-table-column tr :class "w3-text-gray" :content (attribute-name attribute))
                 (create-table-column tr :class "w3-text-blue" :content (princ-to-string (attribute-type attribute)))
                 (set-on-click tr
                               (lambda (_)
                                 ;; When attribute is selected, show its data in inputs
                                 (declare (ignore _))
                                 (setf current-attribute attribute)
                                 (setf (element-value attribute-name-input)
                                       (princ-to-string (attribute-name attribute)))
                                 (setf (element-value attribute-type-input)
                                       (princ-to-string (attribute-type attribute)))))))))

      (update-attributes-table)

      ;; Add/update attribute
      (let ((div (create-div form :class "w3-panel")))
        (-> div
           (create-child (html (:header :class "w3-green" (:h5 (who:str "Add/update attribute"))))))

        ;; Attribute name
        (setf attribute-name-input
              (create-child div (html (:input :type :text
                                              :class "w3-input w3-border"
                                              :placeholder "Attribute name"))))
        ;; Attribute type
        (setf attribute-type-input
              (create-select div :class "w3-select"))

        (dolist (attribute-type *model-attribute-types*)
          (create-option attribute-type-input
                         :value (princ-to-string attribute-type)
                         :selected (and (attribute-name current-attribute) ;; existent attribute
                                        (eql (attribute-type current-attribute) attribute-type))
                         :content (princ-to-string attribute-type)))

        ;; Add/update button
        (let ((actions (create-div div)))
          (-> (create-button actions :class "w3-button w3-khaki" :content "Apply")
             (clog:set-styles '(("margin-right" "3px")))
             (set-on-click (lambda (_)
                             (declare (ignore _))
                             (let ((add-attribute-p (not (attribute-name current-attribute))))
                               (setf (attribute-name current-attribute) (element-value attribute-name-input))
                               (setf (attribute-type current-attribute) (element-value attribute-type-input))
                               (when add-attribute-p
                                 (push current-attribute (model-attributes model)))
                               (update-attributes-table)))))
          (-> (create-button actions :class "w3-button w3-green w3-margin" :content "New")
             (clog:set-styles '(("margin-right" "3px")))
             (set-on-click (lambda (_)
                             (declare (ignore _))
                             (setf current-attribute (make-instance 'model-attribute))
                             (setf (element-value attribute-name-input) ""))))
          (-> (create-button actions :class "w3-button w3-red" :content "Delete")
             (clog:set-styles '(("margin-right" "3px")))
             (set-on-click (lambda (_)
                             (declare (ignore _))
                             (setf (model-attributes model)
                                   (delete current-attribute (model-attributes model)))
                             (update-attributes-table))))
          )
        ))))

(pushnew :models *mold-modules*)
