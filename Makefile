SBCL=/usr/bin/sbcl
CCL=/home/marian/src/lisp/ccl/lx86cl64

build-sbcl:
	qlot exec ${SBCL} --no-userinit --load build/build.lisp

build-heroku:
	qlot exec ${SBCL} --no-userinit --load build/build-heroku.lisp

build-ccl:
	qlot exec ${CCL} -n --load build/build-ccl.lisp

build-docker-bin:
	rm bin/*
	qlot exec ${CCL} -n --load build/build-docker.lisp
	gzip bin/mold-desktop-docker

build-docker: build-docker-bin
	docker build . -t mold-desktop

run-docker:
	 docker run -it --rm -p 8080:8080 --name mold-desktop mold-desktop

run-sbcl:
	qlot exec ${SBCL} --load mold-desktop.asd --eval '(ql:quickload :mold-desktop)' --eval '(mold-desktop:start)'
