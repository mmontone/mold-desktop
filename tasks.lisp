;; Model for tasks

(in-package :mold-desktop)

(defclass task (object)
  ((status :initarg :status
           :initform :todo
           :type (member :todo :done :canceled)
           :attribute t
           :accessor task-status
           :status t)
   (due-time :initarg :due-time
             :initform nil
             :type (or local-time:timestamp null)
             :accessor due-time
             :attribute t)
   (attached-objects :initarg :attached-objects
                     :initform (make-instance 'collection)
                     :attribute collection)
   (subtasks :initarg :subtasks
             :initform (make-instance 'collection)
             :attribute collection))
  (:metaclass mold-object-class)
  (:default-initargs
   :description ""
   :name "Unnamed task")
  (:icon . "fa-solid fa-list-check"))

(defmethod create-object-view ((task task) clog-obj &rest args)
  (declare (ignore args))
  (create-section clog-obj :h5 :content (object-title task))
  (create-p clog-obj :content (format nil "STATUS: ~a" (task-status task)))
  (create-p clog-obj :content (object-description task)))

#+test(show (make-instance 'task :name "Task 1" :description "Do this"))

(defclass task-collection (collection)
  ((status :initarg :status
           :initform (error "Provide the status")
           :accessor task-status))
  (:metaclass mold-object-class))

(defun list-all-tasks ()
  (remove-if-not (alex:rcurry #'typep 'task) *objects*))

(defmethod members ((collection task-collection))
  (remove-if-not (lambda (task)
                   (equalp (task-status task) (task-status collection)))
                 (list-all-tasks)))

;; add a documentation node to the manual
(defun create-tasks-doc-node ()
  (read-doc-node-from-file (asdf:system-relative-pathname :mold-desktop "docs/modules/tasks.md")
                           :name "Tasks"))

(pushnew 'create-tasks-doc-node *mold-desktop-book-module-nodes*)
