(in-package :mold-desktop)

(defvar *system-log* nil)

(defclass system-log-entry ()
  ((timestamp :initarg :timestamp
	      :initform (get-universal-time)
	      :accessor timestamp)
   (entry-type :initarg :type
	       :accessor log-entry-type
	       :initform (error "Provide entry type"))
   (description :initarg :description
		:accessor description
		:initform nil)
   (data :initarg :data
	 :accessor data
	 :initform nil)))
