(in-package :mold-desktop)

(mold-desktop/settings:defgroup repl nil
  "REPL settings")

(mold-desktop/settings:defcustom *repl-handle-errors*
  t
  "Handle REPL errors"
  :type boolean
  :group repl)

(defclass repl (object)
  ((history :initform nil
            :accessor repl-history
            :documentation "History of commands.")
   (package :initform (find-package :mold-desktop)
            :accessor repl-package
            :documentation "The Lisp package used for evaluation in REPL.")
   (environment :initform nil
                :accessor repl-environment
                :documentation "Environment with variable bindings."))
  (:metaclass mold-object-class)
  (:icon . "fa-solid fa-terminal")
  (:default-initargs
   :name "REPL"))

(defun repl-show-eval-result (expression result output-area)
  "Show the RESULT of EXPRESSION evaluation in OUTPUT-AREA."
  (let* ((expr-evaluation (create-list-item output-area :auto-place :top))
         (expr-div (create-div expr-evaluation :content (prin1-to-string expression)
                                               :class "expr"))
         (result-div (create-div expr-evaluation :style "max-height: 300px; overflow: scroll;"))
         (halos-wrapper (mold-desktop/clog-gui::create-halos-wrapper result-div result
                                                                     :active t
                                                                     :halos-position :top-right))
         (result-view (create-object-view result halos-wrapper)))
    (declare (ignore expr-div))
    (setf (clog:style result-view "min-height") "30px")))

(defun repl-show-error (error output-area)
  "Show REPL ERROR in OUTPUT-AREA."
  (create-list-item output-area :class "error"
                                :content (princ-to-string error)
                                :auto-place :top))

(defcommand (repl-eval :icon "fa fa-cog")
    ((repl repl "The REPL component")
     (expr string "The expression to evaluate"))
  "Evaluate COMMAND in REPL."
  (flet ((maybe-repl-handle (func)
           (if *repl-handle-errors*
               (handler-case
                   (funcall func)
                 (error (e)
                   (event-emitter:emit :error repl e)))
               (funcall func))))
    (maybe-repl-handle (lambda ()
                         (let ((*package* (repl-package repl)))
                           (let ((read-expr (read-from-string expr)))
                             (let ((result (eval read-expr)))
                               (push (list :expression read-expr
                                           :result result)
                                     (repl-history repl))
                               (event-emitter:emit :eval repl read-expr result))))))))

(defmethod create-object-view ((repl repl) clog-obj &rest args)
  (declare (ignore args))
  (let* ((repl-view (create-div clog-obj :class "repl"
                                         :style "display: flex; flex-direction: column; height: 100%;"))
         (output-area (create-unordered-list repl-view :class "w3-ul output-area"
                                                       :style "flex: 1; overflow:scroll;"))
         (command-input-area (create-div repl-view :class "command-input-area"
                                                   :style "margin-top:auto;"))
         (command-input (create-text-area command-input-area
                                          :rows 5
                                          :style "width: 100%;"))
         (eval-button (create-icon-button command-input-area "fa-solid fa-play"
                                          nil :class "w3-button w3-green"
                                          :style "width: 100%;")))

    (event-emitter:on :eval repl (lambda (expression result)
                                   (repl-show-eval-result expression result output-area)))
    (event-emitter:on :error repl (lambda (error)
                                    (repl-show-error error output-area)))

    (setf (clog:advisory-title eval-button) "Evaluate and print expression (Shift+Return)")
    (flet ((evaluate (&rest args)
             (declare (ignore args))
             (repl-eval repl (clog:value command-input))
             ;; scroll output area after evaluation
             ;; I don't know if these are working ...
             (js-execute clog-obj (ps:ps
                                    (ps:chain (clog-elem output-area)
                                      (scroll-top))))
             (js-execute clog-obj (ps:ps
                                    (ps:chain (clog-elem repl-view)
                                      (scroll-top))))
             ))
      (clog:set-on-key-press command-input
                             (lambda* (_ ev)
                               (when (and (string= (getf ev :key) "Enter")
                                          (getf ev :shift-key))
                                 (evaluate)))
                             :disable-default nil)
      (clog:set-on-click eval-button #'evaluate)
      repl-view)))
