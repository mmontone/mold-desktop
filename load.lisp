(declaim (optimize (speed 0) (compilation-speed 0) (safety 3) (debug 3)))

(setf sb-impl::*default-external-format* :utf-8)

(require :sb-cltl2)
(require :sb-concurrency)
(require :asdf)

;;; The following lines added by ql:add-to-init-file:
#-quicklisp
(let ((quicklisp-init (merge-pathnames "quicklisp/setup.lisp"
                                       (user-homedir-pathname))))
  (when (probe-file quicklisp-init)
    (load quicklisp-init)))


(asdf/source-registry:initialize-source-registry
 `(:source-registry :inherit-configuration
		    (:tree ,(uiop/pathname:pathname-directory-pathname *load-pathname*))))
   
(ql:quickload :mold-desktop)
(ql:quickload :mold-desktop-bookmarks)
(ql:quickload :mold-desktop-feeds)

(setf mold-desktop/clog-gui::*client-movement* t)

;; FIXME: do this on start, not on load
(setf mold-desktop::*config* (read-from-string (alexandria:read-file-into-string (merge-pathnames "config.lisp" *load-pathname*))))
