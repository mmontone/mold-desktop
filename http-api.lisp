;; http api objects

(in-package :mold-desktop)

(defclass http-api (object)
  ((baseurl :initarg :baseurl
	    :accessor baseurl
	    :type string
	    :initform ""
	    :attribute t)
   (endpoints :initarg :endpoints
	      :accessor endpoints
	      :type collection
	      :initform (make-instance 'collection
				       :type 'http-api-endpoint)
	      :attribute t))
  (:metaclass mold-object-class))

(defclass http-api-endpoint (object)
  (
   #+nil(path :initarg :path
	 :accessor endpoint-path
	 :initform ""
	 :type string
	 :attribute t)
   (http-method :initarg :http-method
		:accessor http-method
		:initform :get
		:type (member :get :post :delete :patch)
		:attribute t)
   (arguments :initarg :arguments
	      :accessor api-endpoint-arguments
	      :initform (make-instance 'collection
				       :type 'http-api-endpoint-argument)
	      :attribute t)
   (responses :initarg :responses
	      :accessor api-endpoint-responses
	      :initform (make-instance 'collection
				       :type 'http-api-response)
	      :attribute t))
  (:metaclass mold-object-class))

(defclass http-api-endpoint-argument (object)
  ((location :initarg :location
	     :accessor argument-location
	     :type (member :path :query :header :cookie)
	     :initform :path
	     :attribute t)
   (required :initarg :required
	     :accessor argument-required-p
	     :type boolean
	     :initform nil
	     :attribute t))
  (:metaclass mold-object-class))

(defclass http-api-response (object)
  ((code :initarg :code
	 :initform 200
	 :accessor http-code
	 :type (member 200 500)
	 :documentation "HTTP code"
	 :attribute t)
   #+todo(schema :initarg :schema
	   :accessor api-endpoint-schema
	   :type http-api-schema
	   :attribute t)
   )
  (:metaclass mold-object-class))

(defcommand (export-openapi-json
	     :icon "fa-solid fa-file-export")
    ((http-api http-api "The http-api to export")
     (pathname pathname "The pathname to export to"))

  ;; TODO
  )

(defcommand (import-openapi-json
	     :icon "fa-solid fa-file-import")
    ((pathname pathname "The pathname to import from"))
  ;; TODO
  (make-instance 'http-api))

(defcommand (execute-api-endpoint
	     :icon "fa-solid fa-cog")
    ((endpoint http-api-endpoint))
  "Prompt for arguments and execute the api endpoint."
  ;; TODO: prompt for arguments of api endpoint and execute it using HTTP-REQUEST
  ;; The resulting hash-table is inspectable in the environment.
  )

#+test
(show (make-instance 'http-api))
