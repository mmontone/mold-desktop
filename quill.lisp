;;;; * Quill richtext document editor

;; TODO: we are gonna need a custom Quill Blot for embedding objects: https://codepen.io/abolo/pen/xmzZbO
;; Extend either an Embed or an BlockEmbed Blot.

(in-package :mold-desktop)

(defun init-quill (body)
  (load-css (html-document body) "https://cdn.quilljs.com/1.3.6/quill.snow.css")
  (load-script (html-document body) "https://cdn.quilljs.com/1.3.6/quill.js")
  (load-script (html-document body) "quill/quill.js"))

(pushnew 'init-quill *init-functions*)

(defparameter *quill-toolbar-options*
  (list
   (list (sera:dict "font" #())
	 (sera:dict "size" #()))
   '("bold" "italic" "underline" "strike")
   (list (sera:dict "color" #())
	 (sera:dict "background" #()))
   (list (sera:dict "script" "super")
	 (sera:dict "script" "sub"))
   (list (sera:dict "header" 1)
	 (sera:dict "header" 2)
	 "blockquote"
	 "code-block")
   (list (sera:dict "list" "ordered")
	 (sera:dict "list" "bullet")
	 (sera:dict "indent" "-1")
	 (sera:dict "indent" "+1"))
   (list "direction"
	 (sera:dict "align" #()))
   '("link" "image" "video" "formula")
   '("clean")))

(defparameter *quill-theme* "snow")

(defparameter *quill-default-options*
  `((modules . ,(sera:dict "toolbar" *quill-toolbar-options*))
    (theme . ,*quill-theme*)))

(defclass quill-editor (clog-element)
  ((serialized-content :accessor serialized-content)
   (quill-handle :accessor quill-handle)
   (quill-document :initarg :quill-document
                   :accessor quill-document)
   (attached-objects-views :accessor attached-objects-views
                           :documentation "Hidden area where attached objects views are created into to be then transferred to the editor component.")))

(defclass quill-document (object)
  ((contents :initarg :contents
	     :accessor contents
	     :type string
	     :initform ""
	     :attribute t)
   (options :initarg :options
	    :accessor options
	    :initform nil)
   (attached-objects :accessor attached-objects
                     :initform nil
                     :documentation "List of objects embeded into this richtext."))
  (:metaclass mold-object-class)
  (:icon . "fa-regular fa-file-lines"))

(defgeneric create-document-view (object clog-obj &rest args)
  (:documentation "Create a view for OBJECT to be embedded into a document object."))

(defmethod create-document-view (object clog-obj &rest args)
  (apply #'create-object-view object clog-obj args))

(defmethod create-object-view ((object quill-document) clog-obj &rest args)
  (declare (ignore args))
  (let ((quill-editor (create-div clog-obj)))
    (change-class quill-editor 'quill-editor)
    (setf (attached-objects-views quill-editor)
          (create-div clog-obj :hidden t))
    (setf (quill-document quill-editor) object)
    (setf (quill-handle quill-editor) (format nil "window.quill~a" (html-id quill-editor)))
    
    (event-emitter:on 'object-attached object
                      (lambda ()
                        (let* ((attached-object (first (attached-objects object)))
                               (attached-object-view (mold-desktop/clog-gui::create-halos-wrapper (attached-objects-views quill-editor) attached-object)))
			  (create-document-view attached-object attached-object-view)
                          (js-execute clog-obj
				      (format nil
					      (format nil
                                              "quillInsertMoldObject(~a, {objectId: '~a', htmlId: '~a'})"
					      (quill-handle quill-editor)
					      (object-id object)
					      (html-id attached-object-view)))))))
    
    (js-execute clog-obj (format nil "~a = new Quill('#~a', ~a)"
				 (quill-handle quill-editor)
				 (html-id quill-editor)
				 (json:encode-json-to-string *quill-default-options*)))
    quill-editor))

;; Use: https://quilljs.com/docs/api/#insertembed
;; https://codepen.io/abolo/pen/xmzZbO
;; https://codepen.io/DmitrySkripkin/pen/oOBLdo
(defcommand insert-into-document
    ((quill quill-document "The Quill document")
     (object object "The object to insert"))
  "Insert OBJECT into RICHTEXT document, in embedded form."
  (push object (attached-objects quill))
  (event-emitter:emit 'object-attached quill))
