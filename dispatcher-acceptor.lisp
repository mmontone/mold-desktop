(defpackage :dispatcher-acceptor
  (:use :cl)
  (:export #:dispatcher-acceptor
           #:ssl-dispatcher-acceptor
           #:*dispatch-table*))

(in-package :dispatcher-acceptor)

(defvar *dispatch-table* nil)

(defclass dispatcher-acceptor (hunchentoot:acceptor)
  ())

(defclass ssl-dispatcher-acceptor (hunchentoot:ssl-acceptor)
  ())

(defmethod hunchentoot:acceptor-dispatch-request ((acceptor dispatcher-acceptor) request)
  (dolist (func *dispatch-table*)
    (let ((result (funcall func request)))
      (when result
        (return-from hunchentoot:acceptor-dispatch-request result))))
  (call-next-method))

(defmethod hunchentoot:acceptor-dispatch-request ((acceptor ssl-dispatcher-acceptor) request)
  (dolist (func *dispatch-table*)
    (let ((result (funcall func request)))
      (when result
        (return-from hunchentoot:acceptor-dispatch-request result))))
  (call-next-method))
