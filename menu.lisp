;; Simulate a menu using the command bars

(in-package :mold-desktop)

(defparameter *main-menu*
  '((:label "Objects"
     :submenus
     ((:label "New ..."
       ;; When a command is provided, use its icon in the menu
       :command mold-desktop/commands::create-new-object
       :interactive t)
      (:label "My collections"
       ;; When command is provided and label is absent, use the command name
       :command list-my-collections)
      (:label "Browse all"
       :command list-all-objects)))
    (:label "Workspaces"
     :submenus
     ((:label "New ..."
       :command add-workspace
       :interactive t)
      (:label "Switch to ..."
       :command switch-to-workspace
       :interactive t)))
    (:label "System"
     :submenus
     ((:label "Settings"
       :command mold-desktop/settings::customize-system)
      (:label "Load module"
       :command mold-desktop::load-module
       :interactive t)
      (:label "Load user data"
       :command mold-desktop/commands::load-system)
      (:label "Save user data"
       :command mold-desktop/commands::save-system)))
    (:label "Help"
     :submenus
     ((:label "Manual"
       :command open-mold-desktop-book)
      (:label "About"
       :action on-help-about)))))

(defun start-command-bar-menu (menu-spec &key handler stay-opened)
  "Start a menu using the command bar."
  (let ((nav (list menu-spec))) ;; level 0
    (let ((menu-level (car nav))
          (actions nil)
          (menu-entry-index 0))
      (labels
          ((build-menu-choice (menu-entry)
             (when (and (or (getf menu-entry :command)
                            (getf menu-entry :action))
                        (getf menu-entry :submenus))
               (error "Not supported action + submenus: ~s" menu-entry))
             (let* ((value (incf menu-entry-index))
                    (command (and (getf menu-entry :command)
                                  (find-command (getf menu-entry :command))))
                    (action (cond
                              ((getf menu-entry :submenus)
                               (lambda ()
                                 ;; Enter the submenu level
                                 (push (getf menu-entry :submenus) nav)
                                 (setf menu-level (car nav))
                                 (choose-menu)))
                              ((getf menu-entry :action)
                               (lambda ()
                                 (funcall (getf menu-entry :action))
                                 (when stay-opened
                                   (choose-menu))))
                              ((getf menu-entry :command)
                               (if (getf menu-entry :interactive)
                                   (lambda ()
                                     (run-command-interactively command
                                                                (lambda (_)
                                                                  (when stay-opened
                                                                    (choose-menu)))))
                                   (lambda ()
                                     (run-command command)
                                     (when stay-opened
                                       (choose-menu)))))
                              (t (funcall handler menu-entry))))
                    (icon (or (getf menu-entry :icon)
                              (and command (command-icon command))))
                    (label (or (getf menu-entry :label)
                               (and command (str:sentence-case (princ-to-string (command-name command))))
                               (error "Provide label for menu entry: ~s" menu-entry))))
               (push (cons value action) actions)
               (list :label label
                     :description (getf menu-entry :description)
                     :value value
                     :icon icon)))
           (choose-menu ()
             (setf actions nil)
             (setf menu-entry-index 0)
             (let* ((choices (mapcar #'build-menu-choice menu-level)))
               (command-bar-choose
                "Select menu: "
                (if (> (length nav) 1)
                    ;; We are in a submenu. Provide a back button.
                    (append choices '((:value :back
                                       :label "Go back"
                                       :icon "fa fa-solid fa-rotate-left")))
                    ;; else
                    choices)
                (lambda (selection)
                  (if (eql selection :back)
                      (progn
                        (pop nav)
                        (setf menu-level (car nav))
                        (choose-menu))
                      (let ((action (cdr (assoc selection actions))))
                        (funcall action))))
                *command-bar*))))
        (choose-menu)))))

;; (start-command-bar-menu *main-menu*)

(defun system-settings-menu ()
  (labels ((menu-for (variable-or-group)
             (typecase variable-or-group
               (mold-desktop/settings:custom-group
                (list
                 :label (str:sentence-case (princ-to-string (object-name variable-or-group)))
                 :icon "fa fa-solid fa-screwdriver-wrench"
                 :submenus (mapcar #'menu-for (members variable-or-group))))
               (mold-desktop/settings:custom-var
                (list
                 :label (str:sentence-case (princ-to-string (object-name variable-or-group)))
                 :icon "fa fa-solid fa-wrench"
                 :action (lambda ()
                           (run-command 'mold-desktop/settings:customize-variable (object-name variable-or-group))))))))
    (mapcar #'menu-for (members (mold-desktop/settings:system-settings)))))

;; (system-settings-menu)

;; (start-command-bar-menu (system-settings-menu))
;; (start-command-bar-menu (system-settings-menu) :stay-opened t)

(defcommand (open-settings-menu :icon "fa fa-cogs") ()
  "Open a menu with settings"
  (start-command-bar-menu (system-settings-menu)))
