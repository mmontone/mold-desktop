;; https://openlibrary.org/developers/api

(mold-desktop::defmodule mold-desktop::file-mold-module
  open-library
  :description "Open library books api")

(in-package :mold-desktop)

(defun olib-search-books (query)
  (json:decode-json-from-source
   (drakma:http-request
    "https://openlibrary.org/search.json"
    :parameters `(("q" . ,query))
    :want-stream t)))

(defun olib-get-book (isbn)
  "Fetch a book data from OpenLibrary by ISBN."
  (arrows:->
   (drakma:http-request
    "https://openlibrary.org/api/books"
    :parameters `(("bibkeys" . ,(format nil "ISBN:~a" isbn))
                  ("jscmd" . "details")
                  ("format" . "json"))
    :want-stream t)
   (json:decode-json-from-source)
   (cdar)))

(defun remove-if-from-plist (predicate plist)
  "Apply FUNCTION to ARGS, with NULL keyvals from ARGS removed."
  (loop for key in plist by #'cddr
        for val in (rest plist) by #'cddr
        when (not (funcall predicate key val))
          collect key
        when (not (funcall predicate key val))
          collect val))

(defun olib-create-book (book-data)
  "Create a EBOOK object from BOOK-DATA.
BOOK-DATA is the data fetched from OpenLibrary api."
  (let ((book
          (cond
            ((or (string= (acc:access book-data :type)
                          "edition")
                 (and (listp (access book-data :type))
                      (string= (acc:accesses book-data :type :key)
                               "/type/edition")))
             (acc:with-access (title author--name isbn) book-data
               (when isbn
                 (apply #'make-instance 'ebook
                        (remove-if-from-plist (lambda (k v)
                                                (declare (ignore k))
                                                (null v))
                                              (list :name title
                                                    :author (first author--name)
                                                    :isbn (first isbn)))))))
            ((or (string= (acc:access book-data :type)
                          "work")
                 (and (listp (access book-data :type))
                      (string= (acc:accesses book-data :type :key)
                               "/type/work")))
             ;; TODO: extract cover image from one of the work editions?
             (acc:with-access (title description) book-data
               (make-instance 'ebook
                              :name title
                              :description
                              (if (stringp (acc:access book-data :description))
                                  (acc:access book-data :description)
                                  (acc:accesses book-data :description :value)))))
            (t (error "Cannot parse book data in olib-create-book")))))
    (alex:appendf (book-data book) book-data)
    book))

(defcommand (open-library-search-books
             :icon "fa-solid fa-magnifying-glass")
    ((query string "Search query"))
  "Search openlibrary.org"
  (make-instance 'collection
                 :name (format nil "Open Library search: ~a" query)
                 :members
                 (arrows:as-> "https://openlibrary.org/search.json" $
                              (drakma:http-request $
                                                   :parameters `(("q" . ,query))
                                                   :want-stream t)
                              (json:decode-json-from-source $)
                              (acc:access $ :docs)
                              (mapcar #'olib-create-book $)
                              (remove-if #'null $))))

(defun olib-cover-image-url (isbn &optional (size :small))
  (format nil "https://covers.openlibrary.org/b/isbn/~a-~a.jpg"
          isbn
          (ecase size
            (:small "S")
            (:medium "M")
            (:large "L"))))

(defun extract-isbn (string)
  (->
   (loop for char across string
         when (digit-char-p char)
           collect char)
   (coerce 'string)))

(defun read-books-from-csv (pathname)
  (let* ((csv (fare-csv:read-csv-file pathname))
         (keys (first csv))
         (data (rest csv)))
    (flet ((-> (row key)
             "Access csv data at key"
             (let ((pos (or (position key keys :test #'string=)
                            (error "Bad csv key: ~s" key))))
               (nth pos row))))
      (mapcar (lambda (book-data)
                (let ((isbn (extract-isbn (-> book-data "ISBN"))))
                  (make-instance 'ebook
                                 :name (-> book-data "Title")
                                 :author (-> book-data "Author")
                                 :ISBN isbn
                                 :isbn13 (extract-isbn (-> book-data "ISBN13"))
                                 :cover-image (olib-cover-image-url isbn)
                                 :data (map 'list #'cons keys book-data))))
              data))))

(defcommand import-books
    ((pathname pathname "The pathname of the Goodreads CSV from where to import books."))
  (let* ((books (read-books-from-csv pathname)))
    (with-job (job :name "Books import"
                   :progress 0
                   :progress-max (length books)
                   :description (format nil "Import books from ~a" pathname)
		   :display-progress-details t
		   :display-progress-time t)
      (dolist (book books)
        ;; csv doesn't have a description for the book.
        ;; Use some api to get a description and add to the book here.
        (let* ((book-data (olib-get-book (isbn book)))
               (work-key (acc:accesses book-data :details :works 'car :key))
               (work-data (when work-key
                            (json:decode-json-from-source
                             (drakma:http-request (format nil "https://openlibrary.org~a.json" work-key)
                                                  :want-stream t))))
               (book-description (if (stringp (acc:access work-data :description))
                                     (acc:access work-data :description)
                                     (acc:accesses work-data :description :value))))
          (setf (object-description book) book-description))
        (push book *objects*)
        (incf (job-progress job))))))

(defmethod create-item-view ((object ebook) li &rest args &key context &allow-other-keys)
  (declare (ignore args))
  (with-ui-update li object :changed
    (when (cover-image object)
      (create-img li :url-src (cover-image object)
                     :style "height: 32px; width: 32px; margin-right: 5px;"))
    (let ((link
            (create-a li :content (or (object-name object) (object-url object)))))
      (set-on-event link "click"
                    (lambda* (_)
                      (show object :context context))
                    :cancel-event t))))

(defmethod create-thumbnail-view ((object ebook) clog-obj &rest args &key context &allow-other-keys)
  (let* ((view (create-div clog-obj :class "thumbnail"))
         (title (create-child view (html (:h5 :style "cursor:pointer;"
                                              (who:str (object-title object)))))))
    (when (cover-image object)
      (create-img view :url-src (cover-image object)
                       :style "margin: 5px; max-width: 200px; max-height: 200px; float: left;"))

    (create-child view (html (:p (:small (who:str (object-description object))))))
    (let ((click-handler (or (and (getf args :on-click)
                                  (lambda* (&rest rs)
                                    ;; Pass the clicked object to the click handler
                                    (apply (getf args :on-click) object rs)))
                             (lambda* (_)
                               (show object :context context)))))

      (set-on-event title "click" click-handler :cancel-event t))
    view))

(defmethod create-object-view ((object ebook) clog-obj &rest args &key context &allow-other-keys)
  (let* ((view (create-div clog-obj :class "thumbnail"))
         (title (create-child view (html (:h5 :style "cursor:pointer;"
                                              (who:str (object-title object)))))))
    (create-img view :url-src (cover-image object)
                     :style "margin: 5px; max-width: 200px; max-height: 200px; float: left;")

    (create-child view (html (:p (:small (who:str (object-description object))))))
    (let ((click-handler (or (and (getf args :on-click)
                                  (lambda* (&rest rs)
                                    ;; Pass the clicked object to the click handler
                                    (apply (getf args :on-click) object rs)))
                             (lambda* (_)
                               (show object :context context)))))

      (set-on-event title "click" click-handler :cancel-event t))
    view))

(defun match-while (predicate string)
  (arrows:->
   (loop
     with match
     for char across string
     while (funcall predicate char)
     do (push char match)
     finally (return match))
   (reverse)
   (coerce 'string)))

;; (match-while #'digit-char-p "123aaa")
;; (match-while (alex:compose #'not #'digit-char-p) "123aaa")
;; (match-while (alex:compose #'not #'digit-char-p) "abcd123")

(defun parse-open-library-url (url headers)
  (declare (ignore headers))
  (when (str:starts-with-p "https://openlibrary.org/works/" url)
    (let* ((work-id (match-while (lambda (char)
                                   (not (char= char #\/)))
                                 (subseq url (length "https://openlibrary.org/works/"))))
           (book
             (arrows:->
              (drakma:http-request (format nil "https://openlibrary.org/works/~a.json" work-id)
                                   :want-stream t)
              (json:decode-json-from-source)
              (olib-create-book))))
      (setf (object-url book) url)
      book)))

;; (parse-open-library-url "https://openlibrary.org/works/OL1287903W/Journey_to_the_End_of_the_Night_%28Voyage_au_bout_de_la_nuit%29" nil)

(pushnew 'parse-open-library-url *url-parsers*)

(pushnew :open-library *mold-modules*)
