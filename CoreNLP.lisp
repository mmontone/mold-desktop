(in-package :mold-desktop)

;; https://stanfordnlp.github.io/CoreNLP/corenlp-server.html
;; curl --data 'The quick brown fox jumped over the lazy dog.' 'http://localhost:9000/?properties={%22annotators%22%3A%22tokenize%2Cssplit%2Cpos%22%2C%22outputFormat%22%3A%22json%22}' -o -

;; Use 'ner' annotator: https://stanfordnlp.github.io/CoreNLP/ner.html

(defmethod json:encode-json ((object (eql :false)) &optional (stream json:*json-output*))
  (write-string "false" stream))

(defun corenlp-request (data &key (annotators "tokenize,ssplit,pos"))
  (json:decode-json-from-source
   (drakma:http-request "http://localhost:9000"
			:method :post
			:parameters
			(list (cons "properties"
				    (json:encode-json-alist-to-string
				     `(("annotators" . ,annotators)
				       ("ner.applyFineGrained" . :false)
				       ("outputFormat" . "json"))))
			      (cons "data" (json:encode-json-alist-to-string
					    (list (cons "data" data)))))
			:want-stream t)))

(defun entity-mentions (output)
  (loop for  sentence  in (access:accesses output :sentences)
	appending (access:access sentence :entitymentions)))

(defun list-entities (entity-type entities)
  (loop for entity in entities
	when (string= (access:access entity :ner) (string entity-type))
	  collect entity))

(defun find-entity (entity-type entities)
  (first (list-entities entity-type entities)))

(defun parse-calendar-event (text)
  (let* ((nlp-output (corenlp-request text :annotators "ner"))
	 (entities (entity-mentions nlp-output))
	 (dates (list-entities :date entities))
	 (title (find-entity :title entities))
	 (duration (find-entity :duration entities))
	 (time (find-entity :time entities))
	 (people (list-entities :person entities))
	 (urls (list-entities :url entities)))
    (list
     (cons "title" (access:access title :text))
     (cons "date" (access:accesses
		   (find-if (lambda (entity)
			      (chronicity:parse (access:accesses entity :timex :value)))
			    dates)
		   :timex :value))
     (cons "time" (access:accesses time :timex :value))
     (cons "duration" (acc:accesses duration :timex :value))
     (cons "people" (loop for person in people
			  collect (acc:access person :text)))
     (cons "urls" (loop for url in urls
			  collect (acc:access url :text))))))
