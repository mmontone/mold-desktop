;; Additional object views

(in-package :mold-desktop)

(defclass replacing-view (clog-element)
  ((history :initform nil
            :accessor view-history)
   (current-view :accessor current-view)))

(defmethod show-object (object (context replacing-view) view workspace)
  (push (current-view context) (view-history context))
  (add-class (current-view context) "w3-hide")
  (let ((new-view (funcall view object context :context context)))
    (setf (current-view context) new-view)))

(defmethod create-replacing-view (object clog-obj &key (view #'create-object-view))
  (let ((context-view (create-div clog-obj)))
    (change-class context-view 'replacing-view)
    (let ((back-btn (create-button context-view :class "w3-button")))
      (create-icon "fa-solid fa-arrow-left" back-btn)
      (set-on-click back-btn (lambda* (_)
                               (when (> (length (view-history context-view)) 0)
                                 (let ((previous-view (pop (view-history context-view))))
                                   (add-class (current-view context-view) "w3-hide")
                                   (remove-class previous-view "w3-hide")
                                   (setf (current-view context-view) previous-view))))))

    (let ((object-view (funcall view object context-view :context context-view)))
      (setf (current-view context-view) object-view))
    context-view))

#+example
(show (make-instance 'collection :members *objects*) :view #'create-replacing-view)

(defclass master-detail-view (clog-element)
  ((detail-view :accessor detail-view
                :initform nil)))

(defmethod show-object (object (context master-detail-view) view workspace)
  (clear-element (detail-view context))
  (let ((wrapper (mold-desktop/clog-gui:create-halos-wrapper (detail-view context) object :context context)))
    (funcall (or view (object-default-view object)) object wrapper :context context)))

(defmethod create-master-detail-view (object clog-obj &key view)
  (let* ((context-view (create-div clog-obj :class "split" :style "height:100%;"))
         (master-view (create-div context-view :style "overflow:auto;"))
         (detail-view (create-div context-view :class "w3-padding-small"
                                               :style "overflow:auto;")))
    (change-class context-view 'master-detail-view)
    (setf (detail-view context-view) detail-view)
    (funcall (or view (object-default-view object))
             object master-view :context context-view)
    (clog:js-execute clog-obj (format nil "Split(['#~a','#~a'])"
                                      (clog:html-id master-view)
                                      (clog:html-id detail-view)))
    context-view))

#+example
(show (make-instance 'collection :members *objects*) :view #'create-master-detail-view)

;; TODO: folding-view
;; A generalized multi-level master-detail view

(defview create-iframe-view (object clog-obj &rest args)
  (:documentation "Embed the webpage in an iframe."))

(defmethod create-iframe-view ((object web-page) clog-obj &rest args)
  (declare (ignore args))
  (create-child clog-obj
                (html (:iframe :src (object-url object)
                               :height "100%"
                               :width "100%"))))

(defmethod object-view-types append ((object web-page) (mold-module (eql :desktop)))
  '(create-iframe-view))

(defgeneric create-status-change-view (clog-obj status-change)
  (:documentation "Create a view for STATUS-CHANGE."))

(defmethod create-status-change-view (clog-obj (status-change standard-status-change))
  (clog:with-clog-create clog-obj
      (div (:bind view)
           (span (:class "timestamp" :content (timestamp status-change)))
           (unordered-list (:bind changes :class "changes")))
    (dolist (change (changes status-change))
      (destructuring-bind (slot &key from to) change
        (create-list-item changes :content (format nil "~a: ~a -> ~a" slot from to))))
    view))

(defview  create-breadcrumb-view (object clog-obj &rest args)
  (:documentation "Navigation view"))

(defmethod create-breadcrumb-view ((coll collection) clog-obj &rest args)
  (declare (ignore args))
  (let ((navigation-bar (create-unordered-list clog-obj :class "breadcrumb"))
        (navigation-body (create-div clog-obj))
        (navigation-history nil))
    (labels ((navigate-to (item)
               (loop while (not (eq (pop navigation-history) item)))
               (push item navigation-history)
               (render-current))
             (render-navigation-bar ()
               (setf (text navigation-bar) "")
               (dolist (item (butlast (reverse navigation-history)))
                 (let* ((li (create-list-item navigation-bar))
                        (link (create-a li :content (object-title item)))
                        (it item))
                   (set-on-click link (lambda* (&rest _)
                                        (navigate-to it)))))
               (create-list-item navigation-bar :content (object-title (car navigation-history))))
             (render-current ()
               (setf (text navigation-body) "")
               (let ((wrapper (mold-desktop/clog-gui:create-halos-wrapper navigation-body (car navigation-history))))
                 (create-object-view (car navigation-history) wrapper
                                     :item (list :on-select #'navigate-to-child)))
               (render-navigation-bar))
             (navigate-to-child (child)
               (push child navigation-history)
               (render-current)))
      (navigate-to-child coll))))

#+example
(show (make-instance 'collection :members *objects* :name "My objects") :view #'create-breadcrumb-view)

(defview  create-breadcrumb-detail-view (object clog-obj &rest args)
  (:documentation "Navigation view + detail"))

(defmethod create-breadcrumb-detail-view ((object object) clog-obj &rest args)
  (apply #'create-breadcrumb-detail-view (members object) clog-obj args))

(defmethod create-breadcrumb-detail-view ((coll collection) clog-obj &rest args)
  (declare (ignore args))
  (let* ((navigation-bar (create-unordered-list clog-obj :class "breadcrumb"))
         (navigation-body (create-div clog-obj :class "split" :style "height:100%;"))
         (master-view (create-div navigation-body :style "overflow:auto;"))
         (detail-view (create-div navigation-body :class "w3-padding-small"
                                                  :style "overflow:auto;"))
         (navigation-history nil))
    (labels ((navigate-to (coll)
               (loop while (not (eq (pop navigation-history) coll)))
               (push coll navigation-history)
               (render-current-collection))
             (render-navigation-bar ()
               (setf (text navigation-bar) "")
               (dolist (coll (butlast (reverse navigation-history)))
                 (let* ((li (create-list-item navigation-bar))
                        (link (create-a li :content (object-title coll)))
                        (it coll))
                   (set-on-click link (lambda* (&rest _)
                                        ;; If it has members, navigate
                                        (if (members it)
                                            (navigate-to it)
                                            ;; if it doesn't, show as detail
                                            (render-detail it)
                                            )))))
               (create-list-item navigation-bar :content (object-title (car navigation-history))))
             (render-detail (obj)
               (setf (text detail-view) "")
               (let ((wrapper (mold-desktop/clog-gui:create-halos-wrapper detail-view obj)))
                 (create-object-view obj wrapper)))
             (render-current-collection ()
               (setf (text master-view) "")
               (let ((wrapper (mold-desktop/clog-gui:create-halos-wrapper master-view (car navigation-history))))
                 (create-object-view (car navigation-history) wrapper
                                     :item (list :on-select #'navigate-to-child)))
               (render-navigation-bar))
             (navigate-to-child (child)
               (if (members child)
                   (progn
                     (push child navigation-history)
                     (render-current-collection))
                   (render-detail child))))
      (navigate-to-child coll)
      (clog:js-execute clog-obj (format nil "Split(['#~a','#~a'])"
                                        (clog:html-id master-view)
                                        (clog:html-id detail-view))))))

#+example
(show (make-instance 'collection :members *objects* :name "My objects") :view #'create-breadcrumb-detail-view)
