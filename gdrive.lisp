(defpackage :mold-desktop/gdrive
  (:use :cl))

(in-package :mold-desktop/gdrive)

(defclass google-service ()
  ((client-id)
   (client-secret)))

(defclass gdrive-service (google-service)
  ())

(defun gdrive-upload (gdrive pathname)
  (json:decode-json-from-source
   (drakma:http-request "https://www.googleapis.com/upload/drive/v3/files?uploadType=multipart"
		       :method :post
		       ;;:content-type "application/json; charset=UTF-8"
		       :content pathname
		       :want-stream t)))		

