# Mold Desktop

A Programmable Desktop.

## Inspirations

- McCLIM (commands, presentations).
- NakedObjects (expose object's properties to UI, commands apply to specific domain models).
- Emacs (commands, programmability).
- Smalltalk (halos, programmability).

## Running

Download somewhere where QuickLisp can find the project, then:

```lisp
(ql:quickload :mold-desktop)
(mold-desktop:start)
```

Use `qlot` for managing the dependencies:

Install `qlot` if you don't have it:

```
 curl -L https://qlot.tech/installer | sh
```

then install dependencies:

`qlot install`

start a Lisp repl with in the `qlot` environment:

`qlot exec sbcl`

Load and start:

```lisp
(ql:quickload :mold-desktop)
(mold-desktop:start)
```

## Development

When working with a remote SWANK, we would like `*trace-output*` and `*standard-output*` be bound to SLIME streams. But inside threads they are not. This binds them to top-level and makes them accessible via connected SLIME.
```lisp
(setq bt:*default-special-bindings*
      (list (cons '*trace-output* *trace-output*)
            (cons '*standard-output* *standard-output*)))
```

## Presentation Video

[![Overview video](screenshots/video-screenshot.png)](https://drive.google.com/file/d/1ivbHlsgn2aKuRHp3Kgi87_4uBl0yZ4f8/view?usp=sharing "Overview video")

## Screenshots

![screenshot1](screenshots/screenshot1.webp)
![screenshot2](screenshots/screenshot2.webp)

## References

- [EMACS: The Extensible, Customizable Display Editor](https://www.gnu.org/software/emacs/emacs-paper.html)
- [Computational media](https://science-in-the-digital-era.khinsen.net/?s=03#Computational%20media "Computational media")
- [Presentation Based User Interface](https://dspace.mit.edu/handle/1721.1/6946)
- [Future of software](http://pchiusano.github.io/2013-05-22/future-of-software.html)
- [Type systems and UX example](http://pchiusano.github.io/2013-09-10/type-systems-and-ux-example.html)
- [Direct Combination - A new user interaction](https://www.researchgate.net/publication/221270772_Direct_Combination_A_New_User_Interaction)
- [Living in Your Programming Environment](https://www.hpi.uni-potsdam.de/hirschfeld/publications/media/ReinLinckeRamsonMattisHirschfeld_2017_LivingInYourProgrammingEnvironmentTowardsAnEnvironmentForExploratoryAdaptationsOfProductivityTools_AcmDL.pdf)
- [Naked Objects](http://downloads.nakedobjects.net/resources/Pawson%20thesis.pdf)
