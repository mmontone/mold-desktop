(in-package :mold-desktop)

(defgeneric create-naked-view (object clog-obj &rest args)
  (:documentation "Create a view for OBJECT with its attributes exposed.")
  (:method (object clog-obj &rest args)
    (apply #'create-object-view object clog-obj args)))

(defmethod create-naked-view ((object object) clog-obj &rest args)
  "Create a naked view for OBJECT."
  (declare (ignore args))
  (let* ((table (clog:create-table clog-obj :class "w3-table w3-bordered"))
	 (tbody (clog:create-table-body table)))
    (with-ui-update tbody object :changed
      (dolist (slot (object-attributes object))
	(let* ((row (clog:create-table-row tbody))
               (label (clog:create-table-column
                       row
                       :content (arrows:->>
				 (c2mop:slot-definition-name slot)
				 (princ-to-string )
				 (str:sentence-case)
				 (format nil "~a: "))))
               (value (clog:create-table-column
                       row
                       :content (princ-to-string (slot-value object (c2mop:slot-definition-name slot))))))
	  (when (documentation slot t)
	    (setf (clog:advisory-title label) (documentation slot t)))
	  (clog:set-on-click label
			     (lambda (&rest args)
			       (declare (ignore args))
			       (show (slot-value object (c2mop:slot-definition-name slot))
				     :view #'create-naked-view)))
	  (clog:set-on-double-click value
				    (lambda (&rest args)
				      (declare (ignore args))
				      (run-command-interactively 'mold-desktop/commands::set-attribute-value object (c2mop:slot-definition-name slot))))
	  )))
    table))

(defmethod object-view-types append ((object object) (mold-module (eql :naked)))
  '(create-naked-view))

(pushnew :naked *mold-modules*)
