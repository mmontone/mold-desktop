(in-package :mold-desktop)

(defvar *dashboard-enabled* nil)
(defvar *dashboard*)

(pushnew 'init-dashboard *init-functions*)

(defun create-dashboard (ev)
  (when *dashboard-enabled*
    (setf *dashboard*
          (create-div *app-body* :class "grid-stack"))
    (clog:js-execute *app-body* "window.dashboard = GridStack.init()")))

(defun init-dashboard (body)
  (add-script (html-document body) "https://cdnjs.cloudflare.com/ajax/libs/gridstack.js/7.2.2/gridstack-all.min.js")

  (load-css (html-document body) "https://cdnjs.cloudflare.com/ajax/libs/gridstack.js/7.2.2/gridstack.min.css"))

(defmethod clog::set-attribute ((obj clog-obj) attr-name value)
  (jquery-execute obj (format nil "attr('~A','~A')" attr-name (escape-string value)))
  value)

(defun add-to-dashboard (object &rest args)
  "Add OBJECT to dashboard."
  (let* ((item (create-div *dashboard* :class "grid-stack-item"))
         (item-content (create-div item :class "grid-stack-item-content"))
         (halos (mold-desktop/clog-gui::create-halos-wrapper item-content object)))
    (create-object-view object halos)
    (clog:js-execute *body* (format nil "window.dashboard.addWidget(document.getElementById('~a'), ~a)"
                                    (html-id item)
                                    (json:encode-json-plist-to-string args)))
    nil))

#+example
(loop for obj in (subseq *objects* 10 20)
      do (add-to-dashboard obj :w 4 :h 2))

(defcommand mold-desktop/commands::add-to-dashboard
    ((object object))
  "Add OBJECT to dashboard"
  (add-to-dashboard object :w 4 :h 2))

(defclass dashboard-workspace (workspace)
  ((objects :accessor workspace-objects
            :initform nil)
   (gridstack-config :initform nil)))

(defclass dashboard-element (clog:clog-div)
  ())

(defmethod create-dashboard-view (collection clog-obj &rest args)
  (declare (ignore args))
  (let ((dashboard-div (create-div clog-obj :class "grid-stack")))
    (change-class dashboard-div 'dashboard-element)
    (dolist (item (members collection))
      (let* ((dashboard-item (create-div dashboard-div :class "grid-stack-item"))
             (dashboard-item-content (create-div dashboard-item :class "grid-stack-item-content"))
             (halos (mold-desktop/clog-gui::create-halos-wrapper dashboard-item-content item)))

        (create-object-view item halos)))
    (clog:js-execute *app-body* (format nil "GridStack.init({},'#~a')"
                                        (html-id dashboard-div)))
    dashboard-div))

(defmethod object-view-types append ((collection collection) (mold-module (eql :dashboard)))
  '(create-dashboard-view))

(pushnew :dashboard *mold-modules*)
