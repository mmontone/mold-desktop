;; This integrates x-spreadsheet Javascript component.
;; But, how about a native CLOG spreadsheet?:

;; Xerox had developed other cool stuff not shown here. One was a fully object-oriented spreadsheet. You weren't limited to certain data types like text, numbers, currency. You could define any data type with the rules to manipulate it in the spreadsheet. So you could stick images in the cells with rules to do image processing, or stick music notes in the cells and use the spreadsheet to compose music, and so on. This spreadsheet still has no equal today. Xerox never marketed it.

(in-package :mold-desktop)

;; https://github.com/myliang/x-spreadsheet

(defun init-spreadsheet (body)
  (load-css (html-document body) "https://unpkg.com/x-data-spreadsheet@1.1.5/dist/xspreadsheet.css")
  (load-script (html-document body) "https://unpkg.com/x-data-spreadsheet@1.1.5/dist/xspreadsheet.js"))

(pushnew 'init-spreadsheet *init-functions*)

(defclass spreadsheet (object)
  ((data :initarg :data
         :accessor spreadsheet-data
         :initform nil))
  (:metaclass mold-object-class)
  (:icon . "fa fa-table"))

(defmethod create-object-view ((object spreadsheet) clog-obj &rest args)
  (declare (ignore args))
  (let ((div (create-div clog-obj)))
    (js-execute clog-obj (format nil "window.spreadsheet~a = x_spreadsheet('#~a')" (html-id div) (html-id div)))
    div))

#+example
(show (make-instance 'spreadsheet))
