(in-package :mold-desktop)

(defcommand mold-desktop/commands::apropos-all
    ((query string "The string to match in objects' names"))
  "Search all objects matching a certain string in their name."
  (cond
    ((str:starts-with-p "tag:" query)
     (let* ((tag (string-trim '(#\space) (subseq query (1+ (position #\: query)))))
            (collection (make-instance 'collection
                                       :name (format nil "Search by tag: ~a" tag)
                                       :members (remove-if-not
                                               (lambda (object)
                                                 (member tag (tags object) :test 'string=))
                                               *objects*))))
       (open-object-in-window collection *body*)))
    ((str:starts-with-p "type:" query )
     (let* ((type (read-from-string (string-upcase (subseq query (1+ (position #\: query))))))
            (collection (make-instance 'collection
                                       :name (format nil "Search by type: ~a" type)
                                       :members (remove-if-not
                                               (lambda (object)
                                                 (typep object type))
                                               *objects*))))
       (open-object-in-window collection *body*)))
    (t
     (let ((collection (make-instance 'collection
                                      :name (format nil "Search: ~a" query)
                                      :members (remove-if-not (lambda (object)
                                                              (or (and (object-name object) (search query (object-name object)))
                                                                  (and (object-description object) (search query (object-description object)))
                                                                  (and (object-url object) (search query (object-url object)))))
                                                            *objects*))))
       (open-object-in-window collection *app-body*)))))
