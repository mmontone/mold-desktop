;; A collection that uses previous user decisions to decide where to insert a new object

(in-package :mold-desktop)

(defvar *default-classifier-init* '(levenshtein-collection-classifier))

(defun make-default-collection-classifier ()
  (apply #'make-instance *default-classifier-init*))

(defclass classifier-collection (collection)
  ((classifier :initform (make-default-collection-classifier)
               :initarg :classifier
               :accessor collection-classifier))
  (:metaclass mold-object-class)
  (:icon . "fa-solid fa-list")
  (:documentation "A collection of object that classifies automatically."))

(defclass collection-classifier ()
  ((max-suggestions :initarg :max-suggestions
                    :initform 10
                    :accessor max-suggestions)))

(defparameter *levenshtein-minimum-similarity* 0.6)

(defclass levenshtein-collection-classifier (collection-classifier)
  ((history :accessor collection-history
            :initarg :history
            :initform (make-hash-table)
            :documentation "A table from object type to possible subcollections, with a probability number.")
   (minimum-similarity :initarg :minimum-similarity
                       :accessor minimum-similarity
                       :initform *levenshtein-minimum-similarity*)))

(defgeneric classify-object (classifier object coll)
  (:documentation "Classify OBJECT into COLLECTION using CLASSIFIER.
Returns a list of suggested recipients for OBJECT."))

(defgeneric classifier-add (classifier object recipient))

(defun calculate-similarity (a b)
  "Number between 0 and 1 of how similar OBJ is to OBJECT. 1 if it is very similar, 0 if not similar at all."
  (+ (* (if (equalp (type-of a) (type-of b))
            1 0)
        0.5)
     (* (if (typep a (type-of b))
            1
            0)
        0.2)
     (*
      (if (and (object-url a) (object-url b))
          (mk-string-metrics:norm-levenshtein (object-url a) (object-url b))
          0)
      0.3)))

(defmethod classify-object ((classifier levenshtein-collection-classifier)
                            object coll)
  ;; If there's no history, then no suggestions
  (if (zerop (hash-table-count (collection-history classifier)))
      (list)
      ;; else
      (let ((suggestions (list)))
        (serapeum:do-hash-table (key value (collection-history classifier))
          (let ((r (calculate-similarity object key)))
            (format t "Similiarity between ~a and ~a: ~a~%" key object r)
            (when (> r (minimum-similarity classifier))
              (push (cons value r) suggestions))))
        (setf suggestions (remove-duplicates (mapcar #'car (sort suggestions #'> :key #'cdr))))
        (subseq suggestions 0 (min (length suggestions)
                                   (max-suggestions classifier))))))

(defmethod classifier-add ((classifier collection-classifier) object recipient))
(defmethod classifier-add ((classifier levenshtein-collection-classifier) object recipient)
  ;; Record where OBJECT was added
  (setf (gethash object (collection-history classifier)) recipient))

(defmethod collection-add :after ((coll classifier-collection) object)
  (classifier-add (collection-classifier coll) object coll))

(defcommand classify-and-add
    ((coll classifier-collection)
     (object object))
  "Classify object and add to COLL after providing suggestions."
  (let ((suggestions (classify-object (collection-classifier coll) object coll)))
    (command-bar-choose "Add to: " (append suggestions (list coll))
                        (lambda (suggestion)
                          (collection-add suggestion object))
                        *command-bar*)))

;; User collections

(defparameter *user-history* (make-hash-table))
(defparameter *user-classifier-init* `(levenshtein-collection-classifier :history ,*user-history*))

(defclass user-collection (classifier-collection)
  ()
  (:default-initargs
   :classifier (apply #'make-instance *user-classifier-init*))
  (:metaclass mold-object-class)
  (:icon . "fa-solid fa-list")
  (:documentation "A collection for user objects."))

(defclass bfs-levenshtein-collection-classifier (collection-classifier)
  ((minimum-similarity :initarg :minimum-similarity
                       :accessor minimum-similarity
                       :initform *levenshtein-minimum-similarity*)))

(defun bfs (function collection &key (test (constantly t)))
  "Apply FUNCTION to COLLECTION members in breath first search order."
  (dolist (member (members collection))
    (when (funcall test member)
      (funcall function member)))
  (dolist (member (members collection))
    (bfs function member)))

(defun dfs (function collection &key (test (constantly t)))
  "Apply FUNCTION to COLLECTION members in depth first search order. "
  (dolist (member (members collection))
    (dfs function (members member))
    (when (funcall test member)
      (funcall function member))))

(defmethod classify-object ((classifier bfs-levenshtein-collection-classifier) object coll)
  ;; lists of similarity assignments to collections
  (let ((assignments (list)))
    (flet
        ((assign-ranking (coll)
           ;; sum the similarity of collection contents
           ;; the more similar items the more ranking
           (let ((ranking 0)
                 (members (remove-if (lambda (x) (typep x 'collection))
                                     (members coll))))
             (when members
               (dolist (member members)
                 (incf ranking (calculate-similarity object member)))
               (setf ranking (/ ranking (length members)))
               (push (cons coll ranking) assignments)))))
      (assign-ranking coll)
      (bfs #'assign-ranking coll :test (lambda (x) (typep x 'collection)))
      (setf assignments (sort assignments #'> :key #'cdr))
      (mapcar #'car
              (subseq assignments 0 (min (length assignments)
                                         (max-suggestions classifier)))))))
