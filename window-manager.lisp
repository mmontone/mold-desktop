;; Copyright (C) 2022 Mariano Montone - All Rights Reserved
;; You may use, distribute and modify this code under the terms of the
;; GNU Affero General Public License

(in-package :mold-desktop)

(defvar *window-manager* nil
  "The current window manager.")

(defclass window-manager ()
  ((windows :accessor windows
	    :initform nil))
  (:documentation "Superclass of window managers.
Window managers are in charge of creating windows."))

(defgeneric init-window-manager (window-manager clog-obj &rest args)
  (:documentation "Initialize WINDOW-MANAGER."))
(defgeneric create-window (window-manager clog-obj &rest args)
  (:documentation "Create a window."))
(defgeneric wm/close-window (window-manager window)
  (:documentation "Close WINDOW."))

(defmethod :around create-window ((wm window-manager) clog-obj &rest args)
  "Push the created window to the list of windows in WM window-manager."
  (declare (ignore clog-obj args))
  (let ((window (call-next-method)))
    (push window (windows wm))
    window))

(defmethod wm/close-window ((wm window-manager) window)
  (close-window window)
  (setf (windows wm) (delete window (windows wm))))

;;---- CLOG window manager -----------------------------------

(defclass clog-window-manager (window-manager)
  ())

(defmethod init-window-manager ((wm clog-window-manager) clog-obj &rest args)
  (declare (ignore args)))

(defmethod create-window ((wm clog-window-manager) clog-obj &rest args)
  (let ((wnd (apply #'mold-desktop/clog-gui:create-gui-window clog-obj args)))
    (mold-desktop/clog-gui:window-content wnd)))

;;------ Halos window manager --------------------------------

(defclass halos-window-manager (window-manager)
  ())

(defmethod init-window-manager ((wm halos-window-manager) clog-obj &rest args)
  (declare (ignore args)))

(defmethod create-window ((wm halos-window-manager) clog-obj &rest args)
  (let ((wnd (apply #'mold-desktop/clog-gui::create-halos-window
                    clog-obj
                    (getf args :object)
                    (alexandria:remove-from-plist args :object))))
    (setf (left wnd) (format nil "~apx" (+ 300 (random 100))))
    (setf (top wnd) (format nil "~apx" (+ 100 (random 100))))
    (setf (height wnd) "300px")
    (setf (width wnd) "500px")
    wnd))

;;------ Tabs window manager ---------------------------------

(defclass tabs-window-manager (window-manager)
  ((tab-bar :accessor tab-bar)
   (tabs :initform nil
         :accessor tabs)
   (tab-content :accessor tab-content)
   (current-tab :initform nil
                :accessor current-tab)))

(defmethod init-window-manager ((wm tabs-window-manager) clog-obj &rest args)
  (declare (ignore args))
  (setf (tab-bar wm)
        (create-div clog-obj :class "w3-bar w3-green"))
  (setf (tab-content wm)
        (create-div clog-obj)))

(defun set-current-tab (wm tab)
  (when (current-tab wm)
    (setf (hiddenp (cdr (current-tab wm))) t))
  (setf (current-tab wm) tab)
  (setf (hiddenp (cdr tab)) nil))

(defun close-tab (wm tab)
  (setf (tabs wm)
        (delete tab (tabs wm)))
  (clog:remove-from-dom (cdr tab)))

(defmethod create-window ((wm tabs-window-manager) clog-obj &rest args)
  (let* ((tab-name (or (getf args :title) "untitled"))
         (tab-label (str:shorten 20 tab-name))
         (tab-id tab-label)
         (tab-button (create-child (tab-bar wm)
                                   (html (:button :class "w3-bar-item w3-button"
                                                  (who:str tab-label)))))
         (close-tab-button (create-child tab-button (html (:span :class "w3-hover-red" :style "cursor:pointer;user-select:none;padding:1px;"
                                                                 (who:str "&times")))))
         (tab (cons tab-id (create-div (tab-content wm) :class "w3-white"))))
    (push tab (tabs wm))
    (set-current-tab wm tab)
    (set-on-click tab-button (lambda (_)
                               (declare (ignore _))
                               (set-current-tab wm tab)))
    (set-on-click close-tab-button (lambda (_)
                                     (declare (ignore _))
                                     (clog:remove-from-dom tab-button)
                                     (close-tab wm tab)))
    (cdr tab)))
