;; Example of a remote object
;; Remote objects don't require CLOG or Mold Desktop. They only need a web server, a transport (JSON, XML) and HTML.
;; Remote objects are first asked their manifest, then its commands and view endpoints are invoked.

(require :hunchentoot)
(require :cl-who)
(require :cl-json)

(defpackage :remote-task-list
  (:local-nicknames
   (:ros :remote-object-server))
  (:use :cl))

(in-package :remote-task-list)

(ros:define-service tasks-service ()
  ((task-lists :initform nil
               :accessor task-lists))
  (:documentation "Tasks lists")
  (:default-initargs
   :object-types '(task-list task)
   :commands '(create-task add-task remove-task)))

(ros:define-object task-list ()
  ((title :initarg :title
          :accessor title)
   (tasks :initform nil
          :accessor tasks))
  (:documentation "A list of tasks"))

(ros:define-object task ()
  ((title :initarg :title
          :accessor title))
  (:documentation "A task"))

(defmethod ros:render-view ((view (eql 'default-view)) (task-list task-list))
  (who:with-html-output-to-string (html)
    (:ul :class "task-list"
         (dolist (task (tasks task-list))
           (who:htm (:li :data-type "remote-object"
                         :data-rotype "task"
                         :data-roid (ros:object-id task)
                         (:p (who:str (title task)))
                         (:button :command "operate" :remote-object (task-id task))))))))

(ros:define-command create-task ((task-list task-list) (title string))
  "Add a task with TITLE to TASK-LIST."
  (let ((task (make-instance 'task
                             :title title)))
    (push task (tasks task-list))))

(ros:define-command add-task ((task-list task-list) (task task))
  (push task (tasks task-list)))

(ros:define-command remove-task ((task-list task-list) (task task))
  (setf (tasks task-list)
        (delete task (tasks task-list))))

(ros:define-command move-task ((task-list task-list) (task task))
  )

(ros:define-command list-task-lists ()
  "Get the list of task lists"
  (task-lists (find-service 'tasks)))

(setf dispatcher-acceptor:*dispatch-table*
 (list (ros:create-service-dispatcher
  (make-instance 'tasks-service
                 :name "tasks"
                 :documentation "Tasks lists"
                 :url "/tasks-list")
  "/tasks-list-demo")))

(defvar *acceptor* nil)

(defun start (&optional (port 0))
  (setf *acceptor*
        (hunchentoot:start (make-instance 'dispatcher-acceptor:dispatcher-acceptor :port port))))

(defun stop ()
  (hunchentoot:stop *acceptor*))

;; (start)
;; (stop)
