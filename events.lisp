;; Copyright (C) 2022 Mariano Montone - All Rights Reserved
;; You may use, distribute and modify this code under the terms of the
;; GNU Affero General Public License

(defpackage :trivial-events
  (:use :cl)
  (:export
   :event))

(in-package :trivial-events)

(defclass event ()
  ((source :initarg :source
	   :accessor event-source)))

(defclass changed-event (event)
  ())

;; TODO: use week references
(defclass event-source ()
  ((event-handlers :accessor event-handlers
		   :initform (list))))

(defun make-event (event-type &rest args)
  (apply #'make-instance event-type args))

(defun event-handler-equal (entry1 entry2)
  (and (equal (first entry1) (first entry2)) ;; event-type
       (equal (second entry1) (second entry2)) ;; receiver
       ))

(defmethod add-event-handler ((event-source event-source) event-type handler &optional (receiver (gensym)))
  "Add HANDLER as event handler for EVENT-SOURCE.
When RECEIVER is given, then the HANDLER overwrites previous registrations."
  (let ((event-handler (list event-type receiver handler)))
    (pushnew event-handler (event-handlers event-source))))

(defmethod trigger-event ((event-source event-source) event-type &rest args)
  (let ((event (apply #'make-event event-type args)))
    (dolist (event-handler (event-handlers event-source))
      (when (equal (first event-handler) event-type)
	(funcall (third event-handler) event)))))
