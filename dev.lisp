;; When working with a remote SWANK, we would like *trace-output* and *standard-output* be bound to SLIME streams. But inside threads they are not. This binds them to top-level and makes them accessible via connected SLIME.
(setq bt:*default-special-bindings*
      (list (cons '*trace-output* *trace-output*)
            (cons '*standard-output* *standard-output*)))
