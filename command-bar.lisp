(in-package :mold-desktop)

;;;; * Command bar
;;;;
;;;; The command bar is implemented as a state machine.
;;;; Most of the operations are delegated to the command bar status.

(defparameter *default-command-bar-options*
  '(:partial-command-matching t)
  "Options for the command bar.")

(defvar *command-bar-prompt-optional-args* nil
  "Wether to prompt for commands optional and keyword arguments.")

(defvar *command-bar-toggle-prompt-optional-args-modifier*
  :ctrl-key
  "Modifier key to toggle commands optional and keyword arguments prompt.
See CLOG::PARSE-KEYBOARD-EVENT and CLOG::PARSE-MOUSE-EVENT")

(defclass mold-desktop-command-bar (clog-div)
  (;; UI elements
   (command-prompt :accessor command-prompt
                   :documentation "The command prompt UI element.")
   (user-input :accessor user-input
               :documentation "The HTML input element the command bar uses to receive user input.")
   (status-message-area :accessor status-message-area)
   (completions-panel :accessor completions-panel
                      :documentation "The UI panel with current completions available.")
   (message-panel :accessor message-panel
                  :documentation "The UI panel in which command bar shows messages to the user.")
   (selections-panel :accessor selections-panel
                     :documentation "The UI with currently selected objects.")
   (workspaces-area :accessor workspaces-area
                    :documentation "Area with workspace switching buttons")
   completion-items

   ;; Model
   (opened :accessor command-bar-opened
           :initform nil)
   (status-message :accessor status-message
                   :initform "")
   (completions :accessor completions
                :initform nil
                :documentation "The list of current completions available for the user input.")
   (completion-index :accessor completion-index
                     :initform 0
                     :documentation "Index of the currently selected completion")
   (selections :accessor selections
               :initform nil
               :documentation "The list of selected objects.")
   (status :accessor command-bar-status)

   ;; Settings
   (completion-method :initarg :completion-method
                      :accessor completion-method
                      :initform 'completion-substr-match
                      :documentation "The function to use for matching completions.")
   (options :initarg :options
            :accessor command-bar-options
            :initform *default-command-bar-options*))
  (:documentation "Command bar"))

(defmethod initialize-instance :after ((command-bar mold-desktop-command-bar) &rest initargs)
  (declare (ignore initargs))
  (anaphora:awhen (access:accesses *config* :command-bar :completion-method)
    (setf (completion-method command-bar) anaphora:it)))

(defun command-bar-start (command-bar)
  "Start the COMMAND-BAR."
  ;; When the command bar starts, its input is cleared.
  (setf (element-value (user-input command-bar)) ""))

(defgeneric print-completion (thing))

(defmethod print-completion (thing)
  (who:escape-string (princ-to-string thing)))

(defmethod print-completion ((thing mold-desktop::command))
  "Print a command in command-bar."
  ;; Look at settings
  (case (access:accesses *config* :command-bar :command-printing)
    (:name-lowercase (string-downcase (symbol-name (command-name thing))))
    (:name (string-downcase (symbol-name (command-name thing))))
    (:humanized (humanized-symbol-name (symbol-name (command-name thing))))
    (t (humanized-symbol-name (symbol-name (command-name thing))))))

(defgeneric completion-icon (thing))
(defmethod completion-icon (thing)
  nil)
(defmethod completion-icon ((thing mold-desktop::command))
  (command-icon thing))

(defun init-command-bar (command-bar body app-body)
  "Initialize command bar"
  (declare (ignorable app-body))
  (set-on-key-press body (lambda* (_ event)
                           (when (and (getf event :ctrl-key)
                                      (string= (getf event :key) "Enter"))
                             (open-command-bar command-bar))))
  (set-on-key-down body (lambda* (_ event)
                          (when (string= (getf event :key) "Escape")
                            ;; If it is in initial state, reset the command bar, otherwise, close
                            (if (and (zerop (length (user-input-value command-bar)))
                                     (typep (command-bar-status command-bar) 'command-input-cb-status))
                                (close-command-bar command-bar)
                                (reset-command-bar command-bar)
                                ))))
  #+nil(set-on-click app-body (lambda (&rest args)
                                (declare (ignore args))
                                (close-command-bar command-bar)))
  )

(defun prompt-command-optional-arguments? (clog-event)
  (if (getf clog-event *command-bar-toggle-prompt-optional-args-modifier*)
      (not *command-bar-prompt-optional-args*)
      *command-bar-prompt-optional-args*))

(defgeneric cb-status-drop-allowed? (cb-status object)
  (:documentation "Returns wether drop is allowed to the command bar in current state.")
  ;; By default, drop is not allowed
  (:method (cb-status object)
    nil))

(defgeneric cb-status-handle-drop (cb-status object command-bar)
  (:documentation "Generic function specialized on command bar status, the OBJECT being dropped, and the COMMAND-BAR instance.
Specializers should implement what to do when OBJECT is dropped into COMMAND-BAR under CB-STATUS."))

(defmethod cb-status-handle-drop (cb-status object command-bar)
  ;; By default, do nothing
  nil)

(defun drag-over-command-bar (command-bar object)
  ;; OBJECT drop is allowed depending on the command-bar status
  ;; So we let the command bar status instance decide.
  (when (cb-status-drop-allowed? (command-bar-status command-bar) object)
    (add-class command-bar "drop-allowed")))

(defun drag-leave-command-bar (command-bar)
  (remove-class command-bar "drop-allowed"))

(defun drop-on-command-bar (command-bar object)
  "Action to perform when OBJECT is dropped into COMMAND-BAR."
  ;; What to do on OBJECT drop depends on the command-bar status
  (when (cb-status-drop-allowed? (command-bar-status command-bar) object)
    (cb-status-handle-drop (command-bar-status command-bar) object command-bar)))

(defun create-loading-indicator (clog-obj)
  (let ((loading-indicator
          (create-child clog-obj
                        (html (:div :class "w3-spin"
                                    :style "width: 16px; height: 16px; flex-shrink:0;margin-top: 5px; margin-right: 5px;"
                                    (:i :class "fa fa-cog w3-center"
                                        :style "font-size:16px; line-height: 16px;"))))))
    (clog:set-on-click loading-indicator
                       (ignoring-args (show-running-jobs)))
    loading-indicator))

(defun create-command-bar (obj &rest initargs)
  "Attached a command bar to a CLOG-OBJ in general a clog-body."
  (let* ((command-bar (create-div obj :class "mold-command-bar w3-bar w3-light-gray w3-card-4"))
         (bar (create-div command-bar))
         (cb-body (create-div command-bar :class "w3-row")))
    (set-styles bar '(("display" "flex")))
    (apply #'change-class command-bar 'mold-desktop-command-bar initargs)
    ;; Prompt
    (setf (command-prompt command-bar) (create-child bar (html (:label (who:str "Command: ")))))
    ;; Command input
    (setf (user-input command-bar)
          (create-child bar (html (:input :type "text" :class "w3-border" :style "width: 500px; margin-left: 5px;"))))
    (set-on-focus (user-input command-bar) (lambda (&rest args)
                                             (declare (ignore args))
                                             (open-command-bar command-bar)))
    (create-icon "fa fa-circle-xmark"
                 (-> (create-a bar :style "padding-top: 3px;")
                     (set-on-click (lambda* (_)
                                     (close-command-bar command-bar)))
                     (identity)))

    ;; Status message area
    (setf (status-message-area command-bar)
          (create-div bar :class "status-message-area"))

    ;; Drag & drop handling
    ;; When prompting for an argument, objects can be dragged and dropped to the command bar.

    ;; To enable drop, drag-over event handler needs to be set:

    (set-on-drag-over command-bar
                      (lambda (&rest args)
                        (declare (ignore args))
                        (drag-over-command-bar command-bar mold-desktop/clog-gui::*dragging-object*)))

    (set-on-drag-leave command-bar
                       (lambda (&rest args)
                         (declare (ignore args))
                         (drag-leave-command-bar command-bar)))

    (set-on-drop command-bar
                 (lambda (&rest args)
                   (declare (ignore args))
                   (drop-on-command-bar command-bar mold-desktop/clog-gui::*dragging-object*)
                   (setf mold-desktop/clog-gui::*dragging-object* nil)
                   (remove-class command-bar "drop-allowed")))

    ;; Workspaces
    (setf (workspaces-area command-bar)
          (create-div bar :class "workspaces-area w3-bar"))

    (setf *loading-indicator* (create-loading-indicator bar))
    (stop-spinner *loading-indicator*)

    (with-ui-update (workspaces-area command-bar)
        (get '*workspaces* :events) :changed
      (dolist (workspace *workspaces*)
        (let ((btn (create-button (workspaces-area command-bar) :class "w3-button w3-small")))
          (when (workspace-icon workspace)
            (create-icon (workspace-icon workspace) btn))
          (create-span btn
                       :content (string (workspace-status-label workspace))
                       :style "margin-left:3px;")
          (set-on-click btn (lambda* (_)
                              (switch-to-workspace workspace)))
          (set-on-drag-over btn (lambda* (_)
                                  (switch-to-workspace workspace)))
          ;; Close workspace button
          (-> (create-icon "fa-solid fa-xmark" btn)
              (set-styles '(("margin-left" "10px")("padding-left" "2px")("padding-right" "2px")))
              (set-on-event "click"
                            (lambda* (_)
                              (close-workspace workspace))
                            :cancel-event t))))

      (let ((add-workspace-btn (create-button (workspaces-area command-bar) :class "w3-button w3-small")))
        (setf (property add-workspace-btn "title") "Open new workspace")
        (create-icon "fa fa-plus" add-workspace-btn)
        (set-on-click add-workspace-btn (lambda* (_)
                                          (run-command-interactively 'add-workspace)))))

    ;; Completions
    (setf (completions-panel command-bar)
          (create-child cb-body (html (:div :class "w3-col s4 m4 l4 w3-border completions" (who:str "No completions")))))
    (set-on-key-down (user-input command-bar)
                     (lambda* (_ event)
                       ;;(break "keydown: ~a" event)
                       (user-input-key-down command-bar event)))
    ;; Completions description
    (let ((panel (create-div cb-body :class "w3-col s4 m4 l4 w3-border w3-sand message-panel")))
      #+nil(setf (property
                  (-> (create-icon "core-question-circle" panel)
                      (set-styles  '(("float" "right") ("font-size" "3rem"))))
                  "title")
                 "Completion description")
      (setf (message-panel command-bar)
            (create-div panel :content "&nbsp;" :class "w3-padding-small")))

    ;; Selections
    (let ((panel (create-div cb-body :class "w3-col s4 m4 l4 w3-border w3-pale-green selections")))

      ;; Toolbar with buttons: clear all, grab collection, create collection
      (let ((toolbar (create-div panel :class "w3-bar w3-blue-gray")))
        ;; Clear all button
        (let ((btn (create-icon-button toolbar "fa fa-trash"
                                       (lambda* (_)
                                         (cb-status-unselect-all (command-bar-status command-bar))))))
          (setf (advisory-title btn) "Clear selections"))

        ;; Grab collection with selections
        (let ((btn (create-icon-button toolbar "fa fa-hand-back-fist")))
          (setf (advisory-title btn) "Drag as collection")
          (setf (attribute btn "data-drag-title") "Selections"
                (property btn "draggable") "true")
          (set-on-drag-start btn (lambda* (_)
                                   (setf mold-desktop/clog-gui::*dragging-object*
                                         (make-instance 'collection
                                                        :name "Selections"
                                                        :members (selected-arguments (command-bar-status command-bar))))))))

      ;; Icon
      (setf (property
             (-> (create-icon "core-object-select" panel)
                 (set-styles  '(("float" "right") ("font-size" "3rem"))))
             "title")
            "Selections panel")
      (setf (selections-panel command-bar)
            (create-div panel :content "No selections" :class "w3-padding-small")))

    ;; Set initial status
    (set-cb-status command-bar (make-instance 'command-input-cb-status))

    command-bar))

(defgeneric cb-status-input-key-down (status command-bar event)
  (:documentation "Handle key-down event on command-bar input. Specialized on command-bar status."))

(defun user-input-key-down (command-bar keyboard-event)
  ;; Delegate to command bar status
  (cb-status-input-key-down (command-bar-status command-bar) command-bar keyboard-event))

(defun run-command-interactively (command &rest args)
  "Run COMMAND using command bar from user interface."
  (let ((command-object (cond
                          ((symbolp command) (find-command command))
                          ((typep command 'command) command)
                          (t (error "Not a command: ~s" command)))))
    (reset-command-bar *command-bar*)
    (blackbird:create-promise
     (lambda (resolver rejecter)
       (declare (ignore rejecter))
       (prompt-command-arguments command-object *command-bar*
                                 (lambda (args)
                                   (close-command-bar *command-bar*)
                                   (let ((result (apply #'run-command command-object args)))
                                     (funcall resolver result)))
                                 :values args)))))

;;;; ** Completions

(defun active-completion (command-bar)
  (nth (completion-index command-bar) (completions command-bar)))

(defun set-active-completion (index command-bar)
  (setf (completion-index command-bar) index))

(defun update-completions (command-bar)
  (with-slots (completions completion-index user-input completions-panel message-panel) command-bar
    (let ((user-input-val (element-value user-input)))
      (setf (text message-panel) "")
      (setf completions (cb-status-get-completions (command-bar-status command-bar) user-input-val))
      (show-completions command-bar))))

(defun activate-completion (command-bar completion)
  (with-slots (completion-index completions completion-items message-panel) command-bar
    (let ((new-index (position completion completions)))
      (when new-index
        (anaphora:awhen (nth completion-index completion-items)
          (remove-class anaphora:it "active"))
        (setf completion-index new-index)
        (add-class (nth new-index completion-items) "active")
        (let ((doc (active-completion-description (command-bar-status command-bar) command-bar)))
          (setf (text message-panel) doc))))))

(defun activate-next-completion (command-bar)
  (with-slots (completion-index completions completion-items message-panel) command-bar
    (let ((new-index (mod (1+ completion-index) (length completions))))
      (remove-class (nth completion-index completion-items) "active")
      (setf completion-index new-index)
      (add-class (nth new-index completion-items) "active")
      (let ((doc (active-completion-description (command-bar-status command-bar) command-bar)))
        (setf (text message-panel) doc)))))

(defun activate-previous-completion (command-bar)
  (with-slots (completion-index completions completion-items message-panel) command-bar
    (let ((new-index (mod (1- completion-index) (length completions))))
      (remove-class (nth completion-index completion-items) "active")
      (setf completion-index new-index)
      (add-class (nth new-index completion-items) "active")
      (let ((doc (active-completion-description (command-bar-status command-bar) command-bar)))
        (setf (text message-panel) doc)))))

;;;; ** Command bar status
;;;;
;;;; The command bar is implemented as a state machine. Most of the operations
;;;; are delegated to the command bar status.

(defclass command-bar-status ()
  ((command-bar :initarg :command-bar
                :accessor command-bar)))

(defgeneric cb-status-start (cb-status)
  (:documentation "Start a command-bar-status"))

(defmethod cb-status-start ((status command-bar-status)))

(defgeneric cb-status-stop (cb-status)
  (:documentation "Stop a command-bar-status"))

(defmethod cb-status-stop ((status command-bar-status)))

(defun set-cb-status (command-bar status)
  "Set the COMMAND-BAR to a new STATUS."
  (cb-status-stop status)
  (with-slots (completion-items completion-index completions message-panel completions-panel) command-bar
    (setf (text message-panel) "")
    (setf completion-items nil)
    (setf completion-index 0)
    (setf completions nil)
    (setf (text completions-panel) ""))
  (setf (command-bar-status command-bar) status)
  (setf (command-bar status) command-bar)
  (cb-status-start status))

;;;; *** Completions status

(defclass completions-cb-status ()
  ()
  (:documentation "Mixing for handling completions in command-bar"))

(defmethod cb-status-input-key-down ((status completions-cb-status) command-bar keyboard-event)
  (when (string= (getf keyboard-event :key) "ArrowDown")
    (activate-next-completion command-bar)
    (return-from cb-status-input-key-down))

  (when (string= (getf keyboard-event :key) "ArrowUp")
    (activate-previous-completion command-bar)
    (return-from cb-status-input-key-down))

  ;; Character entered. Update completions list.
  (setf (completion-index command-bar) 0)
  (update-completions command-bar))

;;;; *** Prompt status

(defclass prompt-cb-status (command-bar-status)
  ((prompt :initarg :prompt
           :accessor prompt
           :type string)
   (prompt-handler :initarg :handler
                   :accessor prompt-handler)))

(defmethod cb-status-start ((status prompt-cb-status))
  (with-slots (command-prompt) (command-bar status)
    (setf (text command-prompt) (prompt status)))
  (set-user-input-value "" (command-bar status))
  ;;(open-command-bar (command-bar status))
  )

;;;; *** Command input status

(defclass command-input-cb-status (command-bar-status completions-cb-status)
  ((selected-command :accessor selected-command
                     :initform nil)
   (selected-arguments :accessor selected-arguments
                       :initform nil)
   (commands :initarg :commands
             :initform (lambda (cb-status)
                         (declare (ignore cb-status))
                         *commands*)
             :documentation "The unfiltered list of commands."))
  (:documentation "Status for selecting a command."))

(defmethod commands ((cb-status command-input-cb-status))
  (with-slots (commands) cb-status
    (cond
      ((functionp commands) (funcall commands cb-status))
      ((listp commands) commands)
      (t (error "Invalid commands: ~a" commands)))))

(defmethod cb-status-drop-allowed? ((cb-status command-input-cb-status) object)
  ;; Always allow a drop. The dropped object is added to argument selections.
  t)

(defmethod cb-status-handle-drop ((cb-status command-input-cb-status) object command-bar)
  ;; Add object to the list of seletions
  (command-bar-select-object command-bar object))

(defgeneric cb-status-select-object (cb-status object &optional exclusive))

(defmethod cb-status-start ((status command-input-cb-status))
  (setf (text (command-prompt (command-bar status))) "Command: ")
  (set-user-input-value "" (command-bar status))
  (update-completions (command-bar status)))

(defun command-bar-select-object (command-bar object &key (exclusive nil))
  "Add OBJECT to the list of selected arguments in COMMAND-BAR."
  (cb-status-select-object (command-bar-status command-bar) object exclusive))

(defun command-bar-unselect-object (command-bar object)
  "Remove OBJECTS from the list of selected arguments in COMMAND-BAR."
  (cb-status-unselect-object (command-bar-status command-bar) object))

(defun command-bar-toggle-object (command-bar object)
  "Toggle OBJECT selection."
  (if (member object (selected-arguments (command-bar-status command-bar)))
      (cb-status-unselect-object (command-bar-status command-bar) object)
      (cb-status-select-object (command-bar-status command-bar) object)))

(defmethod cb-status-unselect-object ((cmd-bar-status command-input-cb-status) object)
  ;; remove from selections
  (setf (selected-arguments cmd-bar-status)
        (delete object (selected-arguments cmd-bar-status)))
  ;; redraw view
  (update-selected-arguments-view cmd-bar-status)
  (update-completions (command-bar cmd-bar-status)))

(defmethod cb-status-unselect-all ((cmd-bar-status command-input-cb-status))
  (setf (selected-arguments cmd-bar-status) nil)
  (update-selected-arguments-view cmd-bar-status)
  (update-completions (command-bar cmd-bar-status)))

(defun update-selected-arguments-view (cmd-bar-status)
  (let ((selections-panel (selections-panel (command-bar cmd-bar-status))))

    ;; Delete current view
    (clear-element selections-panel)

    ;; If no selections, show message
    (when (zerop (length (selected-arguments cmd-bar-status)))
      (create-p selections-panel :content "No selections")
      (return-from update-selected-arguments-view))

    ;; List of selections
    (let ((selections-list (create-unordered-list selections-panel :class "w3-ul")))
      (dolist (obj (selected-arguments cmd-bar-status))
        (let* ((li-text (str:shorten 50 (princ-to-string
                                         (or (object-name obj)
                                             (object-id obj)
                                             (object-url obj)))))
               (selection-li (create-list-item selections-list)))
          (create-span selection-li :content (who:escape-string li-text))
          (create-icon-button selection-li "fa fa-trash"
                              (lambda* (_)
                                (command-bar-unselect-object (command-bar cmd-bar-status) obj)
                                (clog:remove-from-dom selection-li))))))))

(defmethod active-completion-description ((cb-status command-input-cb-status) command-bar)
  (documentation (active-completion command-bar) 'function))

(defmethod cb-status-select-object ((cmd-bar-status command-input-cb-status) object &optional exclusive)
  (when exclusive
    (setf (selected-arguments cmd-bar-status) nil))
  ;; add object to the list of currently selected objects for command
  (push-end object (selected-arguments cmd-bar-status))
  ;; redraw view
  (update-selected-arguments-view cmd-bar-status)
  (update-completions (command-bar cmd-bar-status)))

(defmethod cb-status-input-key-down ((status command-input-cb-status) command-bar keyboard-event)

  ;; Enter key
  (when (string= (getf keyboard-event :key) "Enter")
    ;; Try to match a whole command
    (let ((command
            (let ((fname (intern (string-upcase (user-input-value command-bar)) 'mold-desktop/commands)))
              (if (and (fboundp fname)
                       (typep (symbol-function fname) 'mold-desktop::command))
                  (symbol-function fname)
                  ;; else, select the completion
                  (active-completion command-bar)))))
      (when command
        ;; Prompt for arguments and run the command
        (prompt-command-arguments
         command command-bar
         (lambda (args)
           (close-command-bar command-bar)
           (apply #'run-command command args))
         :prompt-optional-args (prompt-command-optional-arguments? keyboard-event)))
      (return-from cb-status-input-key-down)))

  (call-next-method))

(defun completion-substr-match (input completion)
  (search input completion :test #'equalp))

(defun completion-start-match (input completion)
  (str:starts-with? input completion :ignore-case t))

(defmethod cb-status-get-completions ((status command-input-cb-status) value)
  (let ((completion-method (completion-method (command-bar status))))
    (remove-if-not (lambda (command)
                     (and (funcall completion-method value (symbol-name (command-name command)))
                          (command-matches-arguments-p command (selected-arguments status) :partial (getf (command-bar-options (command-bar status)) :partial-command-matching))))
                   (commands status))))

(defmethod cb-status-select-completion ((status command-input-cb-status) completion &optional clog-event)
  (let ((command completion))
    (setf (selected-command status) command)
    (prompt-command-arguments
     command
     (command-bar status)
     (lambda (args)
       (close-command-bar (command-bar status))
       (apply #'run-command command args)
       (command-bar-start (command-bar status)))
     :prompt-optional-args (prompt-command-optional-arguments? clog-event))))

;;;; *** Command options status
;;;;
;;;; This status is active for reading a command's optional arguments.

(defclass command-options-cb-status (command-bar-status)
  ((command :initarg :command
            :accessor command)
   (command-options :accessor command-options
                    :initform nil)
   (handler :initarg :handler
            :accessor handler))
  (:documentation "Command bar status for setting up a command's options."))

(defstruct command-option
  arginfo value)

(defmethod print-completion ((cmd-option command-option))
  (with-slots (arginfo value) cmd-option
    (format nil "~a=~a" (getf arginfo :name) value)))

(defmethod cb-status-start ((status command-options-cb-status))
  (call-next-method)
  (setf (text (command-prompt (command-bar status)))
        (format  nil "~A options: " (command-name (command status))))
  (set-user-input-value "" (command-bar status))
  (when (null (command-options status))
    (setf (command-options status)
          (mapcar (lambda (arginfo)
                    (make-command-option
                     :arginfo arginfo
                     :value (getf arginfo :default)))
                  ;; optional arguments are second and fourth
                  (append (second (args-info (command status)))
                          (fourth (args-info (command status)))))))
  (update-completions (command-bar status)))

(defmethod cb-status-get-completions ((cb-status command-options-cb-status) value)
  (command-options cb-status)
  ;; FIXME: code for filtering options? Not working ATM
  #+fixme(let ((completion-method (completion-method (command-bar cb-status))))
           (if (stringp value)
               (remove-if-not (lambda (item)
                                (funcall completion-method value (princ-to-string (getf (command-option-arginfo item) :name))))
                              (command-options cb-status))
               (command-options cb-status))))

(defmethod active-completion-description ((cb-status command-options-cb-status) command-bar)
  (getf (command-option-arginfo (active-completion command-bar)) :doc))

(defmethod cb-status-select-completion ((status command-options-cb-status) completion &optional clog-event)
  (declare (ignore clog-event))
  ;; Configure the selected command option
  (prompt-command-argument (command-option-arginfo completion)
                           nil
                           (command-bar status)
                           (lambda (value)
                             (setf (command-option-value completion) value)
                             ;; fixme: why reset needed?
                             (reset-command-bar (command-bar status))
                             (set-cb-status (command-bar status) status))
                           nil))

(defmethod cb-status-input-key-down ((status command-options-cb-status) command-bar keyboard-event)

  ;; TODO: refactor? Should this be the default? (put in cb-status-input-key-down in superclass)
  ;; Enter key
  (when (string= (getf keyboard-event :key) "Enter")
    (let ((completion (active-completion command-bar)))
      (if (and completion (not (str:emptyp (user-input-value command-bar))))
          (progn
            (select-completion command-bar completion keyboard-event)
            (return-from cb-status-input-key-down))
          ;; else, return the arguments values in a lambda list format
          (let ((args ()))
            (dolist (cmd-option (command-options status))
              (with-slots (arginfo value) cmd-option
                (ecase (getf arginfo :lambda-list-type)
                  (:optional (push value args))
                  (:keyword
                   (push (alexandria:make-keyword (symbol-name (getf arginfo :name))) args)
                   (push value args)))))
            (funcall (handler status) (nreverse args)))))))

(defun prompt-command-options (command command-bar handler)
  "Prompt for COMMAND options.
Return a lambda list with argument values."
  (set-cb-status command-bar (make-instance 'command-options-cb-status
                                            :command command
                                            :handler handler)))

#+example
(defcommand (command-with-options-1 :show-result t)
    (&key (x string nil "A number")
     (y integer nil "Other number")
     (z (member (foo "A foo")
                (bar "A bar")) nil "A choice"))
  "Command for testing optional arguments"
  (list x y z))

#+example
(defcommand (command-with-options-2 :show-result t)
    ((r string "A required argument")
     &key
     (x integer nil "A number")
     (y boolean nil "A boolean")
     (z (member (foo "A foo")
                (bar "A bar")) nil "A choice"))
  "Command for testing optional arguments"
  (list r x y z))

;;;; *** Message status
;;;;
;;;; Command bar status for showing a message.

(defclass message-cb-status (command-bar-status)
  ((message :initarg :message
            :accessor message)
   (format :initarg :format
           :accessor message-format
           :initform :text
           :type (member :text :html)))
  (:documentation "Command bar status for showing a message."))

(defmethod cb-status-start ((status message-cb-status))
  (ecase (message-format status)
    (:text
     (setf (text (message-panel (command-bar status)))
           (message status)))
    (:html
      (clog:create-child (message-panel (command-bar status))
                         (message status)))))

(defmethod cb-status-input-key-down ((status message-cb-status) command-bar keyboard-event)
  )

(defun command-bar-message (message &optional (command-bar *command-bar*))
  (open-command-bar command-bar)
  (set-cb-status command-bar
                 (make-instance 'message-cb-status
                                :message message)))

(defun command-bar-html-message (html &optional (command-bar *command-bar*))
  (open-command-bar command-bar)
  (set-cb-status command-bar
                 (make-instance 'message-cb-status
                                :message html
                                :format :html)))

;; (command-bar-message "Hello world")
;; (command-bar-message "Hello world!!")

;; (command-bar-html-message "<b>hello</b><i>world</i>")

;;;; ** Command bar api

(defun user-input-value (command-bar)
  (with-slots (user-input) command-bar
    (element-value user-input)))

(defun set-user-input-value (value command-bar)
  (with-slots (user-input) command-bar
    (setf (element-value user-input) value)))

(defun reset-command-bar (command-bar)
  "Reset COMMAND-BAR to the initial state."
  (with-slots (completion-items completions-panel message-panel selections-panel completion-index completions) command-bar
    (set-user-input-value "" command-bar)
    (setf (clog:attribute (user-input command-bar) "type") "text")
    (setf (text completions-panel) "No completions")
    (setf completion-items nil)
    (setf completions nil)
    (setf completion-index 0)
    (setf (text selections-panel) "No selections")
    (set-cb-status command-bar
                   (make-instance 'command-input-cb-status))))

(defun open-command-bar (command-bar)
  "Open the command bar.
If it is already open, it remains opened."
  (set-styles command-bar '(("height" "200px")))
  (when (string= (jquery-query (user-input command-bar) "is(':focus')") "false")
    (focus (user-input command-bar)))
  (setf (command-bar-opened command-bar) t))

(defun close-command-bar (command-bar)
  "Close the command bar.
If it is already closed, it remains closed."
  (reset-command-bar command-bar)
  (blur (user-input command-bar))
  (set-styles command-bar '(("height" "28px")))
  (setf (command-bar-opened command-bar) nil))

(defun toggle-command-bar (command-bar)
  "Open or close the command bar."
  (if (command-bar-opened command-bar)
      (close-command-bar command-bar)
      (open-command-bar command-bar)))

(defun select-completion (command-bar completion &optional clog-event)
  (cb-status-select-completion (command-bar-status command-bar) completion clog-event))

(defun show-completions (command-bar)
  "Show the current completions."
  (with-slots (completions-panel completions completion-items) command-bar
    (setf (text completions-panel) "")
    (setf completion-items nil)
    (let ((ul (create-unordered-list completions-panel :class "w3-ul w3-border")))
      (dolist (completion completions)
        (let ((li (create-list-item ul)))
          (when (completion-icon completion)
            (create-icon (completion-icon completion) li))
          (create-span li :content (print-completion completion))
          (set-styles li '(("cursor" "pointer")))
          (push li completion-items)
          (set-on-mouse-click li (lambda* (_ mouse-event)
                                   (select-completion command-bar completion mouse-event)))
          (set-on-mouse-over li (lambda* (_)
                                  (activate-completion command-bar completion)))))
      (setf completion-items (nreverse completion-items)))))

;;;; *** Prompt type status

(defclass prompt-instance-cb-status (prompt-cb-status)
  ((instance-type :initarg :instance-type
                  :accessor instance-type
                  :initform (error "Provide the instance-type"))
   (input-parser :initarg :input-parser
                 :accessor input-parser
                 :initform (error "Provide an input parser")))
  (:documentation "Prompt for an instance of some class."))

(defmethod cb-status-start ((cb-status prompt-instance-cb-status))
  (call-next-method)
  (case (instance-type cb-status)
    (number (setf (clog:attribute (user-input (command-bar cb-status)) "type") "number"))
    (integer (setf (clog:attribute (user-input (command-bar cb-status)) "type") "number"))
    (local-time:timestamp (setf (clog:attribute (user-input (command-bar cb-status)) "type") "datetime-local"))
    (pathname (setf (clog:attribute (user-input (command-bar cb-status)) "type") "file"))))

(defmethod cb-status-input-key-down ((cb-status prompt-instance-cb-status) command-bar keyboard-event)
  ;; Enter key
  (when (string= (getf keyboard-event :key) "Enter")
    ;; Try parsing the input and see if we get an object of the correct type
    (let ((result
            (funcall (input-parser cb-status) (user-input-value command-bar))))
      (when (typep result (instance-type cb-status))
        ;; type is correct, call the handler
        (funcall (prompt-handler cb-status) result)))))

(defmethod cb-status-drop-allowed? ((cb-status prompt-instance-cb-status) object)
  (typep object (instance-type cb-status)))

(defmethod cb-status-handle-drop ((cb-status prompt-instance-cb-status) object command-bar)
  ;; double check
  (when (typep object (instance-type cb-status))
    (funcall (prompt-handler cb-status) object)))

(defun command-bar-prompt-instance (prompt instance-type input-parser handler command-bar &key (close t))
  "Top-level function for prompting for an instance using the command-bar."
  (with-callback handler
    (open-command-bar command-bar)
    (set-cb-status command-bar
                   (make-instance 'prompt-instance-cb-status
                                  :prompt prompt
                                  :instance-type instance-type
                                  :input-parser input-parser
                                  :handler (lambda (result)
                                             (when close
                                               (close-command-bar command-bar))
                                             (funcall handler result))))))

#+test
(command-bar-prompt-instance "Number: " 'integer #'parse-integer #'show *command-bar*)
#+test
(blackbird:alet ((num (command-bar-prompt-instance "Number: " 'integer #'parse-integer nil *command-bar*)))
  (show num))

;;;; *** Prompt string status

(defclass prompt-string-cb-status (prompt-cb-status)
  ())

(defmethod cb-status-input-key-down ((status prompt-string-cb-status) command-bar keyboard-event)
  ;; Enter key
  (when (string= (getf keyboard-event :key) "Enter")
    (funcall (prompt-handler status) (user-input-value command-bar))))

(defun command-bar-prompt (prompt handler command-bar &key (close t))
  "Top-level function for prompting for a string using the command-bar."
  (with-callback handler
    (open-command-bar command-bar)
    (set-cb-status command-bar
                   (make-instance 'prompt-string-cb-status
                                  :prompt prompt
                                  :handler (lambda (result)
                                             (when close
                                               (close-command-bar command-bar))
                                             (funcall handler result))))))

#+example
(command-bar-prompt "Search: " #'show *command-bar*)

;;;; *** Option selection status

(defstruct choice-item
  value label description icon)

(defmethod completion-icon ((thing choice-item))
  (choice-item-icon thing))

(defclass option-selection-cb-status (prompt-cb-status completions-cb-status)
  ((options :initarg :options
            :accessor options
            :initform (error "Provide the options")
            :documentation "A list of CHOICE-ITEM."))
  (:documentation "Select one of many options from the command bar."))

(defmethod cb-status-start ((status option-selection-cb-status))
  (call-next-method)
  (update-completions (command-bar status)))

(defmethod cb-status-get-completions ((cb-status option-selection-cb-status) value)
  (let ((completion-method (completion-method (command-bar cb-status))))
    (remove-if-not (lambda (choice-item)
                     (funcall completion-method value (or (choice-item-label choice-item)
                                                          (choice-item-value choice-item))))
                   (options cb-status))))

(defmethod active-completion-description ((cb-status option-selection-cb-status) command-bar)
  (choice-item-description (active-completion command-bar)))

(defmethod print-completion ((choice-item choice-item))
  (or (choice-item-label choice-item)
      (choice-item-value choice-item)))

(defmethod cb-status-select-completion ((status option-selection-cb-status) completion &optional clog-event)
  (declare (ignore clog-event))
  (funcall (prompt-handler status) (choice-item-value completion)))

(defmethod cb-status-input-key-down ((status option-selection-cb-status) command-bar keyboard-event)

  ;; Enter key
  (when (string= (getf keyboard-event :key) "Enter")
    (let ((completion (active-completion command-bar)))
      (when completion
        (funcall (prompt-handler status) (choice-item-value completion))
        (return-from cb-status-input-key-down))))

  (call-next-method))

(defun command-bar-choose (prompt choices handler command-bar &key (close t))
  "Select one of CHOICES using the COMMAND-BAR.
Items in CHOICES can be either Lisp objects or property lists with :value, :label , :icon and :description keys."
  (with-callback handler
    (open-command-bar command-bar)
    (set-cb-status command-bar
                   (make-instance 'option-selection-cb-status
                                  :options (mapcar (lambda (args)
                                                     (if* (listp args)
                                                        then
                                                            (apply #'make-choice-item args)
                                                        else
                                                            (make-choice-item :value args
                                                                              :label (object-title args)
                                                                              :description (object-description args)
                                                                              :icon (object-icon args))))
                                                   choices)
                                  :prompt prompt
                                  :handler (lambda (result)
                                             (when close
                                               (close-command-bar command-bar))
                                             (funcall handler result))))))

#+example
(command-bar-choose "Choose:" '("foo" "bar" "baz")
                    #'show
                    *command-bar*)

#+example
(command-bar-choose "Choose:" '(foo bar baz)
                    #'show
                    *command-bar*)

#+example
(bb:alet ((x (command-bar-choose "Choose:" '(foo bar baz)
                                 nil
                                 *command-bar*)))
  (show x))

;;;; *** Object selection status

(defclass prompt-object-cb-status (option-selection-cb-status)
  ((object-type :initarg :object-type
                :initform 'object
                :accessor object-type
                :documentation "The object type to prompt.")
   (objects :initarg :objects
            :accessor objects
            :initform mold-desktop::*objects*
            :documentation "The list of objects to choose from."))
  (:default-initargs :options nil)
  (:documentation "Select an object of type OBJECT-TYPE."))

(defmethod cb-status-start ((cb-status prompt-object-cb-status))
  ;; For prompting an OBJECT instance, set the UI state to :selection
  ;; so that the argument can be selected from one of the objects
  ;; available at the moment in the UI.
  (start-command-argument-selection-mode
   (object-type cb-status)
   (command-bar cb-status)
   (prompt-handler cb-status))

  (setf (options cb-status)
        (cons
         ;; Additional option to create a new instance:
         (make-choice-item
          :label "Create new ..."
          :value :create-new
          :description (format nil "Create a new ~a" (object-type cb-status))
          :icon "fa-solid fa-wand-magic-sparkles")
         (loop for object in (objects cb-status)
               when (typep object (object-type cb-status))
                 collect
                 (make-choice-item
                  :label (object-title object)
                  :value object
                  :description (object-description object)))))
  (call-next-method))

(defmethod cb-status-select-completion ((cb-status prompt-object-cb-status) completion &optional clog-event)
  (if (eql (choice-item-value completion) :create-new)
      ;; If create new instance chosen:
      (let ((subclasses (all-subclasses (find-class (object-type cb-status)))))
        (if (null subclasses)
            (funcall (prompt-handler cb-status)
                     (make-instance (object-type cb-status)))
            ;; If object-type has subclasses, let user choose one
            (command-bar-choose "Instance of: "
                                (cons (list :label (princ-to-string (object-type cb-status))
                                            :value (object-type cb-status)
                                            :description (documentation (find-class (object-type cb-status)) t))
                                      (mapcar (lambda (subclass)
                                                (list :label (princ-to-string (class-name subclass))
                                                      :value (class-name subclass)
                                                      :description (documentation subclass t)))
                                              subclasses))
                                (lambda (subclass-type)
                                  (funcall (prompt-handler cb-status) (make-instance subclass-type)))
                                (command-bar cb-status))))
      ;; Otherwise, one of the object instances was selected:
      (funcall (prompt-handler cb-status) (choice-item-value completion))))

;; Drop when on prompt status. Use the dropped object as input when the type of object being prompted matches.

(defmethod cb-status-drop-allowed? ((cb-status prompt-object-cb-status) object)
  (typep object (object-type cb-status)))

(defmethod cb-status-handle-drop ((cb-status prompt-object-cb-status) object command-bar)
  ;; double check
  (when (typep object (object-type cb-status))
    (funcall (prompt-handler cb-status) object)))

(defun command-bar-prompt-object (prompt object-type handler command-bar &key (close t) (objects mold-desktop::*objects*))
  (with-callback handler
    (open-command-bar command-bar)
    (set-cb-status command-bar (make-instance 'prompt-object-cb-status
                                              :prompt prompt
                                              :object-type object-type
                                              :objects objects
                                              :handler (lambda (result)
                                                         (when close
                                                           (close-command-bar command-bar))
                                                         (funcall handler result))))))

#+example
(command-bar-prompt-object "Object: " 'object #'show *command-bar*)

#+example
(command-bar-prompt-object "Webpage: " 'web-page #'show *command-bar*)

;;;; *** Prompt command arguments

;; TODO: consider implementing a pattern matching (trivia) generic function metaclass (MOP). Use for matching types of arguments specs, extensibly.

(defun format-argname (argname)
  (cond
    ((stringp argname)
     argname)
    ((symbolp argname)
     (str:join #\space
               (mapcar (alex:compose #'string-capitalize #'string-downcase)
                       (str:split #\- (princ-to-string argname)))))
    (t (princ-to-string argname))))

;;(format-argname "lala")
;;(format-argname 'foo-bar)

(defgeneric prompt-command-argument-type (arg-type arg command-bar handler)
  (:documentation "Prompt for an argument of type ARG-TYPE.
Specialized on ARG-TYPE for specific prompting for the type."))

(defgeneric prompt-command-argument-compound-type (type-discriminator arg-type arg command-bar handler)
  (:documentation "Prompt for an argument of type TYPE-DISCRIMINATOR.
TYPE-DISCRIMINATOR is a compound type."))

(defmethod prompt-command-argument-type :around (arg-type arg command-bar handler)
  (with-callback handler
    (call-next-method arg-type arg command-bar handler)))

(defmethod prompt-command-argument-type :after (arg-type arg command-bar handler)
  (setf (text (message-panel command-bar))
        (or (getf arg :doc) "")))

(defmethod prompt-command-argument-type ((arg-type cons) arg command-bar handler)
  (prompt-command-argument-compound-type (first arg-type) arg-type arg command-bar handler))

(defmethod prompt-command-argument-type ((arg-type (eql 'string)) arg command-bar handler)
  (command-bar-prompt-instance
   (format nil "~a: " (format-argname (getf arg :name)))
   'string #'identity
   handler command-bar :close nil))

(defmethod prompt-command-argument-type ((arg-type (eql 'text)) arg command-bar handler)
  (clog:with-clog-create *app-body*
      (div (:bind modal :class "w3-modal" :style "display:block; z-index:999;")
           (div (:class "w3-modal-content")
                (div (:class "w3-container")
                     (span (:bind close-btn
                             :class "w3-button w3-display-topright"
                             :content "&times;"))
                     (text-area (:bind text-area
                                  :style "width:100%;"
                                  :rows 20))
                     (button (:bind accept-btn
                               :class "w3-button"
                               :content "Accept"))
                     (button (:bind cancel-btn
                               :class "w3-button"
                               :content "Cancel")))))
    (set-on-click close-btn (lambda (&rest args)
                              (declare (ignore args))
                              (clog:remove-from-dom modal)))
    (set-on-click cancel-btn (lambda (&rest args)
                               (declare (ignore args))
                               (clog:remove-from-dom modal)))
    (set-on-click accept-btn (lambda (&rest args)
                               (declare (ignore args))
                               (clog:remove-from-dom modal)
                               (funcall handler (value text-area))))))

(defmethod prompt-command-argument-type ((arg-type (eql 'integer)) arg command-bar handler)
  (command-bar-prompt-instance
   (format nil "~a: " (format-argname (getf arg :name)))
   'integer
   #'parse-integer
   handler command-bar :close nil))

(defmethod prompt-command-argument-type ((arg-type (eql 'number)) arg command-bar handler)
  (command-bar-prompt-instance
   (format nil "~a: " (format-argname (getf arg :name)))
   'number
   #'parse-integer
   handler command-bar :close nil))

(defmethod prompt-command-argument-type ((arg-type (eql 'boolean)) arg command-bar handler)
  (command-bar-confirm (format nil "~a: " (format-argname (getf arg :name))) handler command-bar :close nil))

(defmethod prompt-command-argument-type ((arg-type (eql 'symbol)) arg command-bar handler)
  (command-bar-prompt
   (format nil "~a: " (format-argname (getf arg :name)))
   (alexandria:compose handler #'intern #'string-upcase)
   command-bar :close nil))

(defmethod prompt-command-argument-type ((arg-type (eql 'pathname)) arg command-bar handler)
  (command-bar-prompt
   (format nil "~a: " (format-argname (getf arg :name)))
   (alexandria:compose handler #'pathname)
   command-bar :close nil))

(defmethod prompt-command-argument-type ((arg-type (eql 'command)) arg command-bar handler)
  (command-bar-choose
   (format nil "~a: " (format-argname (getf arg :name)))
   (mapcar (lambda (cmd)
             (list :label (princ-to-string (command-name cmd))
                   :value cmd
                   :description (documentation cmd 'function)
                   :icon (command-icon cmd)))
           (instances-of 'command))
   handler command-bar
   :close nil))

(defmethod prompt-command-argument-type (arg-type arg command-bar handler)
  "Prompt for ARG of type ARG-TYPE, when no PROMPT-COMMAND-ARGUMENT-TYPE specializer was found for the particular ARG-TYPE."
  ;; When the type to prompt for is a subtype of OBJECT, start UI selection  mode
  ;; and start completion from the objects in the store
  (cond
    ((subtypep arg-type 'object)
     (command-bar-prompt-object (format nil "~a (~a): " (format-argname (getf arg :name)) arg-type)
                                arg-type handler command-bar :close nil))
    ;; When everything fails, give the user a chance to evaluate
    ;; something that returns an object of that type.
    (t
     (command-bar-prompt (format nil "~a (~a). Eval: " (format-argname (getf arg :name)) arg-type)
                         (lambda (string)
                           (let ((obj (handler-case
                                          (eval (read-from-string string))
                                        (error (e)
                                          (alert-toast *body* "Error" (condition-message e))
                                          nil))))
                             (if (typep obj arg-type)
                                 ;; use the evaluated object
                                 (funcall handler obj)
                                 ;; else, try again
                                 (prompt-command-argument-type arg-type arg command-bar handler))))
                         command-bar :close nil))))

(defmethod prompt-command-argument-compound-type :around (type-discriminator arg-type arg command-bar handler)
  (with-callback handler
    (call-next-method type-discriminator arg-type arg command-bar handler)))

(defmethod prompt-command-argument-compound-type ((type-discriminator (eql 'subclass-of)) arg-type arg command-bar handler)
  "Prompt a 'subclass' argument type."
  (let ((subclasses
          (mold-desktop::all-subclasses (find-class (second arg-type)))))
    (command-bar-choose "Select class: "
                        (mapcar (lambda (class)
                                  ;; If it is an instance of mold-object-class,
                                  ;; then use special formatting for label,
                                  ;; and also include specified icon.
                                  (if (typep class 'mold-object-class)
                                      (list :label (str:sentence-case (princ-to-string (class-name class)))
                                            :value (class-name class)
                                            :description (documentation class t)
                                            :icon (class-icon class))
                                      ;; else
                                      (list :label (princ-to-string (class-name class))
                                            :value (class-name class)
                                            :description (documentation class t))))
                                subclasses)
                        handler command-bar :close nil)))

(defmethod prompt-command-argument-compound-type ((type-discriminator (eql 'member)) arg-type arg command-bar handler)
  "Prompt for 'member' argument types.
Example: (member foo bar)
But also, member argument type spec can contain choice descriptions, like:
(member (foo \"Foo is this\") bar)"

  (let ((members (mapcar (lambda (member)
                           (if (listp member)
                               (destructuring-bind (value description) member
                                 (list :label (object-title value)
                                       :value value
                                       :description description))
                               (list :label (object-title member)
                                     :value member
                                     :description "Not documented")))
                         (rest arg-type))))
    (command-bar-choose (format nil "~a: " (getf arg :name))
                        members handler command-bar :close nil)))

(deftype member-of-list (list-var &rest args)
  (declare (ignore args))
  ;; I would like to do this:
  ;;(flet ((member-of-list-p (x)
  ;;(member x (eval list))))
  ;;  `(satisfies ,#'member-of-list-p))
  `(member ,@(eval list-var)))

(defmethod prompt-command-argument-compound-type ((type-discriminator (eql 'member-of-list)) arg-type arg command-bar handler)
  "Prompt for 'member-of-list' argument types.
Example: (member-of-list *my-list* :label label-accessor :description description-accessor)"
  (destructuring-bind (member-list &key label description) (rest arg-type)
    (let ((members (mapcar (lambda (member)
                             (list :label (or (and label
                                                   (funcall label member))
                                              (object-title member))
                                   :value member
                                   :icon (object-icon member)
                                   :description (or (and description
                                                         (funcall description member))
                                                    (object-description member)
                                                    "Not documented")))
                           (eval member-list))))
      (command-bar-choose (format nil "~a: " (getf arg :name))
                          members handler command-bar :close nil))))

(defun do-prompt-command-arguments (args command command-bar handler &optional arg-values)
  "Prompt for COMMAND ARGS, one after another.
ARGS is the list of required arguments of COMMAND.
ARG-VALUES is the list of current argument values. The implementation of this function is recursive, and appends argument values to ARG-VALUES as it goes."
  (let ((arg (first args)))
    (when (not arg)
      (return-from do-prompt-command-arguments (funcall handler arg-values)))
    (prompt-command-argument arg command command-bar
                             (lambda (arg-value)
                               (reset-command-bar command-bar)
                               ;; Append the value to the current list of argument values, and continue prompting.
                               (do-prompt-command-arguments
                                   (rest args) command command-bar handler (append arg-values (list arg-value))))
                             arg-values)))

(defun prompt-command-arguments (command command-bar handler
                                 &key (values (selected-arguments (command-bar-status command-bar)))
                                   prompt-optional-args)
  "Prompt for getting arguments values for COMMAND.
Call HANDLER with the user entered arguments."
  ;; Use selected arguments in command bar if available
  (let* ((required-args (subseq
                         (first (args-info command)) ;; required args
                         (length values) ;; start at length of selected arguments
                         )))
    (do-prompt-command-arguments
      required-args command command-bar
      (lambda (req-args-values)
        ;; Prompt for command options after required args
        (let ((optional-args (append (second (args-info command))
                                     (fourth (args-info command)))))
          (if (and prompt-optional-args optional-args)
              (prompt-command-options command command-bar
                                      (lambda (opt-args-values)
                                        (funcall handler (append req-args-values opt-args-values))))
              (funcall handler req-args-values))))
      values)))

(defun start-command-argument-selection-mode (object-type command-bar handler)
  (declare (ignore command-bar handler))
  (setf *ui-state* :command-argument-selection)
  ;; (mold-desktop/clog-gui::do-presented-objects
  ;;     view
  ;;   (anaphora:awhen (mold-desktop/clog-gui::desktop-object view)
  ;;     (when (subtypep object-type (type-of anaphora:it))
  ;;       (mold-desktop/clog-gui::set-selection-mode view))))
  )

(defun prompt-command-argument (arg command command-bar handler arg-values)
  "Prompt for getting ARGINFO value.
- ARG-VALUES - alist: The current list of arguments and its values. May be used when prompting for some types of arguments."
  (let* ((arg-options (getf arg :options))
         (prompt-spec (eval (getf arg-options :prompt)))
         (choices-spec (getf arg-options :choices)))
    (cond
      ;; If choices are specified, make the user select one of them
      (choices-spec
       (let ((choices (cond
                        ((function-designator-p choices-spec)
                         (funcall choices-spec))
                        ((listp choices-spec)
                         choices-spec)
                        (t (error "Invalid choices spec: ~s" choices-spec)))))
         (command-bar-choose (format nil "~a: " (getf arg :name))
                             choices handler command-bar)))
      ;; There's a specific prompt specification for ARG
      (prompt-spec
       (cond
         ;; If prompt specified as a function, then call it with the current argument values.
         ((function-designator-p prompt-spec)
          (let* ((required-args (first (args-info command)))
                 (current-arg-values (map 'list (lambda (arg value) (cons (getf arg :name) value))
                                          required-args arg-values))
                 (prompter (funcall prompt-spec current-arg-values)))
            (if (functionp prompter)
                ;; If a function is returned, then use if for prompting the argument.
                (funcall prompter arg command-bar handler)
                ;; Otherwise, dispatch on the returned prompt-spec.
                (prompt-command-argument-type prompter arg command-bar handler))))
         ;; otherwise, dispatch on the prompt-spec
         (t
          (prompt-command-argument-type prompt-spec arg command-bar handler))))
      ;; If no prompt or choices specified, prompt the argument type using PROMPT-COMMAND-ARGUMENT-TYPE
      (t
       (prompt-command-argument-type (getf arg :type) arg command-bar handler)))))

;;;; ** Confirmation

(defun command-bar-confirm (prompt handler command-bar &key (close t))
  (with-callback handler
    (command-bar-choose prompt '("yes" "no")
                        (lambda (choice)
                          (funcall handler (equalp choice "yes")))
                        command-bar :close close)))

(defun command-bar-confirm-and-do (prompt handler command-bar &key (close t))
  (command-bar-choose prompt '("yes" "no")
                      (lambda (choice)
                        (when (equalp choice "yes")
                          (funcall handler)))
                      command-bar :close close))
#+example
(command-bar-confirm "Are you sure?" #'show *command-bar*)

#+test
(prompt-command-argument-type
 t (list :name 'my-arg :type t) *command-bar*
 #'show)

#+test
(prompt-command-argument-type
 'string (list :name 'my-arg) *command-bar*
 #'show)
