;; Track changes of remote objects.
;; Check if an object has changed, and trigger a change event accordingly.
;; The check depends on the type of object.
;; For example, for WEB-PAGE objects, an HTTP request is made.

;; TODO: rename to "monitor" instead of "tracker" ?

(in-package :mold-desktop)

(md/settings:defgroup change-tracker nil
  "Change tracker settings")

(md/settings:defcustom *change-tracker-enabled-p*
  t
  "Change tracker is enabled"
  :type boolean
  :group change-tracker)

(md/settings:defcustom *change-tracker-check-time-interval*
  (* 60 5)
  "Time interval used for checking objects changed, in seconds."
  :type integer
  :group change-tracker)

(defvar *change-tracked-objects* nil)

(defgeneric change-tracker-object-changed-p (object)
  (:documentation "Returns T if OBJECT has changed.
Should be specialized for the different type of objects."))

(defmethod change-tracker-object-changed-p (object)
  "The default implementation is to return that the object didn't change."
  nil)

(defun start-change-tracker ()
  (loop do
    (dolist (object *change-tracked-objects*)
      (when (change-tracker-object-changed-p object)
	(event-emitter:emit object :changed object)))
    (sleep *change-tracker-check-time-interval*)))
