(in-package :mold-desktop)

;; TODO: can I use element-type here??
(deftype collection-of (element-type)
  `(and collection))

(deftype list-of (element-type)
  `(and list))

(deftype text ()
  'string)

