(in-package :mold-desktop)

;; an experiment of a trivial "output stream"

(defun format* (ui-stream thing &rest args)
  "Used for formatting THINGS into UI-STREAM."
  (cond
    ((typep thing 'object)
     ;; Views are created for objects
     (let ((span (create-span ui-stream)))
       (create-object-view thing span)))
    ;;((typep thing 'clog-element)
     ;; If an UI element, append
    (t (create-span ui-stream :content (princ-to-string thing)))))

#+example
(with-open-window wnd (*body* :title "Stream output")
  (format* wnd (first *objects*))
  (format* wnd "lalala")
  (format* wnd (second *objects*))
  (format* wnd "laasdf")
  (format* wnd (third *objects*)))
