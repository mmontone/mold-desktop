(defpackage :mold-desktop/dialog
  (:use :cl)
  (:export :with-dialog))

(in-package :mold-desktop/dialog)

(defmacro with-dialog (options inputs &body body))

(with-dialog (:title "GDrive options" :buttons "Accept")
  ((client-id string)
   (client-secret string))
  ;; entered data is bound to client-id and client-secret in body
  (print "Data: ~s" (list client-id client-secret)))

