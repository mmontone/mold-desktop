(defpackage :value-models
  (:use :cl)
  (:export
   :value
   :value-model
   :value-holder
   :aspect-adaptor
   :as-value
   :make-aspect-adaptor
   :make-slot-adaptor))

(in-package :value-models)

(defgeneric value (value-model))

(defmethod value (object)
  object)

(defclass value-model (event-emitter:event-emitter)
  ())

(defmethod print-object ((value-model value-model) stream)
  (print-unreadable-object (value-model stream :type t :identity t)
    (format stream "value: ~s" (value value-model))))

(defclass value-holder (value-model)
  ((value :accessor value
	  :initarg :value)))

(defun as-value (object)
  (make-instance 'value-holder :value object))

(defmethod (setf value) :around (new-value (value-model value-model))
  (let ((current-value (value value-model)))
    (prog1
	(call-next-method)
      (when (not (equalp new-value current-value))
	(event-emitter:emit :changed value-model new-value)))))

(defclass aspect-adaptor (value-model)
  ((subject :initarg :subject
	    :accessor subject)
   (getter :initarg :getter
	   :accessor getter)
   (setter :initarg :setter
	   :accessor setter)))

(defmethod initialize-instance :after ((aspect-adaptor aspect-adaptor) &rest initargs)
  (declare (ignore initargs))
  ;; If subject is an event emitter, propagate its change events
  (when (typep (subject aspect-adaptor) 'event-emitter:event-emitter)
    (event-emitter:on :changed (subject aspect-adaptor)
		      (lambda (val)
			(event-emitter:emit :changed aspect-adaptor val)))))

(defmethod value ((aspect-adaptor aspect-adaptor))
  (funcall (getter aspect-adaptor) (subject aspect-adaptor)))

(defmethod (setf value) (val (aspect-adaptor aspect-adaptor))
  (funcall (setter aspect-adaptor) val (subject aspect-adaptor)))

(defun make-aspect-adaptor (subject aspect)
  "ASPECT is an accessor name."
  (make-instance 'aspect-adaptor
		 :subject subject
		 :getter (fdefinition aspect)
		 :setter (fdefinition `(setf ,aspect))))

(defclass slot-adaptor (value-model)
  ((subject :initarg :subject
	    :accessor subject)
   (slot-name :initarg :slot-name
	      :accessor slot-name)))

(defmethod value ((slot-adaptor slot-adaptor))
  (slot-value (subject slot-adaptor) (slot-name slot-adaptor)))

(defmethod (setf value) (new-value (slot-adaptor slot-adaptor))
  (setf (slot-value (subject slot-adaptor) (slot-name slot-adaptor)) new-value))

(defun make-slot-adaptor (object slot-name)
  (make-instance 'slot-adaptor :subject object :slot-name slot-name))

(defun value-model-ref-reader (stream char)
  (declare (ignore char))
  (list (quote value) (read stream t nil t)))

;; (set-macro-character #\@ #'value-model-ref-reader)

(named-readtables:defreadtable :value-model-syntax
   (:merge :standard)
   (:macro-char #\@ #'value-model-ref-reader))
