(in-package :mold-desktop)

(defclass chart-view (clog-element)
  ((options :initform nil
            :accessor chart-options)))

(defclass chart (object)
  ((js-handle :initarg js-handle
              :accessor js-handle)
   (labels :accessor chart-labels
     :initarg :labels
     :initform nil)
   (datasets :accessor chart-datasets
             :initarg :datasets
             :initform nil)
   (chart-type :accessor chart-type
               :initform "line"
               :initarg :chart-type)
   (options :accessor chart-options
            :initarg :options
            :initform nil))
  (:icon . "fa-solid fa-chart-line")
  (:metaclass mold-object-class))

(defun init-charts (body)
  (add-script (html-document body) "https://cdn.jsdelivr.net/npm/chart.js"))

(pushnew 'init-charts *init-functions*)

(defun write-chart-config (chart stream)
  (json:with-object (stream)
    (json:encode-object-member :type (chart-type chart) stream)
    (json:as-object-member (:data stream)
      (write-chart-data chart stream))
    (json:as-object-member (:options stream)
      (write-chart-options chart stream))))

(defun write-chart-data (chart stream)
  (json:with-object (stream)
    (when (chart-labels chart)
      (json:encode-object-member :labels (chart-labels chart) stream))
    (when (chart-datasets chart)
      (json:as-object-member (:datasets stream)
        (json:with-array (stream)
          (dolist (dataset (chart-datasets chart))
            (json:as-array-member (stream)
              (json:encode-json-alist dataset stream))))))))

(defun write-chart-options (chart stream)
  (json:encode-json-alist (chart-options chart) stream))

(defmethod create-object-view ((chart chart) clog-obj &rest args)
  (declare (ignore args))
  ;; TODO: fetch chart options from ARGS?
  (let* ((chart-view (create-canvas clog-obj)))
    (change-class chart-view 'chart-view)
    (setf (js-handle chart)
          (format nil "window.chart~a" (html-id chart-view)))
    (js-execute chart-view
                (with-output-to-string (s)
                  (format s "~a = new Chart(document.getElementById('~a'),"
                          (js-handle chart)
                          (html-id chart-view))
                  (write-chart-config chart s)
                  (write-string ");" s)))
    chart-view))

(defun serialize-chart-dataset (&key label background-color data)
  (with-output-to-string (json:*json-output*)
    (json:with-object ()
      (json:encode-object-member :label label)
      (json:encode-object-member :background-color background-color)
      (if (alistp data)
          (json:as-object-member (:data)
            (json:with-array ()
              (dolist (cons data)
                (json:as-array-member ()
                  (json:with-object ()
                    (json:encode-object-member :x (car cons))
                    (json:encode-object-member :y  (cdr cons)))))))
          (json:encode-object-member :data data)))))

(defcommand add-dataset
    ((chart chart "A chart")
     (dataset collection "A dataset"))
  (js-execute *body*
              (with-output-to-string (s)
                (when (null (chart-labels chart))
                  (setf (chart-labels chart)
                        (if (alistp (members dataset))
                            (mapcar #'car (members dataset))))
                  (format s  "~a.data.labels = " (js-handle chart))
                  (json:encode-json (chart-labels chart) s)
                  (write-char #\; s)
                  (terpri s))
                (push (members dataset) (chart-datasets chart))
                (format s "~a.data.datasets.push(~a);"
                        (js-handle chart)
                        (serialize-chart-dataset
                         :label (object-title dataset)
                         :background-color "yellow"
                         :data (members dataset)))
                (format s "~a.update();" (js-handle chart)))))

(defcommand (chart-example-1 :show-result t)
    ()
  (make-instance 'chart
                 :name "Chart Example 1"
                 :labels '("January" "February" "March" "April" "May" "June")
                 :datasets '(((:label . "My first dataset")
                              (:background-color . "rgb(255, 99, 132)")
                              (:border-color . "rgb(255, 99, 132)")
                              (:data . #(0 10 5 2 20 30 45))))))

(defcommand (chart-example-1-dataset :show-result t)
    ((name string))
  (make-instance 'collection
                 :name name
                 :members (coerce (ironclad:random-data 7) 'list)))
