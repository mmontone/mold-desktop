;; Model editor that uses a tree component

(in-package :mold-desktop)

;; For using an icon as mouse pointer, extract the image data from here: http://jsfiddle.net/rqq8B/2/
;; Then use it for cursor property: cursor: url(<dataUrl>).

(defparameter +wrench-icon-img-data+
  "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABgAAAAYCAYAAADgdz34AAAB1ElEQVRIS7WUSytFURiGz3G/FANh4FZMlKGYKTGgRJFTikgkUQYGBmYyUkwkhISUSFJ0ykAmDPwARgZuRSZkIuTyfLU2q93ex177OLue9rp8633X+tYlGIjxF4yxfsCvQTwT64BmKIZnOIdVONEn7ccgH4FdKHdY/R1tRfBu9ZkaZDDwFEpt4m/Up2Fcrean29RggpEjNvE96sNw4bSfJgZxCNxDtib0Sjkl0kExMShA6Nom9q8GJQ5pcDMIEVsh6TRZgaTiCZJtqwirfTnjXwYzUA3bEDIxyGHAHLQ45PyDtkOogQTV38A/7NWgk+AxJX7MPy3SxtJ3pMw83eR2gtdATtEiyCXbgnQXE7kn9Sqdfxq0EbgO8jRYn5hInkehUVvNjZrAJP8XKzhSiloJ2tByqk/4kkolPEIuyNPw4LQiNwN5xCQN1obpYz+pdKmVuWTpt9nJoIluOWKJDqNFvBtkTzx9dgM5WjuQ5CLeQ/uKJ2UVpBvIzssJsV8kCf2CXlg2EZdYy6CW8j7IbZ2CTCVoifdRWDIV1w1uqeQpATn3WSDvu8y8Hxb8iOsGV1QKbSIiPgDzfsV1gyoqB5CqiQ1Sno1GXDeQch1sgrwzQ9HO3JrYN7y+TBmoAmhFAAAAAElFTkSuQmCC")

(defview create-tree-model-editor-view (object clog-obj &rest args)
  (:documentation "A model editor that uses a tree component.")
  (:icon "fa-solid fa-folder-tree")
  (:label "Tree model editor view"))

(defview create-model-editor-view (object clog-obj &rest args)
  (:documentation "A model editor view.")
  (:icon "fa-solid fa-pencil")
  (:label "Model editor view"))

(defmethod object-view-types append ((object object) (mold-module (eql :tree-model-editor)))
  '(create-tree-model-editor-view create-model-editor-view))

(defclass model-attribute ()
  ((model :initarg :model
          :accessor model)
   (attribute :initarg :attribute
              :accessor model-attribute)))

(defmethod model-attribute-value ((attr model-attribute))
  (slot-value (model attr) (c2mop:slot-definition-name (model-attribute attr))))

(defmethod (setf model-attribute-value) (value (attr model-attribute))
  (setf (slot-value (model attr) (c2mop:slot-definition-name (model-attribute attr))) value))

(defmethod model-members (obj)
  nil)

(defmethod model-members ((obj object))
  (mapcar (lambda (attribute)
            (make-instance 'model-attribute
                           :model obj
                           :attribute attribute))
          (attributes (class-of obj))))

(defmethod model-members ((coll collection))
  (members coll))

(defmethod model-members ((list list))
  list)

(defmethod model-label (obj)
  (object-title obj))

(defmethod model-members ((attr model-attribute))
  (model-members (model-attribute-value attr)))

(defmethod model-label ((model-attribute model-attribute))
  (str:sentence-case (princ-to-string (c2mop:slot-definition-name (model-attribute model-attribute)))))

(defmethod model-icon (obj)
  (object-icon obj))

(defgeneric container-p (object)
  (:documentation "Returns T if OBJECT is a container of other objects."))

(defmethod container-p (object)
  (typep object '(or collection cons list vector)))

(defun container-type-p (type)
  "Returns T if TYPE is a type that contains values of other types.
For example, lists, collections, vectors."
  (or (member type '(collection cons list vector))
      (and (listp type)
           (member (car type) '(list-of collection-of)))))

(defmethod create-model-tree-view ((object object) clog-obj &rest args)
  (let ((tree (create-unordered-list clog-obj :class "tree")))
    (setf (attribute tree "role") "tree")
    (labels ((add-tree-level (item li)
               (let* ((attribute-type (when (typep item 'model-attribute)
                                        (attribute-type (model-attribute item))))
                      ;; When the model attribute is a container like a collection, list, etc
                      ;; Then attach presentation handlers to the value of the attribute
                      ;; That allows adding objects to the collection from the UI.
                      (presented-object (if (and attribute-type
                                                 (container-type-p attribute-type))
                                            (model-attribute-value item)
                                            item)))

                 (cond
                   ((not (model-members item)) ;; a leaf
                    (let ((span (create-span li :class "tree-leaf")))
                      (with-ui-update li item :changed
                        (setf (text span) (model-label item))
                        (alex:when-let ((icon (model-icon item)))
                          (create-icon icon span :auto-place :top)))
                      (mold-desktop/clog-gui::attach-presentation-handlers
                       presented-object span :on-click (getf args :on-click))))
                   ((model-members item) ;; a node
                    (let ((span (create-span li :class "caret tree-node")))
                      (with-ui-update span item :changed
                        (setf (text span) (model-label item))
                        (alex:when-let ((icon (model-icon item)))
                          (create-icon icon span :auto-place :top)))
                      (mold-desktop/clog-gui::attach-presentation-handlers
                       presented-object span :on-click (getf args :on-click))
                      (let ((ul (create-unordered-list li :class "nested")))
                        (setf (attribute ul "role") "group")
                        (with-ui-update ul item :changed
                          (gcl:doseq (child (model-members item))
                            (let ((li (create-list-item ul)))
                              (setf (attribute li "role") "treeitem")
                              (setf (attribute li "aria-expanded") "false")
                              (add-tree-level child li))))))))))
             (draw-tree (&rest _)
               (declare (ignore _))
               (setf (text tree) "")
               (if (getf args :show-root)
                   (let ((li (create-list-item tree)))
                     (setf (attribute li "role") "treeitem")
                     (setf (attribute li "aria-expanded") "false")
                     (add-tree-level object li))
                   (gcl:doseq (item (model-members object))
                     (let ((li (create-list-item tree)))
                       (setf (attribute li "role") "treeitem")
                       (setf (attribute li "aria-expanded") "false")
                       (add-tree-level item li))))
               (clog:js-execute clog-obj (format nil "initTreeView(document.getElementById('~a'));" (html-id tree)))))
      (draw-tree)
      (event-emitter:on :changed object #'draw-tree)
      )))

(pushnew :tree-model-editor *mold-modules*)

#+test(open-object-in-window
       (make-instance 'task)
       *body* #'create-model-tree-view)
#+test(open-object-in-window
       (make-instance 'task)
       *body* #'create-model-tree-view :show-root t)

(defmethod create-model-editor-view (object clog-obj &rest args)
  (declare (ignore args))
  (create-span clog-obj :content (princ-to-string object)))

(defmethod create-model-editor-view ((object object) clog-obj &rest args)
  "Create editors for the object attributes."
  (declare (ignore args))
  (let ((table (create-table clog-obj :class "w3-table w3-bordered")))
    (dolist (member (model-members object))
      (let ((tr (create-table-row table)))
        (let ((td (create-table-column tr :content (model-label member))))
          (setf (advisory-title td) (documentation (model-attribute member) t)))
        (create-model-editor-view member (create-table-column tr))))))

(defmethod create-model-editor-view ((coll collection) clog-obj &rest args)
  (declare (ignore args))
  (let ((view (create-div clog-obj)))
    (create-object-view coll view)
    (with-clog-create view
        (div (:class "w3-bar")
             (icon-button ("fa-solid fa-square-plus"
                           (lambda (&rest args)
                             (declare (ignore args))
                             (run-command-interactively 'mold-desktop/commands::add-to-collection coll))
			   :class "w3-button"))
             (icon-button ("fa-solid fa-square-minus"
                           (lambda (&rest args)
                             (declare (ignore args))
                             (run-command-interactively 'mold-desktop/commands::remove-from-collection coll))
			   :class "w3-button"))))
    view))

(defmethod create-model-editor-view ((model-attribute model-attribute) clog-obj &rest args)
  "Editor for a model attribute."
  (declare (ignore args))
  (let* ((value (slot-value (model model-attribute)
                            (c2mop:slot-definition-name (model-attribute model-attribute)))))
    (if (typep value 'collection)
        (create-model-editor-view value clog-obj)
        ;; else
        (let ((div (create-div clog-obj :style (format nil "cursor: url(~s), auto" +wrench-icon-img-data+))))
          (with-ui-update div (model model-attribute) :changed
            (setf (text div) (princ-to-string
                              (slot-value (model model-attribute)
                                          (c2mop:slot-definition-name (model-attribute model-attribute))))))
          (setf (clog:advisory-title div) "Double click to edit.")
          (set-on-double-click
           div
           (lambda (&rest args)
             (declare (ignore args))
             (prompt-command-argument-type
              (attribute-type (model-attribute model-attribute))
              (list :name (c2mop::slot-definition-name (model-attribute model-attribute)))
              *command-bar*
              (lambda (value)
                (setf (slot-value (model model-attribute)
                                  (c2mop:slot-definition-name (model-attribute model-attribute)))
                      value)))))
          div))))

(defmethod create-tree-model-editor-view (object clog-obj &rest args)
  (let* ((editor-view (create-div clog-obj :class "split" :style "height:100%;"))
         (tree-view (create-div editor-view :style "overflow:auto;"))
         (detail-view (create-div editor-view :class "w3-padding"
                                              :style "overflow:auto;")))
    (create-model-tree-view object tree-view
                            :on-click (lambda (node)
                                        (clear-element detail-view)
                                        (create-model-editor-view node detail-view))
                            :show-root t)
    (clog:js-execute clog-obj (format nil "Split(['#~a','#~a'])"
                                      (clog:html-id tree-view)
                                      (clog:html-id detail-view)))
    editor-view))

(defcommand (set-model-attribute :icon "fa fa-cog")
    ((model-attribute model-attribute "The model attribute")
     (value t "The model attribute value"
            :prompt (lambda (args)
                      (let ((model-attribute (cdr (assoc 'model-attribute args))))
                        (if model-attribute
                            (attribute-type (model-attribute model-attribute))
                            t)))))
  (setf (slot-value (model model-attribute)
                    (c2mop:slot-definition-name (model-attribute model-attribute)))
        value))

;; Set tree-model-editor as default view for OBJECTs that have not defined
;; a custom view.

(defmethod create-object-view ((object object) clog-obj &rest args)
  (apply #'create-tree-model-editor-view object clog-obj args))

#+test(show (make-instance 'task) :view #'create-tree-model-editor-view)
