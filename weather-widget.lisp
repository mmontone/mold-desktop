(in-package :mold-desktop)

(defclass weather-widget (object)
  ((location :initarg :location
	     :accessor location))
  (:metaclass mold-object-class)
  (:icon . "fa-solid fa-cloud-sun")
  (:default-initargs
   :icon "fa-solid fa-cloud-sun"
   :name "Weather widget"))

(defmethod create-object-view ((weather-widget weather-widget) clog-obj &rest args)
  (clog:create-child clog-obj
		     "<a class=\"weatherwidget-io\" href=\"https://forecast7.com/en/40d71n74d01/new-york/\" data-label_1=\"NEW YORK\" data-label_2=\"WEATHER\" data-theme=\"original\" >NEW YORK WEATHER</a>")
  (clog:create-child clog-obj
"<script>
!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0];if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src='https://weatherwidget.io/js/widget.min.js';fjs.parentNode.insertBefore(js,fjs);}}(document,'script','weatherwidget-io-js');
</script>"))
