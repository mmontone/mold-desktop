;; TODO: consider https://shoelace.style

(cl:in-package :mold-desktop/clog-gui)

(defmethod create-gui-window ((obj clog-obj) &key (title "New Window")
			       (content "")
			       (left nil)
			       (top nil)
			       (width 300)
			       (height 200)
			       (maximize nil)
			       (has-pinner nil)
			       (keep-on-top nil)
			       (hidden nil)
			       (client-movement *client-movement*)
			       (html-id nil)
			       (object nil))
  (let ((app (connection-data-item obj "clog-gui")))
    (unless html-id
      (setf html-id (clog-connection:generate-id)))
    (when (eql (hash-table-count (windows app)) 0)
      ;; If previously no open windows reset default position
      (setf (last-x app) 0)
      (setf (last-y app) 0))
    (unless left
      ;; Generate sensible initial x location
      (setf left (last-x app))
      (incf (last-x app) 10))
    (unless top
      ;; Generate sensible initial y location
      (when (eql (last-y app) 0)
	(setf (last-y app) (menu-bar-height obj)))
      (setf top (last-y app))
      (incf (last-y app) top-bar-height)
      (when (> top (- (inner-height (window (body app))) (last-y app)))
	(setf (last-y app) (menu-bar-height obj))))
    (let ((win (create-child (or (app-body app) (body app))
		 (html (:div :style (inline-css `(:position "absolute"
						  :top ,(px top) :left ,(px left) :width ,(px width) :height ,(px height)
						  :z-index ,(incf (last-z app))
						  :visibility "hidden"))
			     :class "ui top attached segment"
			 ;; Title bar
			 (:div :id (format nil "~a-title-bar" html-id)
			   :class "ui top attached label"
			   :style "position:absolute;top:0;right:0;left:0;height:25px"
			   ;; Title
			   (:span :data-drag-obj html-id :data-drag-type "m" :id (format nil "~a-title" html-id)
			     :style "cursor:move;width: 100%;"
			     (when (and object (mold-desktop::object-icon object))
			       (who:htm (:i :class (format nil "fa fa-~a" (mold-desktop::object-icon object))
					  :style "margin-right: 4px;")))
			     (who:str title))
			   ;; Title bar buttons
			   (when has-pinner
			     (who:htm (:span :id (format nil "~a-pinner" html-id)
					:style "position:absolute;top:0;right:20px;cursor:pointer;user-select:none;")))
			   (when object
			     (who:htm
			      (:div :id (format nil "~a-menu" html-id) :class "circular-menu"
				    :style "position:absolute;top:-30px;right:-10px;cursor:pointer;"
				    (:div :class "circle"
					  (:a :href "#" :id (format nil "~a-trash" html-id)
				 	      (:i :class "fa fa-trash"))
					  (:a :href "#" :id (format nil "~a-settings" html-id)
				 	      (:i :class "fa fa-cog"))
					  (:a :id (format nil "~a-actions" html-id)
				 	      (:i :class "fa fa-terminal"))
					  (:a :id (format nil "~a-properties" html-id)
				 	      (:i :class "fa fa-list"))
					  (:a :id (format nil "~a-info" html-id)
					      (:i :class "fa fa-eye"))
					  (:a :id (format nil "~a-drag" html-id)
					      :draggable "true"
					      (:i :class "fa fa-hand-back-fist")))
				    (:a :class "menu-button fa fa-bars"))
			      (:a :id (format nil "~a-open-link" html-id)
				 :href (mold-desktop::object-url object)
				 :target "_blank"
				 :style "position:absolute;top:3px;right:20px;cursor:pointer;user-select:none;"
				 (:i :class "fa fa-solid fa-arrow-up-right-from-square"))))
			   (:span :id (format nil "~a-closer" html-id)
				  :style "position:absolute;top:3px;right:5px;cursor:pointer;user-select:none;"
			     (who:str "&times")))
			 
			 ;; Window body
			 (:div :id (format nil "~a-body" html-id)
			       :style "position:absolute;top:0;left:0;right:0;bottom:3px;overflow:auto"
			   (who:str content))
			 #+nil(:div
			   :style "position:absolute;bottom:0;left:0;height:3px;"
			   (:span :id (format nil "~a-drag" html-id)
				 :draggable "true"
			     (:i :class "fa fa-solid fa-hand-pointer"
			       :draggable "true")))
			 ;; Sizer
			 (:div :id (format nil "~a-sizer" html-id)
			   :style "position:absolute;right:0;bottom:0;left:0;user-select:none;height:3px;cursor:se-resize;opacity:0"
			   :class "w3-right" :data-drag-obj html-id :data-drag-type "s"
			   (who:str "+"))))
		 :clog-type 'clog-gui-window
		 :html-id html-id)))
      (setf (win-title win)
	(attach-as-child win (format nil "~A-title" html-id)))
      (setf (title-bar win)
	(attach-as-child win (format nil "~A-title-bar" html-id)))
      (when has-pinner
	(setf (pinner win) (attach-as-child win (format nil "~A-pinner" html-id))))
      (setf (closer win) (attach-as-child win (format nil "~A-closer" html-id)))
      (setf (sizer win) (attach-as-child win (format nil "~A-sizer" html-id)))
      (setf (content win) (attach-as-child win (format nil "~A-body"  html-id)))

      (when object
	(setf (desktop-object win) object)
	(setf mold-desktop::*object* object)
	;; Settings button
	(let ((settings-btn (attach-as-child win (format nil "~a-settings" html-id))))
	  (set-on-click settings-btn (lambda (clog-obj) (mold-desktop::open-object-settings object clog-obj))))
	
	;; Selection button
	#+nil(let ((select-btn (attach-as-child win (format nil "~a-select" html-id))))
	  (set-on-click select-btn (lambda (_)
				     (declare (ignore _))
				     (mold-desktop::select-object object))))
	;; Initialize circular menu
	(clog:js-execute obj (format nil "initCircularMenu(document.getElementById('~a-menu'))" html-id))
	)
      
      (setf (gethash (format nil "~A" html-id) (windows app)) win)
      (if maximize
	(window-maximize win)
	(fire-on-window-change win app))
      (unless hidden
	(setf (visiblep win) t))
      (when (window-select app)
	(setf (window-select-item win) (create-option (window-select app)
					 :content title
					 :value html-id)))
      (set-on-double-click (win-title win) (lambda (obj)
					     (declare (ignore obj))
					     (window-toggle-maximize win)))
      (if has-pinner
	(set-on-click (pinner win) (lambda (obj)
       				     (declare (ignore obj))
       				     (window-toggle-pin win keep-on-top)))
	(when keep-on-top
	  (window-keep-on-top win)))      
      (set-on-click (closer win) (lambda (obj)
				   (declare (ignore obj))
				   (when (fire-on-window-can-close win)
				     (window-close win))))
      (cond (client-movement
	      (clog::jquery-execute win
		(format nil "draggable({handle:'#~A-title'})" html-id))
	      (clog::jquery-execute win "resizable({handles:'se'})")
	      (set-on-pointer-down (win-title win)
	    	(lambda (obj data)
		  (declare (ignore obj) (ignore data))
	    	  (setf (z-index win) (incf (last-z app)))
	    	  (fire-on-window-change win app)))
	      (clog::set-on-event win "dragstart"
		(lambda (obj)
		  (declare (ignore obj))
		  (fire-on-window-move win)))
	      (clog::set-on-event win "dragstop"
		(lambda (obj)
		  (declare (ignore obj))
		  (fire-on-window-move-done win)))
	      (clog::set-on-event win "resizestart"
		(lambda (obj)
		  (declare (ignore obj))
		  (fire-on-window-size win)))
	      (clog::set-on-event win "resizestop"
		(lambda (obj)
		  (declare (ignore obj))
		  (fire-on-window-size-done win))))
	(t
	  (set-on-pointer-down
	    (win-title win) 'on-gui-drag-down :capture-pointer t)
	  (set-on-pointer-down
	    (sizer win) 'on-gui-drag-down :capture-pointer t)))
      win)))


