(in-package :mold-desktop)

(defvar *modules-directories*
  (list (asdf:system-source-directory :mold-desktop)
        (asdf:system-relative-pathname :mold-desktop "modules/"))
  "A list of directories in which to look for module files.")

(defvar *module-extensions* (list "mod.lisp" "asd"))
(defvar *loaded-modules* nil)
(defvar *registered-modules* nil)

(defclass mold-module ()
  ((name :initarg :name
         :accessor module-name
	 :type symbol
         :initform (error "Provide the module name"))
   (description :initarg :description
                :accessor module-description
                :initform "")
   (dependencies :initarg :dependencies
                 :accessor module-dependencies
                 :initform nil)))

(defmethod print-object ((module mold-module) stream)
  (print-unreadable-object (module stream :type t :identity t)
    (format stream "~a" (module-name module))))

(defmethod object-title ((module mold-module))
  (str:sentence-case (princ-to-string (module-name module))))

(defmethod object-description ((module mold-module))
  (module-description module))

(defmethod object-icon ((module mold-module))
  "fa-solid fa-box-open")

(defgeneric-command load-module
    ((module mold-module "The module to install"
             :prompt '(member-of-list (list-available-modules))))
  (:icon . "fa fa-upload"))

(defclass file-mold-module (mold-module)
  ((pathname :initarg :pathname
             :accessor module-pathname)))

(defmethod load-module ((module file-mold-module))
  (with-job (job :name (format nil "Load ~a" (module-name module)))
    (declare (ignore job))
    (load (module-pathname module)))
  nil)

(defclass asdf-mold-module (mold-module)
  ((system-name :initarg :system-name
                :accessor module-system-name)))

(defmethod load-module ((module asdf-mold-module))
  (with-job (job :name (format nil "Load ~a" (module-name module)))
    (declare (ignore job))
    (asdf:operate 'asdf:load-op (module-system-name module)))
  nil)

(defun register-module (module)
  (pushnew module *registered-modules* :key #'module-name))

(defmacro defmodule (class name &rest args)
  `(register-module
    (make-instance ',class :name ',name ,@args)))

(defun list-available-modules ()
  "Loads the list of modules found on disk."
  (let (modules)
    (dolist (module-directory *modules-directories*)
      (dolist (module-extension *module-extensions*)
        (let ((module-files (uiop/filesystem:directory-files module-directory
                                                             (format nil "*.~a" module-extension))))
          (dolist (module-file module-files)
            (let ((module-spec (read-from-string (alexandria:read-file-into-string module-file))))
              (when (not (eql (first module-spec) 'defmodule))
                (warn "No module spec in ~s" module-file))
              (when (eql (first module-spec) 'defmodule)
                (destructuring-bind (module-class name &rest args)
                    (rest module-spec)
                  (let ((module (apply #'make-instance module-class :name name  args)))
                    (when (typep module 'file-mold-module)
                      (setf (module-pathname module) module-file))
                    (push module modules)))))))))
    (append modules *registered-modules*)))

;; Some modules that come with default distribution

(register-module
 (make-instance 'asdf-mold-module
		:name 'tasks-workspace
		:system-name :mold-desktop-tasks
		:description "A workspace for managing tasks"))

(register-module
 (make-instance 'asdf-mold-module
		:name 'bookmarks-workspace
		:system-name :mold-desktop-bookmarks
		:description "A workspace for managing bookmarks"))

(register-module
 (make-instance 'asdf-mold-module
		:name 'feeds-workspace
		:system-name :mold-desktop-feeds
		:description "A workspace for managing RSS feeds"))


;; UI

(defmethod create-item-view ((object mold-module) li &rest args &key context &allow-other-keys)
  (declare (ignore args))
  (cond
    ((object-icon object)
     (create-child li (html (:i :class (format nil "fa fa-solid fa-~a"
                                               (object-icon object))
                                :style "margin-right: 3px;")))))
  (let ((link
          (create-a li :content (object-title object))))
    (set-on-event link "click"
                  (lambda* (_)
                    (show object :context context))
                  :cancel-event t)))

(defmethod create-object-view ((object mold-module) clog-obj &rest args)
  "The default view for objects with no specialized view."
  (declare (ignore args))
  (create-div clog-obj :content (who:escape-string (object-description object)))
  (arrows:->
   (create-div clog-obj)
   (create-button :content "Load"
		  :class "w3-button w3-green")
   (clog:set-on-click (ignoring-args (load-module object)))))
