(defpackage :remote-object-server
  (:use :cl)
  (:export
   #:object
   #:object-id
   #:make-object
   #:command
   #:define-command
   #:define-object
   #:render-view
   #:define-service
   #:object-views
   #:create-service-dispatcher
   #:find-service
   #:service-manifest
   #:serialize-service-manifest)
  (:documentation "Server facilities for providing a Mold remote object from Common Lisp."))

(in-package :remote-object-server)

(defclass service ()
  ((name :initarg :name
         :accessor service-name)
   (url :initarg :url
        :accessor service-url)
   (object-types :initform nil
                 :initarg :object-types
                 :accessor object-types)
   (objects :initform (make-hash-table :test 'equalp)
            :accessor objects
            :documentation "A table with the created objects for this service.
A mapping from id to objects.")
   (commands :initform (make-hash-table :test 'equalp)
             :initarg :commands
             :accessor commands)
   (documentation :initarg :documentation
                  :accessor service-documentation)
   (options :initarg :options
            :accessor service-options)))

(defun create-service-dispatcher (service &optional (path "/"))
  "Create a Hunchentoot dispatcher for remote-object SERVICE."
  (lambda (request)
    (when (str:starts-with-p path (hunchentoot:script-name request))
      (let ((endpoint (subseq (hunchentoot:script-name request)
                              (length path))))
        (trivia:match (list endpoint (hunchentoot:request-method request))
          ((list "/manifest" :get)
           (setf (hunchentoot:content-type*) "application/json")
           (serialize-service-manifest service :json))
          ((list (trivia.ppcre:ppcre "^/commands/(.+)" command-name) :post)
           (let ((command (find-command service command-name))
                 (args (resolve-arguments (hunchentoot:post-parameter "args"))))
             (funcall (command-function command) args)))
          (t
           (setf (hunchentoot:return-code*) hunchentoot:+http-not-found+)
           (hunchentoot:abort-request-handler (format nil "Endpoint not found: ~a" endpoint))))))))

(defclass object ()
  ((id :initarg :id
       :accessor object-id
       :initform (gen-object-id))
   (description :initarg :description
                :accessor description)))

(defun gen-object-id ()
  (princ-to-string (uuid:make-v4-uuid)))

(defun make-object (service class &rest initargs)
  (let ((object (apply #'make-instance class initargs)))
    (setf (gethash (object-id object) (objects service)) object)
    object))

(defun find-object (service id &optional (error-p t))
  (or (gethash id (objects service))
      (when error-p
        (error "Object not found: ~s" id))))

(defclass command ()
  ((name :accessor command-name
         :initarg :name)
   (arglist :accessor command-arglist
            :initarg :arglist)
   (description :initarg :description
                :accessor command-description
                :initform nil)
   (function :accessor command-function
             :initarg :function)))

(defun find-command (service name &optional (error-p t))
  (or (gethash name (commands service))
      (when error-p
        (error "Command not found: ~s" name))))

(defmacro define-object (name super-classes slots &rest options))

(defmacro define-command (name args &body body)
  `(let ((command (make-instance 'command :name ',name
                                          :arglist ',args
                                          :function (lambda ,(mapcar #'car args) ,@body))))
     (setf (gethash ',name *commands*) command)
     command))

(defun serialize-service-manifest-to-json (service)
  (json:encode-json-to-string
   (service-manifest service)))

(defun serialize-service-manifest (service format &rest args)
  (ecase format
    (:json (serialize-service-manifest-to-json service))
    (:xml (serialize-service-manifest-to-xml service))))

(defgeneric render-view (view object))

(defmacro define-service (name superclasses slots &rest options)
  `(defclass ,name (service ,@superclasses)
     ,slots
     ,@options))

(defgeneric object-views (object service)
  (:documentation "Lists the views for OBJECT")
  (:method-combination append)
  (:method append (object service)
    '(default-view)))

(defun service-manifest (service)
  (flet ((serialize-object-type (type-name)
           (let ((type (find-class type-name)))
             (list (cons :name type-name)
                   (cons :documentation (documentation type t))
                   (cons :icon "TODO"))))
         (serialize-command (command-name)
           (let ((command (find-command service command-name)))
             (list (cons :name command-name)
                   (cons :arglist (command-arglist command))
                   (cons :documentation (command-description command))))))
    (list (cons :name (service-name service))
          (cons :url (service-url service))
          (cons :description (service-documentation service))
          (cons :object-types (mapcar #'serialize-object-type (object-types service)))
          (cons :commands (mapcar #'serialize-command (commands service))))))

(defun serialize-object (object service)
  (list (cons :id (object-id object))
        (cons :type (class-of object))
        (cons :description (description object))
        (cons :url (format nil "~a/objects/~a" (service-url service) (object-id object)))))
