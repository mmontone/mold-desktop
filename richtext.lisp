(in-package :mold-desktop)

(defclass richtext-editor (clog-element)
  ((serialized-content :accessor serialized-content)
   (editorjs-instance :accessor editorjs-instance)
   (richtext-object :initarg :richtext-object
                    :accessor richtext-object)
   (attached-objects-views :accessor attached-objects-views
                           :documentation "Hidden area where attached objects views are created into to be then transferred to the editor component.")))

(defclass richtext (object)
  ((attached-objects :accessor attached-objects
                     :initform nil
                     :documentation "List of objects embeded into this richtext."))
  (:metaclass mold-object-class)
  (:documentation "Richtext document")
  (:icon . "fa-regular fa-file-lines"))

(defun editorjs-varname (clog-obj)
  (format nil "window.editorjs~a" (html-id clog-obj)))

(defun init-richtext-editor (body)
  (load-css (html-document body) "editor-js/object.css")
  (load-script (html-document body) "https://cdn.jsdelivr.net/npm/@editorjs/editorjs@latest")
  (load-script (html-document body) "editor-js/object-url.js")
  (load-script (html-document body) "editor-js/object.js"))

(pushnew 'init-richtext-editor *init-functions*)

(defmethod create-object-view ((object richtext) clog-obj &rest args)
  (declare (ignore args))
  (let ((rt-editor (create-div clog-obj)))
    (change-class rt-editor 'richtext-editor)
    (setf (attached-objects-views rt-editor)
          (create-div rt-editor :hidden t))
    (setf (richtext-object rt-editor) object)
    (event-emitter:on 'object-attached object
                      (lambda ()
                        (let* ((attached-object (first (attached-objects object)))
                               (view (mold-desktop/clog-gui::create-halos-wrapper (attached-objects-views rt-editor) attached-object)))
			  (create-object-view attached-object view)
                          (js-execute clog-obj (format nil
                                                       "~a.blocks.insert('moldObject', {objectId: '~a', htmlId: '~a'})"
						       (editorjs-varname rt-editor)
						       (object-id object)
						       (html-id view))))))
    (js-execute clog-obj (format nil "
~a = new EditorJS({
  holder: '~a',
  autofocus:true,
  tools: {
    objectUrl: {
      class: ObjectUrl,
      inlineToolbar: true,
      config: {
        placeholder: 'Paste URL'
      }
    },
    moldObject: {
      class: MoldObject,
      inlineToolbar: false
    }
  }
});
" (editorjs-varname rt-editor) (html-id rt-editor)))))

(defun find-attached-object (richtext id)
  (find id (attached-objects richtext) :test #'equalp
                                       :key #'object-id))

(defcommand insert-into-document
    ((richtext richtext "The richtext")
     (object object "The object to insert"))
  "Insert OBJECT into RICHTEXT, in embedded form."
  (push object (attached-objects richtext))
  (event-emitter:emit 'object-attached richtext))

(defcommand insert-as-link
    ((richtext richtext "The richtext")
     (object object "The object to link to"))
  "Insert a link to OBJECT into RICHTEXT."
  (push object (attached-objects richtext))
  (event-emitter:emit 'object-attached richtext))

#+example
(show (make-instance 'richtext))
