;; TODO: how to avoid interference with web browser and OS key bindings?

(in-package :mold-desktop)

(defvar *global-keymap* (nkeymaps:make-keymap "global-keymap"))

(nkeymaps:define-key *global-keymap*
  "C-RET" 'open-command-bar
  "M-x" 'open-command-bar
  "C-c" 'copy-object
  "C-v" 'paste-object
  "C-x" 'cut-object
  "C-e" 'mold-desktop/commands::evaluate
  "C-:" 'mold-desktop/commands::evaluate
  "C-H" 'mold-desktop::open-mold-desktop-book)

;; (nkeymaps:lookup-key ":" *global-keymap*)

;; TODO: improve this.
;; Does nkeymaps provide this?
(defun keyboard-event-key-binding (keyboard-event)
  "Return the key-binding spec for KEYBOARD-EVENT.
For example, for Ctrl pressed + e returns 'C-e'.
Used for RUN-KEY-BINDING after.
KEYBOARD-EVENT is a Clog keyboard event."
  ;; Key-bindings start with either a Ctrl or Meta key
  (when (or (getf keyboard-event :ctrl-key)
	    (getf keyboard-event :meta-key))
    (with-output-to-string (s)
      (when (getf keyboard-event :ctrl-key)
	(write-string "C-" s))
      (when (getf keyboard-event :meta-key)
	(write-string "M-" s))
      (write-char (code-char (getf keyboard-event :key-code)) s))))
    

(defun run-key-binding (key-binding)
  (let ((f (nkeymaps:lookup-key key-binding *global-keymap*)))
    (when f
      (if (command-p f)
	  (run-command-interactively f)
	  (funcall f)))))
