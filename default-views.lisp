(in-package :mold-desktop)

(defview create-object-view (object clog-obj &rest args)
  (:documentation "The default view for objects.
Specialized for each type of object.")
  (:label "Default view")
  (:icon "fa fa-eye"))

(defview create-thumbnail-view (object clog-obj &rest args)
  (:documentation "Create a 'thumbnail' view for OBJECT.")
  (:label "Thumbnail view")
  (:icon "fa fa-image"))

(defview create-thumbnails-view (object clog-obj &rest args)
  (:documentation "Create a 'thumbnails' view for OBJECT.")
  (:label "Thumbnails view")
  (:icon "fa fa-image"))

(defview create-icons-view (collection clog-obj &rest args)
  (:documentation "View for collection that displays elements as icons.")
  (:label "Icons view")
  (:icon "fa fa-image"))

(defmethod create-object-view ((object hash-table) clog-obj &rest args)
  (declare (ignore args))
  (let* ((table (create-table clog-obj :class "w3-table w3-bordered"))
	 (tbody (create-table-body table)))
    (maphash (lambda (key value)
	       (let ((tr (create-table-row tbody)))
		 (let ((td (create-table-column tr
						:content (object-title key))))
		   (mold-desktop/clog-gui::attach-presentation-handlers
		    key td))

		 (let ((td (create-table-column tr
						:content (object-title value))))
		   (mold-desktop/clog-gui::attach-presentation-handlers
		    value td))))
	     object)
    table))

(defmethod object-view-types append
    ((hash-table hash-table) (module (eql :desktop)))
  '(create-object-view))
