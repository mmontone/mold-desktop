;; Copyright (C) 2022 Mariano Montone - All Rights Reserved
;; You may use, distribute and modify this code under the terms of the
;; GNU Affero General Public License

(in-package :mold-desktop)

(defclass media-object (object)
  ((media-type :initarg :media-type
	       :accessor media-type
	       :initform nil))
  (:metaclass mold-object-class))

(defclass image (media-object)
  ()
  (:default-initargs
   :icon "image"
   :tags '("image"))
  (:metaclass mold-object-class))

(defun parse-image-url (url headers)
  ;; Check media type. Return with nil if doesn't match.
  (let ((content-type (acc:accesses headers :content-type)))
    (when (not (search "image" content-type
                       :test 'string=))
      (return-from parse-image-url))

    ;; Media type matches. Create the image object.
    (make-instance 'image :url url
			  :media-type content-type)))

(pushnew 'parse-image-url *url-parsers*)

(defmethod create-object-view ((image image) clog-obj &rest args)
  (declare (ignore args))
  (create-img clog-obj :url-src (object-url image)))

(defclass video (media-object)
  ()
  (:default-initargs
   :icon "fa fa-video"
   :tags '("video")
   :category "videos")
  (:icon . "core-play")
  (:metaclass mold-object-class))

(defun parse-video-url (url headers)
  ;; Check media type. Return with nil if doesn't match.
  (let ((content-type (acc:accesses headers :content-type)))
    (when (not (search "video" content-type
                       :test 'string=))
      (return-from parse-video-url))

    ;; Media type matches. Create the video object.
    (make-instance 'video :url url
			  :media-type content-type)))

(pushnew 'parse-video-url *url-parsers*)

(defmethod create-object-view ((video video) clog-obj &rest args)
  (declare (ignore args))
  (create-child clog-obj
		(html
		  (:video :controls t
			  :style "width: 100%; height: 100%;"
			  (:source :src (object-url video)
				   :type (media-type video))
			  (who:str "Your browser does not support the video tag.")))))

(defclass youtube-video (video)
  ((video-id :initarg :video-id :accessor video-id))
  (:icon . "core-play")
  (:metaclass mold-object-class))

;; <iframe width="560" height="315" src="https://www.youtube.com/embed/AgYB13ntuzg" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

(defmethod create-object-view ((video youtube-video) clog-obj &rest args)
  (declare (ignore args))
  (create-child clog-obj
                (html (:iframe :width 560 :height 315
                               :src (format nil "https://www.youtube.com/embed/~a" (video-id video))
                               :title "YouTube video player" :frameborder 0
                               :allow "accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture"
                               :allowfullscreen t))))


;; https://www.youtube.com/watch?v=AgYB13ntuzg
(defun parse-youtube-url (url headers)
  "Return the video id."
  (declare (ignorable headers))
  (when (not (str:starts-with? "https://www.youtube.com/watch?v" url))
    (return-from parse-youtube-url))
  (let* ((uri (quri:uri url))
         (video-id (access:access (quri:uri-query-params uri) "v")))
    (multiple-value-bind (title description image) (fetch-preview-data url)
      (make-instance 'youtube-video
                     :name title
                     :description description
                     :image image
                     :id video-id
                     :video-id video-id
                     :url url
                     :type 'youtube-video))))

(pushnew 'parse-youtube-url *url-parsers*)

(defclass audio (media-object)
  ()
  (:default-initargs
   :icon "fa fa-audio"
   :tags '("audio")
   :category "audios")
  (:icon . "core-play")
  (:metaclass mold-object-class))

(defun parse-audio-url (url headers)
  ;; Check media type. Return with nil if doesn't match.
  (let ((content-type (acc:accesses headers :content-type)))
    (when (not (search "audio" content-type :test 'string=))
      (return-from parse-audio-url))

    ;; Media type matches. Create the audio object.
    (make-instance 'audio :url url
			  :media-type content-type)))

(pushnew 'parse-audio-url *url-parsers*)

(defmethod create-object-view ((audio audio) clog-obj &rest args)
  (declare (ignore args))
  (create-child clog-obj
		(html
		  (:audio :controls t
			  :style "width: 100%; height: 100%;"
			  (:source :src (object-url audio)
				   :type (media-type audio))
			  (who:str "Your browser does not support the audio tag.")))))

;; (parse-youtube-url "https://www.youtube.com/watch?v=AgYB13ntuzg")
(defclass movie (object)
  ((imdb-id :initarg :imdb-id
	    :accessor imdb-id
	    :type string
	    :attribute t
	    :initform "")
   (poster :initarg :poster
	   :accessor movie-poster
	   :type string
	   :initform "")
   (type :initarg :type
	 :accessor movie-type
	 :type string
	 :initform "")
   (year :initarg :year
	 :accessor year
	 :type integer
	 :initform (local-time:timestamp-year (local-time:today)))
   (data :initarg :data
         :accessor book-data
         :documentation "Association list with movie data."))
  (:default-initargs
   :tags '("movie"))
  (:metaclass mold-object-class))

(defclass cuevana-movie (movie)
  ()
  (:metaclass mold-object-class))

(defun parse-cuevana-movie-url (url headers)
  (declare (ignorable headers))
  (when (not (str:starts-with? "https://cuevana2.io/pelicula" url))
    (return-from parse-cuevana-movie-url))
  (let* ((html (drakma:http-request url))
         (dom (lquery:$ (lquery:initialize html))))
    (make-instance 'cuevana-movie
                   :name "cuevana-obj1"
                   :id "aa"
                   :url url
                   :description (format nil "<div>~a</div>" (lquery-funcs:html  (aref (lquery:$ dom "div.mvic-desc") 0))))))

(pushnew 'parse-cuevana-movie-url *url-parsers*)

;; (parse-cuevana-movie-url "https://cuevana2.io/pelicula/break-every-chain/")


(defmethod create-object-view ((object cuevana-movie) clog-obj &rest args)
  (declare (ignore args))
  (create-child clog-obj (object-description object)))

;; https://open.spotify.com/track/0AjWPhrgNzEAO7FTnQIlEf?si=8f1b5191876f440a

(defclass sound-track (object)
  ()
  (:metaclass mold-object-class)
  (:default-initargs
   :tags '("sound" "music")))

(defclass spotify-track (sound-track)
  ()
  (:metaclass mold-object-class))

(defun parse-spotify-track-url (url headers)
  (declare (ignorable headers))
  (when (not (str:starts-with? "https://open.spotify.com/track" url))
    (return-from parse-spotify-track-url))
  (make-instance 'spotify-track
                 :url url
                 :id (subseq url (length "https://open.spotify.com/track/"))))

(pushnew 'parse-spotify-track-url *url-parsers*)

(defmethod create-object-view ((object spotify-track) clog-obj &rest args)
  (declare (ignore args))
  (create-child clog-obj
                (html
                  (:iframe :style "border-radius:12px"
                           :src (format nil "https://open.spotify.com/embed/track/~a?utm_source=generator" (object-id object))
                           :width "100%" :height "380" :frameBorder "0" :allowfullscreen t
                           :allow "autoplay; clipboard-write; encrypted-media; fullscreen; picture-in-picture"))))

(defparameter *google-api-key* nil)

(defclass google-api-object ()
  ((api-key :initarg :api-key :accessor api-key
            :initform *google-api-key*)))

;; <a target="_blank" href="https://calendar.google.com/event?action=TEMPLATE&amp;tmeid=NGU1N3ExczRiaGE4YTJoMXVwNWRtaGo1cDcgYnVubnltb3JyaXNvbjgzQG0&amp;tmsrc=bunnymorrison83%40gmail.com"><img border="0" src="https://www.google.com/calendar/images/ext/gc_button1_en.gif"></a>

;; https://calendar.google.com/calendar/htmlembed?src=addressbook%23contacts%40group.v.calendar.google.com&ctz=America%2FArgentina%2FBuenos_Aires

(defclass google-calendar-calendar (object google-api-object)
  ()
  (:default-initargs :category "calendar")
  (:metaclass mold-object-class))

;; <iframe src="https://calendar.google.com/calendar/htmlembed?src=addressbook%23contacts%40group.v.calendar.google.com&ctz=America%2FArgentina%2FBuenos_Aires" style="border: 0" width="800" height="600" frameborder="0" scrolling="no"></iframe>Q


;; <iframe  width="600"  height="450"  style="border:0"  loading="lazy"  allowfullscreen  referrerpolicy="no-referrer-when-downgrade"  src="https://www.google.com/maps/embed/v1/place?key=API_KEY    &q=Space+Needle,Seattle+WA"></iframe>

;; https://developers.google.com/maps/documentation/embed/get-started

(defclass map-object (object)
  ((latitude :initarg :latitude
             :accessor latitude)
   (longitude :initarg :longitude
              :accessor longitude))
  (:default-initargs
   :category "maps"
   :tags '("map"))
  (:icon . "core-map")
  (:metaclass mold-object-class))

(defclass google-map (map-object google-api-object)
  ()
  (:metaclass mold-object-class))

(defun parse-gmaps-url (url headers)
  (declare (ignorable headers))
  (when (not (str:starts-with? "https://www.google.com/maps/place" url))
    (return-from parse-gmaps-url))
  (make-instance 'google-map :url url))

(pushnew 'parse-gmaps-url *url-parsers*)

(defmethod create-object-view ((object google-map) clog-obj &rest args)
  (declare (ignore args))
  (create-child clog-obj
                (html
                  (:iframe :width "600" :height "450"
                           :style "border:0" :loading "lazy"
                           :allowfullscreen t :src (format nil "https://www.google.com/maps/embed/v1/place?key=~a&q=Space+Needle,Seattle+WA" (api-key object))))))

;; <iframe width="425" height="350" frameborder="0" scrolling="no" marginheight="0" marginwidth="0" src="https://www.openstreetmap.org/export/embed.html?bbox=-57.954913973808296%2C-34.920260017294346%2C-57.9528620839119%2C-34.919019616272855&amp;layer=mapnik" style="border: 1px solid black"></iframe><br/><small><a href="https://www.openstreetmap.org/#map=19/-34.91964/-57.95389">Ver mapa más grande</a></small>

(defclass osm-map (map-object)
  ()
  (:default-initargs :category "maps")
  (:metaclass mold-object-class))

;; https://www.openstreetmap.org/#map=19/-34.91964/-57.95389

(defun parse-osm-map-url (url headers)
  (declare (ignorable headers))
  (when (not (str:starts-with? "https://www.openstreetmap.org" url))
    (return-from parse-osm-map-url))
  (let ((pos (split-sequence:split-sequence #\/ (car (last (multiple-value-list (quri:parse-uri url)))))))
    (when (and (second pos) (third pos))
      (make-instance 'osm-map
                     :url url
                     :latitude (second pos)
                     :longitude (third pos)))))

(pushnew 'parse-osm-map-url *url-parsers*)

(defmethod create-object-view ((object osm-map) clog-obj &rest args)
  (declare (ignore args))
  ;; TODO: create a bbox url paramter from latitude+longitude
  (create-child clog-obj
                (html (:iframe :width "425" :height "350"
                               :frameborder "0" :scrolling "no" :marginheight "0" :marginwidth "0"
                               :src (format nil "https://www.openstreetmap.org/export/embed.html?bbox=-57.954913973808296%2C-34.920260017294346%2C-57.9528620839119%2C-34.919019616272855&amp;layer=mapnik" :style "border: 1px solid black")))))

;; TODO:

(defclass tweet (object)
  ()
  (:icon . "brands-twitter")
  (:metaclass mold-object-class))

(defun parse-tweet-id (url)
  (let ((status-pos (search "status/" url)))
    (subseq url (+ status-pos (length "status/")))))

;;(parse-tweet-id "https://twitter.com/tatut/status/1525464619548086278")

(defun parse-tweet-url (url headers)
  (declare (ignorable headers))
  (when (not (str:starts-with-p "https://twitter.com" url))
    (return-from parse-tweet-url))
  (make-instance 'tweet
                 :id (parse-tweet-id url)
                 :url url))

(pushnew 'parse-tweet-url *url-parsers*)

;;(parse-tweet-url "https://twitter.com/tatut/status/1525464619548086278")

(defmethod create-object-view ((twit tweet) clog-obj &rest args)
  (declare (ignore args))
  #+nil(create-child clog-obj
                     (format nil "<blockquote class=\"twitter-tweet\"><a href=\"~a\"></a></blockquote> <script async src=\"https://platform.twitter.com/widgets.js\" charset=\"utf-8\"></script>" (object-url twit)))
  (create-child clog-obj
                (alexandria:assoc-value (json:decode-json-from-source (drakma:http-request "https://publish.twitter.com/oembed" :parameters `(("url" . ,(object-url twit))) :want-stream t)) :html))
  )

(defclass twitter-account (object)
  ()
  (:metaclass mold-object-class))

(defclass reddit-channel (object)
  ()
  (:metaclass mold-object-class))

;; https://openweathermap.org/
(defvar *owm-api-key* nil)
(defclass owm-weather (object)
  ;; TODO: the slots should all be configurable properties from ui
  ((api-key :initarg :api-key
            :accessor api-key
            :initform *owm-api-key*)
   (lat :initarg :lat
        :accessor lat
        :initform "34.9205")
   (lon :initarg :lon
        :accessor lon
        :initform "57.9536"))
  (:metaclass mold-object-class))

(defmethod fetch-data ((object owm-weather))
  (json:decode-json-from-source
   (drakma:http-request
    (format nil "https://api.openweathermap.org/data/2.5/onecall?lat=~a&lon=~a&appid=~a" (lat object) (lon object) (api-key object))
    :want-stream t)))

(defun parse-owm-url (url headers)
  (declare (ignorable headers))
  (when (not (search "openweathermap.org" url))
    (return-from parse-owm-url))
  (make-instance 'owm-weather :url url))

(pushnew 'parse-owm-url *url-parsers*)

(defmethod create-object-view ((object owm-weather) clog-obj &rest args)
  (declare (ignore args))
  (let ((weather-data (fetch-data object)))
    (create-child clog-obj
                  (with-output-to-string (html)
                    (write-string "<pre>" html)
                    (json:encode-json weather-data html)
                    (write-string "</pre>" html)))))

;; https://www.aakashweb.com/articles/google-news-rss-feed-url/
;; https://news.google.com/rss
;; https://news.google.com/rss/search?q=<KEYWORD>

(defclass clock-object (object)
  ()
  (:metaclass mold-object-class))

(defmethod create-object-view ((object clock-object) clog-obj &rest args)
  (declare (ignore args))
  (create-child clog-obj
                "<iframe src=\"https://free.timeanddate.com/clock/i8bthbm0/n541/szw80/szh80/hocf00/hbw0/hfcc00/cf100/hnca32/fas20/facfff/fdi86/mqcfff/mqs2/mql3/mqw4/mqd70/mhcfff/mhs2/mhl3/mhw4/mhd70/mmv0/hhcfff/hhs2/hmcfff/hms2/hsv0\" frameborder=\"0\" width=\"80\" height=\"80\"></iframe>"))

(defclass book (object)
  ((portrait-image :initarg :portrait-image :accessor portrait-image))
  (:icon . "core-book")
  (:metaclass mold-object-class))

(defclass google-book (book)
  ()
  (:default-initargs :category "books")
  (:metaclass mold-object-class))

;; https://www.googleapis.com/books/v1/volumes/zyTCAlFPjgYC?key=yourAPIKey
(defun read-google-book-data (google-book)
  (let ((book-data (json:decode-json-from-source
                    (drakma:http-request (format nil "https://www.googleapis.com/books/v1/volumes/~a" (object-id google-book))
                                         :parameters (list (cons "key" *google-api-key*))
                                         :want-stream t))))
    (setf (object-name google-book) (accesses book-data :volume-info :title))
    (setf (object-description google-book)
          (accesses book-data :volume-info :description))
    (setf (portrait-image google-book)
          (accesses book-data :volume-info :image-links :thumbnail))
    google-book))

(defun parse-google-book-url (url headers)
  (declare (ignorable headers))
  (when (not (str:starts-with? "https://books.google.com" url))
    (return-from parse-google-book-url))
  ;; TODO: use google api to retrieve book info here ...
  (let* ((uri (quri:uri url))
         (book-id (access:access (quri:uri-query-params uri) "id")))
    (read-google-book-data (make-instance 'google-book :id book-id :url url))))

(pushnew 'parse-google-book-url *url-parsers*)

(defmethod create-object-view ((book book) clog-obj &rest args)
  (declare (ignore args))
  (create-child clog-obj
                (html (:div
                       (:h2 (str (object-name book)))
                       (:img :src (portrait-image book))
                       (:p (who:esc (object-description book)))))))

(defclass chess-game (object)
  ((fen :initarg :fen)
   (players :initarg :players :accessor chess-players)
   (date :initarg :date :accessor chess-game-date))
  (:default-initargs
   :icon "fa fa-chess")
  (:icon . "fa fa-chess")
  (:metaclass mold-object-class))

(defclass lichess-game (chess-game)
  ()
  (:metaclass mold-object-class))

(defun parse-lichess-url (url headers)
  (declare (ignore headers))
  (when (not (str:starts-with? "https://lichess.org" url))
    (return-from parse-lichess-url))
  (make-instance 'lichess-game
                 :url url
                 :id (subseq url (length "https://lichess.org/")
                             (or (position #\# url)
                                 (position #\? url)))))

(pushnew 'parse-lichess-url *url-parsers*)

(defmethod create-object-view ((chess-game lichess-game) clog-obj &rest args)
  (declare (ignore args))
  (create-child clog-obj
                (html (:iframe :src (format nil "https://lichess.org/embed/~a?theme=auto&bg=auto" (object-id chess-game))
                               :width 600 :height 397 :frameborder  0))))

;; <iframe src="https://lichess.org/embed/MPJcy1JW?theme=auto&bg=auto" width=600 height=397 frameborder=0></iframe>

(defclass reddit-comment (object)
  ((info :initarg :info :accessor reddit-info))
  (:icon . "brands-reddit")
  (:metaclass mold-object-class))

;; https://www.reddit.com/api/info.json?id=t1_d99c0y1

(defun parse-reddit-comment-url (url headers)
  (declare (ignore headers))
  (when (and (str:starts-with? "https://www.reddit.com" url)
             (search "/comments/" url))
    (let* ((from (+ (search "/comments/" url) (length "/comments/")))
           (cid (subseq url from (position #\/ url :start from))))
      (make-instance 'reddit-comment
                     :id cid
                     :url url
                     :info (json:decode-json-from-source
                            (drakma:http-request (format nil "https://www.reddit.com/api/info.json?id=t1_~a" cid) :want-stream t))))))

(pushnew 'parse-reddit-comment-url *url-parsers*)

(defmethod create-object-view ((reddit-comment reddit-comment) clog-obj &rest args)
  (declare (ignore args)))

(defclass document (object)
  ((content :accessor document-content :initarg :content)
   (format :accessor document-format :initarg :format)
   (mime-type :initarg :mime-type
	      :accessor mime-type))
  (:icon . "fa fa-file")
  (:metaclass mold-object-class))

(defun parse-document-url (url headers)
  (declare (ignorable headers))
  (let ((docname (arrows:->> (quri:uri url)
			     (quri:uri-path)
			     (str:split #\/)
			     (last)
			     (car))))
    (cond
      ((str:ends-with? ".md" url)
       (make-instance 'document
		      :name docname
                      :url url
                      :content (drakma:http-request url)
                      :format :markdown))
      ((str:ends-with? ".odp" url)
       (make-instance 'document
		      :name docname
		      :url url
		      :content nil ;; don't download
		      :format :odp))
      ((str:ends-with? ".odt" url)
       (make-instance 'document
		      :name docname
		      :url url
		      :content nil ;; don't download
		      :format :odt))
      ((str:ends-with? ".ods" url)
       (make-instance 'document
		      :name docname
		      :url url
		      :content nil ;; don't download
		      :format :odp))
      ((str:ends-with? ".pdf" url)
       (make-instance 'document
		      :name docname
		      :url url
		      :content nil ;; don't download
		      :format :pdf)))))

(pushnew 'parse-document-url *url-parsers*)

(defmethod create-object-view ((doc document) clog-obj &rest args)
  (declare (ignore args))

  (case (document-format doc)
    (:markdown
     (create-child clog-obj
                   (with-output-to-string (s)
                     (write-string "<div>" s)
                     (cl-markdown:markdown (document-content doc) :stream s)
                     (write-string "</div>" s))))
    (t
     (create-div clog-obj :content
                 (newline-to-br
                  (who:escape-string (document-content doc)))))))

(defmethod create-document-editing-view ((doc document) clog-obj)
  (let ((text-area
          (create-text-area clog-obj
                            :value (document-content doc)
                            :style "height:100%;width:100%;")))
    (set-on-change text-area (lambda* (_)
                               (setf (document-content doc) (property text-area "value"))))))

(defmethod object-view-types append ((doc document) (module (eql :desktop)))
  '(create-document-editing-view))

(defmethod mold-desktop/clog-gui::halos-for-object append ((doc document) context)
  (flet ((set-doc-editing-view (object clog-obj)
           (with-slots (mold-desktop/clog-gui::object-view) clog-obj
             (setf (text (mold-desktop/clog-gui::content clog-obj)) "")
             (setf mold-desktop/clog-gui::object-view (create-document-editing-view doc (mold-desktop/clog-gui::content clog-obj))))))
    `((:edit-document
       :label "Edit document" :icon "fa-solid fa-file-pen"
       :position :left-center-top
       :action ,#'set-doc-editing-view))))

#+example
(show (make-instance 'document
                     :name "Example doc"
                     :format :markdown
                     :content "## Hello world"))
