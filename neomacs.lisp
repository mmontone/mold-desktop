;; TODO:
;; - Correct buffer styling. Styles are not inherited precisely by Neomacs buffer elements.
;; - FORWARD-DELETE command doesn't work.

(in-package :mold-desktop)

(defun init-neomacs (body)
  (load-script (html-document body) "neomacs/neomacs.js")
  (neomacs-js::start-connection 'clog-neomacs-connection)
  (neomacs-js::start-command-loop))

(pushnew 'init-neomacs *init-functions*)

(defun create-neomacs-buffer (modes)
  (let ((id (neomacs-js::generate-buffer-id)))
    (neomacs-js::get-buffer-create
     (format nil "*scratch <~a>*" id)
     :id (princ-to-string id)
     :mode modes
     :revert t ;; revert to assign keymap
     )))

(defclass neomacs-buffer (object)
  ((buffer :initform (create-neomacs-buffer '(neomacs-js::text-mode))
           :accessor neomacs-buffer
           :initarg :buffer)
   (spell-check :initarg :spell-check
                :accessor spell-check
                :initform nil))
  (:metaclass mold-object-class)
  (:icon . "fa-regular fa-file-lines"))

(defclass neomacs-lisp-buffer (neomacs-buffer)
  ()
  (:metaclass mold-object-class)
  (:icon . "fa-regular fa-file-lines")
  (:default-initargs
   :buffer (create-neomacs-buffer '(neomacs-js::lisp-mode))))

(defclass neomacs-html-buffer (neomacs-buffer)
  ()
  (:metaclass mold-object-class)
  (:icon . "fa-regular fa-file-lines")
  (:default-initargs
   :buffer
   (let ((id (neomacs-js::generate-buffer-id)))
     (neomacs-js::get-buffer-create
      (format nil "*html <~a>*" id)
      :id (princ-to-string id)
      :mode '(neomacs-js::html-doc-mode)
      :revert nil ;; revert to assign keymap
      :file-path (pathname (cl-fad:open-temporary))))
   :spell-check t))

(defmethod object-title ((object neomacs-buffer))
  (neomacs-js::name (neomacs-buffer object)))

(defun call-sync (obj method &rest args)
  "Call METHOD on CLOG OBJ passing ARGS. Return the result."
  (js-query obj (format nil "~a.~a(~{~a~^, ~})" (script-id obj) method args)))

(defmethod create-object-view ((object neomacs-buffer) clog-obj &rest args)
  (declare (ignore args))
  ;; Neomacs needs a parent div and a body div to work properly
  (let* ((buffer-id (neomacs-js::id (neomacs-buffer object)))
         (parent (clog:create-div clog-obj :class "neomacs"
                                           :style "width:100%; height:100%;"))
         (body (clog:create-element parent "div"
                                    :contenteditable "true"
                                    :neomacs-identifier buffer-id
                                    :spellcheck (if (spell-check object) "true" "false")
                                    :style "width:100%; height:100%;overflow:scroll;caret-color: transparent;")))
    (clog::bind-event-script body "keydown"
                             (format nil "ws.send('neomacs:' + Neomacs.encodeKeyEvent(e, '~a')); return false;" buffer-id))
    (clog::bind-event-script body "focus"
                             (format nil "ws.send('neomacs:' + JSON.stringify({inputEvent: {type: 'focus'}, buffer: '~a'}))" buffer-id))
    ;; Initialize styles
    (neomacs-js::on-buffer-dom-ready (neomacs-buffer object))
    parent))

(defmethod create-object-view ((object neomacs-lisp-buffer) clog-obj &rest args)
  (declare (ignore args))
  (let ((div (clog:create-element clog-obj "div" :style "width:100%;height:100%;")))
    (create-buttons-bar
     div
     '((:label "Evaluate" :description "Evaluate last expression"
        :icon "fa fa-play" :command neomacs-eval-last-expression)
       (:label "Inspect" :description "Eval and inspect last expression"
        :icon "fa fa-search" :command neomacs-inspect-last-expression)
       (:label "Undo" :description "Undo"
        :icon "fa fa-undo" :command neomacs-undo)))
    (call-next-method object div)
    div))

(defmethod create-object-view ((object neomacs-html-buffer) clog-obj &rest args)
  (declare (ignore args))
  (let ((div (clog:create-element clog-obj "div" :style "width:100%;height:100%;")))
    (create-buttons-bar
     div
     '((:label "Increase heading" :description "Increase heading"
        :icon "fa fa-header" :command neomacs-increase-heading)
       (:label "Decrease heading" :description "Decrease heading"
        :icon "fa fa-header" :command neomacs-decrease-heading)
       (:label "Bold" :description "Bold"
        :icon "fa fa-bold" :command neomacs-insert-bold)
       (:label "Italic" :description "Italic"
        :icon "fa fa-italic" :command neomacs-insert-italic)
       (:label "Unordered list" :description "Unordered list"
        :icon "fa fa-list" :command neomacs-insert-unordered-list)
       (:label "Undo" :description "Undo"
        :icon "fa fa-undo" :command neomacs-undo)))
    (call-next-method object div)
    div))

(neomacs-js::defmodel html-doc-mold-element (neomacs-js::element)
    ((object :initarg :object
             :accessor element-object
             :documentation "The Mold object being held in this neomacs node."))
  (:documentation "Neomacs node that holds a Mold object")
  (:default-initargs
   :tag-name "div"))

(defun make-neomacs-element (element-class &rest attributes &key &allow-other-keys)
  (sera:lret ((element (make-instance element-class)))
    (iter:iter (iter:for (k v) on attributes by #'cddr)
      (if (eql k :children)
          (dolist (c v)
            (when (stringp c)
              (setq c (make-instance 'text-node :text c)))
            (neomacs-js::append-child element c))
          (setf (neomacs-js::attribute element (string-downcase (symbol-name k))) v)))))


(defcommand insert-into-document ((buffer neomacs-html-buffer "The Neomacs buffer")
                                  (object object "The object to insert"))
  "Insert OBJECT into Neomacs BUFFER."
  (let* ((marker (neomacs-js::focus))
         (node-id (string (gensym "MOLD-ELEMENT-")))
         (node (make-neomacs-element 'html-doc-mold-element :id node-id))
         (clog-element (clog:create-element *app-body* "div" :auto-place nil :class "mold-element")))
    (create-document-view object clog-element)
    (setf (element-object node) object)
    ;; Insert the neomacs node
    (neomacs-js::insert-nodes marker node)
    ;; Append the view as child of the neomacs node
    (clog:js-execute *app-body*
                     (ps:ps (let ((node (ps:chain document (get-element-by-id (ps:lisp node-id))))
                                  (view (ps:@ clog (ps:lisp (clog:html-id clog-element)))))
                              (ps:chain node (append-child view)))))

    (setf (neomacs-js::pos marker) (neomacs-js::end-pos node))))

(pushnew 'neomacs-message-handler clog-connection::*message-handlers*)

(defun neomacs-message-handler (ml connection-id)
  (declare (ignore connection-id))
  (when (string= (first ml) "neomacs")
    (neomacs-js::on-message (second ml))
    t))

(defclass clog-neomacs-connection (neomacs-js::neomacs-connection)
  ())

(defmethod neomacs-js::conn-start ((conn clog-neomacs-connection)))
(defmethod neomacs-js::conn-stop ((conn clog-neomacs-connection)))

(defmethod neomacs-js::conn-send (message (conn clog-neomacs-connection))
  (clog:js-execute *app-body* message))

(defmethod neomacs-js::conn-send-sync (message (conn clog-neomacs-connection))
  ;; TOO much of a hack!!
  ;; Fix synchronous javascript in neomacs-js
  (let ((response (clog:js-query *app-body* message)))
    (let ((values (split-sequence:split-sequence #\, response)))
      (mapcar #'read-from-string values))))

(defcommand neomacs-eval-last-expression ()
  (let* ((marker (neomacs-js::focus))
         (*package* (neomacs-js::current-package marker))
         (result (eval (neomacs-js::node-to-sexp (neomacs-js::last-expression (neomacs-js::pos marker))))))
    (command-bar-message (prin1-to-string result))))

(defcommand neomacs-inspect-last-expression ()
  (let* ((marker (neomacs-js::focus))
         (*package* (neomacs-js::current-package marker)))
    (eval (neomacs-js::node-to-sexp (neomacs-js::last-expression (neomacs-js::pos marker))))))

(defcommand neomacs-increase-heading ()
  (neomacs-js::run-command 'neomacs-js::increase-heading))

(defcommand neomacs-decrease-heading ()
  (neomacs-js::run-command 'neomacs-js::decrease-heading))

(defcommand neomacs-undo ()
  (neomacs-js::run-command 'neomacs-js::undo-command))

(defcommand neomacs-insert-bold ()
  (neomacs-js::run-command 'neomacs-js::insert-bold))

(defcommand neomacs-insert-italic ()
  (neomacs-js::run-command 'neomacs-js::insert-italic))

(defcommand neomacs-insert-unordered-list ()
  (neomacs-js::run-command 'neomacs-js::insert-unordered-list))
