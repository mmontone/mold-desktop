;; Copyright (C) 2022 Mariano Montone - All Rights Reserved
;; You may use, distribute and modify this code under the terms of the
;; GNU Affero General Public License

(in-package :mold-desktop)

(defparameter *task-model*
  (make-instance 'model-object
		 :name 'task
		 :attributes (list
			      (make-instance 'model-attribute
					     :name 'subject
					     :type 'string)
			      (make-instance 'model-attribute
					     :name 'description
					     :type 'text)
			      (make-instance 'model-attribute
					     :name 'done
					     :type 'boolean))))

(defparameter *my-tasks*
  (make-instance 'model-collection
		 :model-type *task-model*
		 :members
		 (list
		  (make-instance 'model-instance
				 :model *task-model*
				 :attribute-values (list (cons 'subject "Task 1")
							 (cons 'description "Do task 1")
							 (cons 'done nil)))
		  (make-instance 'model-instance
				 :model *task-model*
				 :attribute-values (list (cons 'subject "Task 2")
							 (cons 'description "Task 2")
							 (cons 'done t))))))


#+example
(open-object-in-window *my-tasks* *body*)

#+example
(with-open-window wnd (*body* :title "Model instance editor")
  (create-object-editor-view (first (members *my-tasks*)) wnd))

#+example
(with-open-window wnd (*body* :title "Model")
  (create-object-editor-view *task-model* wnd))

#+example
(with-open-window wnd (*body* :title "Test")
  (create-object-editor-view (first *objects*) wnd))
