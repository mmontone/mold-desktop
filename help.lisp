;; Copyright (C) 2022 Mariano Montone - All Rights Reserved
;; You may use, distribute and modify this code under the terms of the
;; GNU Affero General Public License

(in-package :mold-desktop)

(defgeneric object-help (object)
  (:documentation "Get help for OBJECT.
Should return an object that when displayed shows documentation about OBJECT.
If list of strings is returned, then it is used as a path to a chapter in MOLD DESKTOP BOOK."))

(defgeneric class-help (class)
  (:documentation "Return DocNode for CLASS."))

(defmethod class-help (class)
  nil)

(defmethod object-help (object)
  (or (class-help (class-name (class-of object)))
      (with-accessors ((title object-title)
		       (description object-description))
          object
        (make-instance 'text-doc-node
		       :name title
		       :contents (or description "No description")))))

(defcommand (describe--object :icon "fa-solid fa-question")
    ((object object "The object to describe"))
  (object-help object))

(defmethod class-help ((class (eql 'object)))
  (build-doc-node-from-file (asdf:system-relative-pathname :mold-desktop "book/020-Usage/020-Objects.md")))

(defmethod class-help ((class (eql 'collection)))
  (build-doc-node-from-file (asdf:system-relative-pathname :mold-desktop "book/020-Usage/030-Collections.md")))
