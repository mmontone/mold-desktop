;; Copyright (C) 2022 Mariano Montone - All Rights Reserved
;; You may use, distribute and modify this code under the terms of the
;; GNU Affero General Public License

(require :cl-forms)

(in-package :mold-desktop)

;; Implement utilities for specifying forms in clog/mold-desktop here ..

(defvar *form-field-editor* (make-hash-table :weakness :key)
  "Map between FORMS:FORM-FIELD and the CLOG element created as its input.")

(defgeneric build-clog-form-field (form-field clog-obj &rest args)
  (:documentation "Build a CLOG editor for FORM-FIELD."))

(defmethod build-clog-form-field :around ((form-field forms:form-field) clog-obj &rest args)
  (declare (ignore args))
  (let ((clog-editor (call-next-method)))
    (setf (gethash form-field *form-field-editor*) clog-editor)))

(defmethod build-clog-form-field ((form-field forms:string-form-field) clog-obj &rest args)
  (declare (ignore args))
  (clog:create-child clog-obj (html (:input :class "w3-input w3-border"))))

(defmethod build-clog-form-field ((form-field forms:boolean-form-field) clog-obj &rest args)
  (declare (ignore args))
  (clog:create-child clog-obj (html (:input :type "checkbox" :class "w3-check"))))
  
(defmethod build-clog-form-field ((form-field forms:integer-form-field) clog-obj &rest args)
  (declare (ignore args))
  (clog:create-child clog-obj (html (:input :type "number" :class "w3-input w3-border"))))

(defmethod build-clog-form-field ((form-field forms:text-form-field) clog-obj &rest args)
  (declare (ignore args))
  (clog:create-child clog-obj (html (:textarea :class "w3-input w3-border"))))

(defun build-clog-form (form clog-obj &rest options)
  (let* ((clog-form (create-child clog-obj (html (:form :class "w3-form"))))
	 (table (create-table clog-form :class "w3-table")))
    (dolist (form-field (mapcar 'cdr (forms:form-fields form)))
      (let ((tr (create-table-row table)))
	(-> (create-table-column tr)
	   (create-child (html (:label (who:str (string
						 (or (forms:field-label form-field)
						     (forms::field-name form-field))))))))
	(let ((td (create-table-column tr)))
	  (build-clog-form-field form-field td))))))

;; example

(forms:defform test-form ()
  ((string 'string)
   (text 'forms::text)
   (boolean 'boolean)
   (integer 'integer)))

#+example
(with-open-window wnd (*body* :title "Form test")
  (build-clog-form (forms:find-form 'test-form) wnd))
