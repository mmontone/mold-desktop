(in-package :mold-desktop)

(defclass view (standard-generic-function)
  ((label :initarg :label
          :accessor view-label
          :initform nil
          :type (or string null)
          :documentation "Label used in UI.")
   (icon :initarg :icon
         :accessor view-icon
         :initform nil
         :type (or string null)
         :documentation "The icon for the view."))
  (:metaclass closer-mop:funcallable-standard-class)
  (:documentation "Views for mold objects"))

;; default accessors for view functions that are not of type view:
;; ideally, views should all be defined to be instances of view class.

(defun default-view-label (f)
  (let ((gf-name (princ-to-string (c2mop:generic-function-name f))))
    (when (str:starts-with? "CREATE-" gf-name)
      (setf gf-name (subseq gf-name (length "CREATE-"))))
    (str:sentence-case gf-name)))

(defmethod view-label ((f generic-function))
  (default-view-label f))

(defvar *default-view-icon* "fa fa-eye")

(defmethod view-icon ((f generic-function))
  *default-view-icon*)

(defmacro defview (name lambda-list &rest options)
  "Define a view.
`defview' syntax is similar to `defgeneric'"
  (let* ((standard-generic-function-options '(:documentation :method :metaclass))
         (generic-function-options (remove-if-not (lambda (opt)
                                                    (member opt standard-generic-function-options))
                                                  options :key #'car))
         (view-options (remove-if (lambda (opt)
                                    (member opt standard-generic-function-options))
                                  options :key #'car))
         (view-class (or (anaphora:aand (find :view-class options :key #'car)
                                        (second anaphora:it))
                         'view))
         (slots (closer-mop:class-slots (find-class view-class)))
         (object (first lambda-list))
         (clog-obj (second lambda-list)))
    `(progn
       ;;(export-always ',name (symbol-package ',name))
       ;; declare a generic function that creates the view
       (defgeneric ,name ,lambda-list
         ,@generic-function-options
         (:generic-function-class ,view-class))
       ;; a method that observes :changed events on the object (first argument)
       (defmethod ,name :around ,lambda-list
         (when (typep ,object 'event-emitter:event-emitter)
           (event-emitter:on :changed ,object
                             (lambda (&rest args)
                               (setf (clog:text ,clog-obj) "")
                               (call-next-method))))
         (call-next-method))
       ,@(loop for opt in (mapcar #'car view-options)
               for val in (mapcar #'second view-options)
               for slot := (or (find-if (lambda (initargs) (member opt  initargs))
                                        slots
                                        :key #'closer-mop:slot-definition-initargs)
                               (error "Invalid view option: ~s" opt))
               collect `(setf (slot-value (symbol-function ',name) ',(closer-mop:slot-definition-name slot)) ,val))
       (symbol-function ',name))))

;; needed for defview macro expansion
(c2mop:finalize-inheritance (find-class 'view))

(defgeneric object-view-types (object mold-module)
  (:documentation "Return the view functions applyable to OBJECT.")
  (:method-combination append)
  (:method append (object mold-module)
    '()))

(defgeneric object-default-view (object)
  (:documentation "Default view for OBJECT. Default is CREATE-OBJECT-VIEW.")
  (:method (object)
    'create-object-view))
