(in-package :mold-desktop)

(defcommand (link-objects :icon "fa fa-link")
    ((subject object "The subject to link from")
     (object object "The object to link to")
     (predicate string "The predicate"))
  "Add link between objects"
  (naive-triple-store:put-triple
   (princ-to-string (object-id subject))
   predicate object *triple-store*))

(defcommand (tag-object :icon "fa fa-tag")	     
    ((object object "The object to tag")
     (tag string "The tag"))
  "Add tag to object"
  (naive-triple-store:put-triple (object-id object) "tag" tag *triple-store*))
