;; Copyright (C) 2022 Mariano Montone - All Rights Reserved
;; You may use, distribute and modify this code under the terms of the
;; GNU Affero General Public License

(in-package :mold-desktop)

;;(defun init-table-component (body))

(defclass table-object (object)
  ((collection :initarg :collection
               :accessor collection)
   (columns :initarg :columns
            :accessor table-columns))
  (:metaclass mold-object-class))

;;;; ** Tables API

(defgeneric table-rows (object)
  (:documentation "Returns the rows for OBJECT.
Should be specialized for different types of objects."))

(defgeneric table-columns (object)
  (:documentation "Returns the columns for OBJECT.
Should be specialized for different types of objects."))

;;;; ** Default implementations

(defmethod table-cell-value (row column)
  (acc:access row column))

(defmethod table-rows ((object table-object))
  (members (collection object)))

(defmethod table-rows ((collection collection))
  (members collection))

(defmethod table-columns ((collection collection))
  (if (eq (elements-type collection) t)
      ;; Use first item of the collection as type
      (if (not (zerop (length (members collection))))
          (table-columns (first (members collection)))
          ;; The collection has no objects
          ;; Assume OBJECT as type
          (table-columns (make-instance 'object)))
      ;; else
      (table-columns (make-instance (elements-type collection)))))

(defun object-short-description (object &optional (length 80))
  (str:shorten length (object-description object)))

(defmethod table-columns ((object object))
  '(object-id object-title object-url object-short-description created-at))

;; Slow, but with events:

(defview create-table-view (object clog-obj &rest args)
  (:documentation "Create a table view for OBJECT.")
  (:label "Table view")
  (:icon "fa fa-table"))

(defmethod create-table-view (table clog-obj &rest args &key (lazy-load t))
  "TABLE can be any object respecting the table interface."
  (declare (ignore args))
  (let* ((html-table
           (create-table clog-obj :class "w3-table w3-striped w3-bordered w3-small"))
         (row (create-table-row html-table :class "w3-green")))
    ;; Heading
    (dolist (column (table-columns table))
      (create-table-column row :content (str:sentence-case (princ-to-string column))))
    ;; Rows
    (dolist (row (table-rows table))
      (let ((tr (create-table-row html-table)))
        (dolist (column (table-columns table))
          (let ((colval (table-cell-value row column)))
            (create-table-column tr :content (princ-to-string colval))))
        (mold-desktop/clog-gui::attach-presentation-handlers row tr)))
    
    html-table))

;; Fast, but without events:

(defun render-table-view (table clog-obj &rest args)
  (declare (ignore args))
  (create-child clog-obj
                (html
                  (:table :class "w3-table w3-striped w3-bordered w3-small"
                          ;; Headings
                          (:tr :class "w3-green"
                               (dolist (column (table-columns table))
                                 (who:htm
                                  (:td (who:str (princ-to-string column))))))
                          (dolist (row (table-rows table))
                            (who:htm
                             (:tr
                              (dolist (column (table-columns table))
                                (let ((colval (table-cell-value row column)))
                                  (who:htm (:td (who:str (princ-to-string colval)))))))))))))

(defmethod create-object-view ((object table-object) clog-obj &rest args)
  (apply #'create-table-view object clog-obj args))

#+example
(open-object-in-window (make-instance 'table-object
                                      :collection (make-instance 'collection
                                                                 :members *objects*)
                                      :columns '(id name url created-at))
                       *body*)

(defmethod object-view-types append ((collection collection) (mold-module (eql :table)))
  '(create-table-view))

(pushnew :table *mold-modules*)
