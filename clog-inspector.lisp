;; Copyright (C) 2022 Mariano Montone - All Rights Reserved
;; You may use, distribute and modify this code under the terms of the
;; GNU General Public License

(defpackage :clog-inspector
  (:use :cl :clog :clog-gui)
  (:shadow :inspect)
  (:export :inspect))

(in-package :clog-inspector)

;; html

(defvar *html*)
(defmacro html* (&body body)
  `(who:with-html-output (*html*)
     ,@body))
(defmacro html (&body body)
  `(who:with-html-output-to-string (*html*)
     ,@body))

(defun newline-to-br (string)
  (cl-ppcre:regex-replace-all
   (ppcre:create-scanner '(:sequence (:greedy-repetition 0 nil (:alternation #\space #\tab #\return))
                           #\newline))
   string
   "<br/>"))

(defun open-inspector-window (clog-obj &rest args)
  (let ((wnd (apply #'clog-gui:create-gui-window clog-obj args)))
    (clog-gui:window-content wnd)))

(defgeneric create-object-inspector (object clog-obj &rest args))

(defun inspect (object &optional clog-obj)
  (let ((wnd (open-inspector-window clog-obj :title (princ-to-string (type-of object)))))
    (create-object-inspector object wnd)))

(defmethod create-object-inspector (object clog-obj &rest args)
  (declare (ignore args))
  (clog:create-div clog-obj :content
                   (newline-to-br
                    (who:escape-string
                     (with-output-to-string (s)
                       (describe object s))))))

(defmethod create-object-inspector ((object integer) clog-obj &rest args)
  (call-next-method))
