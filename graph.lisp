;; Graphs visualization
;; TODO: use for visualizing linked objects in a graph. Open object when node clicked.

(in-package :mold-desktop)

(pushnew 'init-graph *init-functions*)

(defun init-graph (body)
  (add-script (html-document body) "https://unpkg.com/vis-network/standalone/umd/vis-network.min.js"))

(defvar *default-vis-graph-options*
  (sera:dict "nodes" (sera:dict "shape" "dot"
				"size" 12)))

(defclass vis-graph-view (clog-element)
  ((options :initarg :options
            :accessor graph-options
            :initform (make-hash-table))))

(defclass vis-graph (object)
  ((js-handle :initarg :js-handle
              :accessor js-handle)
   (nodes :initarg :nodes
          :accessor graph-nodes
          :initform nil
          :documentation "An alist of (id . label). Or a property list with elements with keys :id, :label, etc.")
   (edges :initarg :edges
          :accessor graph-edges
          :initform nil
          :documentation "An alist of (from-id . to-id). Or a property list with elements with keys :from, :to, :label, etc options.")
   (options :initarg :options
            :accessor graph-options
            :initform *default-vis-graph-options*)
   (node-selection-action :initarg :on-node-select
                          :accessor node-selection-action
                          :initform nil))
  (:metaclass mold-object-class)
  (:icon . "fa-solid fa-diagram-project"))

(defun encode-nodes-array (nodes &optional (stream json:*json-output*))
  (let ((json:*json-output* stream))
    (json:with-array ()
      (dolist (node nodes)
        (json:as-array-member ()
          (cond
            ((and (consp node)
                  (not (alex:proper-list-p node)))
             (json:with-object ()
               (json:encode-object-member :id (car node))
               (json:encode-object-member :label (cdr node))))
            ((alex:proper-list-p node)
             (json:encode-json-plist node))
            (t (error "Invalid node spec: ~s" node))))))))

(defun encode-edges-array (edges &optional (stream json:*json-output*))
  (let ((json:*json-output* stream))
    (json:with-array ()
      (dolist (edge edges)
        (json:as-array-member ()
          (cond
            ((and (consp edge)
                  (not (alex:proper-list-p edge)))
             (json:with-object ()
               (json:encode-object-member :from (car edge))
               (json:encode-object-member :to (cdr edge))))
            ((alex:proper-list-p edge)
             (json:encode-json-plist edge))
            (t (error "Invalid edge spec: ~s" edge))))))))

(defmethod create-object-view ((graph vis-graph) clog-obj &rest args)
  (declare (ignore args))

  (let ((graph-view (clog:create-div clog-obj)))
    (setf (clog:style graph-view "width") "100%")
    (setf (clog:style graph-view "height") "100%")
    ;;(setf (clog:style graph-view "border") "1px solid gray")
    (setf (js-handle graph)
          (format nil "window.graph~a" (html-id graph-view)))
    (js-execute graph-view
                (with-output-to-string (s)
                  (format s "~a = new vis.Network(document.getElementById('~a'),"
                          (js-handle graph)
                          (html-id graph-view))
                  (write-string "{nodes: new vis.DataSet(" s)
                  (encode-nodes-array (graph-nodes graph) s)
                  (write-string "), edges: new vis.DataSet(" s)
                  (encode-edges-array (graph-edges graph) s)
                  (write-string ")}," s)
                  (json:encode-json (graph-options graph) s)
                  (write-string ");" s)
                  (when (node-selection-action graph)
                    (graph-set-on-double-click graph s))))
    graph-view))

#+example
(show (make-instance 'vis-graph
                     :nodes '((1 . "Node 1")
                              (2 . "Node 2")
                              (3 . "Node 3")
                              (4 . "Node 4")
                              (5 . "Node 5"))
                     :edges '((1 . 3)
                              (1 . 2)
                              (2 . 4)
                              (2 . 5)
                              (3 . 3))))

#+example
(show (make-instance 'vis-graph
                     :nodes '((1 . "Node 1")
                              (2 . "Node 2")
                              (3 . "Node 3")
                              (4 . "Node 4")
                              (5 . "Node 5"))
                     :edges '((:from 1 :to 3 :label "1 to 3")
                              (:from 1 :to 2 :label "1 to 2")
                              (:from 2 :to 4 :label "2 to 4")
                              (:from 2 :to 5 :label "2 to 5")
                              (:from 3 :to 3 :label "3 to 3"))))

(defun graph-set-on-double-click (graph stream)
  "Handle double click events for GRAPH."
  (let* ((callback (clog-callbacks:callback
                    (lambda (args)
                      (let* ((node-id (car (access:access args :nodes)))
                             (node-spec (find node-id (graph-nodes graph)
                                              :key #'car
                                              :test #'equalp)))
                        (when node-spec
                          (funcall (node-selection-action graph) node-spec graph))))))
         (js-handle (js-handle graph))
         (callback-call (funcall callback "JSON.stringify(params)")))
    (write-string
     (interpol
      "${js-handle}.on('doubleClick', function (params) {
        params.event = '[original event]';
        ${callback-call}
})")
     stream)))

;; graph api

(defun linked-neighbours (object)
  (mapcar (bind:lambda-bind ((_ predicate neighbour))
            (cons predicate neighbour))
          (naive-triple-store:find-triples
           (princ-to-string (object-id object)) nil nil *triple-store*)))

(defgeneric neighbours (object)
  (:documentation "The neighbours of object. Returned as an a list of (edge . neighbour obj)"))

(defmethod neighbours (object)
  "Objects have no neighbours by default."
  nil)

(defmethod neighbours ((object object))
  "The neigbours of a mold OBJECT are its linked objects."
  (linked-neighbours object))

(defmethod neighbours ((coll collection))
  (mapcar (lambda (item)
            (cons "member" item))
          (members coll)))

(defclass object-graph (vis-graph)
  ((object :initarg :object
           :accessor graph-object
           :initform (error "Provide the graph object"))
   (expanded :initform nil
             :accessor graph-expanded)
   (neighbours-accessor :initarg :neighbours-accessor
                        :accessor neighbours-accessor
                        :initform #'neighbours)
   (objects :accessor graph-objects
            :initform nil
            :documentation "Internal. The list of graph node objects."))
  (:default-initargs
   :on-node-select 'object-graph-select-node)
  (:metaclass mold-object-class)
  (:documentation "A graph component for inspecting an OBJECT's neighbours."))

(defun object-graph-select-node (node graph)
  (let ((obj (find (car node) (graph-objects graph)
		   :key (alex:compose #'princ-to-string #'object-id)
		   :test #'equalp)))
    (if obj
	(show obj :view #'create-graph-view)
	(show (cdr node) :view #'create-graph-view))))

(defun generate-object-nodes-and-edges (object graph)
  "Generate the lists of nodes and edges for an OBJECT-GRAPH."
  (with-slots (nodes edges) graph
    (pushnew object (graph-objects graph))
    (pushnew (cons (princ-to-string (object-id object))
                   (object-title object))
             nodes
             :key #'car
             :test #'equalp)
    (dolist (neighbour (funcall (neighbours-accessor graph) object))
      (destructuring-bind (predicate . neighbour-object) neighbour
        (pushnew neighbour-object (graph-objects graph))
        (pushnew (cons (princ-to-string (object-id neighbour-object))
                       (object-title neighbour-object))
                 nodes
                 :key #'car
                 :test #'equalp)
        (push (list :from (princ-to-string (object-id object))
                    :to (princ-to-string (object-id neighbour-object))
                    :label (princ-to-string predicate))
              edges)))))

(defun generate-graph-nodes-and-edges (graph)
  (dolist (object (graph-expanded graph))
    (generate-object-nodes-and-edges object graph)))

(defmethod initialize-instance :after ((graph object-graph) &rest initargs)
  (declare (ignore initargs))
  (setf (graph-expanded graph) (list (graph-object graph)))
  (generate-graph-nodes-and-edges graph))

;; register a graph view

(defview create-graph-view (object clog-obj &rest args)
  (:documentation "Graph view for OBJECT.")
  (:label "Graph view")
  (:icon "fa-solid fa-diagram-project"))

(defmethod create-graph-view (object clog-obj &rest args)
  (declare (ignore args))
  (let ((graph (make-instance 'object-graph
                              :object object)))
    (create-object-view graph clog-obj)))

(defview create-links-graph-view (object clog-obj &rest args)
  (:documentation "Graph view for links of OBJECT.")
  (:label "Links graph view")
  (:icon "fa-solid fa-diagram-project"))

(defmethod create-links-graph-view (object clog-obj &rest args)
  (declare (ignore args))
  (let ((graph (make-instance 'object-graph
                              :object object
                              :neighbours-accessor #'linked-neighbours)))
    (create-object-view graph clog-obj)))

(defmethod object-view-types append (object (mold-module (eql :graph)))
  '(create-graph-view create-links-graph-view))

(pushnew :graph *mold-modules*)
