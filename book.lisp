(in-package :mold-desktop)

(defclass ebook (object)
  ((isbn :initarg :isbn
         :accessor isbn
         :type string
         :initform ""
         :attribute t
         :documentation "ISBN code")
   (isbn13 :initarg :isbn13
           :accessor isbn13
           :type string
           :initform ""
           :attribute t
           :documentation "ISBN13 code")
   (cover-image :initarg :cover-image
                :accessor cover-image
                :initform nil
                :type (or string null)
                :attribute t
                :documentation "URL of the book cover image.")
   (type :initarg :type
         :accessor book-type
         :type string
         :initform "")
   (author :initarg :author
           :accessor author
           :initform "Unknown"
           :type string
           :attribute t
           :documentation "Author of the book.")
   (data :initarg :data
         :accessor book-data
         :initform nil
         :documentation "Association list with book data."))
  (:metaclass mold-object-class)
  (:icon . "core-book-open"))

(defclass book (object)
  ((portrait-image :initarg :portrait-image
                   :accessor portrait-image
                   :initform nil
                   :type (or string null)
                   :attribute t)
   (nodes :initarg :nodes
          :accessor nodes
          :type collection
          :attribute t
          :initform (make-instance 'collection :type 'doc-node)))
  (:metaclass mold-object-class)
  (:icon . "core-book-open"))

(defmethod members ((object book))
  (nodes object))

(defmethod object-view-types append ((object book) (mold-module t))
  '(create-breadcrumb-view create-breadcrumb-detail-view))

(defun doc-node-nav-left (doc-node)
  (multiple-value-bind (left-siblings right-siblings)
      (doc-node-siblings doc-node)
    (declare (ignore right-siblings))
    (if (not left-siblings)
        (parent-node doc-node)
        (car (last left-siblings)))))

(defun doc-node-nav-right (doc-node &optional (subnodes? t))
  (if (and subnodes? (not (gcl:emptyp (subnodes doc-node))))
      (gcl:first (subnodes doc-node))
      (multiple-value-bind (left-siblings right-siblings)
          (doc-node-siblings doc-node)
        (declare (ignore left-siblings))
        (if (not right-siblings)
            (let ((parent (parent-node doc-node)))
              (when parent (doc-node-nav-right parent nil)))
            (first right-siblings)))))

(defmethod create-object-view ((book book) clog-obj &rest args)
  (declare (ignore args))
  (let ((current-node (when (not (gcl:emptyp (nodes book)))
			(gcl:first (nodes book)))))
      (clog:with-clog-create clog-obj
        (div (:bind view :style "height:100%;")
             (div (:class "w3-bar")
                  (icon-button (:bind nav-home "fa fa-home"))
                  (icon-button (:bind nav-up "fa fa-arrow-up"))
                  (icon-button (:bind nav-left "fa fa-arrow-left"))
                  (icon-button (:bind nav-right "fa fa-arrow-right")))
             (div (:bind split :class "split" :style "height:90%;overflow:hidden;")
                  (div (:bind nav :style "overflow:auto;"))
                  (div (:bind body :class "w3-padding-small"
                              :style "overflow:auto;"))))
      (flet ((navigate-to-node (doc-node)
               (setf (text body) ""
                     current-node doc-node)
               (create-object-view current-node body)))
        (set-on-click nav-home (ignoring-args (navigate-to-node (first (nodes book)))))
        (set-on-click nav-up (ignoring-args
                               (when (parent-node current-node)
                                 (navigate-to-node (parent-node current-node)))))
        (set-on-click nav-left (ignoring-args
                                 (let ((left-node (doc-node-nav-left current-node)))
                                   (when left-node
                                     (navigate-to-node left-node)))))
        (set-on-click nav-right (ignoring-args
                                  (let ((right-node (doc-node-nav-right current-node)))
                                    (when right-node
                                      (navigate-to-node right-node)))))
        (create-object-view
         (make-instance 'tree-object
                        :nodes (nodes book))
         nav
         :on-click #'navigate-to-node)
        (navigate-to-node current-node)
        (clog:js-execute clog-obj
                         (format nil "Split(['#~a','#~a'])"
                                 (clog:html-id nav)
                                 (clog:html-id body)))
        view))))

;; Build books from directory/files
(defun extract-doc-node-section-name (pathname)
  (let ((name (if (uiop/pathname:directory-pathname-p pathname)
                  (car (last (pathname-directory pathname)))
                  (pathname-name pathname))))
    (->
     (if (char= (aref name 0) #\0)
         ;; It is a numbered section
         (subseq name (1+ (position #\- name)))
         ;; else
         name)
     (symbol-munger:camel-case->english))))

(defun doc-node-type-for-file (pathname)
  (let ((ext (pathname-type pathname)))
    (cond
      ((member ext '("markdown" "md") :test 'string=)
       'markdown-doc-node)
      ((member ext '("ldoc" "lispdoc") :test 'string=)
       'lisp-doc-node)
      (t
       (error "No doc type for extension: ~s" ext)))))

(defun build-doc-node-from-file (pathname)
  (cond
    ;; A directory
    ((UIOP/PATHNAME:DIRECTORY-PATHNAME-P pathname)
     (let* ((subnodes (make-instance 'collection
                                     :type 'doc-node
                                     :members (mapcar #'build-doc-node-from-file
                                                      (fad:list-directory pathname))))
            (node (make-instance 'doc-node
                                 :name (extract-doc-node-section-name pathname)
                                 :contents ""
                                 :subnodes subnodes)))
       (gcl:doseq (subnode (subnodes node)) 
         (setf (parent-node subnode) node))
       node))
    ;; A file
    ((not (UIOP/PATHNAME:DIRECTORY-PATHNAME-P pathname))
     (make-instance (doc-node-type-for-file pathname)
                    :name (extract-doc-node-section-name pathname)
                    :contents (alexandria:read-file-into-string pathname)))))

;; node api for books
(defmethod subnodes ((book book))
  (nodes book))

(defmethod parent-node ((book book))
  nil)

;; tree api for books

(defmethod tree-node-children ((book book))
  (subnodes book))

;; graph api for books
(defmethod neighbours ((book book))
  (mapcar (lambda (docnode)
            (cons "chapter" docnode))
          (members (nodes book))))

(defmethod neighbours ((docnode doc-node))
  (mapcar (lambda (subnode)
            (cons "subnode" subnode))
          (members (subnodes docnode))))

;; build book from files in a filesystem directory
(defun build-book-from-directory (book directory)
  "Build a BOOK object from DIRECTORY.
Subdirectories are converted to subsections."
  (setf (nodes book)
        (make-instance 'collection
		       :type 'doc-node
		       :members (mapcar #'build-doc-node-from-file
					(fad:list-directory directory))))
  ;; Make the book the parent node of top-nodes
  (gcl:doseq (subnode (nodes book))
    (setf (parent-node subnode) book)))

(defvar *mold-desktop-book-module-nodes* '()
  "A list of DOC-NODEs to include into Mold Desktop manual.
New modules should add their documentation nodes to this.")

;; command for opening mold-desktop book
(defcommand (open-mold-desktop-book :show-result t) ()
  "Open Mold Desktop book."
  (let ((book (make-instance 'book :name "Mold Desktop Book")))
    ;; Build the book from "book" directory
    (build-book-from-directory book
			       (asdf:system-relative-pathname :mold-desktop "book/"))
    ;; Append nodes defined by modules
    (dolist (doc-node *mold-desktop-book-module-nodes*)
      (let ((doc-node
	      (etypecase doc-node
		(doc-node doc-node)
		((or function symbol)
		 (funcall doc-node)))))
	(sera:push-end doc-node (members (nodes book)))))
    
    book))
