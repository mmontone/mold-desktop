(in-package :mold-desktop)

(defun init-viewerjs (body)
  (add-script (html-document body)
              "https://cdnjs.cloudflare.com/ajax/libs/viewerjs/1.11.2/viewer.min.js")
  (load-css (html-document body)
            "https://cdnjs.cloudflare.com/ajax/libs/viewerjs/1.11.2/viewer.css"))

(pushnew 'init-viewerjs *init-functions*)

(defmethod create-viewerjs-view ((doc document) clog-obj &rest args)
  (declare (ignore args))
  (create-child clog-obj
                (html (:iframe :src (object-url doc)
			       :allowfullscreen t
			       :webkitallowfullscreen t
                               :height "100%"
                               :width "100%"))))

(defmethod object-view-types append ((doc document) (module (eql :viewerjs)))
  (when (member (document-format doc) '(:odp :odt :ods :pdf))
    '(create-viewerjs-view)))

(defmethod object-default-view :around ((doc document))
  (if (member (document-format doc) '(:odp :odt :ods :pdf))
      'create-viewerjs-view
      (call-next-method)))

(pushnew :viewerjs *mold-modules*)

#+example
(mapcar (alex:compose #'show #'create-object-from-url)
	'("https://viewerjs.org/ViewerJS/#/OpenDocument-v1.2.odt"
	  "https://viewerjs.org/ViewerJS/#../demo/ohm2013.odp"
	  "https://viewerjs.org/ViewerJS/#../demospreadsheet.ods"
	  "https://viewerjs.org/ViewerJS/#../demodoc.pdf"))
