;;;; * Halos

(in-package :mold-desktop/clog-gui)

(defvar *presented-objects* nil
  "Variable for keeping track of displayed objects and its presenter view.
A list of weak-pointers to view objects.")

(md/settings:defgroup gui nil
  "User interface settings")

(md/settings:defcustom *halos-activation-event*
  :right-click
  "The event that activates halos."
  :type (member :right-click :mouse-enter
                :mouse-enter+shift
                             :mouse-enter+ctrl)
  :group gui)

(md/settings:defcustom *choose-halos-from-command-bar*
  nil
  "Whether to choose halo from command bar."
  :type boolean
  :group gui)

(defgeneric handle-object-click (event object ui-state)
  (:documentation "Action to perform when a halo wrapped object is clicked.
Takes into account the current UI state."))

(defmethod handle-object-click (event object (ui-state t))
  (when (getf event :ctrl-key)
    (toggle-class object "selected")
    (mold-desktop::toggle-object (desktop-object object))))

(defmethod handle-object-click (event object (ui-state (eql :initial)))
  (when (getf event :ctrl-key)
    (toggle-class object "selected")
    (mold-desktop::toggle-object (desktop-object object))))

(defun attach-presentation-handlers
    (object clog-obj
     &key
       (on-click #'mold-desktop::show)
       (on-context-menu #'mold-desktop::show-commands-for-object)
       (draggable nil)
       (droppable t))

  "Attach presentation handlers to CLOG-OBJ for OBJECT."

  (clog:add-class clog-obj "presented-object")

  (when on-click
    (set-on-click clog-obj (lambda (&rest _)
                             (declare (ignore _))
                             (funcall on-click object))
                  :cancel-event t))

  (when on-context-menu
    (set-on-context-menu clog-obj
                         (lambda (&rest _)
                           (declare (ignore _))
                           (funcall on-context-menu object))))

  (when draggable
    (setf (clog:attribute clog-obj "data-drag-title")
          (mold-desktop::object-title object))
    (setf (clog:property clog-obj "draggable") "true")
    (set-on-drag-start clog-obj
                       (lambda (_)
                         (declare (ignore _))
                         (mold-desktop/clog-gui::start-dragging-object object))))

  (when droppable
    ;; To enable drop, drag-over event handler needs to be set:
    (clog:set-on-drag-over clog-obj (lambda (obj)
                                      (declare (ignore obj))
                                      (add-class clog-obj "drop-allowed")))
    (clog:set-on-drag-leave clog-obj
                            (lambda (obj)
                              (declare (ignore obj))
                              (remove-class clog-obj "drop-allowed")))
    (clog:set-on-drop clog-obj (lambda (&rest args)
                                 (declare (ignore args))
                                 (mold-desktop::select-object object)
                                 (mold-desktop::select-object md/clog-gui::*dragging-object*)
                                 (mold-desktop/clog-gui::stop-dragging-object)
                                 (remove-class clog-obj "drop-allowed")))))

;; Halos GUI

(defparameter *window-halos-spec*
  '(
    ;;(:close-window :label "Close window" :icon "fa fa-trash" :position :right-top)
    (:settings :label "Settings" :icon "fa fa-cog"
               :position :center-bottom
               :description "Settings of object.")
    (:actions :label "Actions" :icon "fa fa-terminal"
              :position :right-center
              :description "Run an action with the object as parameter.")
    #+nil(:properties :label "Properties" :icon "fa fa-list"
                      :position :right-bottom)
    ;;(:move :label "Move" :icon "fa fa-arrows-up-down-left-right"
    ;;       :position :center-top)
    (:drag :label "Drag" :icon "fa fa-hand-back-fist"
           :position :left-top
           :description "Drag and drop the object.")
    (:switch-view :label "Switch view" :icon "fa fa-eye"
                  :position :left-center
                  :description "Choose another view for the object.")
    (:refresh :label "Refresh" :icon "fa fa-arrows-rotate"
              :position :right-center-top
              :description "Refresh")
    (:open :label "Open" :icon "fa fa-arrow-up-right-from-square"
           :position :left-bottom
           :description "Open the object on the desktop.")
    (:help :label "Help" :icon "fa-solid fa-question"
           :position :right-center-bottom
           :description "Show help for the object")))

(defparameter *collection-item-halos-spec*
  '((:remove-from-collection :label "Remove from collection"
                             :icon "fa fa-trash"
                             :position :right-bottom
                             :description "Remove from the collection")
    (:settings :label "Settings" :icon "fa fa-cog"
               :position :right-top
               :description "Settings")
    (:actions :label "Actions" :icon "fa fa-terminal"
              :position :right-center)
    (:properties :label "Properties" :icon "fa fa-list"
                 :position :left-bottom)
    (:drag :label "Drag" :icon "fa fa-hand-back-fist"
           :position :left-top)
    (:open :label "Open" :icon "fa fa-arrow-up-right-from-square"
           :position :left-bottom)))

(defclass object-halos-window (mold-desktop/clog-gui::clog-gui-window)
  ((object :accessor desktop-object
           :initarg :object
           :initform nil
           :documentation "The desktop object being wrapped.")
   (object-view :accessor object-view
                :initarg :object-view
                :type (or trivial-types:function-designator mold-desktop::view)
                :initform nil
                :documentation "The view being used to display the object.")
   (halos-active-p :accessor halos-active-p
                   :initform nil
                   :documentation "Controls whether halos are visible or not.")
   (halos-buttons :accessor halos-buttons
                  :initform nil
                  :documentation "The list of halos button elements. Internal.")
   (content
    :accessor content
    :documentation "Window body clog-element")
   (closer
    :accessor closer
    :documentation "Window closer clog-element")
   (sizer
    :accessor sizer
    :documentation "Window sizer clog-element")
   (original-extent
    :accessor original-extent
    :documentation "Extents when not collapsed.")
   (on-window-can-close
    :accessor on-window-can-close
    :initform nil
    :documentation "Return t to allow closing of window")
   (on-window-can-move
    :accessor on-window-can-move
    :initform nil
    :documentation "Return t to allow moving of window")
   (on-window-can-size
    :accessor on-window-can-size
    :initform nil
    :documentation "Return t to allow sizing of window")
   (on-window-can-maximize
    :accessor on-window-can-maximize
    :initform nil
    :documentation "Return t to allow maximizing of window")
   (on-window-focus
    :accessor on-window-focus
    :initform nil
    :documentation "Fired on window focused")
   (on-window-blur
    :accessor on-window-blur
    :initform nil
    :documentation "Fired on window blurred")
   (on-window-close
    :accessor on-window-close
    :initform nil
    :documentation "Fired on window closed")
   (on-window-move
    :accessor on-window-move
    :initform nil
    :documentation "Fired during move of window")
   (on-window-size
    :accessor on-window-size
    :initform nil
    :documentation "Fired during size change of window")
   (on-window-move-done
    :accessor on-window-move-done
    :initform nil
    :documentation "Fired after move of window")
   (on-window-size-done
    :accessor on-window-size-done
    :initform nil
    :documentation "Fired after size change of window")))

(defun register-presentation-view (view)
  (push (trivial-garbage:make-weak-pointer view) *presented-objects*))

(defmethod initialize-instance :after ((view object-halos-window) &rest initargs)
  (declare (ignore initargs))
  ;; Add the view to the presented-objects list
  (register-presentation-view view))

(defmacro do-presented-objects (var &body body)
  "Evaluate BODY for each VAR bound to each element of *PRESENTED-OBJECTS* with a value."
  (alexandria:with-gensyms (ref)
    `(dolist (,ref *presented-objects*)
       (anaphora:awhen (trivial-garbage:weak-pointer-value ,ref)
         (let ((,var anaphora:it))
           ,@body)))))

(defun set-selection-mode (view)
  (clog:add-class view "selection-mode"))

(defmethod fire-on-window-blur ((obj object-halos-window))
  (when (on-window-blur obj)
    (funcall (on-window-blur obj) obj)))

(defmethod close-window ((obj object-halos-window))
  (remove-from-dom obj))

(defun choose-halos-from-command-bar (object halos-object halos-spec)
  ;; TODO: we should pass halos CONTEXT here
  (let* ((halos (or halos-spec
                    (halos-for-object object nil)))
         (choices (loop for halo in halos
                        collect
                        (list :label (getf (rest halo) :label)
                              :value (first halo)
                              :icon (getf (rest halo) :icon)
                              :description (or (getf (rest halo) :description)
                                               (getf (rest halo) :label))))))
    (mold-desktop::command-bar-choose "Choose halo: " choices
                                      (lambda (value)
                                        (eval-halo-action value object halos-object))
                                      mold-desktop::*command-bar*)))

(defun activate-halos (halos-object object &optional halos-spec)
  (mold-desktop::if* *choose-halos-from-command-bar*
     then
         (choose-halos-from-command-bar object halos-object halos-spec)
     else
         (when (not (halos-active-p halos-object))
           (add-class halos-object "active")
           (setf (halos-active-p halos-object) t))))

(defun deactivate-halos (halos-object object)
  (when (not *choose-halos-from-command-bar*)
    (when (halos-active-p halos-object)
      (remove-class halos-object "active")
      (setf (halos-active-p halos-object) nil))))

(defgeneric eval-halo-action (halo-type object clog-obj &rest args)
  (:documentation "Evaluate action for HALO-TYPE on OBJECT."))

(defgeneric create-halo-button (clog-obj halo-type halo-spec &rest args)
  (:documentation "Create button for halos."))

(defmethod create-halo-button (clog-obj halo-type halo-spec &rest args &key size object (handle-click t) &allow-other-keys)
  "Generic halo button creation function.
Specialize for different halos."
  (destructuring-bind (htype &key label icon position action description) halo-spec
    (let ((btn
            (create-child clog-obj (html (:span :id (format nil "~a-~a" (html-id clog-obj) htype)
                                                :href "#"
                                                :class (format nil "halo-btn ~a ~a"
                                                               (string-downcase (symbol-name (or size :medium)))
                                                               (string-downcase (symbol-name position)))
                                                :title label)))))
      (mold-desktop::create-icon icon btn)
      (when handle-click
        ;; Just prevent click propagation so that parent does not receive the click event and closes the halos
        (set-on-click
         btn
         (lambda (&rest _)
           (declare (ignore _))
           (mold-desktop::if* action
              then
                  (funcall action
                           (or object (desktop-object clog-obj))
                           clog-obj)
              else
                  (apply #'eval-halo-action halo-type (or object (desktop-object clog-obj)) clog-obj args)))
         :cancel-event t))
      btn)))

(defmethod create-halo-button (clog-obj (halo-type (eql :drag)) halo-spec &key size object)
  (destructuring-bind (htype &key label icon position description) halo-spec
    (declare (ignore htype label))
    (let ((btn
            (create-child clog-obj
                          (html (:a :id (format nil "~a-drag" (html-id clog-obj))
                                    :class (format nil "halo-btn ~a ~a"
                                                   (string-downcase (symbol-name (or size :medium)))
                                                   (string-downcase (symbol-name position)))
                                    :data-drag-obj nil ;; TODO
                                    :data-drag-title (mold-desktop::object-title (or object (desktop-object clog-obj)))
                                    :draggable "true"
                                    :title "Drag")))))
      (mold-desktop::create-icon icon btn)
      (set-on-drag-start btn (lambda (_)
                               (declare (ignore _))
                               (mold-desktop/clog-gui::start-dragging-object (or object (desktop-object clog-obj)))))
      ;; Just prevent click propagation so that parent does not receive the click event and closes the halos
      (set-on-event btn "click"
                    (mold-desktop::lambda* (_))
                    :cancel-event t)
      btn)))

(defmethod create-halo-button (win (halo-type (eql :move)) halo-spec &key size)
  (let* ((btn (call-next-method win halo-type halo-spec :handle-click nil))
         (app (connection-data-item win "clog-gui")))
    ;; Window movement and resizing
    (when *client-movement*
      (clog::jquery-execute win
                            (format nil "draggable({handle:'#~a'})" (html-id btn)))
      (clog::jquery-execute win "resizable({handles:'se'})")
      (set-on-pointer-down btn
                           (lambda (obj data)
                             (declare (ignore obj) (ignore data))
                             (setf (z-index win) (incf (last-z app)))
                             (fire-on-window-change win app)))
      (clog::set-on-event win "dragstart"
                          (lambda (obj)
                            (declare (ignore obj))
                            (fire-on-window-move win)))
      (clog::set-on-event win "dragstop"
                          (lambda (obj)
                            (declare (ignore obj))
                            (fire-on-window-move-done win)))
      (clog::set-on-event win "resizestart"
                          (lambda (obj)
                            (declare (ignore obj))
                            (fire-on-window-size win)))
      (clog::set-on-event win "resizestop"
                          (lambda (obj)
                            (declare (ignore obj))
                            (fire-on-window-size-done win))))

    (when (not *client-movement*)
      (set-on-pointer-down
       btn 'on-gui-drag-down :capture-pointer t)
      (set-on-pointer-down
       (sizer win) 'on-gui-drag-down :capture-pointer t))

    ;; Just prevent click propagation so that parent does not receive the click event and closes the halos
    (set-on-event btn "click"
                  (mold-desktop::lambda* (_))
                  :cancel-event t)

    btn))

(defmethod eval-halo-action ((halo-type (eql :help)) object clog-obj &rest args)
  (mold-desktop::show (mold-desktop::describe--object object)))

(defmethod eval-halo-action ((halo-type (eql :remove-from-collection)) object clog-obj &rest args)
  (mold-desktop::collection-remove (getf args :context) object))

(defmethod eval-halo-action ((halo-type (eql :actions)) object clog-obj &rest args)
  (mold-desktop::show-commands-for-object object))

(defmethod create-halo-button (clog-obj (halo-type (eql :open)) halo-spec &key size object)
  (destructuring-bind (htype &key label icon position description) halo-spec
    (let ((btn
            (create-child clog-obj (html (:a :id (format nil "~a-~a" htype (html-id clog-obj))
                                             :href (mold-desktop::object-url (or object (desktop-object clog-obj)))
                                             :class (format nil "halo-btn ~a ~a"
                                                            (string-downcase (symbol-name (or size :medium)))
                                                            (string-downcase (symbol-name position)))
                                             :title label
                                             :target "_blank")))))
      (mold-desktop::create-icon icon btn)
      ;; Just prevent click propagation so that parent does not receive the click event and closes the halos
      #+nil(set-on-event btn "click"
                         (mold-desktop::lambda* (_))
                         :cancel-event t)
      btn)))

(defmethod eval-halo-action ((halo-type (eql :close-window)) object clog-obj &rest args)
  (mold-desktop::workspace-remove-object
   mold-desktop::*workspace* object)
  (mold-desktop::wm/close-window mold-desktop::*window-manager* clog-obj))

(defmethod eval-halo-action ((halo-type (eql :settings)) object clog-obj &rest args)
  (mold-desktop::side-panel-open-object-settings mold-desktop::*side-panel* object))

(defun view-types (object)
  (remove-duplicates (apply #'append (mapcar (alexandria:curry #'mold-desktop::object-view-types object) mold-desktop::*mold-modules*))))

(defun switch-view (clog-obj object)
  (let* ((object (or object (desktop-object clog-obj)))
         (view-types (view-types object))
         (view-choices (mapcar (lambda (view-type)
                                 (let ((view-func (symbol-function view-type)))
                                   (list :label (or (mold-desktop::view-label view-func)
                                                    (mold-desktop::default-view-label view-func))

                                         :value view-func
                                         :description (documentation view-func 'function)
                                         :icon (or (mold-desktop::view-icon view-func)
                                                   mold-desktop::*default-view-icon*))))
                               view-types)))
    (mold-desktop::command-bar-choose
     "View: "
     view-choices
     (lambda (view)
       (with-slots (object-view) clog-obj
         (setf (text (content clog-obj)) "")
         (funcall view object (content clog-obj))
         (setf object-view view)))
     mold-desktop::*command-bar*)))

(defmethod eval-halo-action ((halo-type (eql :switch-view)) object clog-obj &rest args)
  (switch-view clog-obj object))

(defmethod eval-halo-action ((halo-type (eql :refresh)) object clog-obj &rest args)
  (with-slots (object-view) clog-obj
    (setf (text (content clog-obj)) "")
    (funcall object-view object
             (content clog-obj))))

(defun setup-halos-event-handlers (clog-obj object &optional halos-spec)
  (set-on-mouse-click
   clog-obj
   (lambda (_ data)
     (declare (ignore _))
     (handle-object-click data clog-obj mold-desktop::*ui-state*))
   :cancel-event nil)

  (ecase *halos-activation-event*
    (:right-click
     (set-on-context-menu clog-obj
                          (lambda (&rest args)
                            (declare (ignore args))
                            (if (not (halos-active-p clog-obj))
                                (activate-halos clog-obj object halos-spec)
                                ;; else
                                (mold-desktop::show-commands-for-object object)))))
    (:mouse-enter
     (set-on-mouse-enter clog-obj
                         (lambda (&rest args)
                           (declare (ignore args))
                           (when (not (halos-active-p clog-obj))
                             (activate-halos clog-obj object halos-spec))))
     (set-on-context-menu clog-obj
                          (lambda (&rest args)
                            (declare (ignore args))
                            (when (halos-active-p clog-obj)
                              (mold-desktop::show-commands-for-object object)))))
    #+todo
    ((:mouse-enter+ctrl :mouse-enter+shift)
     (set-on-event-with-data clog-obj "mouseenter"
                             (lambda (_ data)
                               (declare (ignore _))
                               (let ((mouse-event (clog::parse-mouse-event data)))
                                 (when (or (and (eql *halos-activation-event* :mouse-enter+shift)
                                                (getf mouse-event :shift-key))
                                           (and (eql *halos-activation-event* :mouse-enter+ctrl)
                                                (getf mouse-event :ctrl-key)))
                                   (when (not (halos-active-p clog-obj))
                                     (activate-halos clog-obj object halos-spec))))))
     (set-on-context-menu clog-obj
                          (lambda (&rest args)
                            (declare (ignore args))
                            (when (halos-active-p clog-obj)
                              (mold-desktop::show-commands-for-object object))))))

  (set-on-mouse-leave clog-obj
                      (lambda (&rest args)
                        (declare (ignore args))
                        (deactivate-halos clog-obj object))))

(mold-desktop:defcommand toggle-collapse-window ((win object-halos-window))
  "Collapse window."
  (let* ((collapsed-p (clog:hiddenp (content win)))
         (new-height (if (not collapsed-p)
                         "20px"
                         (fourth (original-extent win)))))
    (when (not collapsed-p)
      (setf (original-extent win) (mold-desktop::element-extent win)))
    (setf (clog:hiddenp (content win))
          (not collapsed-p))
    (setf (height win) new-height)))

;; FIXME: window alignment commands need adjustments because the
;; menubar size has to be measured, etc. Current implementation is approximate.

(mold-desktop:defcommand maximize-window ((win object-halos-window))
  "Maximize window."
  (setf (original-extent win)
        (mold-desktop::element-extent win))
  (set-geometry win :width "100vw" :height "100vh")
  (align-window-top win)
  (align-window-left win))

(mold-desktop:defcommand restore-window ((win object-halos-window))
  "Restore window size."
  (destructuring-bind (left top width height) (original-extent win)
    (set-geometry win :left left :top top :width width :height height)))

(mold-desktop:defcommand align-window-left ((win object-halos-window))
  "Align window to the left."
  (clog:set-geometry win :left 0))

(mold-desktop:defcommand align-window-top ((win object-halos-window))
  "Align window to the top."
  (clog:set-geometry win :top 35 ;; menu height
                     ))

(mold-desktop:defcommand align-window-right ((win object-halos-window))
  "Align window to the right."
  (clog:set-geometry win :right 0))

(mold-desktop:defcommand align-window-bottom ((win object-halos-window))
  "Align window to the bottom."
  (clog:set-geometry win :bottom 0))

(mold-desktop:defcommand align-window-top-left ((win object-halos-window))
  "Align window to top left."
  (clog:set-geometry win :top 35 :left 0))

(mold-desktop:defcommand tile-window-left ((win object-halos-window))
  "Tile window left."
  (clog:set-geometry win :top 35 :left 0 :width "50vw" :height "100vh"))

(mold-desktop:defcommand tile-window-right ((win object-halos-window))
  "Tile window right."
  (clog:set-geometry win :top 35 :left "50vw" :width "50vw" :height "100vh"))

(mold-desktop:defcommand tile-window-top ((win object-halos-window))
  "Tile window top."
  (clog:set-geometry win :top 35 :left 0 :width "100vw" :height "50vh"))

(mold-desktop:defcommand tile-window-bottom ((win object-halos-window))
  "Tile window top."
  (clog:set-geometry win :top "50vh" :left 0 :width "100vw" :height "50vh"))

(mold-desktop:defcommand tile-window-top-left ((win object-halos-window))
  "Tile window top."
  (clog:set-geometry win :top 35 :left 0 :width "50vw" :height "50vh"))

(mold-desktop:defcommand tile-window-bottom-left ((win object-halos-window))
  "Tile window top."
  (clog:set-geometry win :top "52vh" :left 0 :width "50vw" :height "50vh"))

(mold-desktop:defcommand tile-window-top-right ((win object-halos-window))
  "Tile window top."
  (clog:set-geometry win :top 35 :left "50vw" :width "50vw" :height "50vh"))

(mold-desktop:defcommand tile-window-bottom-right ((win object-halos-window))
  "Tile window top."
  (clog:set-geometry win :top "52vh" :left "50vw" :width "50vw" :height "50vh"))

(defun create-halos-window (clog-obj object
                            &key
                              html-id
                              left
                              top
                              (width 300)
                              (height 200)
                              (content "")
                              (title "")
                              (client-movement mold-desktop/clog-gui::*client-movement*)
                              halos-spec)
  (let ((app (connection-data-item clog-obj "clog-gui")))
    (unless html-id
      (setf html-id (clog-connection:generate-id)))
    (when (eql (hash-table-count (windows app)) 0)
      ;; If previously no open windows reset default position
      (setf (last-x app) 0)
      (setf (last-y app) 0))
    (unless left
      ;; Generate sensible initial x location
      (setf left (last-x app))
      (incf (last-x app) 10))
    (unless top
      ;; Generate sensible initial y location
      (when (eql (last-y app) 0)
        (setf (last-y app) (mold-desktop/clog-gui::menu-bar-height clog-obj)))
      (setf top (last-y app))
      (incf (last-y app) top-bar-height)
      (when (> top (- (inner-height (window (body app))) (last-y app)))
        (setf (last-y app) (mold-desktop/clog-gui::menu-bar-height clog-obj))))
    (let ((win (create-child (or mold-desktop::*app-body* (app-body app) (body app))
                             (html (:div :style (inline-css `(:position "absolute"
                                                              :top ,(px top) :left ,(px left) :width ,(px width) :height ,(px height)
                                                              :z-index ,(incf (last-z app))
                                                              :visibility "hidden"))
                                         :class "w3-card-4 w3-white w3-border halos-window"
                                         :object-id (and object (mold-desktop::object-id object))
                                         ;; Title bar
                                         (:div :id (format nil "~a-title-bar" html-id)
                                               :class "window-title"
                                               :style "position:absolute;top:0;right:0;left:0;height:25px;"
                                               ;; Title
                                               (:span :data-drag-obj html-id :data-drag-type "m"
                                                      :style "position:absolute;top:0;right:20px;left:5px;user-select:none;cursor:move;"
                                                      (when (and object (mold-desktop::object-icon object))
                                                        (write-string (mold-desktop::icon-html (mold-desktop::object-icon object)) *html*))
                                                      (:span :style "margin-left:5px;" :id (format nil "~a-title" html-id) :class "object-title" (who:str (who:escape-string title))))
                                               ;; Collapse button
                                               (:span :id (format nil "~A-collapser" html-id)
                                                      :style "position:absolute;top:0;right:20px;cursor:pointer;user-select:none;" (who:str "&#10064;"))
                                               ;; Close button
                                               (:span :id (format nil "~A-closer" html-id)
                                                      :style "position:absolute;top:0;right:5px;cursor:pointer;user-select:none;" (who:str "&times")))
                                         ;; Window body
                                         (:div :id (format nil "~a-body" html-id)
                                               :class "w3-padding-small"
                                               :style "position:absolute;top:25px;left:0;right:0;bottom:3px;overflow:auto"
                                               (who:str content))
                                         ;; Sizer
                                         (:div :id (format nil "~a-sizer" html-id)
                                               :style "position:absolute;right:0;bottom:0;left:0;user-select:none;height:3px;cursor:se-resize;opacity:0"
                                               :class "w3-right" :data-drag-obj html-id :data-drag-type "s"
                                               (who:str "+"))))
                             :clog-type 'object-halos-window
                             :html-id html-id)))

      (setf (win-title win) (attach-as-child win (format nil "~A-title" html-id)))
      (setf (closer win) (attach-as-child win (format nil "~A-closer" html-id)))
      (setf (sizer win) (attach-as-child win (format nil "~A-sizer" html-id)))
      (setf (content win) (attach-as-child win (format nil "~A-body"  html-id)))
      (setf (desktop-object win) object)

      ;; Update window title when object name changes
      (when (typep object 'mold-desktop::object)
        (event-emitter:on :changed object
                          (lambda (&rest args)
                            (declare (ignore args))
                            (setf (text (win-title win))
                                  (mold-desktop::object-title object)))))

      ;; Set object name when title bar is double clicked
      (set-on-double-click (win-title win)
                           (lambda (&rest args)
                             (declare (ignore args))
                             (mold-desktop::run-command-interactively
                              'mold-desktop/commands::set-object-name object))
                           :cancel-event t)

      (set-on-click (closer win)
                    (lambda (obj)
                      (declare (ignore obj))
                      (mold-desktop::workspace-remove-object mold-desktop::*workspace* (or object (desktop-object win)))
                      (mold-desktop::wm/close-window mold-desktop::*window-manager* win)))

      (let ((title-bar (attach-as-child win (format nil "~A-title-bar" html-id)))
            (collapser (attach-as-child win (format nil "~A-collapser" html-id))))
        (setf (clog:advisory-title collapser) "Click: (un)collapse. Right-Click: menu")
        (set-on-click collapser (mold-desktop::ignoring-args (toggle-collapse-window win)))
        (set-on-context-menu collapser (mold-desktop::ignoring-args (mold-desktop::show-commands-for-object win)))
        (set-on-double-click title-bar (mold-desktop::ignoring-args (toggle-collapse-window win))))

      (setup-halos-event-handlers win object halos-spec)

      (set-on-pointer-down win
                           (lambda (obj data)
                             (declare (ignore obj) (ignore data))
                             (setf (z-index win) (incf (last-z app)))
                             (fire-on-window-change win app)))

      (when client-movement
        (clog::jquery-execute win (format nil "draggable({handle:'#~a'})"
                                          (format nil "~A-title-bar" html-id)))

        (clog::jquery-execute win "resizable({handles:'se'})")

        (clog::set-on-event win "dragstart"
                            (lambda (obj)
                              (declare (ignore obj))
                              (fire-on-window-move win)))
        (clog::set-on-event win "dragstop"
                            (lambda (obj)
                              (declare (ignore obj))
                              (fire-on-window-move-done win)))
        (clog::set-on-event win "resizestart"
                            (lambda (obj)
                              (declare (ignore obj))
                              (fire-on-window-size win)))
        (clog::set-on-event win "resizestop"
                            (lambda (obj)
                              (declare (ignore obj))
                              (fire-on-window-size-done win))))

      ;; Halos
      (dolist (halo-spec (or halos-spec (halos-for-object object nil)))
        (create-halo-button win (first halo-spec) halo-spec
                            :object object
                            :context nil))

      ;; To enable drop, drag-over event handler needs to be set:
      (set-on-drag-over (content win) (lambda (obj)
                                        (declare (ignore obj))
                                        (add-class (content win) "drop-allowed")))
      (set-on-drag-leave (content win)
                         (lambda (obj)
                           (declare (ignore obj))
                           (remove-class (content win) "drop-allowed")))
      (set-on-drop (content win) (lambda (&rest args)
                                   (declare (ignore args))
                                   (mold-desktop::select-object object)
                                   (mold-desktop::select-object *dragging-object*)
                                   (mold-desktop/clog-gui::stop-dragging-object)
                                   (remove-class (content win) "drop-allowed")))

      (setf (visiblep win) t)

      (clog:js-execute clog-obj (format nil "initHalos(document.getElementById('~a'));" (html-id win)))

      win
      )))

(defun open-object-in-halos-window (object clog-obj &optional (view #'mold-desktop::create-object-view))
  (let ((wnd (create-halos-window clog-obj object
                                  :title (or (mold-desktop::object-name object)
                                             (mold-desktop::object-url object)
                                             (type-of object))
                                  )))
    (let* ((spinner (create-child wnd (html (:i :class "fa fa-spinner w3-spin" :style "font-size:32px")))))
      (funcall view object wnd)
      (setf (object-view wnd) view)
      (remove-from-dom spinner))))

#+example
(open-object-in-halos-window (first mold-desktop::*objects*) mold-desktop::*body*)

(defgeneric halos-for-object (object context)
  (:documentation "Return the halos for OBJECT in CONTEXT.")
  (:method-combination append)
  (:method append (object context)
    nil))

(defmethod halos-for-object append (object (context mold-desktop::collection))
  *collection-item-halos-spec*)

(defmethod halos-for-object append (object (context (eql nil)))
  *window-halos-spec*)

(defmethod halos-for-object append ((object mold-desktop::collection) context)
  '((:collection-settings
     :label "Collection settings" :icon "fa fa-solid fa-sliders"
     :position :center-top
     :description "Collection settings."
     :action configure-collection)))

(defun handle-set-collection-sort (collection view)
  (lambda (attr)
    (with-slots (mold-desktop::collection-configuration) view
      (mold-desktop::add-collection-sorter
       mold-desktop::collection-configuration
       (c2mop:slot-definition-name attr)
       (first (mold-desktop::attribute-sorters attr))))
    (mold-desktop::refresh-view view)))

(defun handle-set-collection-filter (collection view)
  (lambda (attr)
    (let* ((filter-command (first (mold-desktop::attribute-filters attr)))
           (filter (mold-desktop::run-command-interactively filter-command)))
      (with-slots (mold-desktop::collection-configuration) view
        (mold-desktop::add-collection-filter
         mold-desktop::collection-configuration
         (c2mop:slot-definition-name attr)
         filter)))))

(defun handle-collection-configuration (collection halos-view)
  (let ((collection-view (object-view halos-view)))
    (lambda (selection)
      (case selection
        (:sort
         (mold-desktop::command-bar-choose
          "Sort by"
          (mapcar (lambda (attr)
                    (list :label (princ-to-string (c2mop:slot-definition-name attr))
                          :value attr))
                  (mold-desktop::sortable-attributes (class-of collection)))
          (handle-set-collection-sort collection collection-view)
          mold-desktop::*command-bar*))
        (:filter
         (mold-desktop::command-bar-choose
          "Filter by"
          (mapcar (lambda (attr)
                    (list :label (princ-to-string (c2mop:slot-definition-name attr))
                          :value attr))
                  (mold-desktop::filterable-attributes (class-of collection)))
          (handle-set-collection-filter collection collection-view)
          mold-desktop::*command-bar*))))))

(defun configure-collection (collection clog-obj)
  (mold-desktop::command-bar-choose
   "Configure collection view: "
   '((:label "View as ..."
      :value :select-view
      :description "Select a view"
      :icon "fa fa-eye")
     (:label "Sort"
      :value :sort
      :description "Sort the collection"
      :icon "fa fa-solid fa-sort")
     (:label "Filter"
      :value :filter
      :description "Filter the collection"
      :icon "fa fa-solid fa-filter")
     (:label "Group"
      :value :group
      :description "Apply grouping to the collection"
      :icon "fa fa-solid fa-list")
     (:label "Clear all"
      :value :clear-all
      :description "Clear all the settings"
      :icon "fa fa-solid fa-broom"))
   (handle-collection-configuration collection clog-obj)
   mold-desktop::*command-bar*))

(defun create-halos-wrapper (clog-obj object
                             &key
                               context
                               halos-spec
                               (create-function #'create-div)
                               class
                               (style "")
                               (handle-drop t)
                               active
                               (halos-position :top-right))
  "A halos wrapper is an HTML element with toggleable halos that wraps a rendered view for OBJECT."
  (let ((wrapper (funcall create-function clog-obj
                          :class (mold-desktop::css-classes "halos-window" class
                                                            (when active "active"))
                          :style (concatenate 'string "position:relative " style))))

    (setf (attribute wrapper "object-id") (and object (mold-desktop::object-id object)))

    (change-class wrapper 'object-halos-window)
    (register-presentation-view wrapper)

    (setf (desktop-object wrapper) object)

    (setup-halos-event-handlers wrapper object)

    ;; Halos
    (let ((halos-buttons (create-div wrapper :class (mold-desktop::css-classes "halos-buttons" halos-position))))
      (dolist (halo-spec (or halos-spec (halos-for-object object context)))
        (push (create-halo-button halos-buttons (first halo-spec) halo-spec
                                  :size :small
                                  :object object
                                  :context context)
              (halos-buttons wrapper))))

    (when handle-drop
      ;; To enable drop, drag-over event handler needs to be set:
      (set-on-drag-over wrapper (lambda (obj)
                                  (declare (ignore obj))
                                  (add-class wrapper "drop-allowed")))
      (set-on-drag-leave wrapper
                         (lambda (obj)
                           (declare (ignore obj))
                           (remove-class wrapper "drop-allowed")))
      (set-on-drop wrapper (lambda (&rest args)
                             (declare (ignore args))
                             (mold-desktop::select-object object)
                             (mold-desktop::select-object *dragging-object*)
                             (mold-desktop/clog-gui::stop-dragging-object)
                             (remove-class wrapper "drop-allowed"))))

    (clog:js-execute clog-obj (format nil "initHalos(document.getElementById('~a'));" (html-id wrapper)))

    wrapper
    ))

#+nil
(defun toggle-halos (halos-window)
  (setf (halos-active-p halos-window)
        (not (halos-active-p halos-window)))
  (toggle-class halos-window "halos"))

#+nil
(defun create-thin-halos-wrapper (clog-obj object
                                  &key
                                    context
                                    (content "")
                                    (title "")
                                    halos-spec
                                    (create-function #'create-div)
                                    class)
  (let ((wrapper (funcall create-function clog-obj
                          :class (format nil "halos-window ~a" (or class ""))
                          :style "position:relative"
                          :content content)))

    (setf (attribute wrapper "object-id") (and object (mold-desktop::object-id object)))

    (change-class wrapper 'object-halos-window)

    (setf (desktop-object wrapper) object)

    (set-on-event wrapper "click"
                  (lambda (event)
                    (toggle-halos wrapper)
                    ;; Halos
                    (if (halos-active-p wrapper)
                        (dolist (halo-spec (or halos-spec (halos-for-object object context)))
                          (push (create-halo-button wrapper (first halo-spec) halo-spec :size :small)
                                (halos-buttons wrapper)))
                        (progn
                          (dolist (halo-button (halos-buttons wrapper))
                            (remove-from-dom halo-button))
                          (setf (halos-buttons wrapper) nil))))
                  :cancel-event t ;; this prevents event propagation
                  )

    (set-on-context-menu wrapper (lambda (&rest args)
                                   (mold-desktop::select-object object)))

    ;; To enable drop, drag-over event handler needs to be set:
    #+todo(set-on-drag-over wrapper (lambda (obj)
                                      (declare (ignore obj))
                                      (add-class (content win) "drop-allowed")))
    #+todo(set-on-drag-leave (content win)
                             (lambda (obj)
                               (declare (ignore obj))
                               (remove-class (content win) "drop-allowed")))
    #+todo(set-on-drop (content win) (lambda (&rest args)
                                       (mold-desktop::select-object object)
                                       (mold-desktop::select-object *dragging-object*)
                                       (setf *dragging-object* nil)
                                       (remove-class (content win) "drop-allowed")))
    wrapper
    ))

(defun create-halos-bar (object clog-obj &key halos-spec context)
  (let ((bar (create-div clog-obj :class "w3-bar")))
    (dolist (halo-spec (or halos-spec (halos-for-object object context)))
      (let ((btn (create-div bar :class "w3-button")))
        (mold-desktop::->
            (create-halo-button btn (first halo-spec) halo-spec
                                :object object
                                :context context)
            ;; Very bad hack. Rethink.
            (set-styles '(("display" "block")
                          ("position" "relative")
                          ("left" "0"))))))
    bar))
