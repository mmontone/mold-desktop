;; Copyright (C) 2022 Mariano Montone - All Rights Reserved
;; You may use, distribute and modify this code under the terms of the
;; GNU Affero General Public License

(in-package :mold-desktop)

(md/settings:defcustom *desktop-notifications*
  nil
  "Enable desktop notifications"
  :type boolean
  :group gui)

(md/settings:defcustom *notifications-alert-func*
  'command-bar-notify
  "Function/command used for showing a notification."
  :type symbol
  :group gui)

(defvar *notifications* (make-instance 'collection :name "Notifications")
  "The list of triggered notifications.")

(defclass notification ()
  ((title :initarg :title
          :accessor object-title)
   (body :initarg :body
         :accessor body)
   (icon :initarg :icon
         :accessor object-icon
         :initform nil)
   (type :initarg :type
         :accessor notification-type
         :initform :info
         :type (member :info :success :error :warning)
         :documentation "The notification type")
   (description :initarg :description
                :accessor object-description
                :initform "")
   (data :initarg :data
         :accessor notification-data)
   (timestamp :initform (get-universal-time)))
  (:documentation "A notification"))

(defun trigger-notification (title &rest args)
  (let ((notification (apply #'make-instance 'notification
                             :title title
                             args)))
    (collection-add *notifications* notification)
    (funcall *notifications-alert-func* notification)))

(defun show-notifications ()
  (show *notifications*))

(defun notification-icon (notification)
  (ecase (notification-type notification)
    ;;(:info "fa-solid fa-bell")
    (:info "fa fa-info-circle")
    (:success "fa fa-check-circle")
    (:warning "fa fa-exclamation-triangle")
    (:error "fa fa-exclamation-circle")))

(defun notification-color (notification)
  (ecase (notification-type notification)
    ;;(:info "fa-solid fa-bell")
    (:info "blue")
    (:success "green")
    (:warning "orange")
    (:error "red")))

(defmethod create-object-view ((object notification) clog-obj &rest args)
  (declare (ignore args))
  (create-icon (notification-icon object) clog-obj)
  (arrows:->
      (create-a clog-obj :content (object-title object)
                         :style "margin-left: 10px;")
      (set-on-click (ignoring-args (show object))
       :cancel-event t))
  (create-p clog-obj :content (object-description object)))

(defmethod create-item-view ((object notification) li &rest args &key context &allow-other-keys)
  (declare (ignore args))
  (create-child li (html (:i :class (format nil "fa fa-solid fa-~a"
                                            (or (object-icon object) "bell"))
                             :style "margin-right: 3px;")))
  (let ((link
          (create-a li :content (object-title object))))
    (set-on-event link "click"
                  (ignoring-args (show object :context context))
                  :cancel-event t)))

;;;; *** Notification alert functions

(defun toast-notify (notification)
  "Use GUI toasts for alerting the NOTIFICATION.
Set *NOTIFICATIONS-ALERT-FUNC* to 'TOAST-NOTIFY to use."
  (let ((text (with-output-to-string (s)
                (write-string (object-title notification) s)
                (terpri s)
                (terpri s)
                (write-string (object-description notification) s))))
    (mold-desktop/commands::notify-info text)))

(defun command-bar-notify (notification)
  "Use command bar for alerting NOTIFICATION. The default.
Set *NOTIFICATIONS-ALERT-FUNC* to 'COMMAND-BAR-NOTIFY to use."
  (let ((html
          (who:with-html-output-to-string (html)
            (:div
             (:p :style (format nil "color: ~a" (notification-color notification))
                 (:i :class (notification-icon notification))
                 (who:str "  ")
                 (:b (who:str (object-title notification))))
             (:p (who:str (object-description notification)))))))
    (command-bar-html-message html)))

#+test
(trigger-notification "Test")
#+test
(trigger-notification "Test 2" :description "This is a notification")
#+test
(trigger-notification "Warning!!" :icon "fa-solid fa-triangle-exclamation" :description "Be careful!!" :type :warning)
#+test
(trigger-notification "Error!!" :icon "fa-solid fa-triangle-exclamation" :description "Oops!! Sorry" :type :error)
