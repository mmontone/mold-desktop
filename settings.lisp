;; Emacs-like settings framework
;; TODO: perhaps rethink this implementation: variables and groups could form a graph with
;; actual references stored in the CLOS object slots (parent, group, members, etc).

(defpackage :mold-desktop/settings
  (:nicknames :md/settings)
  (:local-nicknames
   (:md :mold-desktop))
  (:export
   #:custom-var
   #:custom-group
   #:defcustom
   #:defgroup
   #:find-custom-var
   #:find-custom-group
   #:customize-variable
   #:system-settings
   #:custom-var-value
   #:set-custom-var-value)
  (:use :cl :mold-desktop))

(in-package :mold-desktop/settings)

(defvar *custom-vars* (make-hash-table)
  "map of customization variables.")

(defvar *custom-groups* (make-hash-table)
  "map of customization groups.")

(defclass custom-var (md::object)
  ((type :initarg :type
         :accessor vartype)
   (group :initarg :group
          :accessor group)
   (setter :initarg :setter
           :accessor setter
           :type (or null symbol function)
           :initform nil
           :documentation "Function for setting the value of the variable.")
   (default-value :initarg :default
                  :accessor default-value
                  :documentation "The variable default value"))
  (:metaclass md::mold-object-class)
  (:icon . "fa-solid fa-wrench")
  (:documentation "Customization variable."))

(defclass custom-group (md::object)
  ((group :initarg :group
          :accessor group
          :initform nil))
  (:metaclass md::mold-object-class)
  (:icon . "fa-solid fa-screwdriver-wrench"))

;; TODO: perhaps rething this implementation: variables and groups could form a graph with
;; actual references stored in the CLOS object slots (parent, group, members, etc).
(defun group-vars (group)
  (remove-if-not (lambda (var)
                   (eql (group var) (md::object-name group)))
                 (alexandria:hash-table-values *custom-vars*)))

;; TODO: perhaps rething this implementation: variables and groups could form a graph with
;; actual references stored in the CLOS object slots (parent, group, members, etc).
(defun subgroups (group)
  (remove-if-not (lambda (g)
                   (eql (group g) (md::object-name group)))
                 (alexandria:hash-table-values *custom-groups*)))

(defmethod md::members ((group custom-group))
  (append (subgroups group) (group-vars group)))

(defun format-var-name (name)
  (str:sentence-case (princ-to-string name)))

(defun format-group-name (name)
  (str:sentence-case (princ-to-string name)))

(defmethod mold-desktop::tree-node-label ((group custom-group))
  (format-group-name (md::object-title group)))

(defmethod mold-desktop::tree-node-label ((var custom-var))
  (format-group-name (md::object-title var)))

(defun make-customizable (variable &rest args)
  (let ((vartype (or (getf args :type)
                     (type-of (symbol-value variable)))))
    (setf (gethash variable *custom-vars*)
          (make-instance 'custom-var
                         :name variable
                         :type vartype
                         :group (or (getf args :group) 'desktop)
                         :description (documentation variable 'variable)))
    (when (getf args :on-change)
      (event-emitter:on :changed
                        (gethash variable *custom-vars*)
                        (getf args :on-change))
      (gethash variable *custom-vars*))))

(defmacro defcustom (symbol value doc &rest args)
  "Define customization variable."
  (let ((vartype (or (getf args :type)
                     (error "Provide the type"))))
    `(progn
       (defvar ,symbol ,value ,doc)
       (check-type ,symbol ,vartype)
       (setf (gethash ',symbol *custom-vars*)
             (make-instance 'custom-var
                            :name ',symbol
                            :type ',vartype
                            :group ',(or (getf args :group) 'desktop)
                            :default ,value
                            :description ,doc))
       ,@(when (getf args :on-change)
           `((event-emitter:on :changed
                               (gethash ',symbol *custom-vars*)
                               ,(getf args :on-change))))
       (gethash ',symbol *custom-vars*))))

(defmacro defgroup (symbol members doc &rest args)
  `(progn
     (setf (gethash ',symbol *custom-groups*)
           (make-instance 'custom-group
                          :name ',symbol
                          :description ,doc
                          ,@args))
     ,@(loop for member in members
             collect `(defcustom ,@member))))

(defcommand (find-custom-var :show-result t)
    ((name symbol) &optional (error-p boolean t))
  (or (gethash name *custom-vars*)
      (when error-p
        (error "Custom var not found: ~s" name))))

(defcommand (find-custom-group :show-result t)
    ((name symbol) &optional (error-p boolean t))
  (or (gethash name *custom-groups*)
      (when error-p
        (error "Custom group not found: ~s" name))))

(defmethod mold-desktop::create-object-view ((group custom-group) clog-obj &rest args)
  (let ((div (clog:create-div clog-obj)))
    (when (md::object-description group)
      (clog:create-p div :content (md::object-description group)
                         :style "font-style:italic;"))
    (clog:create-p div :content "Settings:" :style "font-weight: bold;")
    ;; Vars:
    (let ((ul (clog:create-unordered-list div :class "w3-ul")))
      (dolist (var (group-vars group))
        (let ((li (clog:create-list-item ul)))
          (apply #'mold-desktop::create-item-view var li (getf args :item)))))
    ;; Subgroups
    (when (subgroups group)
      (clog:create-p div :content "Subgroups:" :style "font-weight: bold;")
      (let ((ul (clog:create-unordered-list div :class "w3-ul")))
        (dolist (var (subgroups group))
          (let ((li (clog:create-list-item ul)))
            (apply #'mold-desktop::create-item-view var li (getf args :item))))))
    div))

(defmethod mold-desktop::create-object-view ((var custom-var) clog-obj &rest args)
  (declare (ignore args))
  (let ((div (clog:create-div clog-obj)))
    (let ((header (clog:create-div div :style "margin-bottom: 5px;")))

      ;; Setting name
      (clog:create-child header (md::html (:b (who:str (md::object-name var)))))

      ;; Edit button
      (let ((btn
              (mold-desktop::create-icon-button
               header "fa fa-edit"
               (lambda (&rest args)
                 (declare (ignore args))
                 (md::run-command-interactively #'customize-variable (md::object-name var))))))
        (setf (clog:advisory-title btn) "Edit"))

      ;; Reset button
      (let ((btn
              (mold-desktop::create-icon-button
               header "fa fa-arrows-rotate"
               (lambda (&rest args)
                 (declare (ignore args))
                 (reset-variable-value var)))))
        (setf (clog:advisory-title btn) "Reset")))

    ;; Setting value
    (clog:create-child div (md::html (:div :style "margin-bottom: 5px;"
                                           (:b (who:str "Value: "))
                                           (who:str (princ-to-string (symbol-value (md::object-name var)))))))

    ;; Setting description
    (when (md::object-description var)
      (clog:create-p div :content (md::object-description var)
                         :style "font-style:italic;"))
    div))

(deftype custom-var-name () 'symbol)

(defmethod mold-desktop::prompt-command-argument-type
    ((arg-type (eql 'custom-var-name)) arg command-bar handler)
  (mold-desktop::command-bar-choose
   (format nil "~a: " (mold-desktop::format-argname (getf arg :name)))
   (mapcar (lambda (var)
             (list :label (princ-to-string (md::object-name var))
                   :value (md::object-name var)
                   :description (md::object-description var)))
           (alexandria:hash-table-values *custom-vars*))
   handler command-bar
   :close nil))

(mold-desktop:defcommand
    (customize-variable :icon "fa fa-cog")
    ((varname custom-var-name))
  (let ((var (find-custom-var varname)))
    (mold-desktop::prompt-command-argument-type
     (vartype var)
     (list :name (princ-to-string varname))
     mold-desktop::*command-bar*
     (lambda (value)
       (set-custom-var-value var value)
       (mold-desktop::close-command-bar mold-desktop::*command-bar*)))
    ;; Set the initial value in command-bar
    (setf (mold-desktop::element-value (mold-desktop::user-input mold-desktop::*command-bar*))
          (princ-to-string (custom-var-value var)))
    ))

(defun custom-var-value (var)
  (symbol-value (md::object-name var)))

(defun set-custom-var-value (var value)
  (if (setter var)
      (funcall (setter var) value var)
      (setf (symbol-value (md::object-name var)) value))
  (event-emitter:emit :changed var var))

(defun reset-variable-value (var)
  (set-custom-var-value var (default-value var)))

(defmethod mold-desktop::object-view-types append
    ((var custom-var) (module (eql :settings)))
  '(mold-desktop::create-object-view))

(defmethod mold-desktop::object-view-types append
    ((group custom-group) (module (eql :settings)))
  '(mold-desktop::create-object-view
    mold-desktop::create-tree-detail-view))

(pushnew :settings mold-desktop::*mold-modules*)

(defgroup desktop nil
  "Desktop options")

;;(members (find-custom-group 'desktop))

;;(mold-desktop::show (find-custom-group 'desktop))

(defun system-settings ()
  (make-instance 'md::collection
                 :name "System settings"
                 :members (remove-if #'group (alexandria:hash-table-values *custom-groups*))
                 :type 'custom-group))

(md::defcommand (customize-system :icon "fa fa-cog")
    ()
  "Customize desktop settings."
  (md::show (system-settings)
            :view #'md::create-tree-detail-view))

;; Redefine some variables as customizable

(make-customizable 'mold-desktop:*handle-command-errors*
                   :type 'boolean
                   :group 'desktop)
(make-customizable 'mold-desktop:*handle-view-errors*
                   :type 'boolean
                   :group 'desktop)

(make-customizable 'mold-desktop::*command-bar-prompt-optional-args*
                   :type 'boolean
                   :group 'desktop)
(make-customizable 'mold-desktop::*command-bar-toggle-prompt-optional-args-modifier*
                   :type '(member :ctrl-key :alt-key :shift-key :meta-key)
                   :group 'desktop)
