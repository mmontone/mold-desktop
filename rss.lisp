(in-package :mold-desktop)

;; https://fishbowl.pastiche.org/2002/10/21/http_conditional_get_for_rss_hackers

(defclass rss-feed (collection)
  ((read-count :initarg :read-count ;; this should be a configurable object attribute
               :accessor read-count
               :initform 10
               :type integer)
   (rss-data :initarg :rss-data
             :initform nil)
   (last-fetched :initarg :last-fetched
                 :accessor last-fetched
                 :initform nil)
   (cache-timespan :initarg :cache-timespan
                   :accessor cache-timespan
                   :initform (* 2 60))
   (etag :initarg :etag
         :accessor rss-feed-etag
         :initform nil)
   (last-modified
    :initarg :last-modified
    :accessor rss-feed-last-modified
    :initform nil))
  (:icon . "fa-solid fa-rss")
  (:metaclass mold-object-class)
  (:documentation "RSS feed"))

(defmethod rss-data ((feed rss-feed))
  (with-slots (rss-data last-fetched cache-timespan) feed
    (when (or (not rss-data)
              (> (get-universal-time) (+ last-fetched cache-timespan)))
      (setf rss-data
            (rss:parse-rss-stream (drakma:http-request (object-url feed) :want-stream t)))
      (setf last-fetched (get-universal-time)))
    rss-data))

(defmethod members ((feed rss-feed))
  (with-accessors ((rss-data rss-data)) feed
    (when rss-data
      (loop for rss-item in (rss:items rss-data)
            collect (make-instance 'web-page
                                   :url (rss:link rss-item)
                                   :name (rss:title rss-item)
                                   :description (str:shorten 1000 (rss:description rss-item)))))))

(defun rss-feed-outdated-p (rss-feed)
  (with-slots (etag last-modified) rss-feed
    (when (and (not etag)
               (not last-modified))
      (return-from rss-feed-outdated-p t))
    (multiple-value-bind (result status headers)
        (drakma:http-request (object-url rss-feed) :method :head)
      (declare (ignore result))
      (when (>= status 400)
        (return-from rss-feed-outdated-p nil))
      (or (and etag (not (string= (acc:accesses headers :etag)
                                  etag)))
          (and last-modified (not (= last-modified (net.telent.date:parse-time (acc:accesses headers :last-modified)))))))))

(defun parse-rss-url (url headers)

  ;; TODO: the correct way is to look for "alternate" links in the header of the webpage, with type application/rss-xml

  ;; <link rel="alternate" type="application/rss+xml" title="Planet Lisp RSS"
  ;;    href="http://planet.lisp.org/rss20.xml" />
  ;; <link rel="alternate" href="http://planet.clojure.in/atom.xml" type="application/rss+xml" title="Planet Clojure RSS Feed" />

  (flet ((make-rss-object (rss-url)
           (multiple-value-bind (rss-content status rss-headers)
               (drakma:http-request rss-url :want-stream t)
             (when (>= status 400)
               (return-from make-rss-object nil))
	     (handler-case
             (let ((rss-data (ignore-errors (rss:parse-rss-stream rss-content))))
	       (make-instance 'rss-feed
                              :name (rss:title rss-data)
                              :url rss-url
                              :rss-data rss-data
                              :last-fetched (get-universal-time)
                              :last-modified (net.telent.date:parse-time (acc:accesses rss-headers :last-modified))
                              :etag (acc:accesses rss-headers :etag)))
	       (error (e)
		 (warn "Error parsing rss-data of ~a: ~a" rss-url e)
		 nil)))))
    (cond
      ;; An RSS type feed
      ((or (search "rss" (acc:accesses headers :content-type)
                   :test 'string=)
           (search "xml" (acc:accesses headers :content-type)
		   :test 'string=))
       (make-rss-object url))
      ;; An HTML page.
      ;; Look for "alternate" links in the header of the webpage, with type application/rss-xml
      ;; <link rel="alternate" type="application/rss+xml" title="Planet Lisp RSS"
      ;;    href="http://planet.lisp.org/rss20.xml" />
      ;; <link rel="alternate" href="http://planet.clojure.in/atom.xml" type="application/rss+xml" title="Planet Clojure RSS Feed" />
      ((search "html" (acc:accesses headers :content-type) :test 'string=)
       (multiple-value-bind (content status)
           (drakma:http-request url)
         (when (< status 400)
           (let* ((doc (ignore-errors (lquery:$ (initialize content)))))
	     (when doc
               (dolist (link (coerce (lquery:$ doc "link") 'list))
		 (when (and (plump:attribute link "type")
                            (search "rss" (plump:attribute link "type") :test #'string=))
                   (let ((rss-url (plump:attribute link "href")))
                     (when rss-url
                       (return-from parse-rss-url (make-rss-object rss-url))))))))))))))

(pushnew 'parse-rss-url *url-parsers*)

;; (add-object-from-url "https://news.google.com/rss" *body*)
;; (add-object-from-url "https://news.google.com/rss?hl=es&gl=AR&ceid=AR:es" *body*)

(defcommand import-rss-feeds
    ((file string "The OPML file to import RSS feeds from"))
  "Import RSS feeds from OPML file"
  (flet ((attribute-value (attribute node)
           (let ((res
                   (xpath:first-node (xpath:evaluate (format nil "@~a" attribute) node))))
             (when res
               (dom:value res)))))
    (let ((xml (cxml:parse-file (pathname file) (rune-dom:make-dom-builder))))
      (let* ((nodes (xpath:all-nodes (xpath:evaluate "//outline" xml))))
	(with-job (job :name "Parse RSS feed"
		       :progress 0
		       :progress-max (length nodes)
		       :display-progress-details t
		       :display-progress-time t)
          (dolist (node nodes)
            (let ((title (attribute-value "title" node))
                  (url (attribute-value "xmlUrl" node))
                  (description (attribute-value "text" node)))
              (when url
		(push
		 (make-instance 'rss-feed :name title
                                          :url url
                                          :description description)
		 *objects*)))
	    (incf (job-progress job))))))))
