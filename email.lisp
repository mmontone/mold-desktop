;; TODO: build a simple email reader app.
;; TODO: implement an emails overview view like in this image: https://joshondesign.com//images2/mail.png
;; TODO: make the email view presentation based on the from/to fields. They should be presented as CONTACT/PERSON objects.
;; TODO: implement an on-disk index + synchronization for mel-base imap backend.

;; NOTE: I need this for ssl to work:
;; (setf cl+ssl:*make-ssl-client-stream-verify-default* nil)

(in-package :mold-desktop)

(mold-desktop/settings:defgroup email nil
  "Email settings.")

(mold-desktop/settings:defgroup smtp nil
  "SMTP settings"
  :group 'email)

(mold-desktop/settings:defcustom *smtp-host* "localhost"
  "SMTP host"
  :type string
  :group smtp)

(mold-desktop/settings:defcustom *smtp-port* 25
  "SMTP port"
  :type integer
  :group smtp)

(mold-desktop/settings:defcustom *smtp-ssl* nil
  "Use SSL for SMTP?"
  :type boolean
  :group smtp)

(mold-desktop/settings:defcustom *smtp-from* "no-reply@app.com"
  "SMTP from address"
  :type string
  :group smtp)

(defcommand send-email
    ((subject string "The email subject")
     (message text "The email message")
     (to string "Email to send to"))
  "Send an email"
  (cl-smtp:send-email *smtp-host*
                      *smtp-from* to subject message
                      :ssl *smtp-ssl*
                      :port *smtp-port*))

(defclass email-server ()
  ())

(defclass email-account (object)
  ((host :initarg :host
         :accessor email-account-host
         :type string
         :initform "localhost"
         :setting t)
   (username :initarg :username
             :accessor email-account-username
             :type string
             :initform "username"
             :setting t)
   (password :initarg :password
             :accessor email-account-password
             :type secret-values:secret-value
             :initform (secret-values:conceal-value "123456")
             :setting t)
   (port :initarg :port
         :accessor email-account-port
         :type integer
         :initform 143
         :setting t)
   (mailbox :initarg :mailbox
            :accessor email-account-mailbox
            :type string
            :initform "INBOX"
            :setting t)
   (server :initarg :server
           :accessor email-account-server
           :initform (make-instance 'imap-server)))
  (:metaclass mold-object-class)
  (:icon . "fa-solid fa-envelopes-bulk")
  (:documentation "An email account"))

(defgeneric email-server-list-folders (server account))

(defmethod folders ((email-account email-account))
  (email-server-list-folders (email-account-server email-account) email-account))

(defmethod members ((email-account email-account))
  (folders email-account))

(defclass email-folder (collection)
  ((account :initarg :account
            :accessor email-account))
  (:metaclass mold-object-class)
  (:icon . "fa-solid fa-inbox")
  (:documentation "An email folder"))

(defgeneric folder-messages (email-folder))

(defmethod members ((folder email-folder))
  (folder-messages folder))

(defclass email-message (object)
  ((folder :initarg :folder
           :accessor message-folder)
   (subject :initarg :subject
            :accessor message-subject
            :type string)
   (date :initarg :date
         :accessor message-date)
   (from :initarg :from
         :accessor message-from)
   (to :initarg :to
       :accessor message-to)
   (sender :initarg :sender
           :accessor message-sender)
   (cc-list :initarg :cc-list
            :accessor message-cc-list)
   (header-fields :initarg :header-fields
                  :accessor message-header-fields))
  (:metaclass mold-object-class)
  (:icon . "fa-regular fa-envelope")
  (:documentation "An email message"))

(defmethod initialize-instance :after ((email-message email-message) &rest initargs)
  (declare (ignore initargs))
  (setf (object-name email-message)
        (message-subject email-message)))

(defgeneric message-content (email-message))

;;;; ** Views

(defmethod create-object-view ((account email-account) clog-obj &rest args &key context)
  (let ((ul (create-unordered-list clog-obj :class "w3-ul")))
    (with-ui-update ul account :changed
      (dolist (folder (folders account))
        (let* ((li (create-list-item ul))
               (wrapper (mold-desktop/clog-gui::create-halos-wrapper li folder :context account)))
          (apply #'create-item-view folder wrapper :context context (getf args :member)))))
    ul))

(defmethod create-object-view ((folder email-folder) clog-obj &rest args &key context)
  ;; We use a collection view, but we may want to show more
  (call-next-method))

(defmethod create-object-view ((email-message email-message) clog-obj &rest args)
  (declare (ignore args))
  (create-child clog-obj
		(html
		  (:div
		   (:table :class "w3-table w3-bordered"
			   (:tr (:td (who:str "Subject"))
				(:td (who:str (message-subject email-message))))
			   (:tr (:td (who:str "From"))
				(:td (who:str (message-from email-message))))
			   (:tr (:td (who:str "To"))
				(:td (who:str (message-to email-message))))
			   (:tr (:td (who:str "Date"))
				(:td (who:str (format-timestring (message-date email-message))))))
		   (:pre (who:str (message-content email-message)))))))

;;;; ** MEL-BASE implementation

(defclass imap-server (email-server)
  ((mel-folder :accessor mel-folder
               :initform nil)))

(defmethod email-server-list-folders ((server imap-server) (account email-account))

  (when (not (mel-folder server))
    ;; Connect via mel
    (setf (mel-folder server)
          (mel.folders.imap:make-imap-folder
           :host (email-account-host account)
           :username (email-account-username account)
           :password (secret-values:reveal-value (email-account-password account))
           :port (email-account-port account)
           :mailbox (email-account-mailbox account))))

  (let ((mailboxes (mel.folders.imap::list-mailboxes (mel-folder server) "*")))
    (mapcar (lambda (mailbox)
              (let ((mailbox-name (caddr mailbox)))
                (make-instance 'mel-email-folder
                               :name mailbox-name
                               :account account
			       :mel-imap-folder
			       (mel.folders.imap:make-imap-folder
				:host (email-account-host account)
				:username (email-account-username account)
				:password (secret-values:reveal-value (email-account-password account))
				:port (email-account-port account)
				:mailbox mailbox-name))))
            mailboxes)))

(defclass mel-email-folder (email-folder)
  ((mel-imap-folder :initarg :mel-imap-folder
                    :accessor mel-imap-folder))
  (:metaclass mold-object-class)
  (:documentation "An email folder"))

(defmethod initialize-instance :after ((email-folder mel-email-folder) &rest initargs)
  (declare (ignore initargs))
  (setf (object-name email-folder)
	(mel.folders.imap::mailbox (mel-imap-folder email-folder))))

(defmethod folder-messages ((email-folder mel-email-folder))
  (mapcar (lambda (mel-message)
            (make-instance 'mel-email-message
                           :folder email-folder
                           :mel-email-message mel-message
                           :subject (mel:subject mel-message)
                           :from (mel:from mel-message)
                           :to (mel:to mel-message)
			   :date (mel:date mel-message)))
          (mel:messages (mel-imap-folder email-folder))))

(defclass mel-email-message (email-message)
  ((mel-email-message :initarg :mel-email-message
                      :accessor mel-email-message))
  (:metaclass mold-object-class)
  (:documentation "An email message"))

(defmethod message-content ((email-message mel-email-message))
  (with-output-to-string (out)
    (with-open-stream (stream (mel:message-body-stream (mel-email-message email-message)))
      (loop for c = (read-char stream nil nil)
            while c do (write-char c out)))))

;; add a documentation node to the manual
(defun create-email-doc-node ()
  (read-doc-node-from-file (asdf:system-relative-pathname :mold-desktop "docs/modules/email.md")
			   :name "Email"))

(pushnew 'create-email-doc-node *mold-desktop-book-module-nodes*)
